﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using NHibernate.Linq;
using System.Linq;

namespace Akvariet.Fishbase.Repositories
{
    public class ScientificNameRepository : Repository<ScientificName>
    {
        #region Constructor
        public ScientificNameRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
        #endregion

        #region Public methods
        public ScientificName GetByGenusAndSpecies(string genus, string species)
        {
            return Session.Query<ScientificName>().Where(x => x.Genus.Equals(genus)).FirstOrDefault(x => x.Species.Equals(species));
        }
        #endregion
    }
}
