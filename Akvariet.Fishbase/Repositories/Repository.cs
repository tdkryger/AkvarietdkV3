﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Database;
using NHibernate;
using NLog;
using System;
using System.Linq;

namespace Akvariet.Fishbase.Repositories
{
    public class Repository<T> : IRepositories<T> where T : IEntity
    {
        private FishbaseUnitOfWork _unitOfWork;
        protected static Logger logger = LogManager.GetCurrentClassLogger();


        public Repository(IUnitOfWork unitOfWork)
        {
            try
            {
                _unitOfWork = (FishbaseUnitOfWork)unitOfWork;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        protected ISession Session { get { return _unitOfWork.Session; } }

        public IQueryable<T> GetAll()
        {
            try
            {
                return Session.Query<T>();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        public T GetById(int id)
        {
            try
            {
                return Session.Get<T>(id);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        public void Create(T entity)
        {
            try
            {
                Session.Save(entity);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        public void Update(T entity)
        {
            try
            {
                Session.SaveOrUpdate(entity);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                Session.Delete(Session.Load<T>(id));
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Exception in Repository");
                throw ex;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
