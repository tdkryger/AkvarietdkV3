﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Repositories
{
    public class AnimalRepository : Repository<Animal>
    {
        #region Constructors
        public AnimalRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }
        #endregion

        #region Public methods
        /// <summary>
        /// Returns the top viewed animals
        /// </summary>
        /// <param name="count">defaults to 5</param>
        /// <returns>List of animals</returns>
        public IList<Animal> GetTopViewed(int count = 5)
        {
            return Session.QueryOver<Animal>()
                .OrderBy(x => x.ViewCount).Desc
                .Take(count).List();
        }

        /// <summary>
        /// Returns the newests modified animals
        /// </summary>
        /// <param name="count">defaults to 5</param>
        /// <returns>List of animals</returns>
        public IList<Animal> GetNewest(int count = 5)
        {
            return Session.QueryOver<Animal>()
                .OrderBy(x => x.ModifiedDate).Desc
                .Take(count).List();
        }

        /// <summary>
        /// Returns a list of animals
        /// </summary>
        /// <param name="genus"></param>
        /// <param name="species"></param>
        /// <param name="includeSynonyms"></param>
        /// <returns></returns>
        public IList<Animal> GetByScientificName(string genus, string species, bool includeSynonyms)
        {
            //TODO: Is this realy the way to do it? 3 seperate querys into the DB
            ScientificName sciName = Session.QueryOver<ScientificName>()
                .WhereRestrictionOn(x => x.Genus).IsLike(genus)
                .WhereRestrictionOn(x => x.Species).IsLike(species)
                .SingleOrDefault();
            //.Where(x => x.Genus.Equals(genus))
            //.And(x => x.Species.Equals(species))
            //.SingleOrDefault();
            if (sciName == null)
                return new List<Animal>();



            List<Animal> fromSynonyms = new List<Animal>();

            var query = Session.QueryOver<Animal>();
            if (includeSynonyms)
            {
                //TODO: Code belov fails with exception
                //fromSynonyms = (List<Animal>)Session.QueryOver<Animal>()
                // .Where(x => x.Synonyms.Contains(sciName))
                // .List();
            }
            List<Animal> animals = GetAll().Where(x => x.ScientificName.Id == sciName.Id).ToList(); // (List<Animal>)Session.QueryOver<Animal>().List();
            //animals = animals
            //    .Where(x => x.ScientificName.Id == sciName.Id)
            //    .ToList();
            if (fromSynonyms.Count > 0)
            {
                animals.AddRange(fromSynonyms);
            }

            return animals;
        }

        public IList<Animal> GetByScientificName(ScientificName sciName, bool includeSynonyms)
        {
            return GetByScientificName(sciName.Genus, sciName.Species, includeSynonyms);
        }
        #endregion

    }
}
