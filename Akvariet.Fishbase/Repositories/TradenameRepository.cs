﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using System.Linq;

namespace Akvariet.Fishbase.Repositories
{
    public class TradenameRepository : Repository<Tradename>
    {
        public TradenameRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public Tradename GetByText(string text, string languageCode)
        {
            return GetAll().Where(x=>x.Language.Code.Equals(languageCode)).FirstOrDefault(x => x.Name.Equals(text));
        }
    }
}
