﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Akvariet.Fishbase.Repositories
{
    public class BlogEntityRepository : Repository<BlogEntity>
    {
        public BlogEntityRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public List<BlogEntity> GetNewestBlogs(int count)
        {
            List<BlogEntity> list = null;
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                list = Session.Query<BlogEntity>().OrderByDescending(x => x.Posted).Take(count).ToList();
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
            }
            finally
            {
                sw.Stop();
                logger.Debug("Runtime: {0}", sw.Elapsed);
            }
            return list;
        }

        public List<BlogEntity> GetMostReadBlogs(int count)
        {
            List<BlogEntity> list = null;
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                list = Session.Query<BlogEntity>().OrderByDescending(x => x.PostRead).Take(count).ToList();
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
            }
            finally
            {
                sw.Stop();
                logger.Debug("Runtime: {0}", sw.Elapsed);
            }
            return list;
        }

        public List<BlogEntity> GetBlogsFromTag(TagEntity tag)
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<BlogEntity> list = null;
            try
            {
                list = Session.Query<BlogEntity>().Where(x => x.Tags.Contains(tag)).OrderByDescending(x=>x.Posted).ToList();
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
            }
            finally
            {
                sw.Stop();
                logger.Debug("Runtime: {0}", sw.Elapsed);
            }

            return list;
        }
    }
}
