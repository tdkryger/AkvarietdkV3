﻿using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database
{
    public static class DBHelper
    {
        public static void GenerateDefaultSupportTableContent(bool deleteFirst = true)
        {
            Helpers.AnimalGroupHelper.GenerateDefaultAnimalGroups(deleteFirst);
            Helpers.IllnessHelper.GenerateDefaultIllness(deleteFirst);
            Helpers.OrderHelper.GenerateDefaultOrder(deleteFirst);
            Helpers.SocialHelper.CreateDefaults(deleteFirst);
            Helpers.LanguageHelper.GenerateDefault(deleteFirst);
        }

        [System.Obsolete("GetUnitOfWork is deprecated, please use create it directly instead.")]
        public static FishbaseUnitOfWork GetUnitOfWork()
        {
            return new FishbaseUnitOfWork();
        }

        static DBHelper()
        {

        }

        #region Hardness
        public static Hardness GetHardness(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Hardness item = null;
            using (Repository<Hardness> repo = new Repository<Hardness>(uow))
            {
                item = repo.GetAll().Where(x => x.Min == min).FirstOrDefault(x => x.Max == max);
                if (item == null)
                {
                    item = new Hardness() { Max = max, Min = min };
                }
            }
            return item;
        }
        #endregion

        #region Light
        public static Light GetLight(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Light item = null;

            using (Repository<Light> repo = new Repository<Light>(uow))
            {
                item = repo.GetAll().Where(x => x.Min == min).FirstOrDefault(x => x.Max == max);
                if (item == null)
                {
                    item = new Light() { Max = max, Min = min, Text = $"{min}-{max}" };
                }
            }
            return item;
        }

        public static Light GetLight(FishbaseUnitOfWork uow, string text)
        {
            Light item = null;
            using (Repository<Light> repo = new Repository<Light>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Text.Equals(text));
                if (item == null)
                {
                    item = new Light() { Text = text };
                }
            }
            return item;
        }
        #endregion

        #region Family
        public static Family GetFamily(FishbaseUnitOfWork uow, string family)
        {
            Family item = null;
            using (Repository<Family> repo = new Repository<Family>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Name.Equals(family));
                if (item == null)
                {
                    item = new Family() { Name = family };
                }
            }
            return item;
        }
        #endregion

        #region Order
        public static Order GetOrder(FishbaseUnitOfWork uow, string text)
        {
            Order item = null;
            using (Repository<Order> repo = new Repository<Order>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Name.Equals(text));
                if (item == null)
                {
                    item = new Order() { Name = text };
                }
            }
            return item;
        }
        #endregion

        #region AnimalGroup
        public static AnimalGroup GetAnimalGroup_AqualogCode(FishbaseUnitOfWork uow, string aqualogCode)
        {
            AnimalGroup item = null;
            using (Repository<AnimalGroup> agRepo = new Repository<AnimalGroup>(uow))
            {
                item = agRepo.GetAll().Where(x => x.AqualogCode.ToUpper().Equals(aqualogCode.ToUpper())).FirstOrDefault();
            }
            if (item == null)
                item = new AnimalGroup() { AqualogCode = aqualogCode };

            return item;
        }
        #endregion

        #region Plant Group

        public static PlantGroup GetPlantGroup(FishbaseUnitOfWork uow, string text)
        {
            PlantGroup plantGroup = null;
            using (Repository<PlantGroup> agRepo = new Repository<PlantGroup>(uow))
            {
                plantGroup = agRepo.GetAll().Where(x => x.Text.ToUpper().Equals(text.ToUpper())).FirstOrDefault();
            }
            if (plantGroup == null)
            {
                plantGroup = new PlantGroup() { Text = text };
            }
            return plantGroup;
        }
        #endregion

        #region Scientific Name
        public static ScientificName GetScientificName(FishbaseUnitOfWork uow, string genus, string species)
        {
            ScientificName sciName;
            using (ScientificNameRepository repo = new ScientificNameRepository(uow))
                sciName = repo.GetByGenusAndSpecies(genus, species);
            if (sciName == null)
            {
                sciName = new ScientificName()
                {
                    Genus = genus,
                    Species = species
                };
            }
            return sciName;
        }

        #endregion

        #region Plant Care
        public static PlantCare GetPlantCare(FishbaseUnitOfWork uow, string text)
        {
            PlantCare item;
            using (Repository<PlantCare> repo = new Repository<PlantCare>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Text.Equals(text));
                if (item == null)
                {
                    item = new PlantCare() { Text = text };
                }
            }
            return item;
        }
        #endregion

        #region Characteristics
        public static PlantCharacteristic GetPlantCharacteristics(FishbaseUnitOfWork uow, string text)
        {
            PlantCharacteristic item;
            using (Repository<PlantCharacteristic> repo = new Repository<PlantCharacteristic>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Text.Equals(text));
                if (item == null)
                {
                    item = new PlantCharacteristic() { Text = text };
                }
            }
            return item;
        }
        #endregion

        #region Plants
        public static int PlantCount()
        {
            using (Repository<Plant> repo = new Repository<Plant>(new FishbaseUnitOfWork()))
                return repo.GetAll().ToList().Count;
        }
        #endregion

        #region Animal
        public static int AnimalCount()
        {
            try
            {
                using (AnimalRepository repo = new AnimalRepository(new FishbaseUnitOfWork()))
                    return repo.GetAll().ToList().Count;
            }
            catch
            {
                return 0;
            }
        }

        public static bool CheckExisting(FishbaseUnitOfWork uow, ScientificName sciName)
        {
            //string sqlString = $"select count(*) FROM animal a, scientificname sn Where a.scientificname_id=sn.id AND sn.genus = '{sciName.Genus}' AND sn.species = '{sciName.Species}';";
            //var count = uow.Session.CreateSQLQuery(sqlString).UniqueResult();

            //return (count > 0);
            return true;
        }

        public static Animal GetAnimal(FishbaseUnitOfWork uow, int id)
        {
            using (AnimalRepository repo = new AnimalRepository(new FishbaseUnitOfWork()))
                return repo.GetById(id);
        }

        public static Animal GetAnimal(FishbaseUnitOfWork uow, ScientificName sciName)
        {
            using (AnimalRepository repo = new AnimalRepository(new FishbaseUnitOfWork()))
                return repo.GetByScientificName(sciName, false).FirstOrDefault();
        }

        public static Animal GetAnimal(FishbaseUnitOfWork uow, string genus, string species)
        {
            using (AnimalRepository repo = new AnimalRepository(new FishbaseUnitOfWork()))
                return repo.GetByScientificName(GetScientificName(uow, genus, species), false).FirstOrDefault();
        }
        #endregion

        #region Size
        public static Size GetSize(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Size item = null;
            using (Repository<Size> repo = new Repository<Size>(uow))
            {
                item = repo.GetAll().Where(x => x.Min == min).FirstOrDefault(x => x.Max == max);
                if (item == null)
                {
                    item = new Size() { Max = max, Min = min };
                }
            }
            return item;
        }
        #endregion

        #region Origin
        public static Origin GetOrigin(FishbaseUnitOfWork uow, string text, string subOrigin)
        {
            Origin origin;
            using (Repository<Origin> repo = new Repository<Origin>(uow))
            {
                origin = repo.GetAll().Where(x => x.Text.Equals(text)).FirstOrDefault(x => x.SubOrigin.Equals(subOrigin));
            }
            if (origin == null)
            {
                origin = new Origin() { Text = text, SubOrigin=subOrigin };
            }
            return origin;
        }
        #endregion

        #region Language
        public static Language GetLanguage(FishbaseUnitOfWork uow, string code)
        {
            using (Repository<Language> repo = new Repository<Language>(uow))
                return repo.GetAll().FirstOrDefault(x => x.Code.Equals(code));
        }
        #endregion

        #region Social & behavior
        public static Social GetSocial(FishbaseUnitOfWork uow, string text)
        {
            Social item = null;
            using (Repository<Social> repo = new Repository<Social>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Text.Equals(text));

                if (item == null)
                {
                    item = new Social()
                    {
                        Text = text
                    };
                }
            }
            return item;
        }

        #endregion

        #region Diet
        public static Diet GetDiet(FishbaseUnitOfWork uow, string text)
        {
            Diet diet = null;
            using (Repository<Diet> repo = new Repository<Diet>(uow))
            {
                diet = repo.GetAll().FirstOrDefault(x => x.Name.Equals(text));

                if (diet == null)
                {
                    diet = new Diet()
                    {
                        Name = text
                    };
                }
            }
            return diet;
        }
        #endregion

        #region Tradenames
        public static Tradename GetTradename(FishbaseUnitOfWork uow, string text, Language language)
        {
            Tradename item = null;
            using (Repository<Tradename> repo = new Repository<Tradename>(uow))
            {
                item = repo.GetAll().Where(x => x.Language.Id == language.Id).FirstOrDefault(x => x.Name.Equals(text));
                if (item == null)
                {
                    item = new Tradename() { Name = text, Language = language };
                }
            }
            return item;
        }
        #endregion

        #region Fishbase Images
        public static List<FishbaseImage> GetImages(FishbaseUnitOfWork uow, string title)
        {
            List<FishbaseImage> list = new List<FishbaseImage>();

            using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
            {
                list = repo.GetAll().Where(x => x.Title.StartsWith(title)).ToList();
            }

            return list;
        }
        #endregion

        #region pH
        public static PH GetPH(FishbaseUnitOfWork uow, decimal min, decimal max, bool addSalt)
        {
            PH item = null;
            using (Repository<PH> repo = new Repository<PH>(uow))
            {
                item = repo.GetAll().Where(x => x.AddSalt == addSalt).Where(x => x.Min == min).FirstOrDefault(x => x.Max == max);
                if (item == null)
                {
                    item = new PH() { Max = max, Min = min, AddSalt = addSalt };
                }
            }
            return item;
        }
        #endregion

        #region Temperatur
        public static Temperature GetTemperature(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Temperature item = null;
            using (Repository<Temperature> repo = new Repository<Temperature>(uow))
            {
                item = repo.GetAll().Where(x => x.Min == min).FirstOrDefault(x => x.Max == max);
                if (item == null)
                {
                    item = new Temperature() { Max = max, Min = min };
                }
            }
            return item;
        }
        #endregion

        #region temperament
        public static Temperament GetTemperament(FishbaseUnitOfWork uow, string text)
        {
            Temperament item = null;
            using (Repository<Temperament> repo = new Repository<Temperament>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.Text.Equals(text));
                if (item == null)
                {
                    item = new Temperament() { Text = text };
                }
            }
            return item;
        }
        #endregion

        #region User
        public static User GetDefaultUser(FishbaseUnitOfWork uow)
        {
            User item = null;
            using (Repository<User> repo = new Repository<User>(uow))
            {
                item = repo.GetAll().FirstOrDefault(x => x.DisplayName.Equals("Admin"));
                if (item == null)
                {
                    item = new User() {DisplayName="Admin", EMail="info@akvariet.dk", JoinDateTime=DateTime.Now, Role=Common.UserLevels.Admin, Username="admin" };
                }
            }
            return item;
        }
        #endregion

        #region Private methods

        #endregion
    }
}
