﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    public static class AnimalGroupHelper
    {
        public static void DeleteAllAnimalGroups()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            List<AnimalGroup> list;

            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (AnimalGroup ag in list)
                {
                    repository.Delete(ag.Id);
                }
                UnitOfWork.Commit();
            }
        }

        public static void GenerateDefaultAnimalGroups(bool deleteFirst = true)
        {
            if (deleteFirst)
                DeleteAllAnimalGroups();
            List<AnimalGroup> list = GenerateAnimalGroupList();
            foreach (AnimalGroup ag in list)
            {
                IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
                try
                {
                    using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
                    {
                        UnitOfWork.BeginTransaction();
                        repository.Update(ag);
                        UnitOfWork.Commit();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"GenerateDefaultAnimalGroups.", ex);
                }
            }
        }

        private static List<AnimalGroup> GenerateAnimalGroupList()
        {
            List<AnimalGroup> list = new List<AnimalGroup>();
            using (var fs = File.OpenRead(@"data\animalgroups.csv"))
            using (var reader = new StreamReader(fs))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    //AqualogCode;Navn;Zone
                    if (values.Length == 3)
                    {
                        if (!string.IsNullOrEmpty(values[0].Trim()) && !string.IsNullOrEmpty(values[1].Trim()) && !string.IsNullOrEmpty(values[2].Trim()))
                            list.Add(new AnimalGroup() { Name = values[1].Trim(), AqualogCode = values[0].Trim(), Zone = values[2].Trim() });
                    }

                }
            }

            return list;
        }
    }
}
