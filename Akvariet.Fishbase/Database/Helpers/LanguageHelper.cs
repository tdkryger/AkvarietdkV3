﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    class LanguageHelper
    {
        public static void DeleteAll()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            List<Language> list;

            using (Repository<Language> repository = new Repository<Language>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (Language ag in list)
                {
                    repository.Delete(ag.Id);
                }
                UnitOfWork.Commit();
            }
        }

        public static void GenerateDefault(bool deleteFirst = true)
        {
            if (deleteFirst)
                DeleteAll();

            List<Language> list = new List<Language>();
            list.Add(new Language() { Code = "DK", Name = "Dansk" });
            list.Add(new Language() { Code = "EN", Name = "Engelsk" });
            list.Add(new Language() { Code = "DE", Name = "Tysk" });
            list.Add(new Language() { Code = "NL", Name = "Holland" });
            foreach (Language lan in list)
            {
                IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
                using (Repository<Language> repository = new Repository<Language>(UnitOfWork))
                {
                    UnitOfWork.BeginTransaction();
                    repository.Create(lan);
                    UnitOfWork.Commit();
                }
            }
        }
    }
}
