﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    public static class SocialHelper
    {
        public static void DeleteAll()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            List<Social> list;

            using (Repository<Social> repository = new Repository<Social>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (Social ag in list)
                {
                    repository.Delete(ag.Id);
                }
                UnitOfWork.Commit();
            }
        }

        public static void CreateDefaults(bool deleteFirst = true)
        {
            if (deleteFirst)
                DeleteAll();
            List<Social> list = GenerateList();
            foreach (Social item in list)
            {
                IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
                try
                {
                    using (Repository<Social> repository = new Repository<Social>(UnitOfWork))
                    {
                        UnitOfWork.BeginTransaction();
                        repository.Update(item);
                        UnitOfWork.Commit();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"SocialHelper.CreateDefaults.", ex);
                }
            }
        }

        private static List<Social> GenerateList()
        {
            List<Social> list = new List<Social>();

            list.Add(new Social() { Text = "Stimefisk" });
            list.Add(new Social() { Text = "Æglægger" });
            list.Add(new Social() { Text = "Levendefødende" });
            list.Add(new Social() { Text = "Mundruger" });
            list.Add(new Social() { Text = "Bobblerede" });
            list.Add(new Social() { Text = "Æg har særlige krav" });
            list.Add(new Social() { Text = "Kun som par eller trio" });
            list.Add(new Social() { Text = "Algeæder" });
            list.Add(new Social() { Text = "Let at holde" });
            list.Add(new Social() { Text = "Meget svær at holde" });
            list.Add(new Social() { Text = "Beskyttet af CITES" });


            return list;
        }
    }
}
