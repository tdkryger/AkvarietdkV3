﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    public static class OrderHelper
    {
        public static void DeleteAllOrders()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            List<Order> list;

            using (Repository<Order> repository = new Repository<Order>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (Order ag in list)
                {
                    repository.Delete(ag.Id);
                }
                UnitOfWork.Commit();
            }

        }

        public static void GenerateDefaultOrder(bool deleteFirst = true)
        {
            if (deleteFirst)
                DeleteAllOrders();
            List<Order> list = GenerateOrderList();
            foreach (Order item in list)
            {
                IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
                try
                {
                    using (Repository<Order> repository = new Repository<Order>(UnitOfWork))
                    {
                        UnitOfWork.BeginTransaction();
                        repository.Update(item);
                        UnitOfWork.Commit();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"GenerateDefaultOrder.", ex);
                }
            }
        }

        private static List<Order> GenerateOrderList()
        {
            // Source: http://faculty.college-prep.org/~bernie/sciproject/project/Kingdoms/animals6/orders.htm
            List<Order> list = new List<Order>();
            list.Add(new Order() { Name = "Ukendt" });
            //Crustaceans
            list.Add(new Order() { Name = "Branchiopoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Copepoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Ostracoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Cirrepedia", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Stomatopoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Mysidacea", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Decapoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Amphipoda", OrderGroup = "Krebsdyr" });
            list.Add(new Order() { Name = "Isopoda", OrderGroup = "Krebsdyr" });

            // Insects
            list.Add(new Order() { Name = "Thysanura", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Diplura", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Protura", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Collembola", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Ephemeroptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Odonata", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Plecoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Grylloblattodea", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Orthoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Phasmida", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Dermaptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Embioptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Dictyoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Isoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Zoraptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Psocoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Mallophaga", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Siphunculata (Anoplura)", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Hemiptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Thysanoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Neuroptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Coleoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Strepsiptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Mecoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Siphonaptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Diptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Lepidoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Trichoptera", OrderGroup = "Insekter" });
            list.Add(new Order() { Name = "Hymenoptera", OrderGroup = "Insekter" });

            // Bony Fish
            list.Add(new Order() { Name = "Anguilliformes", OrderGroup = "Benfisk" });
            list.Add(new Order() { Name = "Salmoniformes", OrderGroup = "Benfisk" });
            list.Add(new Order() { Name = "Cypriniformes", OrderGroup = "Benfisk" });
            list.Add(new Order() { Name = "Siluriformes", OrderGroup = "Benfisk" });
            list.Add(new Order() { Name = "Perciformes", OrderGroup = "Benfisk" });

            // Amphibian
            list.Add(new Order() { Name = "Caudata", OrderGroup = "Padder" });
            list.Add(new Order() { Name = "Salientia", OrderGroup = "Padder" });

            // Reptile
            list.Add(new Order() { Name = "Testudines", OrderGroup = "Krybdyr" });
            list.Add(new Order() { Name = "Squamata", OrderGroup = "Krybdyr" });
            list.Add(new Order() { Name = "Crocodilia", OrderGroup = "Krybdyr" });

            return list;
        }
    }
}
