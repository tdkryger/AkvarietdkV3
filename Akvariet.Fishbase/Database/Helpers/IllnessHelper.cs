﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    public static class IllnessHelper
    {
        public static void DeleteAllIllness()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            List<Illness> list;

            using (Repository<Illness> repository = new Repository<Illness>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (Illness ag in list)
                {
                    repository.Delete(ag.Id);
                }
                UnitOfWork.Commit();
            }
        }

        public static void GenerateDefaultIllness(bool deleteFirst = true)
        {
            if (deleteFirst)
                DeleteAllIllness();
            List<Illness> list = GenerateIllnessList();
            foreach (Illness item in list)
            {
                IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
                try
                {
                    using (Repository<Illness> repository = new Repository<Illness>(UnitOfWork))
                    {
                        UnitOfWork.BeginTransaction();
                        repository.Update(item);
                        UnitOfWork.Commit();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"GenerateDefaultIllness.", ex);
                }
            }
        }

        private static List<Illness> GenerateIllnessList()
        {
            //Source (in danish): http://www.kobenhavnsakvarieforening.dk/default.asp?id=30

            List<Illness> list = new List<Illness>();
            list.Add(new Illness()
            {
                Name = "Fiskedræber (hvide pletter)",
                Group = "Encellede snyltere",
                Cause = "Fiskedræberen, Ichthyopthirius multifiliis, er en encellet kugleformet og behåret organisme. Ichthysporen svømmer frit og skal inden 48 timer finde sig en vært. Den borer sig ind under fisken overhud, hvor den æder af fiskens væsker. Efter 1-3 uger borer den sig ud og sætter sig fast på en plante eller sten, hvor den efter 8-10 timer har delt sig i op til 1000 nysporer, som svømmer ud for at finde sig en vært.",
                Symptoms = "1. bølge: Enkelte små hvide prikker - ½-1 mm - på enkelte fisk. Det overses ofte.\n2. bølge: Alle fisk er angrebet og med mange prikker over krop og finner. Fiskene forsøger at gnide snylteren af på planter sten m.m. Fiskene patter efter luft i overfladen, da også gællerne er angrebet. Efter 1 uge forsvinder de fleste pletter.\n3. og 4. bølge: Pletterne kommer massivt igen. Ofte kommer svampeangreb til (Saprolegnia. Massedødsfald. Enkelte fisk kan i visse tilfælde overleve, men sættes nye fisk ned til dem, vil de hurtigt blive angrebet.\n\nUnder mikroskop kan man konstatere en lille cirkelrund, roterende organisme med en hesteskoformet cellekærne.\nAngriber alle fisk!\nDen findes også i en saltvandsudgave.\nFiskedræberen kan indslæbes med foderdyr, men om regel indføres snylteren med inficerede fisk.",
                Treatment = "Der findes i handelen effektive midler mod fiskedræber. Det er vigtigt, at følge brugsanvisningen. Fiskene behandles i et særskilt helt tomt glasakvarium. Har opholdsakvariet stået tomt helt uden fisk i over 48 timer er alle snyltere og deres larver døde, men blot en enkelt lille efterladt fiskeunge i akvariet vil gøre, at sygdommen kommer igen."
            });
            list.Add(new Illness()
            {
                Name = "Oodinium (Forkølelsespletter)",
                Group = "Encellede snyltere",
                Cause = "Oodinium er en såkaldt dino-flagellat. Den indeholder klorofyl, men kan svømme, altså en sammenblanding af dyr og plante. Angrebene kaldes forkølelsespletter, da de ofte kommer efter store vandskift, temperaturfald eller andre store forandringer i miljøet. Er ofte følgesvend til sygdomme som fisketuberkulose og fiskedræber eller situationer, hvor fiskenes kondition er svækket. Formeringen er stort set som fiskedræberens.",
                Symptoms = "Fisken ser ud, som om den er dækket af et lag pudder, især på ryg og finner. Undertiden har pudderlaget et gulligt skær.\nUnder mikroskop ses snylteren som en pæreformet organisme, der med en stilk har sat sig fast på fisken. Der ses ingen bevægelse i organismen.\nAngriber hovedsagelig barber og rasboraarter, labyrintfisk og tandkarper. Den ses ikke på karpelaks og maller.",
                Treatment = "Der findes i handelen effektive midler mod oodinium."
            });
            list.Add(new Illness()
            {
                Name = "Indvendige snyltende flagellater, bl.a. hulsyge",
                Group = "Encellede snyltere",
                Cause = "Flagellater af slægterne Hexamita, Octominus eller Spironucleus. Det er små pæreformede, encellede organismer forsynet med svingtråde – heraf navnet flagellater.\nDe formerer sig i fiskenes tarmsystem, hvorfra de via blodsystemet spredes rundt i kroppen og kan angribe nyrer, lever, milt, galdeblære, svømmeblære m.m. Visse flagellater forårsager bylder, som efterlader sig huller, som ikke heles, den såkaldte hulsyge, som fortrinsvis kendes fra Diskosfisk, Skalarer og andre cichlider.",
                Symptoms = "Fiskene holder helt eller delvist op med at æde. De bliver sløve i bevægelserne og undertiden mørke i farvetegningen. Afføringen er trådtynd og hvidgrå og flagrer efter fisken.",
                Treatment = "I akvariehandelen fås præparatet Hexa-ex. Flagellaangreb kan også behandles med den receptpligtige medicin metronidazol, der bl.a. forhandles under navne som Flagyl og Trichomol.\nBlandt Diskosfolk anvendes først og fremmest den såkaldte varmekur, hvor temperaturen hæves fra de normale 28-29°C til 35°, hvor den holdes i en uge. Derefter sænkes temperaturen til de normale 28-29° i en uge og hæves til slut i endnu en uge til 35°."
            });
            list.Add(new Illness()
            {
                Name = "Neonsyge",
                Group = "Encellede snyltere",
                Cause = "Et snyltende sporedyr (Plistophora hyphessobryconis). Der sidder bl.a. sporer i halefinnen på angrebne fisk. Når andre fisk så nipper i halen, vil sporen i maven på den nye fisk udvikle sig og sende nye sporer ud i hele fisken muskulatur.",
                Symptoms = "Urolig svømning, manglende ædelyst og ligevægtsforstyrrelser. På et senere stadium kan halepartiet angribes af skimmelsvamp (Saprolegnia).\nSygdommen angriber især karpelaks. På yngel af karpelaks kan angreb ses ved, at ungerne skruer sig op gennem vandet for derefter at falde mod bunden.",
                Treatment = "Uhelbredelig. Angrebne fisk fjernes og aflives straks for at undgå smitte."
            });

            list.Add(new Illness()
            {
                Name = "Ichthyoponus",
                Group = "Svampesygdomme",
                Cause = "Ichthyosporidium hoferi, der er en 5-20 μ stor algesvamp, som lever indvendig i den angrebne fisk. Den formerer sig i fisken og sender svampesporer ud til alle organer. Smitter ved, at andre fisk æder den døde inficerede fisk eller ved, at svampen sender direkte sporer ud i vandet fra bylder på fiskens krop.",
                Symptoms = "Mange! Indfalden bug, sår, bylder og knuder, Gule over brun- til sort-farvede ansamlinger i muskulaturen, fremtrædende øjne, misdannelser, skælrejsning og ligevægtsforstyrrelser. Kan være meget svær at skelne fra fisketuberkulose.",
                Treatment = "Uhelbredelig. Syge fisk fjernes og aflives så hurtigt som muligt for at undgå yderligere infektioner af akvariets fisk."
            });
            list.Add(new Illness()
            {
                Name = "Udvendige svampeangreb",
                Group = "Svampesygdomme",
                Cause = "Angreb af skimmelsvampe (Saprolegnia eller Achlya) som følge af, at overhuden og fiskens slimlag er beskadiget. Skader fra fangstnet rammer ofte øjnene (fungus), mens mundfungus ofte opstår ved, at fiskene i panik er svømmet ind i akvarieruderne. Ubehandlet vil svampenes hyfer ikke alene trænge ind i overhuden, men også i fiskens muskulatur. Det vil efterhånden føre til fiskens død.",
                Symptoms = "Hvide vattot-lignende belægninger. Findes det på mund eller øjne, kaldes det henholdsvis mundfungus og øjefungus.",
                Treatment = "Der findes effektive svampemidler i handelen."
            });

            list.Add(new Illness()
            {
                Name = "Bugvattersot",
                Group = "Virus sygdomme",
                Cause = "Sandsynligvis en virusinfektion efterfulgt af en infektion af bakterieslægterne Aeromonas eller  Pseudomonas.",
                Symptoms = "Udspilet bug. Skælrejsning. Evt. udstående øjne.\nLabyrintfisk skulle være særligt udsatte.",
                Treatment = "Uhelbredelig. Angrebne fisk fjernes og aflives straks for at undgå smitte."
            });
            list.Add(new Illness()
            {
                Name = "Hindbærsyge",
                Group = "Virus sygdomme",
                Cause = "Virusinfektion.",
                Symptoms = "Samling af hvide, knappenålsstore knuder på finner, gællelåg eller kropssider.\nAngriber først og fremmest saltvandsfisk, men kan også forekomme på ferskvandsfisk.",
                Treatment = "Syge fisk bør isoleres. Knuderne fjernes operativt, på finnerne ved at klippe de angrebne områder af, på kroppen ved at skære dem af med en skarp skalpel eller et barberblad. De behandlede områder pensles med et desinficerende middel, f.eks. euflavin."
            });

            list.Add(new Illness()
            {
                Name = "Fisketuberkulose (Høstpest)",
                Group = "Bakterielle sygdomme",
                Cause = "Stavformede bakterier, 5-20 tusindedele mm. Findes tit i raske fisk i mindre mængder. Forekommer tit i øget mængde i fisk, der er angrebne af snyltere.\nVitaminmangel ser ud til at øge risikoen for angreb af fisketuberkulose.\nFisketuberkulose skyldes bakterien Mycobacterium piscium, som imidlertid trives bedst ved temperaturer på 25-30° C og dør ved 37°. Den kan således ikke leve i mennesker.\nMen bakterien Mycobacterium marinum kan også forårsage en fiskesygdom, som regnes under kategorien fisketuberkulose, og den kan være meget alvorlig for mennesker, hvis man inficeres og ikke kommer til behandling i tide.\nFår man en infektion,  f.eks. i et sår på hånden, efter at have været i kontakt med akvarievand, så skal man straks søge læge og gøre opmærksom på, at det kan dreje sig om en infektion forårsaget af bakterien Mycobacterium marinum.",
                Symptoms = "Fiskene svømmer langsommere og ofte i ryk efterfulgt af en baglæns bevægelse. Undertiden ligevægtsforstyrrelser. Huden kan miste farven og blive mælket at se på. Fiskene søger som oftest op til overfladen. På gællerne kan man se afgrænsede røde områder. ",
                Treatment = "Fisken bør aflives med en skarp kniv og fisken gennemkoges, så alle bakterier er dræbt, før den lægges i dagrenovationen. De anvendte redskaber skoldes efter brugen."
            });
            list.Add(new Illness()
            {
                Name = "Finneråd",
                Group = "Bakterielle sygdomme",
                Cause = "Bakterieinfektion.",
                Symptoms = "Finnerne ”smelter” bort. Rammer især Sort Spenops, men undertiden også andre ungefødende tandkarper. Skimmelsvamp (Saprolegnia) støder ofte til. ",
                Treatment = "Rodalon (Benzalkoniumklorid). Stamopløsning: 1 ml Rodalon i 1 liter destilleret vand. Til endeligt bad: 5 ml stamopløsning pr. liter vand. Pas på ikke at overdosere!"
            });
            list.Add(new Illness()
            {
                Name = "Columnarissyge",
                Group = "Bakterielle sygdomme",
                Cause = "Angreb af bakterierne af slægterne Cytophaga og Flexibacter.",
                Symptoms = "Svampelignende belægning omkring munden og rundt om på kroppen. Fisken bliver sløv. Kan forveksles med et svampeangreb, men sammenholder man de to sygdomme vil man se, at svampenes tråde er meget mere grove.",
                Treatment = "Nifurpirinol. Forhandles hos akvariehandleren under navnet Furamor-P. Importerede fisk kan være resistente over for midlet."
            });
            list.Add(new Illness()
            {
                Name = "Rødsyge",
                Group = "Bakterielle sygdomme",
                Cause = "Angreb af en eller flere bakterier af gruppen Pseudomonas.",
                Symptoms = "Fiskens bug får et kraftigt rødt skær fra overfyldte blodkar. Sygdommen kun kendt fra  barber.",
                Treatment = "Uhelbredelig. Angrebne fisk aflives."
            });

            list.Add(new Illness()
            {
                Name = "Gyrodactylus",
                Group = "Orme/Ikter",
                Cause = "En 0,3-0,8 mm aflang snylter, Gyrodactylus, der med den ene ende sætter sig fast på fisken og den anden ende æder af hudens slimlag. Den føder levende unger, som straks efter fødslen sætter sig fast på fiskens hud ved siden af moderdyret.",
                Symptoms = "Fisken hud og finner ser ud, som om de er overtrukket med et mælkeagtigt lag. Fisken er urolig og gnider sig mod planter og sten m.m.",
                Treatment = "Et 20 minutters bad i en kogsaltsopløsning – 10-15 g pr liter vand – får snylteren til at slippe fisken."
            });
            list.Add(new Illness()
            {
                Name = "Dactylogyrus",
                Group = "Orme/Ikter",
                Cause = "En snylter meget lig Gyrodactylus, men som kun sætter sig på fiskens gæller. Den er æglæggende. Æggene klækker på 3-4 timer, og den fimrehårsbeklædte larve skal finde sig en vært inden for et par timer.",
                Symptoms = "Fisken er urolig og gnider hovedet/gællerne mod planter og sten m.m.",
                Treatment = "Et 20 minutters bad i en kogsaltsopløsning – 10-15 g pr liter vand – får snylteren til at slippe fisken."
            });
            list.Add(new Illness()
            {
                Name = "Endetarmsorm",
                Group = "Orme/Ikter",
                Cause = "Endetarmsormen, Camallanus lacustris, overføres med cyclops fra søer og vandhuller, der huser fisk, idet cyclops fungerer som mellemvært i endetarmsormens formering.",
                Symptoms = "De grårøde ormeender hænger ud af gattet.",
                Treatment = "Ormen har forankret sig oppe i fiskens tarm ved hjælp af nogle modhager. Det hjælper derfor ikke at prøve at trække ormen ud med en pincet.\nI stedet gives en ormekur.Et par tabletter med piperazin eller mebendazol(Vermox) knuses og opløses i 10 ml vand.Foderet lægges i vandet, så det kan opsuge opløsningen, før det gives til fiskene.Der fodres med det medicinerede foder til ormene er væk, normalt en uges tid."
            });
            list.Add(new Illness()
            {
                Name = "Orm i øjet (Diplostomum volvens)",
                Group = "Orme/Ikter",
                Cause = "Fiskene er en af to mellemværter for en ikte (Prolaria spathaseum), som lever i tarmen hos flere af vore svømmefugle. Iktens æg falder med fuglenes afføring ned i vandet, hvor de klækkes. Larven opsøger herefter en snegl, hvori den lever og til sidst udvikler en sporocyst, som efter en tid sender en antal gaffelhalecercarier ud i vandet. Vi kan få dem ind i akvarierne enten gennem en inficeret snegl eller sammen med levende foder som cyclops. Når gaffelhalecercarierne kommer ind i mundhulen eller i gællerne, borer de sig ind i fisken og finder vej til øjet, hvorved fisken blindes. I naturen vil det gøre fisken til et nemmere bytte for fuglene og kredsløbet er sluttet.",
                Symptoms = "Fisken bliver blind på det ene øje eller på begge øjnene.",
                Treatment = "Uhelbredelig."
            });

            list.Add(new Illness()
            {
                Name = "Karpelus (Argulus)",
                Group = "Snyltende krebsdyr",
                Cause = "Karpelusene kommer ind i akvariet sammen med det levende foder. De er sorte til mørkegrønne og er lette at kende fra cyclops, dels på grund af deres runde kropsform, dels på grund af deres jævne svømmemåde, som er helt anderledes end cyclops’  hoppende svømmefacon. Hvis foderet hældes i en hvid skål før fodringen, kan man derfor let få øje på dem og sortere dem fra.",
                Symptoms = "Det op til 13 mm store krebsdyr kan ses på fisken, som svømmer uroligt rundt og forsøger at gnide dem af sig.",
                Treatment = "Fjernes fra fisken med en pincet, hvis de ikke slipper af sig selv."
            });
            list.Add(new Illness()
            {
                Name = "Ankerorm",
                Group = "Snyltende krebsdyr",
                Cause = "Ankerormene fås ind i akvariet enten på nyindkøbte fisk eller sammen med det levende foder. Halvdelen af den op til 2 cm lange krop kan være boret ind i fisken.",
                Symptoms = "Ses som 10-12 mm lange pinde, der stikker ud fra fisken. I den yderste ende er den forsynet med to ægsække, som vi kender det fra de nært beslægtede Cyclops.",
                Treatment = "På større fisk fjernes ankerormen forsigtigt med en pincet, og det efterladte sår pensles med euflavin. Snylteren kan forinden duppes med metrifonat, da det er lidt lettere at trække parasitten ud, når den er død.\n\nPå mindre fisk kan man prøve at klippe snylterens krop over med en saks og lade det være op til fisken selv at udstøde den døde kropsdel og hele det fremkomne sår.En behandling med euflavin kan måske forhindre infektioner."
            });
            list.Add(new Illness()
            {
                Name = "Artystone trysiba",
                Group = "Snyltende krebsdyr",
                Cause = "Snylteren Artystone trysibia, en snyltende isopod med et interessant livsforløb. Se den efterfølgende artikel: Historie fra det virkelige liv.",
                Symptoms = "Et hul på 1-2 mm i huden ved gællehulen. Ud fra hullet blafrer nogle \'hudlapper\'.",
                Treatment = "Ingen. Større fisk, som f.eks. diskosfisk, der af og til er påvist som vært for denne snylter, kan sandsynligvis overleve i flere år med snylteren inden i sig. Mindre fisk dør."
            });

            return list;
        }
    }
}
