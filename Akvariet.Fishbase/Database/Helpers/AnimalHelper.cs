﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Akvariet.Fishbase.Database.Helpers
{
    public static class AnimalHelper
    {
        public static void DeleteAll()
        {
            IUnitOfWork unitOfWork = new FishbaseUnitOfWork();
            List<Animal> list;
            using (AnimalRepository repository = new AnimalRepository(unitOfWork))
            {
                unitOfWork.BeginTransaction();
                list = repository.GetAll().ToList();
                foreach (Animal a in list)
                {
                    repository.Delete(a.Id);
                }
                unitOfWork.Commit();
            }
        }
    }
}
