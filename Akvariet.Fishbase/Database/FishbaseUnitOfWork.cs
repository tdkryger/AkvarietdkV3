﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NLog;
using System;
using System.Diagnostics;

namespace Akvariet.Fishbase.Database
{
    public class FishbaseUnitOfWork : IUnitOfWork, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly ISessionFactory _sessionFactory;
        private ITransaction _transaction;

        public ISession Session { get; private set; }

        static FishbaseUnitOfWork()
        {
            _sessionFactory = CreateConnection();
        }

        public FishbaseUnitOfWork()
        {
            Session = _sessionFactory.OpenSession();
            Session.FlushMode = FlushMode.Auto;
        }

        public void BeginTransaction()
        {
            logger.Debug("Starting transaction");
            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            logger.Debug("Commiting transaction");
            try
            {
                // commit transaction if there is one active
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Commit();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "UnitOfWork.Commit");

                // rollback if there was an exception
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();

                throw;
            }
            finally
            {
                //Session.Dispose();
            }
        }

        public void Rollback()
        {
            logger.Debug("Rolling back transaction");
            try
            {
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();
            }
            finally
            {
                //Session.Dispose();
            }
        }

        #region private methods
        private static ISessionFactory CreateConnection()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            /*
                drop database akvariet_dk_db2;
                //CREATE DATABASE akvariet_dk_db2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
            */
            if (_sessionFactory != null)
                return _sessionFactory;
            try
            {
                string conString = Akvariet.Common.AkvarietConfiguration.MySQLConnectionString;

                var lowercaseTables = ConventionBuilder.Class.Always(x => x.Table(x.EntityType.Name.ToLower()));

                //database configs
                FluentConfiguration _config = Fluently.Configure().Database(
                    MySQLConfiguration.Standard.ConnectionString(conString))
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<DBFile>()
                        .Conventions.Add(lowercaseTables));

                _config.ExposeConfiguration(cfg => cfg.SetProperty("hbm2ddl.keywords", "auto-quote"));
                _config.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, false));
//#if (DEBUG)
//                _config.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true));
//#else
//                _config.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, false));
//#endif
                //_config.ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true, true));


                ISessionFactory temp = _config.BuildSessionFactory();
                stopwatch.Stop();
                logger.Debug("Connected to the MySQL Database in {0}.", stopwatch.Elapsed);
                return temp;
            }
            catch (Exception ex)
            {
                stopwatch.Stop();
                logger.Fatal(ex, "Exception after {0}", stopwatch.Elapsed);
                throw;
            }

        }

#region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (Session != null)
                    {
                        Rollback();
                    }
                }


                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
#endregion
#endregion
    }
}
