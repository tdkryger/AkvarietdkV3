﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class TradenameMap : ClassMap<Tradename>
    {
        public TradenameMap()
        {
            Table("tradename");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(190).Not.Nullable();
            References(x => x.Language).Cascade.All().Not.LazyLoad();
        }
    }
}
