﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class LastModifiedFishbaseItemMap   :ClassMap<LastModifiedFishbaseItem>
    {
       public LastModifiedFishbaseItemMap()
        {
            Table("lastmodifiedfishbaseitem");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.ScientificNameText).Length(190);
            Map(x => x.ModifiedDate);
        }
    }
}
