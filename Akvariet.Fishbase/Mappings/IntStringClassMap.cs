﻿
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Akvariet.Fishbase.Mappings
{
    class IntStringClassMap : ClassMap<IntStringClass>
    {
        public IntStringClassMap()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Text).Length(190);
            Map(x => x.Selected);
        }
    }
}