﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class PlantGroupMap : ClassMap<PlantGroup>
    {
        public PlantGroupMap()
        {
            Table("plantgroup");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(75).Not.Nullable().Unique();
        }
    }
}
