﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mapping
{
    class TagEntityMap  : ClassMap<TagEntity>
    {
        public TagEntityMap()
        {
            Table("tagentity");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.TagName).Length(20).Not.Nullable();
        }
    }
}
