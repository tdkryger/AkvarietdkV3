﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Table("user");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.DisplayName).Length(70).Not.Nullable();
            Map(x => x.Username).Length(70).Not.Nullable();
            Map(x => x.EMail).Length(500).Not.Nullable();
            Map(x => x.Role).Not.Nullable();
            Map(x => x.JoinDateTime);
        }
    }
}
