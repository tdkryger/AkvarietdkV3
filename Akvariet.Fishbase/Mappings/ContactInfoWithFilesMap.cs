﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class ContactInfoWithFilesMap : ClassMap<ContactInfoWithFiles>
    {
        public ContactInfoWithFilesMap()
        {
            Table("contactinfo");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Content).Length(70000).Not.Nullable();

            Map(x => x.FromEMail).Length(190).Not.Nullable();
            Map(x => x.FromName).Length(190).Not.Nullable();
            Map(x => x.Subject).Length(190).Not.Nullable();
            HasMany<DBFile>(x => x.Files).Cascade.All();
            Map(x => x.Ip).Length(25);
            Map(x => x.CountryCode).Length(255);
            Map(x => x.CountryName).Length(255);
            Map(x => x.RegionCode).Length(255);
            Map(x => x.RegionName).Length(255);
            Map(x => x.City).Length(255);
            Map(x => x.ZipCode).Length(255);
            Map(x => x.TimeZone).Length(255);
            Map(x => x.Latitude).Length(255);
            Map(x => x.Longitude).Length(255);
            Map(x => x.MetroCode).Length(255);
        }
    }
}
