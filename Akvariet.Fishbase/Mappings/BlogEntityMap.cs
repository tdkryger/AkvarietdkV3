﻿
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mapping
{
    class BlogEntityMap : ClassMap<BlogEntity>
    {
        public BlogEntityMap()
        {
            Table("blogentity");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Title).Length(150).Not.Nullable();
            Map(x => x.Teaser).Length(500).Not.Nullable();
            Map(x=>x.Body).Length(65000).Not.Nullable();
            Map(x=>x.Posted).Not.Nullable();
            Map(x=>x.PostRead).Not.Nullable();
            HasManyToMany(x => x.Tags).Cascade.All().Not.LazyLoad().Table("tagentitytoblogentity");
            References(x=>x.Image).Cascade.All().Not.LazyLoad();
            References(x => x.Contributor).Cascade.All().Not.LazyLoad();
            HasMany<Comment>(x => x.Comments).Cascade.All().Not.LazyLoad().Table("commenttoblog");
        }
    }
}
