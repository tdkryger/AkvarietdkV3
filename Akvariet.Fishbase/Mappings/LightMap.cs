﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class LightMap : ClassMap<Light>
    {
        public LightMap()
        {
            Table("light");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Min).Not.Nullable();
            Map(x => x.Max).Not.Nullable();
            Map(x => x.Text).Length(35).Not.Nullable().Unique();
        }
    }
}
