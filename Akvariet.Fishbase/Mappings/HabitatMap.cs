﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class HabitatMap : ClassMap<Habitat>
    {
        public HabitatMap()
        {
            Table("habitat");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(500).Not.Nullable();//.Unique();
            Map(x => x.Description).Length(70000);
            Map(x => x.BiotopAkvarietId);
        }
    }
}
