﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class PlantCharacteristicMap : ClassMap<PlantCharacteristic>
    {
        public PlantCharacteristicMap()
        {
            Table("plantcharacteristic");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(75).Not.Nullable().Unique();
        }
    }
}
