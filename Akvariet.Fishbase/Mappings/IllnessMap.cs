﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class IllnessMap : ClassMap<Illness>
    {
        public IllnessMap()
        {
            Table("illness");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(190).Not.Nullable().Unique();
            Map(x => x.Group).Length(40).Not.Nullable().Column("illnessGroup");

            Map(x => x.Cause).Length(5000);
            Map(x => x.Symptoms).Length(5000);
            Map(x => x.Treatment).Length(5000);
        }

    }
}
