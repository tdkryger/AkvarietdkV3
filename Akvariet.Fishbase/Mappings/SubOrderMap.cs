﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class SubOrderMap : ClassMap<SubOrder>
    {
        public SubOrderMap()
        {
            Table("suborder");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(190).Not.Nullable();
        }
    }
}
