﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class LastModifiedBlogMap   : ClassMap<LastModifiedBlog>
    {
        public LastModifiedBlogMap()
        {
            Table("lastmodifiedblog");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Title).Length(150);
            Map(x => x.Posted);
        }
    }
}
