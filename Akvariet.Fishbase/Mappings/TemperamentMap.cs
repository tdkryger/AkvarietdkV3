﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class TemperamentMap : ClassMap<Temperament>
    {
        public TemperamentMap()
        {
            Table("temperament");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(500).Not.Nullable();//.Unique();
        }
    }
}
