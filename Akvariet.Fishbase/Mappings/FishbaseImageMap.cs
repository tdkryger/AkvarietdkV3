﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class FishbaseImageMap : ClassMap<FishbaseImage>
    {
        public FishbaseImageMap()
        {
            Table("fishbaseimage");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Image);
            Map(x => x.Content).Length(75);
            Map(x => x.Description).Length(5000);
            Map(x => x.Title).Length(75).Index("fishbaseImageTitle");
            Map(x => x.CopyRight).Length(150);
            Map(x => x.CreateDate);
        }
    }
}
