﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class PlantMap : ClassMap<Plant>
    {
        public PlantMap()
        {
            Table("plant");
            #region From Interface
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.ScientificName).Cascade.All().Not.LazyLoad();
            HasManyToMany(x => x.Tradenames).Cascade.All().Not.LazyLoad().Table("tradenametoplant");
            HasManyToMany(x => x.Synonyms).Cascade.All().Not.LazyLoad().Table("scientificnametoplant");
            References(x => x.Origin).Cascade.All().Not.LazyLoad();
            References(x => x.WaterTemperature).Cascade.All().Not.LazyLoad();
            References(x => x.WaterHardness).Cascade.All().Not.LazyLoad();
            References(x => x.WaterPh).Cascade.All().Not.LazyLoad();
            Map(x => x.Description).Length(20000);
            HasManyToMany(x => x.Images).Cascade.All().Not.LazyLoad().Table("fishbaseimagetoplant");
            Map(x => x.CreateDate).Not.Nullable();
            Map(x => x.ModifiedDate).Not.Nullable();
            Map(x => x.ViewCount);
            HasMany<Comment>(x => x.Comments).Cascade.All().Not.LazyLoad().Table("commenttoplant");
            Map(x => x.ScientificNameText).Length(190).Column("scientificNameText").Index("plantScientificNameText");
            #endregion


            Map(x => x.TankPosition);
            References(x => x.Height).Cascade.All().Not.LazyLoad();
            References(x => x.Width).Cascade.All().Not.LazyLoad();
            References(x => x.LightRequirements).Cascade.All().Not.LazyLoad();
            Map(x => x.CO2Requirements);
            References(x => x.PlantGroup).Cascade.All().Not.LazyLoad();
            Map(x => x.GrowthSpeed);
            HasManyToMany(x => x.Care).Cascade.All().Not.LazyLoad().Table("plantcaretoplant");
            References(x => x.Contributor).Cascade.All().Not.LazyLoad();
        }
    }
}
