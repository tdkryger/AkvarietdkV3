﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class DBFileMap : ClassMap<DBFile>
    {
        public DBFileMap()
        {
            Table("dbfile"); 
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.ContentType).Length(190);
            Map(x => x.Filename).Length(500);
            Map(x => x.FileContent).Not.Nullable().Length(int.MaxValue);
        }
    }
}
