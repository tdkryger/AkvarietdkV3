﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class OriginMap : ClassMap<Origin>
    {
        public OriginMap()
        {
            Table("origin");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(190).Not.Nullable().Index("originZone");
            Map(x => x.SubOrigin).Length(190).Index("originSubOrigin");
        }
    }
}
