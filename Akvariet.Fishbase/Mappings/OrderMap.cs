﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class OrderMap : ClassMap<Order>
    {
        public OrderMap()
        {
            Table("order");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(150).Not.Nullable().Unique();
            Map(x => x.OrderGroup).Length(150).Not.Nullable();
            HasMany(x => x.SubOrders).Cascade.All().Not.LazyLoad().Not.LazyLoad(); 
        }
    }
}
