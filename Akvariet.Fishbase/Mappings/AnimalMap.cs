﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class AnimalMap : ClassMap<Animal>
    {
        public AnimalMap()
        {

            Table("animal");
            #region From Interface
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.ScientificName).Cascade.All().Unique().Not.LazyLoad();
            HasManyToMany(x => x.Tradenames).Cascade.All().Not.LazyLoad().Table("tradenametoanimal") ;
            HasManyToMany(x => x.Synonyms).Cascade.All().Not.LazyLoad().Table("scientificnametoanimal") ;
            References(x => x.Origin).Cascade.All().Not.LazyLoad();
            References(x => x.WaterTemperature).Cascade.All().Not.LazyLoad();
            References(x => x.WaterHardness).Cascade.All().Not.LazyLoad();
            References(x => x.WaterPh).Cascade.All().Not.LazyLoad();
            Map(x => x.Description).Length(20000);
            HasManyToMany(x => x.Images).Cascade.All().Not.LazyLoad().Table("fishbaseimagetoanimal");
            Map(x => x.CreateDate).Not.Nullable(); 
            Map(x => x.ModifiedDate).Not.Nullable();
            Map(x => x.ViewCount);
            HasMany<Comment>(x => x.Comments).Cascade.All().Not.LazyLoad().Table("commenttoanimal");
            Map(x => x.ScientificNameText).Length(190).Column("scientificNameText").Index("animalScientificNameText");
            #endregion

            References(x => x.Order).Cascade.All().Not.LazyLoad();
            References(x => x.SubOrder).Cascade.All().Not.LazyLoad();
            References(x => x.Family).Cascade.All().Not.LazyLoad();
            References(x => x.CommonGroup).Cascade.All().Not.LazyLoad();
            References(x => x.Size).Cascade.All().Not.LazyLoad();
            Map(x => x.Bioload);
            Map(x => x.SwimLevel);
            HasManyToMany(x => x.DietaryRequirements).Cascade.All().Not.LazyLoad().Table("diettoanimal");
            HasManyToMany(x => x.CommonIllnesses).Cascade.All().Not.LazyLoad().Table("illnesstoanimal");
            References(x => x.Temperament).Cascade.All().Not.LazyLoad();
            HasManyToMany(x => x.Sociableness).Cascade.All().Not.LazyLoad().Table("socialtoanimal");
            Map(x => x.MingroupSize);
            Map(x => x.PlantedTank);
            Map(x => x.MinTankSize);
            Map(x => x.MinFiltrationLevel);
            HasManyToMany(x => x.PreferedHabitat).Cascade.All().Not.LazyLoad().Table("habitattoanimal"); ;
            Map(x => x.BreadInCaptivity);
            Map(x => x.RequiresWood);
            References(x=>x.Contributor).Cascade.All().Not.LazyLoad();
        }
    }
}
