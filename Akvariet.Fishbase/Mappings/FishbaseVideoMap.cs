﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class FishbaseVideoMap : ClassMap<FishbaseVideo>
    {
        public FishbaseVideoMap()
        {
            Table("fishbasevideo"); // need to learn spell..
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Title).Length(190).Not.Nullable().Unique();
            Map(x => x.Description).Length(68000);
            Map(x => x.Content).Length(68000);
            Map(x => x.CopyRight).Length(200);
            Map(x => x.CreateDate).Not.Nullable();
            Map(x=>x.MediaType).Not.Nullable();
            Map(x => x.Media).Length(int.MaxValue); 
        }
    }
}
