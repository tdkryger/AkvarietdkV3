﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class PlantCareMap : ClassMap<PlantCare>
    {
        public PlantCareMap()
        {
            Table("plantcare");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Text).Length(75).Not.Nullable().Unique();
        }
    }
}
