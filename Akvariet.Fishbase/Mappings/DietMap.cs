﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;


namespace Akvariet.Fishbase.Mappings
{
    public class DietMap :ClassMap<Diet>
    {
        public DietMap()
        {
            Table("diet");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(150).Not.Nullable().Unique();
        }
    }
}
