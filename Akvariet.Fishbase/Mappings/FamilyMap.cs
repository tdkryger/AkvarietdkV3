﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class FamilyMap : ClassMap<Family>
    {
        public FamilyMap()
        {
            Table("family");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(150).Unique().Not.Nullable();//.Index("familyNameIndex");
        }
    }
}
