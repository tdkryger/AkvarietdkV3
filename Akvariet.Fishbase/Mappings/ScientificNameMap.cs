﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class ScientificNameMap : ClassMap<ScientificName>
    {
        public ScientificNameMap()
        {
            Table("scientificname");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Genus).Length(190).Not.Nullable().UniqueKey("scientificname_sciname");
            Map(x => x.Species).Length(190).Not.Nullable().UniqueKey("scientificname_sciname");
        }
    }
}
