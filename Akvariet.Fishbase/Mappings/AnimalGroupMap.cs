﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;


namespace Akvariet.Fishbase.Mappings
{
    public class AnimalGroupMap : ClassMap<AnimalGroup>
    {
        public AnimalGroupMap()
        {
            Table("animalgroup");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(150).Not.Nullable().UniqueKey("groupzone");
            Map(x => x.AqualogCode).Length(10);
            Map(x => x.Zone).Length(150).Not.Nullable().UniqueKey("groupzone");
            Map(x => x.Active);
        }
    }
}
