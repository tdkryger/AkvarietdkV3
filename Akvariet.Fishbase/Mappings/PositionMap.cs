﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class PositionMap  : ClassMap<Position>
    {
        public PositionMap()
        {
            Table("position");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Depth).Length(75);
            Map(x => x.Latititude).Length(75);
            Map(x => x.Longitude).Length(75);
            Map(x => x.NameUsed).Length(75);
            Map(x => x.Year).Length(75);
        }
    }
}
