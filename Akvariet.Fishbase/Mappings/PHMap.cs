﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class PHMap : ClassMap<PH>
    {
        public PHMap()
        {
            Table("ph");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Min).Not.Nullable();
            Map(x => x.Max).Not.Nullable();
            Map(x => x.AddSalt);
        }
    }
}
