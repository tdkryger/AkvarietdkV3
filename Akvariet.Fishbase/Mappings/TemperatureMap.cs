﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class TemperatureMap : ClassMap<Temperature>
    {
        public TemperatureMap()
        {
            Table("temperature");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Min).Not.Nullable();
            Map(x => x.Max).Not.Nullable();
        }
    }
}
