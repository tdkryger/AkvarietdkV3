﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class SizeMap : ClassMap<Size>
    {
        public SizeMap()
        {
            Table("size");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Max);
            Map(y => y.Min);
        }
    }
}
