﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class LanguageMap : ClassMap<Language>
    {
        public LanguageMap()
        {
            Table("language");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(150).Not.Nullable();
            Map(x=>x.Code).Length(5).Not.Nullable().Unique();
        }
    }
}
