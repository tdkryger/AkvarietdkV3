﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class SimpleSearchMap : ClassMap<SimpleSearch>
    {
        public SimpleSearchMap()
        {
            Table("simplesearch");
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.EntryType);
        }
    }
}
