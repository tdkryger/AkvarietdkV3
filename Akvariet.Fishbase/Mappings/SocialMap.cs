﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class SocialMap : ClassMap<Social>
    {
        public SocialMap()
        {
            Table("social");
            Id(x => x.Id);
            Map(x => x.Text).Length(190).Not.Nullable().Unique();
        }
    }
}
