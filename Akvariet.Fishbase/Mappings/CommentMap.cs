﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class CommentMap : ClassMap<Comment>
    {
        public CommentMap()
        {
            Table("comment");
            Id(x => x.Id).GeneratedBy.Identity();
            References(x => x.User).Not.LazyLoad().Not.Nullable().Class(typeof(User));
            Map(x => x.PostDate).Not.Nullable();
            Map(x => x.Title).Length(100).Not.Nullable();
            Map(x => x.Body).Length(65000).Not.Nullable();
            Map(x => x.Mark);
            Map(x => x.Score);
        }
    }
}
