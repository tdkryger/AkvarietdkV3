﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    public class HardnessMap : ClassMap<Hardness>
    {
        public HardnessMap()
        {
            Table("hardness");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Min).Not.Nullable();
            Map(x => x.Max).Not.Nullable();
        }
    }
}
