﻿using Akvariet.Fishbase.Model;
using FluentNHibernate.Mapping;

namespace Akvariet.Fishbase.Mappings
{
    class AlgaeMap : ClassMap<Algae>
    {
        public AlgaeMap()
        {
            Table("algea"); // need to learn spell..
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Length(190).Not.Nullable().Unique();

            Map(x => x.Cause).Length(65000);
            Map(x => x.Symptoms).Length(65000);
            Map(x => x.Treatment).Length(65000);
        }
    }
}
