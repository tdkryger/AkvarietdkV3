update akvariet_dk_db2.origin  Set Text='Mellem- og sydamerika' where text='Mellem- og sydamerikanske';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Amazonas%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Amazonien%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Brazil%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Argentina%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Bolivia%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Surinam%' and text='';
update akvariet_dk_db2.origin set text='Opdræt' WHERE suborigin like '%Aquarium strain%' and text='';
update akvariet_dk_db2.origin set suborigin='Opdrættet variant' WHERE suborigin like '%Aquarium strain%' AND text='Opdræt' and text='';
update akvariet_dk_db2.origin set text='Mellemamerika' WHERE suborigin like '%Belize%' and text='';
update akvariet_dk_db2.origin set text='Mellemamerika' WHERE suborigin like '%Mexico%' and text='';
update akvariet_dk_db2.origin set text='Mellemamerika' WHERE suborigin like '%Panama%' and text='';
update akvariet_dk_db2.origin set text='Opdrætsform' WHERE suborigin like '%Breeding form%' and text='';
update akvariet_dk_db2.origin set suborigin='Opdrættet variant' WHERE suborigin like '%Breeding form%' AND text='Opdrætsform' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Venezuela%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Colombia%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika', suborigin=concat_ws(': ', 'Brasilien', suborigin) WHERE suborigin like '%Rio Grande de Sol%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Ecuador%' and text='';
update akvariet_dk_db2.origin set text='Mellemamerika' WHERE suborigin like '%Costa-rico%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Guayana%' AND Text='';
update akvariet_dk_db2.origin set text='Sydamerika' WHERE suborigin like '%Guyana%' AND Text='';
update akvariet_dk_db2.origin set text='Sydamerika', suborigin=concat_ws(': ', 'Guyana', suborigin) WHERE suborigin like '%Essequibo%' and text='';
update akvariet_dk_db2.origin set text='Sydamerika', suborigin=concat_ws(': ', 'Guyana', suborigin) WHERE suborigin like '%Essequibo%' and text='';
update akvariet_dk_db2.origin set text='Mellemamerika' WHERE suborigin like '%El Salvador%' AND Text='';

update akvariet_dk_db2.animal SET Origin_Id=8 WHERE Origin_Id IN (SELECT Id FROM akvariet_dk_db2.origin where text='Opdrætsform' AND suborigin='Opdrættet variant');
update akvariet_dk_db2.plant SET Origin_Id=8 WHERE Origin_Id IN (SELECT Id FROM akvariet_dk_db2.origin where text='Opdrætsform' AND suborigin='Opdrættet variant');

DELETE from akvariet_dk_db2.origin where 
id NOT IN (SELECT distinct Origin_Id FROM akvariet_dk_db2.animal)  AND  
id NOT IN (SELECT distinct Origin_Id FROM akvariet_dk_db2.plant);

-- ALTER TABLE `akvariet_dk_db2`.`origin` ADD UNIQUE INDEX `originUnique` (`Text` ASC, `SubOrigin` ASC);