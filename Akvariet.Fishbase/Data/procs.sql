
USE akvariet_dk_db2;

DELIMITER $$

DROP PROCEDURE IF EXISTS drop_index_if_exists $$
CREATE PROCEDURE drop_index_if_exists(in theTable varchar(128), in theIndexName varchar(128) )
BEGIN
 IF((SELECT COUNT(*) AS index_exists FROM information_schema.statistics WHERE TABLE_SCHEMA = DATABASE() and table_name =
theTable AND index_name = theIndexName) > 0) THEN
   SET @s = CONCAT('DROP INDEX ' , theIndexName , ' ON ' , theTable);
   PREPARE stmt FROM @s;
   EXECUTE stmt;
 END IF;
END $$

DELIMITER ;

CALL drop_index_if_exists('blogentity', 'blogentityFullText');
ALTER TABLE blogentity ADD FULLTEXT INDEX `blogentityFullText` (`Title` ASC,  `Body` ASC);
CALL drop_index_if_exists('plant', 'plantFullText');
ALTER TABLE plant ADD FULLTEXT INDEX `plantFullText` (`Description` ASC);
CALL drop_index_if_exists('animal', 'animalFullText');
ALTER TABLE animal ADD FULLTEXT INDEX `animalFullText` (`Description` ASC);
CALL drop_index_if_exists('plant', 'plantFullTextscientificNameText');
ALTER TABLE plant ADD FULLTEXT INDEX `plantFullTextscientificNameText` (`scientificNameText` ASC);
CALL drop_index_if_exists('animal', 'animalFullTextscientificNameText');
ALTER TABLE animal ADD FULLTEXT INDEX `animalFullTextscientificNameText` (`scientificNameText` ASC);
CALL drop_index_if_exists('habitattoanimal', 'habitattoanimal');
ALTER TABLE `habitattoanimal` ADD UNIQUE INDEX `habitattoanimal` (`Animal_id` ASC, `Habitat_id` ASC);

DROP PROCEDURE IF EXISTS originSearch;
DELIMITER $$
CREATE PROCEDURE originSearch(IN origin TEXT)
BEGIN
	SELECT Id, `Text`, `suborigin` FROM origin WHERE `Text` LIKE origin OR `suborigin` LIKE origin;
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchAnimal;
DELIMITER $$
CREATE PROCEDURE simpleSearchAnimal(IN scientificName TEXT)
BEGIN
	SELECT
		a2.id, 'animal' as EntryType
	FROM 
		animal a2,
		scientificname sn2,
		scientificnametoanimal sna
	WHERE
		a2.id=sna.animal_id AND sn2.Id=sna.scientificname_id AND concat_ws('', sn2.genus, sn2.species) like scientificName
UNION DISTINCT
	SELECT 
		a.id, 'animal' as EntryType
	FROM
		animal a
	WHERE
		 MATCH (a.description) AGAINST (scientificName) OR
         MATCH (a.scientificNameText) AGAINST (scientificName);
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchPlant;
DELIMITER $$
CREATE PROCEDURE simpleSearchPlant(IN scientificName TEXT)
BEGIN
	SELECT
	p.id, 'plant' as EntryType
FROM 
	plant p,
    scientificname sn,
    scientificnametoplant snp
WHERE
	p.id=snp.Plant_id AND
    sn.Id=snp.scientificname_id AND 
    concat_ws(sn.genus, ' ', sn.species) like scientificName
UNION DISTINCT
	SELECT 
		id, 'plant'
	FROM
		plant
	WHERE
     MATCH (description) AGAINST (scientificName) OR
         MATCH (scientificNameText) AGAINST (scientificName);
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchBlog;
DELIMITER $$
CREATE PROCEDURE simpleSearchBlog(IN scientificName TEXT)
BEGIN
	SELECT b.id, 'blog' as EntryType FROM blogentity b WHERE MATCH (title, body) AGAINST (scientificName);
END;
$$
DELIMITER ;



DROP PROCEDURE IF EXISTS updateAnimalHabitat;
DELIMITER $$
CREATE PROCEDURE updateAnimalHabitat(IN scientificName_in varchar(190), IN BiotopId_in int)
BEGIN
DECLARE animalId INT DEFAULT NULL;
    DECLARE habitatId INT DEFAULT NULL;
    DECLARE returnValue INT DEFAULT 0;
    DECLARE done INTEGER DEFAULT 0;
    
    DECLARE animalCursor CURSOR FOR SELECT id FROM `akvariet_dk_db2`.animal WHERE `scientificNameText` LIKE scientificName_in;
    DECLARE synonymCursor CURSOR FOR SELECT a2.id FROM animal a2, scientificname sn2, scientificnametoanimal sna WHERE a2.id=sna.animal_id AND sn2.Id=sna.scientificname_id AND concat_ws('', sn2.genus, sn2.species) like scientificName_in;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    open animalCursor;
    igmLoop: loop
        fetch animalCursor into animalId;
        if done = 1 then leave igmLoop; end if;
    
		IF(animalId IS NOT NULL) THEN
			SET habitatId := (SELECT id from `akvariet_dk_db2`.habitat WHERE BiotopAkvarietId=BiotopId_in);
			IF (habitatId IS NOT NULL) THEN
				INSERT ignore INTO `akvariet_dk_db2`.`habitattoanimal` (`Animal_id`,`Habitat_id`) VALUES (animalId, habitatId);
                UPDATE `akvariet_dk_db2`.`animal` SET ViewCount=ViewCount+1, `ModifiedDate`= NOW() WHERE Id=animalId;
				SET returnValue := returnValue + 1;
			END IF;
		END IF;
	END loop igmLoop;
	close animalCursor;
    SET done := 0;
	open synonymCursor;
    synonymLoop: loop
        fetch synonymCursor into animalId;
        if done = 1 then leave synonymLoop; end if;
    
		IF(animalId IS NOT NULL) THEN
			SET habitatId := (SELECT id from `akvariet_dk_db2`.habitat WHERE BiotopAkvarietId=BiotopId_in);
			IF (habitatId IS NOT NULL) THEN
				INSERT ignore INTO `akvariet_dk_db2`.`habitattoanimal` (`Animal_id`,`Habitat_id`) VALUES (animalId, habitatId);
                UPDATE `akvariet_dk_db2`.`animal` SET ViewCount=ViewCount+1, `ModifiedDate`= NOW() WHERE Id=animalId;
				SET returnValue := returnValue + 1;
			END IF;
		END IF;
	END loop synonymLoop;
	close synonymCursor;
    
	SELECT returnValue;
END;
$$
DELIMITER ;



UPDATE animal SET ViewCount=0;
UPDATE plant SET ViewCount=0;
UPDATE `blogentity` SET PostRead=0;

-- From: http://datamakessense.com/mysql-rownum-row-number-function/
DROP FUNCTION IF EXISTS rownum;
DELIMITER $$
CREATE FUNCTION rownum()
  RETURNS int(11)
BEGIN
  set @prvrownum=if(@ranklastrun=CURTIME(6),@prvrownum+1,1);
  set @ranklastrun=CURTIME(6);
  RETURN @prvrownum;
END $$
$$
DELIMITER ;