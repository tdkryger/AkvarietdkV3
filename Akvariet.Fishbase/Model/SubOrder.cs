﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class SubOrder : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; } = string.Empty;
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
