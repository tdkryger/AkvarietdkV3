﻿using Akvariet.Common;
using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class Comment : IComment
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual IUser User { get; set; }
        public virtual DateTime PostDate { get; set; }
        public virtual string Title { get; set; }
        public virtual string Body { get; set; }
        public virtual Marks Mark { get; set; } = Marks.Posted;
        public virtual int Score { get; set; } = 0;
        #endregion
    }
}
