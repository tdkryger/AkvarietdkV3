﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Akvariet.Fishbase.Model
{
    public class IntStringClass
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual bool Selected { get; set; }
    }
}