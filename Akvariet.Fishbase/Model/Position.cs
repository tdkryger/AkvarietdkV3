﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Position : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Latititude { get; set; }
        public virtual string Longitude { get; set; }
        public virtual int Year { get; set; }
        public virtual string NameUsed { get; set; }
        public virtual string Depth { get; set; }
        #endregion
    }
}
