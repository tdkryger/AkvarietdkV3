﻿using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class Size : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual Decimal Min { get; set; }
        public virtual Decimal Max { get; set; }
        #endregion

        public override string ToString()
        {
            if (Min == 0)
            {
               return  "Ukendt";
            }
            else
            {
                if ((Min == Max) || (Min > Max))
                {
                    return $"Op til {Min:#0.#} cm";
                }
                else
                {
                    return $"{Min:#0.#}-{Max:#0.#} cm";

                }
            }
        }
    }
}
