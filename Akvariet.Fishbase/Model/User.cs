﻿using Akvariet.Common;
using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class User : IUser
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string Username { get; set; }
        public virtual string EMail { get; set; }
        public virtual UserLevels Role { get; set; } = UserLevels.None;
        public virtual DateTime JoinDateTime { get; set; } = DateTime.Now;
        #endregion
    }
}
