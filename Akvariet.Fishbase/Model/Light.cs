﻿using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class Light : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual Decimal Min { get; set; }
        public virtual Decimal Max { get; set; }
        public virtual string Text { get; set; }
        #endregion

        public override string ToString()
        {
            if (Min == 0)
            {
                return "Ukendt";
            }
            else
            {
                if (Min == Max)
                {
                    return $"{Min:#0.#} W/L";
                }
                else
                {
                    return $"{Min:#0.#}-{Max:#0.#} W/L";
                }
            }
        }
    }
}
