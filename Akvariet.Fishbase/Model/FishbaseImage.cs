﻿using Akvariet.Common.Interfaces;
using System;
using System.Drawing;


namespace Akvariet.Fishbase.Model
{
    public partial class FishbaseImage : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual Image Image { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string Content { get; set; }
        public virtual string CopyRight { get; set; } = "© Akvariet.dk";
        public virtual DateTime CreateDate { get; set; } = DateTime.Now;
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return Title;
        }

        public virtual Image Thumbnail()
        {
            return TDK.Tools.ImageTools.FixedSize(Image, 120, 120, Color.White);
            //return Image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
        }

        public virtual Image Thumbnail(int width, int height)
        {
            return TDK.Tools.ImageTools.FixedSize(Image, width, height, Color.White);
            //return Image.GetThumbnailImage(width, height, () => false, IntPtr.Zero);
        }
        #endregion


    }
}
