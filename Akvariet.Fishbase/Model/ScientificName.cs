﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class ScientificName : IEntity//, IComparable<ScientificName>, IEquatable<ScientificName>
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Genus { get; set; }
        public virtual string Species { get; set; }
        #endregion

        #region Constructors
        public ScientificName()
        {
            Genus = string.Empty;
            Species = string.Empty;
        }

        public ScientificName(string text)
        {
            int idx = text.IndexOf(' ');
            if (idx > 0)
            {
                Genus = text.Substring(0, idx).Trim();
                Species = text.Substring(idx).Trim();
            }
            else
            {
                Genus = text;
                Species = string.Empty;
            }
            
        }
        #endregion

        #region Public methods
        //public virtual bool CompareThem(string text, bool contains)
        //{
        //    ScientificName other = new ScientificName(text);
        //    return CompareThem(other, contains);
        //}

        //public virtual bool CompareThem(ScientificName other, bool contains)
        //{
        //    if (contains)
        //    {
        //        return (this.Genus.Contains(other.Genus) || this.Species.Contains(other.Species));
        //    }
        //    else
        //    {
        //        return (this.Genus.Equals(other.Genus) || this.Species.Equals(other.Species));
        //    }
        //}

        public override string ToString()
        {
            return $"{Genus} {Species}";
        }

        //public virtual int CompareTo(ScientificName other)
        //{
        //    bool straight = CompareThem(other, false);
        //    if (straight)
        //        return 0;
        //    else
        //    {
        //        int i = this.Genus.CompareTo(other.Genus);
        //        if (i != 0)
        //            return i;
        //        else
        //            return this.Species.CompareTo(other.Species);
        //    }
        //}

        //public virtual bool Equals(ScientificName other)
        //{
        //    return CompareThem(other, true);
        //}
        #endregion
    }
}
