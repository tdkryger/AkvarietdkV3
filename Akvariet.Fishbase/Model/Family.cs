﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Family : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        #endregion

        #region Public methods
        public override string ToString()
        {
            return Name;
        }
        #endregion
    }
}
