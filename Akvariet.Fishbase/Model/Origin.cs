﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Origin : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual string SubOrigin { get; set; }
        #endregion

        #region Public methods

        public override string ToString()
        {
            if (string.IsNullOrEmpty(SubOrigin))
                return Text;
            if (Text.Equals(SubOrigin))
                return Text;
            return $"{Text} ({SubOrigin})";
        }
        #endregion
    }
}
