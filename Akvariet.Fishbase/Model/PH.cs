﻿using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class PH : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual Decimal Min { get; set; }
        public virtual Decimal Max { get; set; }
        public virtual bool AddSalt { get; set; } = false;
        #endregion
        public override string ToString()
        {
            return $"{Min:#0.#}-{Max:#0.#}";
        }
    }
}
