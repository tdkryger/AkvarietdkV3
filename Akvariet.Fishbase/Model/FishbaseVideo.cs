﻿using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class FishbaseVideo : IEntity
    {
        // TODO: How do we store a video file?
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string Content { get; set; }
        public virtual string CopyRight { get; set; } = "© Akvariet.dk";
        public virtual DateTime CreateDate { get; set; } = DateTime.Now;
        public virtual string MediaType { get; set; }
        public virtual byte[] Media { get; set; }
    }
}
