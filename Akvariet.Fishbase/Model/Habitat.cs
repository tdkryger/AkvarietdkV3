﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Habitat : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        public virtual string Description { get; set; }
        public virtual int BiotopAkvarietId { get; set; }
        #endregion

        #region Public methods
        public override string ToString()
        {
            return Text;
        }
        #endregion
    }
}
