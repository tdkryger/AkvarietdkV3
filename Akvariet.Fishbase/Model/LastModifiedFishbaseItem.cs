﻿using System;

namespace Akvariet.Fishbase.Model
{
    public class LastModifiedFishbaseItem
    {
        public virtual int Id { get; set; }
        public virtual DateTime ModifiedDate { get; set; }
        public virtual string ScientificNameText { get; set; }

        public override string ToString()
        {
            return ScientificNameText;
        }
    }

   
}
