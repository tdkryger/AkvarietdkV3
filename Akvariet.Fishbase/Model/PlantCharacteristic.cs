﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class PlantCharacteristic : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
    }
}
