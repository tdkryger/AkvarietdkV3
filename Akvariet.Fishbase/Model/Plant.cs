﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Interfaces;
using System;
using System.Collections.Generic;

namespace Akvariet.Fishbase.Model
{
    public class Plant : IFishbaseEntry, IEntity, ICommentableEntity
    {
        #region Fields
        private DateTime _modifiedDate = DateTime.Now;
        private string _scientificNameText = string.Empty;
        #endregion

        #region Enums
        public enum PlantTankPositions { Unknown, Front, Middle, Back, Solitary, Floating };
        public enum PlantCO2Requirements { Unknown, None, Low, Medium, High };
        public enum PlantGrowthSpeed { Unknown, VerySlow, Slow, Moderate, Fast, VeryFast };
        #endregion

        #region Properties
        #region From interface
        public virtual int Id { get; set; }
        public virtual ScientificName ScientificName { get; set; } = new ScientificName();
        public virtual IList<Tradename> Tradenames { get; set; } = new List<Tradename>();
        public virtual IList<ScientificName> Synonyms { get; set; } = new List<ScientificName>();
        public virtual Origin Origin { get; set; } = new Origin();
        public virtual Temperature WaterTemperature { get; set; } = new Temperature();
        public virtual Hardness WaterHardness { get; set; } = new Hardness();
        public virtual PH WaterPh { get; set; } = new PH();
        public virtual string Description { get; set; } = string.Empty;
        public virtual IList<FishbaseImage> Images { get; set; } = new List<FishbaseImage>();
        public virtual DateTime CreateDate { get; set; } = DateTime.Now;
        public virtual DateTime ModifiedDate { get { return GetModifiedDate(); } set { _modifiedDate = value; } }
        public virtual int ViewCount { get; set; } = 0;
        public virtual IList<IComment> Comments { get; set; } = new List<IComment>();
        public virtual string ScientificNameText { get { return GetScientificNameText(); } set { _scientificNameText = value; } }
        #endregion

        public virtual PlantTankPositions TankPosition { get; set; } = PlantTankPositions.Unknown;
        public virtual Size Height { get; set; } = new Size();
        public virtual Size Width { get; set; } = new Size();
        public virtual Light LightRequirements { get; set; } = new Light();
        public virtual PlantCO2Requirements CO2Requirements { get; set; } = PlantCO2Requirements.Unknown;
        public virtual PlantGroup PlantGroup { get; set; }
        public virtual PlantGrowthSpeed GrowthSpeed { get; set; } = PlantGrowthSpeed.Unknown;
        public virtual IList<PlantCare> Care { get; set; } = new List<PlantCare>();
        public virtual IList<PlantCharacteristic> Characteristics { get; set; } = new List<PlantCharacteristic>();
        public virtual User Contributor { get; set; }
        #endregion

        #region Private methods
        private DateTime GetModifiedDate()
        {
            if (_modifiedDate < CreateDate && _modifiedDate == DateTime.MinValue)
            {
                _modifiedDate = CreateDate;
            }
            return _modifiedDate;
        }

        private string GetScientificNameText()
        {
            if (string.IsNullOrEmpty(_scientificNameText))
            {
                _scientificNameText = ScientificName.ToString();
            }
            return _scientificNameText;
        }
        #endregion
    }
}
