﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class AnimalGroup : IEntity
    {
        #region Properties
        public virtual int Id { get; set; } = 0;
        public virtual string Name { get; set; } = string.Empty;
        public virtual string Zone { get; set; } = string.Empty;
        public virtual string AqualogCode { get; set; } = string.Empty;
        public virtual bool Active { get; set; } = true;
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return $"{Name}";// ({Zone})";
        }
        #endregion
    }
}
