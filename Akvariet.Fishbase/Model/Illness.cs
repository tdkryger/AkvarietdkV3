﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Illness : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Group { get; set; }

        public virtual string Symptoms { get; set; }
        public virtual string Cause { get; set; }
        public virtual string Treatment { get; set; }
        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
