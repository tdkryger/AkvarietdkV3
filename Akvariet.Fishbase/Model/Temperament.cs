﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Temperament : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return Text;
        }
        #endregion
    }
}
