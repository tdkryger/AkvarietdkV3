﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class ContactInfoWithFiles : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string FromName { get; set; }
        public virtual string FromEMail { get; set; }
        public virtual string Subject { get; set; }
        public virtual string Content { get; set; }
        public virtual IList<DBFile> Files { get; set; } = new List<DBFile>();
        public virtual string Ip { get; set; }
        public virtual string CountryCode { get; set; }
        public virtual string CountryName { get; set; }
        public virtual string RegionCode { get; set; }
        public virtual string RegionName { get; set; }
        public virtual string City { get; set; }
        public virtual string ZipCode { get; set; }
        public virtual string TimeZone { get; set; }
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }
        public virtual string MetroCode { get; set; }
    }
}
