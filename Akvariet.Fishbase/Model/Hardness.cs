﻿using Akvariet.Common.Interfaces;
using System;

namespace Akvariet.Fishbase.Model
{
    public class Hardness : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual Decimal Min { get; set; }
        public virtual Decimal Max { get; set; }
        #endregion

        public override string ToString()
        {
            if (Min == Max || Min == 0)
                return "Ukendt";
            if (Min == Max || Min > Max)
                return $"{Min:#0.#}";
            return $"{Min:#0.#}-{Max:#0.#}";
        }
    }
}
