﻿using Akvariet.Common.Interfaces;


namespace Akvariet.Fishbase.Model
{
    public class Diet : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
