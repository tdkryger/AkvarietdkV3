﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Social : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }
        #endregion

        public override string ToString()
        {
            return Text;
        }
    }
}
