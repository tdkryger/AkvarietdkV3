﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Language : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        #endregion

        #region Public methods
        public override string ToString()
        {
            return $"{Name} ({Code})";
        }
        #endregion
    }
}
