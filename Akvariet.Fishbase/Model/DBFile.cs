﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class DBFile : IEntity
    {
        public virtual int Id { get; set; }
        public virtual byte[] FileContent { get; set; }
        public virtual string Filename { get; set; }
        public virtual string ContentType { get; set; }
    }
}
