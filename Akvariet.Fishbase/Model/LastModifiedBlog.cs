﻿using System;

namespace Akvariet.Fishbase.Model
{
    public class LastModifiedBlog
    {
        public virtual int Id { get; set; }
        public virtual DateTime Posted { get; set; }
        public virtual string Title { get; set; }
    }
}
