﻿using Akvariet.Common.Interfaces;
using System.Collections.Generic;

namespace Akvariet.Fishbase.Model
{
    public class Order : IEntity
    {

        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; } = "Ukendt";
        public virtual string OrderGroup { get; set; } = "Ukendt";
        public virtual IList<SubOrder> SubOrders { get; set; } = new List<SubOrder>();
        #endregion

        #region Public methods
        public override string ToString()
        {
            return $"{OrderGroup} - {Name}";
        }
        #endregion
    }
}
