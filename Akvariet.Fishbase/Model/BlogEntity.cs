﻿using Akvariet.Common.Interfaces;
using System;
using System.Collections.Generic;

namespace Akvariet.Fishbase.Model
{
    public class BlogEntity : IEntity, ICommentableEntity
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Teaser { get; set; }
        public virtual string Body { get; set; }
        public virtual DateTime Posted { get; set; }
        public virtual int PostRead { get; set; }
        public virtual IList<TagEntity> Tags { get; set; } = new List<TagEntity>();
        public virtual FishbaseImage Image { get; set; }
        public virtual User Contributor { get; set; }
        public virtual IList<IComment> Comments { get; set; } = new List<IComment>();
    }
}
