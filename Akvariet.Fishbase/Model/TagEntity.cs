﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class TagEntity : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string TagName { get; set; }
    }
}
