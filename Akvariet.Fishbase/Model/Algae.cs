﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Algae : IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        public virtual string Symptoms { get; set; }
        public virtual string Cause { get; set; }
        public virtual string Treatment { get; set; }
        #endregion
    }
}
