﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class Tradename :  IEntity
    {
        #region Properties
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Language Language { get; set; }
        #endregion

        #region Public Methods
        public override string ToString()
        {
            return $"{Name} ({Language.Code})";
        }
        #endregion
    }
}
