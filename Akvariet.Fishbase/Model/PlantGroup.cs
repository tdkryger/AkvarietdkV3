﻿using Akvariet.Common.Interfaces;

namespace Akvariet.Fishbase.Model
{
    public class PlantGroup : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Text { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}
