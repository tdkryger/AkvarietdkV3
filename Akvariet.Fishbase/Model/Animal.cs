﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Interfaces;
using System;
using System.Collections.Generic;

namespace Akvariet.Fishbase.Model
{
    public class Animal : IFishbaseEntry, IEntity, ICommentableEntity
    {
        #region Enums
        public enum AnimalBioload { Unknown, Low, Medium, Heavy };
        public enum AnimalSwimLevel { Unknown, Bottom, Lower, LowerMiddle, Middle, MiddleUpper, Upper, Top, All };
        public enum AnimalPlantedTank { Unknown, Yes, No, Partial };
        public enum AnimalFiltration { Unknown, None, Low, Medium, Heavy };
        #endregion

        #region Fields
        private DateTime _modifiedDate = DateTime.Now;
        private string _scientificNameText = string.Empty;
        #endregion

        #region Properties
        #region From Interface
        public virtual int Id { get; set; }
        public virtual ScientificName ScientificName { get; set; } = new ScientificName();
        public virtual IList<Tradename> Tradenames { get; set; } = new List<Tradename>();
        public virtual IList<ScientificName> Synonyms { get; set; } = new List<ScientificName>();
        public virtual Origin Origin { get; set; } = new Origin();
        public virtual Temperature WaterTemperature { get; set; } = new Temperature();
        public virtual Hardness WaterHardness { get; set; } = new Hardness();
        public virtual PH WaterPh { get; set; } = new PH();
        public virtual string Description { get; set; } = string.Empty;
        public virtual IList<FishbaseImage> Images { get; set; } = new List<FishbaseImage>();
        public virtual DateTime CreateDate { get; set; } = DateTime.Now;
        public virtual DateTime ModifiedDate { get { return GetModifiedDate(); } set { _modifiedDate = value; } }
        public virtual int ViewCount { get; set; } = 0;
        public virtual IList<IComment> Comments { get; set; } = new List<IComment>();
        public virtual string CompleteScientificName { get { return ScientificName.ToString(); } }
        public virtual string ScientificNameText { get { return GetScientificNameText(); } set { _scientificNameText = value; }  }
        #endregion

        public virtual Order Order { get; set; }
        public virtual SubOrder SubOrder { get; set; }
        public virtual Family Family { get; set; }
        public virtual AnimalGroup CommonGroup { get; set; } = new AnimalGroup() { Name = "Ukendt" };
        public virtual Size Size { get; set; } = new Size();
        public virtual AnimalBioload Bioload { get; set; } = AnimalBioload.Unknown;
        public virtual AnimalSwimLevel SwimLevel { get; set; } = AnimalSwimLevel.Unknown;
        public virtual IList<Diet> DietaryRequirements { get; set; } = new List<Diet>();
        public virtual IList<Illness> CommonIllnesses { get; set; } = new List<Illness>();
        public virtual Temperament Temperament { get; set; }
        public virtual IList<Social> Sociableness { get; set; } = new List<Social>();
        public virtual int MingroupSize { get; set; } = 1;
        public virtual AnimalPlantedTank PlantedTank { get; set; } = AnimalPlantedTank.Unknown;
        public virtual decimal MinTankSize { get; set; }
        public virtual AnimalFiltration MinFiltrationLevel { get; set; } = AnimalFiltration.Unknown;
        public virtual IList<Habitat> PreferedHabitat { get; set; }
        public virtual bool BreadInCaptivity { get; set; } = false;
        public virtual bool RequiresWood { get; set; } = false;

        public virtual User Contributor { get; set; }



        #endregion

        public override string ToString()
        {
            return this.ScientificName.ToString();
        }

        #region Private methods
        private DateTime GetModifiedDate()
        {
            if (_modifiedDate < CreateDate && _modifiedDate == DateTime.MinValue)
            {
                _modifiedDate = CreateDate;
            }
            return _modifiedDate;
        }

        private string GetScientificNameText()
        {
            if (string.IsNullOrEmpty(_scientificNameText))
            {
                _scientificNameText = ScientificName.ToString();
            }
            return _scientificNameText;
        }
        #endregion
    }
}
