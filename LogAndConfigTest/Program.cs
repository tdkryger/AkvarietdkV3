﻿using Common.Logging;
using System;

namespace LogAndConfigTest
{
    class Program
    {
        //TODO: Logging is not working?

        private static readonly ILog Log = LogManager.GetLogger<Program>();

        static void Main(string[] args)
        {
            Console.WriteLine("Getting Connection string from config");
            Console.WriteLine(Akvariet.Common.AkvarietConfiguration.MySQLConnectionString);

            Console.WriteLine("Testing logging");
            LogTraceMessage();
            LogDebugMessage();
            LogInfoMessage();
            LogWarningMessage();
            LogErrorMessage();
            LogFatalErrorMessage();

            Console.ReadLine();
        }


        private static void LogTraceMessage()
        {
            Log.Trace("This is a trace...");
        }

        private static void LogDebugMessage()
        {
            Log.Debug("This is a debug message...");
        }

        private static void LogInfoMessage()
        {
            Log.Info("This is an info message...");
        }

        private static void LogWarningMessage()
        {
            Log.Warn("This is a warning...");
        }

        private static void LogErrorMessage()
        {
            Log.Error("This is an error...");
        }

        private static void LogFatalErrorMessage()
        {
            Log.Fatal("This is a fatal error...");
        }
    }
}
