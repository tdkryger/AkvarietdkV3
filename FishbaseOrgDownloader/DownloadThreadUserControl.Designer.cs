﻿namespace FishbaseOrgDownloader
{
    partial class DownloadThreadUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWhat = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblSumary = new System.Windows.Forms.Label();
            this.lblCommonNames = new System.Windows.Forms.Label();
            this.lblPhotos = new System.Windows.Forms.Label();
            this.lblPointData = new System.Windows.Forms.Label();
            this.lblMySQL = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Henter:";
            // 
            // lblWhat
            // 
            this.lblWhat.AutoSize = true;
            this.lblWhat.Location = new System.Drawing.Point(86, 0);
            this.lblWhat.Name = "lblWhat";
            this.lblWhat.Size = new System.Drawing.Size(36, 13);
            this.lblWhat.TabIndex = 1;
            this.lblWhat.Text = "what?";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblSumary
            // 
            this.lblSumary.AutoSize = true;
            this.lblSumary.Location = new System.Drawing.Point(3, 13);
            this.lblSumary.Name = "lblSumary";
            this.lblSumary.Size = new System.Drawing.Size(35, 13);
            this.lblSumary.TabIndex = 4;
            this.lblSumary.Text = "label3";
            // 
            // lblCommonNames
            // 
            this.lblCommonNames.AutoSize = true;
            this.lblCommonNames.Location = new System.Drawing.Point(166, 13);
            this.lblCommonNames.Name = "lblCommonNames";
            this.lblCommonNames.Size = new System.Drawing.Size(35, 13);
            this.lblCommonNames.TabIndex = 5;
            this.lblCommonNames.Text = "label4";
            // 
            // lblPhotos
            // 
            this.lblPhotos.AutoSize = true;
            this.lblPhotos.Location = new System.Drawing.Point(308, 13);
            this.lblPhotos.Name = "lblPhotos";
            this.lblPhotos.Size = new System.Drawing.Size(35, 13);
            this.lblPhotos.TabIndex = 6;
            this.lblPhotos.Text = "label5";
            // 
            // lblPointData
            // 
            this.lblPointData.AutoSize = true;
            this.lblPointData.Location = new System.Drawing.Point(452, 13);
            this.lblPointData.Name = "lblPointData";
            this.lblPointData.Size = new System.Drawing.Size(35, 13);
            this.lblPointData.TabIndex = 7;
            this.lblPointData.Text = "label6";
            // 
            // lblMySQL
            // 
            this.lblMySQL.AutoSize = true;
            this.lblMySQL.Location = new System.Drawing.Point(590, 13);
            this.lblMySQL.Name = "lblMySQL";
            this.lblMySQL.Size = new System.Drawing.Size(35, 13);
            this.lblMySQL.TabIndex = 8;
            this.lblMySQL.Text = "label3";
            // 
            // DownloadThreadUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.lblMySQL);
            this.Controls.Add(this.lblPointData);
            this.Controls.Add(this.lblPhotos);
            this.Controls.Add(this.lblCommonNames);
            this.Controls.Add(this.lblSumary);
            this.Controls.Add(this.lblWhat);
            this.Controls.Add(this.label1);
            this.Name = "DownloadThreadUserControl";
            this.Size = new System.Drawing.Size(1127, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblWhat;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblSumary;
        private System.Windows.Forms.Label lblCommonNames;
        private System.Windows.Forms.Label lblPhotos;
        private System.Windows.Forms.Label lblPointData;
        private System.Windows.Forms.Label lblMySQL;
    }
}
