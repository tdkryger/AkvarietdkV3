﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishbaseOrgDownloader
{
    public class DownloadInfo
    {
        public string What { get; set; }
        public string Url { get; set; }
    }
}
