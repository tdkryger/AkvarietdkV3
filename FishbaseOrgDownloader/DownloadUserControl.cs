﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Threading;
using HtmlAgilityPack;
using System.Diagnostics;

namespace FishbaseOrgDownloader
{
    public partial class DownloadUserControl : UserControl
    {
        public enum States { Unknown, Initialized, Running, Done, Error, Cancelled };

        public delegate void DoneDelegate(int id);
        public delegate void NewQueueItemDelegate(DownloadInfo item);
        public event DoneDelegate OnDone;
        public event NewQueueItemDelegate OnNewQueueItem;

        #region consts
        private const string FISHBASE_XML_SUMMARY = "http://www.fishbase.us/maintenance/FB/showXML.php?identifier=FB-{0}";
        private const string FISHBASE_XML_POINT_DATA = "http://www.fishbase.se/webservice/Occurrence/PointData.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_COMMON_NAMES = "http://www.fishbase.se/webservice/ComNames/comnamesxml.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_PHOTOS = "http://www.fishbase.se/webservice/Photos/FishPicsList.php?Genus={0}&Species={1}&type=";

        private const string SQL_INSERT = "INSERT INTO `fishbaseRaw` (`fishbaseId`, `genus`, `species`, `xmlSummary`, `xmlPointData`, `xmlCommonNames`, `xmlPhotos`) "
            + "VALUES (@fishbaseId, @genus, @species, @xmlSummary, @xmlPointData, @xmlCommonNames, @xmlPhotos) "
            + "ON DUPLICATE KEY UPDATE "
            + "`genus`=@genus, `species`=@species, `xmlSummary`=@xmlSummary, `xmlPointData`=@xmlPointData, `xmlCommonNames`=@xmlCommonNames, `xmlPhotos`=@xmlPhotos; ";

        #endregion

        #region Fields
        private DownloadInfo _downloadInfo = null;
        private States _state = States.Unknown;
        private Random _rnd = new Random(DateTime.Now.Millisecond);
        private MySqlConnection conn = new MySqlConnection("Server=192.168.1.36;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;");
        #endregion

        #region Properties
        public int Id { get; private set; }
        public States State { get { return _state; } }
        #endregion

        public DownloadUserControl(int id)
        {
            this.Id = id;
            InitializeComponent();
            lblId.Text = $"Id: {this.Id}";

            backgroundWorker1.ProgressChanged += BackgroundWorker1_ProgressChanged;
            backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;
            UpdateGUI();
        }

        private void BackgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            //if (e.UserState is string)
            //    lblUrl.Text = e.UserState.ToString();
            Application.DoEvents();
        }

        private void UpdateGUI()
        {
            //lblId.Invoke((MethodInvoker)delegate {
            //    // Running on the UI thread
            //    lblId.Text = $"Id: {this.Id}";
            //});

            //lblState.Invoke((MethodInvoker)delegate {
            //    // Running on the UI thread
            //    lblState.Text = $"Status: {_state.ToString()}";
            //});

            //lblUrl.Invoke((MethodInvoker)delegate {
            //    // Running on the UI thread
            //    if (_downloadInfo != null)
            //        lblUrl.Text = $"Url: {_downloadInfo.What}";
            //    else
            //        lblUrl.Text = string.Empty;
            //});
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // check error, check cancel, then use result
            if (e.Error != null)
            {
                _state = States.Error;
                throw new Exception("Worker", e.Error);
                UpdateGUI();
            }
            else if (e.Cancelled)
            {
                _state = States.Cancelled;
                UpdateGUI();
            }
            else
            {
                _state = States.Done;
                UpdateGUI();
                // use it on the UI thread
            }
            OnDone?.Invoke(this.Id);
        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DownloadInfo diArg = (DownloadInfo)e.Argument;
            _state = States.Running;
            backgroundWorker1.ReportProgress(1, diArg.What);
            UpdateGUI();
            if (diArg.Url.StartsWith("http://www.fishbase.se/ListByLetter"))
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlAgilityPack.HtmlDocument doc = hw.Load(diArg.Url);
                int doneSoFar = 0;
                Application.DoEvents();
                HtmlNodeCollection hRefs = doc.DocumentNode.SelectNodes("//a[@href]");
                foreach (HtmlNode link in hRefs)
                {
                    HtmlAttribute att = link.Attributes["href"];
                    if (getIntegerFromString(att.Value, out int fishbaseID))
                    {
                        string xmlurl = string.Format(FISHBASE_XML_SUMMARY, fishbaseID);
                        string what = link.InnerText;
                        DownloadInfo diNew = new DownloadInfo()
                        {
                            Url = xmlurl,
                            What = what
                        };
                        OnNewQueueItem?.Invoke(diNew);
                        doneSoFar++;
                        
                        backgroundWorker1.ReportProgress((int)(100 / hRefs.Count) * doneSoFar, diArg.What);
                    }
                }
            }
            else
            {
               
            }
            backgroundWorker1.ReportProgress(100);
        }

        private bool getIntegerFromString(string input, out int value)
        {
            try
            {
                var stack = new Stack<char>();
                for (var i = input.Length - 1; i >= 0; i--)
                {
                    if (!char.IsNumber(input[i]))
                    {
                        break;
                    }
                    stack.Push(input[i]);
                }
                string result = new string(stack.ToArray());
                value = 0;
                return int.TryParse(result, out value);
            }
            catch
            {
                value = 0;
                return false;
            }
        }

        public void Start(DownloadInfo info)
        {
            _downloadInfo = info;
            _state = States.Initialized;
            UpdateGUI();

            backgroundWorker1.RunWorkerAsync(info);
        }
    }
}
