﻿using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FishbaseOrgDownloader
{
    public partial class Form1 : Form
    {
        private const int THREADS = 8;

        #region consts
        private const string FISHBASE_XML_SUMMARY = "http://www.fishbase.us/maintenance/FB/showXML.php?identifier=FB-{0}";
        private const string FISHBASE_XML_POINT_DATA = "http://www.fishbase.se/webservice/Occurrence/PointData.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_COMMON_NAMES = "http://www.fishbase.se/webservice/ComNames/comnamesxml.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_PHOTOS = "http://www.fishbase.se/webservice/Photos/FishPicsList.php?Genus={0}&Species={1}&type=";

        private const string SQL_INSERT = "INSERT INTO `fishbaseRaw` (`fishbaseId`, `genus`, `species`, `xmlSummary`, `xmlPointData`, `xmlCommonNames`, `xmlPhotos`) "
            + "VALUES (@fishbaseId, @genus, @species, @xmlSummary, @xmlPointData, @xmlCommonNames, @xmlPhotos) "
            + "ON DUPLICATE KEY UPDATE "
            + "`genus`=@genus, `species`=@species, `xmlSummary`=@xmlSummary, `xmlPointData`=@xmlPointData, `xmlCommonNames`=@xmlCommonNames, `xmlPhotos`=@xmlPhotos; ";

        #endregion

        private MySqlConnection conn = new MySqlConnection("Server=192.168.1.36;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;");

        //private Queue<DownloadThreadUserControl> _downloadControls = new Queue<DownloadThreadUserControl>();
        private Queue<DownloadInfo> _downloadQueue = new Queue<DownloadInfo>();
        //private List<DownloadUserControl> _userControls = new List<DownloadUserControl>();
        //private BackgroundWorker[] _workers = new BackgroundWorker[THREADS];
        private int _activeCount = 0;
        private object _lockObject = new object();


        private string[] FISHBASE_LATIN_NAMES = {
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesA.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesB.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesC.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesD.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesE.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesF.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesG.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesH.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesI.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesJ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesK.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesL.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesM.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesN.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesO.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesP.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesQ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesR.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesS.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesT.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesU.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesV.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesW.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesX.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesY.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesZ.htm"
                                                };

        public Form1()
        {
            InitializeComponent();
            int controlHeight = panel1.ClientSize.Height / THREADS;
            for (int i = 0; i < FISHBASE_LATIN_NAMES.Length; i++)
            {
                DownloadInfo di = new DownloadInfo()
                {
                    Url = FISHBASE_LATIN_NAMES[i],
                    What = FISHBASE_LATIN_NAMES[i]
                };
                _downloadQueue.Enqueue(di);
            }
            tspbThreads.Maximum = _downloadQueue.Count;
            Dc_OnQueueUpdate();
            tspbThreads.Value = 0;
            tspbThreads.Step = 1;
            tspbThreads.Visible = false;
            timer1.Enabled = true;
        }

        private void StartThread(BackgroundWorker theWorker)
        {
            lock (_lockObject)
            {
                if (_downloadQueue.Count > 0 && !theWorker.IsBusy)
                {
                    DownloadInfo di = _downloadQueue.Dequeue();
                    theWorker.RunWorkerAsync(di);
                    _activeCount++;
                }
            }
        }

        private void StartThreads()
        {
            StartThread(backgroundWorker1);
            StartThread(backgroundWorker2);
            StartThread(backgroundWorker3);
            StartThread(backgroundWorker4);
            StartThread(backgroundWorker5);
            StartThread(backgroundWorker6);
            StartThread(backgroundWorker7);
            StartThread(backgroundWorker8);
            Dc_OnQueueUpdate();
        }


        private void Dc_OnQueueUpdate()
        {
            try
            {
                tspbThreads.Maximum = _downloadQueue.Count;
                tsslActiveThreads.Text = $"Status: {_activeCount} kører. {_downloadQueue.Count} tilbage i køen. {tspbThreads.Value} behandlet.";
            }
            catch { }
            Application.DoEvents();
        }

        private void tsbtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsbtnGo_Click(object sender, EventArgs e)
        {
            tspbThreads.Visible = true;
            StartThreads();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Dc_OnQueueUpdate();
        }

        private bool getIntegerFromString(string input, out int value)
        {
            try
            {
                var stack = new Stack<char>();
                for (var i = input.Length - 1; i >= 0; i--)
                {
                    if (!char.IsNumber(input[i]))
                    {
                        break;
                    }
                    stack.Push(input[i]);
                }
                string result = new string(stack.ToArray());
                value = 0;
                return int.TryParse(result, out value);
            }
            catch
            {
                value = 0;
                return false;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker me = (BackgroundWorker)sender;
            DownloadInfo diArg = (DownloadInfo)e.Argument;

            me.ReportProgress(1, diArg.What);
            if (diArg.Url.StartsWith("http://www.fishbase.se/ListByLetter"))
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlAgilityPack.HtmlDocument doc = hw.Load(diArg.Url);
                int doneSoFar = 0;
                Application.DoEvents();
                HtmlNodeCollection hRefs = doc.DocumentNode.SelectNodes("//a[@href]");
                foreach (HtmlNode link in hRefs)
                {
                    HtmlAttribute att = link.Attributes["href"];
                    if (getIntegerFromString(att.Value, out int fishbaseID))
                    {
                        string xmlurl = string.Format(FISHBASE_XML_SUMMARY, fishbaseID);
                        string what = link.InnerText;
                        DownloadInfo diNew = new DownloadInfo()
                        {
                            Url = xmlurl,
                            What = what
                        };
                        lock (_lockObject)
                        {
                            _downloadQueue.Enqueue(diNew);
                        }
                        doneSoFar++;

                        me.ReportProgress((int)(100 / hRefs.Count) * doneSoFar, diArg.What);
                    }
                }
            }
            else
            {

            }
            backgroundWorker1.ReportProgress(100);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // check error, check cancel, then use result
            if (e.Error != null)
            {
                //lock(_lockObject)
                //{

                //}
            }
            else if (e.Cancelled)
            {

            }
            else
            {
                StartThreads();
                // use it on the UI thread
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar2.Value = e.ProgressPercentage;
        }

        private void backgroundWorker3_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar3.Value = e.ProgressPercentage;
        }

        private void backgroundWorker4_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar4.Value = e.ProgressPercentage;
        }

        private void backgroundWorker5_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar5.Value = e.ProgressPercentage;
        }

        private void backgroundWorker6_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar6.Value = e.ProgressPercentage;
        }

        private void backgroundWorker7_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar7.Value = e.ProgressPercentage;
        }

        private void backgroundWorker8_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //progressBar8.Value = e.ProgressPercentage;
        }
    }
}
