﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MySql.Data.MySqlClient;
using HtmlAgilityPack;
using System.Diagnostics;

namespace FishbaseOrgDownloader
{
    public partial class DownloadThreadUserControl : UserControl
    {
        public enum States { Unknown, Initialized, Running, Done };

        public delegate void DoneDelegate(string what);
        public delegate void QueueUpdateDelegate();
        public delegate void NewQueueItemDelegate(DownloadInfo item);
        public event DoneDelegate OnDone;
        public event QueueUpdateDelegate OnQueueUpdate;
        public event NewQueueItemDelegate OnNewQueueItem;

        #region consts
        private const string FISHBASE_XML_SUMMARY = "http://www.fishbase.us/maintenance/FB/showXML.php?identifier=FB-{0}";
        private const string FISHBASE_XML_POINT_DATA = "http://www.fishbase.se/webservice/Occurrence/PointData.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_COMMON_NAMES = "http://www.fishbase.se/webservice/ComNames/comnamesxml.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_PHOTOS = "http://www.fishbase.se/webservice/Photos/FishPicsList.php?Genus={0}&Species={1}&type=";

        private const string SQL_INSERT = "INSERT INTO `fishbaseRaw` (`fishbaseId`, `genus`, `species`, `xmlSummary`, `xmlPointData`, `xmlCommonNames`, `xmlPhotos`) "
            + "VALUES (@fishbaseId, @genus, @species, @xmlSummary, @xmlPointData, @xmlCommonNames, @xmlPhotos) "
            + "ON DUPLICATE KEY UPDATE "
            + "`genus`=@genus, `species`=@species, `xmlSummary`=@xmlSummary, `xmlPointData`=@xmlPointData, `xmlCommonNames`=@xmlCommonNames, `xmlPhotos`=@xmlPhotos; ";

        #endregion

        #region Fields
        private DownloadInfo _downloadInfo = null;
        private States _state = States.Initialized;
        private Random _rnd = new Random(DateTime.Now.Millisecond);
        private MySqlConnection conn = new MySqlConnection("Server=192.168.1.36;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;");
        #endregion

        #region Properties
        public DownloadInfo Info { get { return _downloadInfo; } }
        public States State { get { return _state; } }
        #endregion

        public DownloadThreadUserControl(DownloadInfo info)
        {
            _downloadInfo = info;
            InitializeComponent();

            lblWhat.Text = info.What;
        }

        public void Start()
        {
            try
            {
                if (_state == States.Initialized)
                {
                    _state = States.Running;
                    if (_downloadInfo.Url.StartsWith("http://www.fishbase.se/ListByLetter"))
                    {
                        BackgroundWorker bw = new BackgroundWorker()
                        {
                            WorkerReportsProgress = true
                        };
                        bw.DoWork += Bw_DoWork;
                        bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
                        bw.RunWorkerAsync();
                    }
                    else
                    {
                        timer1.Interval = _rnd.Next(1000, 10000);
                        timer1.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Start error {0}", ex.Message);
            }
        }

        private bool getIntegerFromString(string input, out int value)
        {
            try
            {
                var stack = new Stack<char>();
                for (var i = input.Length - 1; i >= 0; i--)
                {
                    if (!char.IsNumber(input[i]))
                    {
                        break;
                    }

                    stack.Push(input[i]);
                }

                string result = new string(stack.ToArray());

                value = 0;
                return int.TryParse(result, out value);
            }
            catch
            {
                value = 0;
                return false;
            }
        }


        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                OnDone?.Invoke(_downloadInfo.What);
            }
            catch { }
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlAgilityPack.HtmlDocument doc = hw.Load(_downloadInfo.Url);

                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    HtmlAttribute att = link.Attributes["href"];
                    int fishbaseID = 0;
                    if (getIntegerFromString(att.Value, out fishbaseID))
                    {
                        string xmlurl = string.Format(FISHBASE_XML_SUMMARY, fishbaseID);
                        string what = link.InnerText;
                        DownloadInfo di = new DownloadInfo()
                        {
                            Url = xmlurl,
                            What = what
                        };
                        OnNewQueueItem?.Invoke(di);

                        OnQueueUpdate?.Invoke();
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Bw_DoWork error {0}", ex.Message);

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            OnDone?.Invoke(_downloadInfo.What);
        }
    }
}
