﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.PhantomJS;

namespace TestSeleniumWeb
{
    [TestClass]
    public class SeleniumWebTest
    {
        private string baseUrl = "http://www.demoqa.com";
        private RemoteWebDriver driver=null;

        public TestContext TestContext { get; set; }

        private static TestContext _testContext;

        [ClassInitialize]
        public static void SetupTests(TestContext testContext)
        {
            _testContext = testContext;
        }

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new RemoteWebDriver(DesiredCapabilities.Firefox());

            //string browsername = _testContext.DataRow["Browser"].ToString();
            //switch(browsername.ToUpper())
            //{
            //    case "FIREFOX":
            //        driver = new RemoteWebDriver(DesiredCapabilities.Firefox());
            //        break;
            //    case "CHROME":
            //        driver = new RemoteWebDriver(DesiredCapabilities.Chrome());
            //        break;
            //    case "IE":
            //        driver = new RemoteWebDriver(DesiredCapabilities.InternetExplorer());
            //        break;
            //}
            //Assert.IsNotNull(driver, $"{browsername} is not valid.");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (driver != null)
            {
                driver.Close();
                driver.Dispose();
            }
        }

        //[TestMethod]

        [TestMethod]
        [TestCategory("Selenium")]
        //[TestCategory("Firefox")]
        public void TestMethod2()
        {
            Assert.IsNotNull(driver, "Driver not initialized");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(baseUrl);
            //IWebDriver driver = new FirefoxDriver();
            //driver.Url = "http://www.demoqa.com";
        }
    }
}
