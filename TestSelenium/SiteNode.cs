﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSelenium
{
    class SiteNode
    {
        public string Url { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return $"{Title} - ({Url})";
        }
    }
}
