﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace TestSelenium
{
    public partial class Form1 : Form
    {
        private bool _mustStop = false;

        public Form1()
        {
            InitializeComponent();
        }

        #region Private methods
        private List<SiteNode> ParseSitemapFile(string url)
        {
            XmlDocument rssXmlDoc = new XmlDocument();

            // Load the Sitemap file from the Sitemap URL
            rssXmlDoc.Load(url);

            List<SiteNode> urls = new List<SiteNode>();

            // Iterate through the top level nodes and find the "urlset" node. 
            foreach (XmlNode topNode in rssXmlDoc.ChildNodes)
            {
                if (topNode.Name.ToLower() == "urlset")
                {
                    // Use the Namespace Manager, so that we can fetch nodes using the namespace
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(rssXmlDoc.NameTable);
                    nsmgr.AddNamespace("ns", topNode.NamespaceURI);

                    // Get all URL nodes and iterate through it.
                    XmlNodeList urlNodes = topNode.ChildNodes;
                    foreach (XmlNode urlNode in urlNodes)
                    {
                        // Get the "loc" node and retrieve the inner text.
                        XmlNode locNode = urlNode.SelectSingleNode("ns:loc", nsmgr);
                        string link = locNode != null ? locNode.InnerText : "";

                        urls.Add(new SiteNode() { Url = link, Title = string.Empty });
                    }
                }
            }

            return urls;
        }

        private bool TestUrl(SiteNode sn, IWebDriver localDriver)
        {
            //TODO: Somehow wait for calls to api to finish before returning
            //string theUrl = sn.Url.Replace("test.akvariet.dk", "");
            tsslCurrentUrl.Text = sn.Url;
            Application.DoEvents();
            WaitForLoad(localDriver);
            tsslCurrentUrl.Text = sn.Url;
            localDriver.Url = sn.Url;
            sn.Title = localDriver.Title;

            int elapsed = 0;
            while (elapsed == 0)
            {
                IWebElement status = localDriver.FindElement(By.Id("elapsedMs"));
                int.TryParse(status.Text, out elapsed);
                Thread.Sleep(10);
                Application.DoEvents();
            }

            if (sn.Title == string.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public static void WaitForLoad(IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            int timeoutSec = 15;
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, timeoutSec));
            wait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
        }
        #endregion

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

            IWebDriver localDriver = new FirefoxDriver();
            localDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(120);
            localDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(30);
            WaitForLoad(localDriver);
            listBox1.Items.Clear();
            tsbtnStart.Enabled = false;
            tsbtnStop.Enabled = true;
            int ok = 0;
            int error = 0;
            try
            {
                listBox1.Items.Clear();
                List<SiteNode> urls = ParseSitemapFile(tstbSitemapUrl.Text);
                tsslUrls.Text = $"Urls: {urls.Count}";
                tspb.Maximum = urls.Count;
                tspb.Value = 0;
                tspb.Visible = true;
                foreach (SiteNode sn in urls)
                {
                    if (_mustStop)
                        return;
                    tspb.PerformStep();
                    string result = string.Empty;
                    if (TestUrl(sn, localDriver))
                    {
                        result = $"{sn.ToString()}. Result: Empty title";
                        error++;
                    }
                    else
                    {
                        result = $"{sn.ToString()}. Result: OK";
                        ok++;
                    }
                    listBox1.Items.Insert(0, result);

                    tsslCounts.Text = $"OK: {ok} / Error: {error}";
                    Application.DoEvents();
                }
            }
            finally
            {
                localDriver.Quit();
                tspb.Visible = false;
                tsbtnStart.Enabled = true;
                tsbtnStop.Enabled = false;
            }
        }

        private void tsbtnStop_Click(object sender, EventArgs e)
        {
            _mustStop = true;
        }
    }
}
