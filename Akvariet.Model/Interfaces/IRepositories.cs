﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model.Interfaces
{
    public interface IRepositories<T> : IDisposable where T : IEntity
    {
        IEnumerable<T> GetAll();
        T Get(int id);
        bool AddOrUpdate(T item);
        bool Delete(int id);
    }
}
