﻿using Akvariet.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model
{
    public class BlogEntry : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Teaser { get; set; }
        public string Body { get; set; }
        public DateTime Posted { get; set; }
        public int PostRead { get; set; }
        public List<TagEntity> Tags { get; set; } = new List<TagEntity>();
        public ImageEntity TheImage { get; set; }
    }
}
