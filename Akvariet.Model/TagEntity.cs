﻿using Akvariet.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model
{
    public class TagEntity : IEntity
    {
        public int Id { get; set; }
        public string TagName { get; set; }
    }
}
