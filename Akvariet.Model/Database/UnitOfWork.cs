﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Akvariet.Model.Database
{
    public class UnitOfWork : Interfaces.IUnitOfWork, IDisposable
    {
        #region Private fields
        private MySqlTransaction _transaction = null;
        private TDK.Logging.ILogging _logFile;
        #endregion

        //public ISession Session { get; private set; }
        #region Properties
        public MySqlConnection Connection { get; private set; }
        public TDK.Logging.ILogging Log { get { return _logFile; } }
        #endregion

        #region Constructor
        public UnitOfWork(string connectionString)
        {
            _logFile = new TDK.Logging.XmlLogging(@"D:\tmp\database.log.xml", true, true, true, true, true, true);
            Connection = new MySqlConnection(connectionString);
            Connection.Open();
        }

        public UnitOfWork()
        {
            //TODO: Get connection string and logpath from Web.config or something
            _logFile = new TDK.Logging.XmlLogging(@"D:\tmp\database.log.xml", true, true, true, true, true, true);
            
            try
            {
                Connection = new MySqlConnection("Server=localhost;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;");
                Connection.Open();
            }
            catch (Exception ex)
            {
                _logFile.Fatal("UnitOfWork", ex);
            }
            
        }
        #endregion

        public void BeginTransaction()
        {
            _logFile.Info("BeginTransaction");
            _transaction = Connection.BeginTransaction();
        }

        public void Commit()
        {
            _logFile.Info("Commit");
            _transaction.Commit();
            _transaction = null;
        }

        public void Rollback()
        {
            _logFile.Info("Rollback");
            _transaction.Rollback();
            _transaction = null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).

                    if (_transaction != null)
                        Rollback();

                    if (Connection.State == System.Data.ConnectionState.Open)
                        Connection.Close();
                    Connection.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
