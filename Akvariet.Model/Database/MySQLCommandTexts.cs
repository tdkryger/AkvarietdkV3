﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model.Database
{
    public static class MySQLCommandTexts
    {
        public static string[] CreateTables = {
            "CREATE TABLE `tags` (`idtags` INT UNSIGNED NOT NULL AUTO_INCREMENT,`tagname` VARCHAR(75) NOT NULL, PRIMARY KEY(`idtags`));",
            "CREATE TABLE `blogs` (`idblogs` INT UNSIGNED NOT NULL AUTO_INCREMENT,`title` VARCHAR(150) NOT NULL,`teaser` VARCHAR(500) NOT NULL,`body` LONGTEXT NOT NULL,`posted` DATETIME NOT NULL,`postreads` INT UNSIGNED NOT NULL DEFAULT 0,`imageid` INT NULL, INDEX `idx_blogs_posted` (`posted`),INDEX `idx_blogs_postreads` (`postreads`),FULLTEXT KEY `idx_blogs_body` (`body`),PRIMARY KEY (`idblogs`));",
            "CREATE TABLE `tagstoblog` (`idtags` INT UNSIGNED NOT NULL,`idblogs` INT UNSIGNED NOT NULL, PRIMARY KEY(`idtags`,`idblogs`),FOREIGN KEY (`idtags`) REFERENCES `tags`(`idtags`),FOREIGN KEY (`idblogs`) REFERENCES `blogs`(`idblogs`));"
        };
        public static string[] DropTabls = {
            "DROP TABLE IF EXISTS `blogs`;",
            "DROP TABLE IF EXISTS `images`;",
            "DROP TABLE IF EXISTS `tags`;"
        };

        public static string InsertTagTable = "INSERT INTO `tags` (`tagname`) VALUES (@tagname);";
        public static string UpdateTagTable = "UPDATE `tags` SET `tagname` = @tagname WHERE `idtags` = @idtags;";
        public static string DeleteTagTable = "DELETE FROM `tags` WHERE `idtags` = @idtags;";
        public static string SelectIdTagTable = "SELECT `tagname`, `idtags` FROM `tags` WHERE `idtags` = @idtags;";
        public static string SelectAllTagTable = "SELECT `tagname`, `idtags` FROM `tags`;";
        public static string SelectTagsForBlogTagTable = "SELECT t.`tagname`,t.`idtags` FROM `tags` t,`tagstoblog` tb WHERE t.`idtags`=tb.`idtags` AND tb.`idblogs`= @idblogs;";


        public static string SelectIdBlogTable = "SELECT `idblogs`,`title`,`teaser`,`body`,`posted`,`postreads`,`imageid` FROM `blogs` WHERE `idblogs`=@idblogs;";
        public static string SelectAllBlogTable = "SELECT `idblogs`,`title`,`teaser`,`body`,`posted`,`postreads`,`imageid` FROM `blogs`;";
        public static string SelectByDateBlogTable = "SELECT `idblogs`,`title`,`teaser`,`body`,`posted`,`postreads`,`imageid` FROM `blogs` ORDER BY `posted` LIMIT {0};";
        public static string SelectByReadsBlogTable = "SELECT `idblogs`,`title`,`teaser`,`body`,`posted`,`postreads`,`imageid` FROM `blogs` ORDER BY `postreads` LIMIT {0};";
        public static string DeleteBlogTable = "DELETE FROM `blogs` WHERE `idblogs`=@idblogs;";
        public static string InsertBlogTable = "INSERT INTO `blogs`(`title`,`teaser`,`body`,`posted`,`postreads`,`imageid`) VALUES (@title,@teaser,@body,@posted,@postreads,@imageid);";
        public static string UpdateBlogTable = "UPDATE `blogs` SET `idblogs`=@idblogs,`title`=@title,`teaser`=@teaser,`body`=@body,`posted`=@posted,`postreads`=@postreads,`imageid`=@imageid WHERE `idblogs`=@idblogs;";



        public static string GetStringSafe(IDataReader reader, int colIndex)
        {
            return GetStringSafe(reader, colIndex, string.Empty);
        }

        public static string GetStringSafe(IDataReader reader, int colIndex, string defaultValue)
        {
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            else
                return defaultValue;
        }

        public static string GetStringSafe(IDataReader reader, string indexName)
        {
            return GetStringSafe(reader, reader.GetOrdinal(indexName));
        }

        public static string GetStringSafe( IDataReader reader, string indexName, string defaultValue)
        {
            return GetStringSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }
    }
}
