﻿using Akvariet.Model.Database;
using Akvariet.Model.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model.Repositories
{
    public class TagRepository : Interfaces.IRepositories<TagEntity>
    {
        #region Private Fields
        private UnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public TagRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }
        #endregion

        public bool AddOrUpdate(TagEntity item)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                if (item.Id > 0)
                {
                    cmd.CommandText = MySQLCommandTexts.UpdateTagTable;
                    cmd.Parameters.AddWithValue("idtags", item.Id);
                }
                else
                    cmd.CommandText = MySQLCommandTexts.InsertTagTable;
                cmd.Parameters.AddWithValue("tagname", item.TagName);

                _unitOfWork.BeginTransaction();
                try
                {
                    int rows = cmd.ExecuteNonQuery();
                    _unitOfWork.Commit();
                    return rows != 0;
                }
                catch (Exception ex)
                {
                    _unitOfWork.Log.Error("TagRepository.AddOrUpdate", ex);
                    _unitOfWork.Rollback();
                    return false;
                }
            }
        }

        public bool Delete(int id)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.DeleteTagTable;
                cmd.Parameters.AddWithValue("idtags", id);

                _unitOfWork.BeginTransaction();
                try
                {
                    int rows = cmd.ExecuteNonQuery();
                    _unitOfWork.Commit();
                    if (rows == 0)
                    {
                        _unitOfWork.Log.Warning($"TagRepository.Delete {id}: No rows affected.");

                    }
                    return rows != 0;
                }
                catch (Exception ex)
                {
                    _unitOfWork.Log.Error("TagRepository.Delete", ex);
                    _unitOfWork.Rollback();
                    return false;
                }
            }
        }

        public TagEntity Get(int id)
        {
            TagEntity tagEntity = null;

            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.SelectIdTagTable;
                cmd.Parameters.AddWithValue("idtags", id);

                try
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            tagEntity = new TagEntity()
                            {
                                Id = reader.GetInt32("idtags"),
                                TagName = reader.GetString("tagname")
                            };
                        }
                    }
                }
                catch (Exception ex)
                {
                    _unitOfWork.Log.Error("TagRepository.Get", ex);
                }
            }
            return tagEntity;
        }

        public IEnumerable<TagEntity> GetAll()
        {
            List<TagEntity> tags = new List<TagEntity>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.SelectAllTagTable;

                try
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            tags.Add( new TagEntity()
                            {
                                Id = reader.GetInt32("idtags"),
                                TagName = reader.GetString("tagname")
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    _unitOfWork.Log.Error("TagRepository.GetAll", ex);
                }
            }

            return tags;
        }

        public IEnumerable<TagEntity> GetForBlogEntry(BlogEntry blogEntry)
        {
            if (blogEntry == null)
                return new List<TagEntity>();
            List<TagEntity> tags = new List<TagEntity>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.SelectTagsForBlogTagTable;
                cmd.Parameters.AddWithValue("idblogs", blogEntry.Id);

                try
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            tags.Add(new TagEntity()
                            {
                                Id = reader.GetInt32("idtags"),
                                TagName = reader.GetString("tagname")
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    _unitOfWork.Log.Error($"TagRepository.GetForBlogEntry for blogentry id {blogEntry.Id}", ex);
                }
            }
            return tags;
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }
    }
}
