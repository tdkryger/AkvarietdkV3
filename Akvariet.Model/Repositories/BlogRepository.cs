﻿using Akvariet.Model.Database;
using Akvariet.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvariet.Model.Repositories
{
    public class BlogRepository : IRepositories<BlogEntry>
    {
        #region Private Fields
        private UnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public BlogRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }
        #endregion

        public IEnumerable<BlogEntry> GetAll()
        {
            List<BlogEntry> list = new List<BlogEntry>();
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = _unitOfWork.Connection;
                    cmd.CommandText = MySQLCommandTexts.SelectAllBlogTable;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        list = BlogListFromReader(reader).ToList();
                    }
                }
            }
            return list;
        }

        public BlogEntry Get(int id)
        {
            BlogEntry blogEntry = null;
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.SelectIdBlogTable;
                cmd.Parameters.AddWithValue("idtags", id);
                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        blogEntry = BlogFromReader(reader);
                    }
                }
            }
            return blogEntry;
        }

        public IEnumerable<BlogEntry> GetTopRead(int count)
        {
            List<BlogEntry> list = new List<BlogEntry>();
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = _unitOfWork.Connection;
                    cmd.CommandText = string.Format(MySQLCommandTexts.SelectByReadsBlogTable, count);
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        list = BlogListFromReader(reader).ToList();
                    }
                }
            }
            return list;
        }

        public IEnumerable<BlogEntry> GetNewest(int count)
        {
            List<BlogEntry> list = new List<BlogEntry>();
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = _unitOfWork.Connection;
                    cmd.CommandText = string.Format(MySQLCommandTexts.SelectByDateBlogTable, count);
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        list = BlogListFromReader(reader).ToList();
                    }
                }
            }
            return list;
        }

        public bool AddOrUpdate(BlogEntry item)
        {
            _unitOfWork.BeginTransaction();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = _unitOfWork.Connection;
                    bool needsInsert = false;
                    if (item.Id > 0)
                    {
                        cmd.CommandText = MySQLCommandTexts.UpdateBlogTable;
                        SetMySQLParameters(cmd, item);
                        cmd.Parameters.AddWithValue("idblogs", item.Id);
                        int rows = cmd.ExecuteNonQuery();
                        needsInsert = rows == 0;
                    }
                    if (needsInsert)
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = MySQLCommandTexts.InsertBlogTable;
                        SetMySQLParameters(cmd, item);

                        cmd.ExecuteNonQuery();
                        item.Id = (int)cmd.LastInsertedId;
                    }

                    if (item.Tags.Count > 0)
                        using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
                        {
                            foreach (TagEntity tag in item.Tags)
                                tagRepository.AddOrUpdate(tag);
                        }
                    if (item.TheImage != null)
                    using (ImageRepository imageRepository = new ImageRepository(new UnitOfWork()))
                    {
                            imageRepository.AddOrUpdate(item.TheImage);
                    }
                    _unitOfWork.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _unitOfWork.Log.Error("BlogRepository.AddOrUpdate", ex);
                _unitOfWork.Rollback();
            }
            return false;
        }

        public bool Delete(int id)
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.Connection = _unitOfWork.Connection;
                cmd.CommandText = MySQLCommandTexts.DeleteBlogTable;
                cmd.Parameters.AddWithValue("idblogs", id);
                return cmd.ExecuteNonQuery() == 1;
            }
        }

        public void Dispose()
        {
            _unitOfWork.Dispose();
        }

        #region Private methods
        private void SetMySQLParameters(MySqlCommand cmd, BlogEntry entry)
        {
            cmd.Parameters.AddWithValue("title", entry.Title);
            cmd.Parameters.AddWithValue("@teaser", entry.Teaser);
            cmd.Parameters.AddWithValue("@body", entry.Body);
            cmd.Parameters.AddWithValue("@posted", entry.Posted);
            cmd.Parameters.AddWithValue("@postreads", entry.PostRead);
            if (entry.TheImage != null)
                cmd.Parameters.AddWithValue("@imageid", entry.TheImage.Id);
            else
                cmd.Parameters.AddWithValue("@imageid", 0);

        }

        private IEnumerable<BlogEntry> BlogListFromReader(MySqlDataReader reader)
        {
            List<BlogEntry> list = new List<BlogEntry>();
            while (reader.Read())
            {
                BlogEntry be = BlogFromReader(reader);
                if (be != null)
                    list.Add(be);
            }

            return list;
        }

        private BlogEntry BlogFromReader(MySqlDataReader reader)
        {
            if (reader != null)
            {
                BlogEntry blogEntry = new BlogEntry()
                {
                    Body = MySQLCommandTexts.GetStringSafe(reader, "body"),
                    Id = reader.GetInt32("idblogs"),
                    Posted = reader.GetDateTime("posted"),
                    PostRead = reader.GetInt32("postreads"),
                    Teaser = MySQLCommandTexts.GetStringSafe(reader, "teaser"),
                    Title = MySQLCommandTexts.GetStringSafe(reader, "title")
                };
                using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
                {

                    blogEntry.Tags = tagRepository.GetForBlogEntry(blogEntry).ToList();
                }
                using (ImageRepository imageRepository = new ImageRepository(new UnitOfWork()))
                {
                    blogEntry.TheImage = imageRepository.Get(reader.GetInt32("imageid"));
                }
            }
            return null;
        }
        #endregion
    }
}
