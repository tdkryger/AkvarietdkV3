﻿using Akvariet.Model.Database;
using Akvariet.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvariet.Model.Repositories
{
    public class ImageRepository : IRepositories<ImageEntity>
    {
        #region Private Fields
        private UnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        public ImageRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }
        #endregion

        public bool AddOrUpdate(ImageEntity item)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public ImageEntity Get(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ImageEntity> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
