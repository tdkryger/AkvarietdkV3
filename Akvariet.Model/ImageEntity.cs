﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Akvariet.Model.Interfaces;

namespace Akvariet.Model
{
    public class ImageEntity : IEntity
    {
        #region Fields
        private Image _image = null;
        private Image _thumbnail = null;
        #endregion

        public int Id { get; set; }
        public List<TagEntity> Tags { get; set; } = new List<TagEntity>();
        public string Name { get; set; }
        public Image FullSize { get { return LoadImage(); } set { _image = value; } }
        public Image Thumbnail { get { return GetThumbnail(); } }

        #region Private methods
        private Image LoadImage()
        {
            if (_image == null)
            {
                //TODO: Load from database
            }
            return _image;
        }

        private Image GetThumbnail()
        {
            if (_thumbnail != null)
                return _thumbnail;

            if (_image == null)
                LoadImage();
            if (_image != null)
                _thumbnail = _image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            return _thumbnail;
        }
        #endregion
    }
}
