﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Akvariet.Model.Repositories;
using Akvariet.Model.Database;
using Akvariet.Model;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestAkvarietModel
{
    [TestClass]
    public class utTagRepository
    {
        private static TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [TestInitialize]
        public void TestInitialize()
        {
        }

        [ClassInitialize]
        public static void ClassSetup(TestContext a)
        {
            testContextInstance = a;
            //TODO: Delete all the tables, and create them again
            using (UnitOfWork uow = new UnitOfWork())
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    cmd.Connection = uow.Connection;
                    uow.BeginTransaction();
                    try
                    {
                        foreach (string sql in MySQLCommandTexts.DropTabls)
                        {
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                        foreach (string sql in MySQLCommandTexts.CreateTables)
                        {
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                        uow.Commit();
                    }
                    catch
                    {
                        uow.Rollback();
                    }
                }
            }
            //TODO: Add some default tags for tests
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                for (int i = 0; i < 10; i++)
                {
                    TagEntity tagEntity = new TagEntity()
                    {
                        TagName = "Tag " + i
                    };
                    tagRepository.AddOrUpdate(tagEntity);
                }
            }
        }

        [TestMethod]
        public void TagRepository_AddOrUpdate()
        {
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                TagEntity tagEntity = new TagEntity()
                {
                    TagName = "Tag"
                };
                Assert.IsTrue(tagRepository.AddOrUpdate(tagEntity));
            }
        }

        [TestMethod]
        public void TagRepository_Delete()
        {
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                Assert.IsTrue(tagRepository.Delete(10));
            }
        }

        [TestMethod]
        public void TagRepository_Get()
        {
            int id = 1;
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                TagEntity tagEntity = tagRepository.Get(id);
                Assert.IsNotNull(tagEntity);
                Assert.AreEqual(id, tagEntity.Id);
            }
        }

        [TestMethod]
        public void TagRepository_Get_NotFound()
        {
            int id = 1111111;
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                TagEntity tagEntity = tagRepository.Get(id);
                Assert.IsNull(tagEntity);
            }
        }

        [TestMethod]
        public void TagRepository_GetAll()
        {
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                List<TagEntity> list = tagRepository.GetAll().ToList();

                Assert.IsNotNull(list);
                Assert.AreNotEqual(0, list.Count);
            }
        }
    }
}
