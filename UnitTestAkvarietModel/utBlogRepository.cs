﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Akvariet.Model.Repositories;
using Akvariet.Model.Database;
using Akvariet.Model;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;

namespace UnitTestAkvarietModel
{
    [TestClass]
    public class utBlogRepository
    {
        private void CreateSomeBlogEntrys()
        {

            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                for (int i = 0; i < 10; i++)
                {
                    BlogEntry blogEntry = new BlogEntry()
                    {
                        Id = i,
                        Body = "Body",
                        Posted = DateTime.Now,
                        PostRead = i * 7,
                        Teaser = "A teaser",
                        Title = $"A Title {i}",
                    };
                    blogRepository.AddOrUpdate(blogEntry);
                }
            }

        }

        [TestMethod]
        public void BlogRepository_Constructor()
        {
            BlogRepository blogRepository = new BlogRepository(new UnitOfWork());
            Assert.IsNotNull(blogRepository);
        }

        [TestMethod]
        public void BlogRepository_GetAll()
        {
            CreateSomeBlogEntrys();
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                List<BlogEntry> list = blogRepository.GetAll().ToList();
                Assert.IsNotNull(list);
                Assert.AreNotEqual(0, list.Count);
            }
        }

        [TestMethod]
        public void BlogRepository_GetOne()
        {
            int id = 1;
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                BlogEntry blogEntry = blogRepository.Get(id);
                Assert.IsNotNull(blogEntry);
                Assert.AreEqual(id, blogEntry.Id);
            }
        }

        [TestMethod]
        public void BlogRepository_GetOne_WrongId()
        {
            int id = -1;
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                BlogEntry blogEntry = blogRepository.Get(id);
                Assert.IsNull(blogEntry);
            }
        }

        [TestMethod]
        public void BlogRepository_GetTopRead()
        {
            int count = 3;
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                List<BlogEntry> list = blogRepository.GetTopRead(count).ToList();
                Assert.IsNotNull(list);
                Assert.AreEqual(count, list.Count);
            }
        }

        [TestMethod]
        public void BlogRepository_GetNewest()
        {
            int count = 3;
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                List<BlogEntry> list = blogRepository.GetNewest(count).ToList();
                Assert.IsNotNull(list);
                Assert.AreEqual(count, list.Count);
            }
        }

        [TestMethod]
        public void BlogRepository_AddOrUpdate()
        {
            BlogEntry blogEntry = new BlogEntry()
            {
                Id = 200,
                Body = "Body",
                Posted = DateTime.Now,
                PostRead = 33,
                Teaser = "A teaser",
                Title = "A Title",
            };
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                Assert.IsTrue(blogRepository.AddOrUpdate(blogEntry));
            }
        }

        [TestMethod]
        public void BlogRepository_Delete()
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                Assert.IsTrue(blogRepository.Delete(1));
            }
        }
    }
}
