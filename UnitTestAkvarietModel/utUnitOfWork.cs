﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Akvariet.Model.Database;

namespace UnitTestAkvarietModel
{
    [TestClass]
    public class utUnitOfWork
    {
        [TestMethod]
        public void CreateUnitOfWork_EmptyConstructor()
        {
            UnitOfWork uow = new UnitOfWork();
            Assert.IsNotNull(uow);
        }

        [TestMethod]
        public void CreateUnitOfWork_FilledConstructor()
        {
            UnitOfWork uow = new UnitOfWork("Server=localhost;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;");
            Assert.IsNotNull(uow);
        }
        //MySql.Data.MySqlClient.MySqlException
        [ExpectedException(typeof(MySql.Data.MySqlClient.MySqlException))]
        [TestMethod]
        public void CreateUnitOfWork_FilledConstructor_WithError()
        {
            UnitOfWork uow = new UnitOfWork("Server=localhost;Database=akvariet_dk_db2;Uid=newuser;Pwd=theWrongPassword;ConvertZeroDateTime=True;");
            Assert.IsNotNull(uow);
        }
    }
}
