﻿using Akvarietdk.Backend;
using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using Akvarietdk.Backend.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnit.Akvariet.Backend
{
    [TestFixture]
    public class TestAlgaeV32
    {
        #region Repository
        [Test]
        public void Repository_Create()
        {
            AlgaeRepository repo;
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                repo = new AlgaeRepository(appDb);
            }
            Assert.IsNotNull(repo);
        }

        [Test]
        public async Task Repository_FindOneAsync()
        {
            Algae algae;
            const long id = 1;
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                var repo = new AlgaeRepository(appDb);
                algae = (Algae)await repo.FindOneAsync(id);
            }
            Assert.IsNotNull(algae);
            Assert.AreEqual(id, algae.Id);
            Assert.AreEqual("Brune alger", algae.Name);
        }

        [Test]
        public async Task Repository_GetAsync()
        {
            List<Algae> algaeList;
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                var repo = new AlgaeRepository(appDb);
                var entityList = await repo.GetAsync(new List<Akvarietdk.Backend.Support.SearchParameters>());
                algaeList = AlgaeRepository.ConvertList(entityList);
            }
            Assert.IsNotNull(algaeList);
            Assert.AreNotEqual(0, algaeList.Count);
        }
        [Test]
        public async Task Repository_LatestPostsAsync()
        {
            List<Algae> algaeList;
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                var repo = new AlgaeRepository(appDb);
                var entityList = await repo.LatestPostsAsync();
                algaeList = AlgaeRepository.ConvertList(entityList);
            }
            Assert.IsNotNull(algaeList);
            Assert.AreNotEqual(0, algaeList.Count);
        }
        #endregion

        #region Model
        [Test]
        public void Model_Create()
        {
            Algae item;
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                item = new Algae(appDb);
            }
            Assert.IsNotNull(item);
        }

        [Test]
        public async Task Model_InsertAsync()
        {
            Algae item;
            Algae newAlgae;
            var newAlgaeName = DateTime.Now.ToString();
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                var repo = new AlgaeRepository(appDb);
                item = new Algae(appDb)
                {
                    Cause = "Some cause",
                    Name = newAlgaeName,
                    Symptoms = "Some symptoms",
                    Treatment = "A treatment",
                    Id=0
                };
                await item.InsertAsync();
                Assert.AreNotEqual(0, item.Id);
                newAlgae = (Algae) await repo.FindOneAsync(item.Id);
                Assert.AreEqual(item.Name, newAlgae.Name);
            }
            Assert.IsNotNull(item);
        }

        [Test]
        public async Task Model_UpdateAsync()
        {
            Algae algae;
            Algae updatedAlgae;
            const long id = 1;
            var newCause = DateTime.Now.ToString();
            using (IAppDb appDb = new AppDb())
            {
                appDb.Connection.Open();
                var repo = new AlgaeRepository(appDb);
                algae = (Algae)await repo.FindOneAsync(id);

                Assert.IsNotNull(algae);
                Assert.AreEqual(id, algae.Id);
                Assert.AreEqual("Brune alger", algae.Name);
                algae.Cause = newCause;
                await algae.UpdateAsync();
                updatedAlgae = (Algae)await repo.FindOneAsync(id);

                Assert.AreEqual(newCause, updatedAlgae.Cause);
            }
        }
        #endregion
    }
}
