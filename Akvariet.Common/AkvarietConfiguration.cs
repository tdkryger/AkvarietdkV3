﻿using System.Configuration;

namespace Akvariet.Common
{
    /* 
        Configuration:
        http://stackoverflow.com/a/396197
    */
    public static class AkvarietConfiguration
    {
        public static string MySQLConnectionString { get { return ConfigurationManager.AppSettings["DB_ConnectionString"]; } }
        public static string MySQLConnectionStringRaw { get { return ConfigurationManager.AppSettings["DB_ConnectionStringRaw"]; } }

        public static string ContactEMail { get { return ConfigurationManager.AppSettings["ContactEmail"]; } }
        public static string EMailSenderName { get { return ConfigurationManager.AppSettings["EMailSenderName"]; } }
        public static string SMTPServer { get { return ConfigurationManager.AppSettings["SMTPServer"]; } }
        public static int SMTPPort { get { return SettingAsInt("SMTPPort"); } }
        public static string SMTPPassword { get { return ConfigurationManager.AppSettings["SMTPPassword"]; } }
        public static string SMTPUser { get { return ConfigurationManager.AppSettings["SMTPUser"]; } }
        public static int ShortTeaserLength { get { return SettingAsInt("ShortTeaserLength"); } }
        public static int MaxFishbasePictureWidth { get { return SettingAsInt("MaxFishbasePictureWidth"); } }
        public static int MaxFishbasePictureHeight { get { return SettingAsInt("MaxFishbasePictureHeight"); } }
        public static string BaseUrl { get { return ConfigurationManager.AppSettings["BaseUrl"]; } }
        public static string NewFilesUploadedSubject { get { return GetNewFilesUploadedSubject(); } }
        public static string NewFilesUploadedBody { get { return GetNewFilesUploadedBody(); } }
        public static string NewMessageBody { get { return GetNewMessageBody(); } }


        public static string ImageHeight
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImageHeight"]))
                    return "300";
                else
                    return ConfigurationManager.AppSettings["ImageHeight"];
            }
        }
        public static string ImageWidth
        {
            get
            {
                if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImageWidth"]))
                    return "620";
                else
                    return ConfigurationManager.AppSettings["ImageWidth"];
            }
        }

        public static string ImageSize { get { return $"&width={ImageWidth}&height={ImageHeight}"; } }


        #region Private
        private static string GetNewMessageBody()
        {
            string value = SettingAsString("NewMessageBody"); ;
            if (value == null)
                value = "Der er kommet en ny besked fra: {0}.<br />Beskeden er:<br />{1}";
            return value;
        }

        private static string GetNewFilesUploadedSubject()
        {
            string value = SettingAsString("NewFilesUploadedSubject");
            if (value == null)
                value = "Nye filer er uploaded";
            return value;
        }

        private static string GetNewFilesUploadedBody()
        {
            string value = SettingAsString("NewFilesUploadedBody");
            if (value == null)
                value = "Der er uploaded {0} nye filer fra {1}.";
            return value;
        }

        private static string SettingAsString(string setting)
        {
            if (ConfigurationManager.AppSettings[setting] == null)
                return string.Empty;
            else
                return ConfigurationManager.AppSettings[setting];
        }

        private static int SettingAsInt(string setting)
        {
            if (int.TryParse(ConfigurationManager.AppSettings[setting], out int value))
                return value;
            else
                return 0;
        }
        #endregion

    }
}
