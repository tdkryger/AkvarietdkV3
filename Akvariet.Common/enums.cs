﻿namespace Akvariet.Common
{
    public enum Marks { Posted, Hidden, OK };
    public enum UserLevels { None, User, Editor, Admin = 99 };
}