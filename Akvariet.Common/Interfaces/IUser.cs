﻿using System;

namespace Akvariet.Common.Interfaces
{
    public interface IUser : IEntity
    {
        string DisplayName { get; set; }
        string Username { get; set; }
        string EMail { get; set; }
        UserLevels Role { get; set; }
        DateTime JoinDateTime { get; set; }
    }
}
