﻿namespace Akvariet.Common.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
