﻿using System.Collections.Generic;

namespace Akvariet.Common.Interfaces
{
    public interface ICommentableEntity : IEntity
    {
        IList<IComment> Comments { get; set; }
    }
}
