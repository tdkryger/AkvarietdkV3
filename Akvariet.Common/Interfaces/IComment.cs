﻿using System;

namespace Akvariet.Common.Interfaces
{
    public interface IComment : IEntity
    {
        Marks Mark { get; set; }
        DateTime PostDate { get; set; }
        string Title { get; set; }
        string Body { get; set; }
        int Score { get; set; }
        IUser User { get; set; }
    }
}
