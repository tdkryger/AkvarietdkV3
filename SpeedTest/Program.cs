﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using WebAkvarietdk.Models;

namespace SpeedTest
{
    class Program
    {
        static void Main(string[] args)
        {
            const int runs = 100;
            Console.WriteLine($"Running {runs} async loops, with waiting for each:");
            var sw = Stopwatch.StartNew();
            for (int i = 0; i < runs; i++)
            {
                var dtoBlogEntrys = DTOHelper.GetNewestBlogsAsync(2).Result;
            }
            sw.Stop();
            Console.WriteLine(Elapsed(sw.Elapsed, runs));

            Console.WriteLine($"Running {runs} sync loops, with waiting for each:");
            var sw2 = Stopwatch.StartNew();
            for (int i = 0; i < runs; i++)
            {
                var dtoBlogEntrys = DTOHelper.GetNewestBlogs(2);
            }
            sw2.Stop();
            Console.WriteLine(Elapsed(sw2.Elapsed, runs));

            Console.WriteLine($"Running {runs} async loops:");
            var elapsed = LoopAsync(runs).Result;
            Console.WriteLine(Elapsed(elapsed, runs));


            Console.ReadLine();
        }

        private static string Elapsed(TimeSpan elapsed, int runs)
        {
            var each = (elapsed.TotalMilliseconds / runs);
            var eachElapsed = TimeSpan.FromMilliseconds(each);
            return $"Runtime: {elapsed} / Average: {eachElapsed}";
        }

        private static async Task<TimeSpan> LoopAsync(int runs)
        {
            var sw = Stopwatch.StartNew();
            for (int i = 0; i < runs; i++)
            {
                var dtoBlogEntrys = await DTOHelper.GetNewestBlogsAsync(2);
            }
            sw.Stop();
            return sw.Elapsed;
        }
    }
}
