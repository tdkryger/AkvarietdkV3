﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Xml;
using System.Collections.Generic;

namespace UnitTestSelenium
{
    [TestClass]
    public class SeleniumPages
    {
        private static IWebDriver _driver;


        private List<string> ParseSitemapFile(string url)
        {
            XmlDocument rssXmlDoc = new XmlDocument();

            // Load the Sitemap file from the Sitemap URL
            rssXmlDoc.Load(url);

            List<string> urls = new List<string>();

            // Iterate through the top level nodes and find the "urlset" node. 
            foreach (XmlNode topNode in rssXmlDoc.ChildNodes)
            {
                if (topNode.Name.ToLower() == "urlset")
                {
                    // Use the Namespace Manager, so that we can fetch nodes using the namespace
                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(rssXmlDoc.NameTable);
                    nsmgr.AddNamespace("ns", topNode.NamespaceURI);

                    // Get all URL nodes and iterate through it.
                    XmlNodeList urlNodes = topNode.ChildNodes;
                    foreach (XmlNode urlNode in urlNodes)
                    {
                        // Get the "loc" node and retrieve the inner text.
                        XmlNode locNode = urlNode.SelectSingleNode("ns:loc", nsmgr);
                        string link = locNode != null ? locNode.InnerText : "";

                        urls.Add(link);
                    }
                }
            }

            return urls;
        }

        private static List<string> ParseSitemap(string xmlData)
        {
            List<string> urls = new List<string>();
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlData);
            XmlNamespaceManager manager = new XmlNamespaceManager(xml.NameTable);
            //manager.AddNamespace("s", "http://www.sitemaps.org/schemas/sitemap/0.9");
            manager.AddNamespace("s", xml.DocumentElement.NamespaceURI); //Using xml's properties instead of hard-coded URI
            XmlNodeList xnList = xml.SelectNodes("/s:sitemapindex/s:sitemap", manager);
            foreach (XmlNode xn in xnList)
            {
                urls.Add(xn["loc"].InnerText);
            }
            return urls;
        }

        [ClassInitialize()]
        [TestCategory("Selenium")]
        [TestCategory("Firefox")]
        public static void ClassInit(TestContext context)
        {
            _driver = new FirefoxDriver();
        }

        [ClassCleanup()]
        [TestCategory("Selenium")]
        [TestCategory("Firefox")]
        public static void ClassCleanup()
        {
            _driver.Quit();
        }

        [TestMethod]
        [TestCategory("Selenium")]
        [TestCategory("Firefox")]
        public void Firefox_Sitemap()
        {
            List<string> urls = ParseSitemapFile("http://test.akvariet.dk/sitemap.xml");
            Assert.IsNotNull(urls);
            Assert.AreNotEqual(0, urls.Count);
            foreach(string url in urls)
            {
                IWebDriver localDriver = new FirefoxDriver()
                {
                    Url = url
                };
                Assert.AreNotEqual(0, localDriver.PageSource.Length);
                localDriver.Quit();
            }
        }

        [TestMethod]
        [TestCategory("Selenium")]
        [TestCategory("Firefox")]
        public void Firefox_StartPage()
        {
            Assert.IsNotNull(_driver);
            _driver.Url = "http://test.akvariet.dk";
            Assert.AreNotEqual(string.Empty, _driver.Title);
        }
    }
}
