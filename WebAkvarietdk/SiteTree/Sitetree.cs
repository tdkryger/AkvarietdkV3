﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.SiteTree
{
    public static class Sitetree
    {


        public static IReadOnlyCollection<SitetreeNode> GetSiteTree()
        {

            string baseUrl = Akvariet.Common.AkvarietConfiguration.BaseUrl;
            if (!baseUrl.EndsWith("/#/"))
                baseUrl = baseUrl + "/#/";
            List<SitetreeNode> nodes = new List<SitetreeNode>
            {
                new SitetreeNode()
                {
                    Url = baseUrl + "alger",
                    NodeType = SitetreeTypes.Page,
                    Title = "Alger"
                },
                 new SitetreeNode()
                {
                    Url = baseUrl + "sygdomme",
                    NodeType = SitetreeTypes.Page,
                    Title = "Sygdomme"
                },
                 new SitetreeNode()
                {
                    Url = baseUrl + "biotoper",
                    NodeType = SitetreeTypes.Page,
                    Title = "Biotoper"
                },
            };

            foreach (LastModifiedFishbaseItem animals in DTOHelper.GetAnimalIds())
            {
                nodes.Add(
                    new SitetreeNode()
                    {
                        Url = $"{baseUrl}animal/{animals.Id}",
                        NodeType = SitetreeTypes.Animal,
                        Title = animals.ScientificNameText
                    }
                    );
            }

            foreach (LastModifiedFishbaseItem plants in DTOHelper.GetPlantIds())
            {
                nodes.Add(
                    new SitetreeNode()
                    {
                        Url = $"{baseUrl}plant/{plants.Id}",
                        NodeType = SitetreeTypes.Plant,
                        Title = plants.ScientificNameText
                    }
                    );
            }

            foreach (LastModifiedBlog blogs in DTOHelper.GetBlogIds())
            {
                nodes.Add(
                    new SitetreeNode()
                    {
                        Url = $"{baseUrl}blog/{blogs.Id}",
                        NodeType = SitetreeTypes.Blog,
                        Title = blogs.Title
                    }
                    );
            }

            return nodes;
        }
    }
}