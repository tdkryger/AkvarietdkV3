﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAkvarietdk.SiteTree
{
    public enum SitetreeTypes { Page, Blog, Animal, Plant};

    public class SitetreeNode
    {
        public SitetreeTypes NodeType { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}