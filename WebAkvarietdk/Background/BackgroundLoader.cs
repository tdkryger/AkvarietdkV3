﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Background
{
    public static class BackgroundLoader
    {
        public static void Init()
        {
            Thread animalWorker = new Thread(new ThreadStart(GetAnimalList));
            animalWorker.Start();
            Thread blogWorkder = new Thread(new ThreadStart(GetBlogList));
            blogWorkder.Start();
            Thread plantWorkder = new Thread(new ThreadStart(GetPlantList));
            plantWorkder.Start();
        }


        #region Private methods
        private static void GetAnimalList()
        {
            DTOHelper.GetList(DTOHelper.ListTypes.Animal);
        }

        private static void GetBlogList()
        {
            DTOHelper.GetList(DTOHelper.ListTypes.Blog);
        }

        private static void GetPlantList()
        {
            DTOHelper.GetList(DTOHelper.ListTypes.Plant);
        }
        #endregion
    }
}