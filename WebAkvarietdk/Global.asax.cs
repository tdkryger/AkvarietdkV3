﻿using System.Web.Http;

namespace WebAkvarietdk
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            Caching.SimpeSearchCaching.LoadCacheData();
        }
    }
}
