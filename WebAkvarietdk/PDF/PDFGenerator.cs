﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NLog;
using QRCoder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using WebAkvarietdk.Tools;

namespace WebAkvarietdk.PDF
{
    public class PDFGenerator
    {
        #region Fields
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string _baseHtml = string.Empty;
        private string _contentHtml = string.Empty;
        private string _tempPath = string.Empty;
        private const string _copyright = "© 2017 Akvariet.dk / Thomas D. Kryger";
        private float _defaultSpacingBefore = 10;
        private float _defaultSpacingAfter = 10;
        private static float _fontSize = 10;
        private Font _normalFont = FontFactory.GetFont(FontFactory.HELVETICA, _fontSize);
        private Font _boldFont = FontFactory.GetFont(FontFactory.HELVETICA, _fontSize, Font.BOLD);
        private int _tableBorder = Rectangle.NO_BORDER;
        #endregion

        #region Constructor
        public PDFGenerator(string contentHtml) : base()
        {
            _contentHtml = contentHtml;
            string filePath = HttpContext.Current.Server.MapPath("/Resources/Pages/printPage.html");
            _baseHtml = File.ReadAllText(filePath);
            _baseHtml = _baseHtml.Replace("[CONTENT]", _contentHtml);

        }

        public PDFGenerator()
        {
            try
            {
                _tempPath = HttpContext.Current.Server.MapPath("~/temp");
            }
            catch
            {
                _tempPath = Path.GetTempPath();
            }
            if (!Directory.Exists(_tempPath))
            {
                Directory.CreateDirectory(_tempPath);
            }
        }
        #endregion

        #region Public methods
        public byte[] CreatePDF(string title)
        {
            return CreatePDFFromHTML(title);
        }

        public byte[] CreatePlantPDF(int plantId)
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Plant> repo = new Repository<Plant>(uow))
                {
                    return CreatePlantPDF(repo.GetById(plantId));
                }
            }
        }

        public byte[] CreatePlantPDF(Plant item)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(
                $"http://test.akvariet.dk/#/plant/{item.Id}",
                QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            if (item == null)
            {
                logger.Warn($"Error creating pdf because item is null");
                return null;
            }
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDocument = new Document(PageSize.A4, 10, 10, 10, 10);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, memoryStream);
                    HeaderFooter headers = new HeaderFooter(item.Id, qrCode);
                    pdfWriter.PageEvent = headers;
                    DefineFont passFont = new DefineFont();
                    // always set this to the largest font used
                    headers.FontPointSize = 8;

                    // set up the highlight or bold font
                    passFont.FontFamily = "Helvetica";
                    passFont.FontSize = 8;
                    passFont.IsBold = true;
                    passFont.IsItalic = false;
                    passFont.IsUnderlined = false;
                    passFont.ForeColor = BaseColor.BLACK;

                    headers.BoldFont = passFont;

                    // now setup the normal text font
                    passFont.FontSize = 7;
                    passFont.IsBold = false;

                    headers.NormalFont = passFont;

                    headers.LeftMargin = 10;
                    headers.BottomMargin = 25;
                    headers.RightMargin = 10;
                    headers.TopMargin = 25;

                    headers.PageNumberSettings = HeaderFooter.PageNumbers.None;

                    headers.FooterLine = _copyright;
                    headers.HasHeader = false;
                    headers.PrintDate = true;
                    headers.HeaderLines = new string[] { item.ScientificNameText };


                    pdfDocument.SetMargins(headers.LeftMargin, headers.RightMargin, headers.TopMargin + headers.Headerheight, headers.BottomMargin + headers.FooterHeight);
                    pdfDocument.Open();

                    pdfDocument.NewPage();

                    pdfDocument.AddAuthor(_copyright);
                    pdfDocument.AddCreator("Akvariet.dk");
                    pdfDocument.AddTitle(item.ScientificNameText);
                    pdfDocument.AddCreationDate();

                    float bottomY = pdfDocument.Bottom;
                    float width = pdfDocument.PageSize.Width;
                    float textX = width / 2;


                    PdfPTable topTable = new PdfPTable(1)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                    };
                    topTable.DefaultCell.Border = _tableBorder;

                    PdfPCell spacer = new PdfPCell()
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER
                    };
                    spacer.AddElement(new Phrase(Chunk.NEWLINE));

                    PdfPCell spacer4 = new PdfPCell()
                    {
                        Colspan = 4,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER

                    };
                    spacer4.AddElement(new Phrase(Chunk.NEWLINE));

                    PdfPCell cellSciName = new PdfPCell(GetPhrase(item.ScientificNameText, FontFactory.GetFont(FontFactory.HELVETICA, 24, Font.BOLD)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER
                    };
                    //cellSciName.AddElement();
                    topTable.AddCell(cellSciName);
                    topTable.AddCell(spacer);

                    if (item.Contributor == null)
                        topTable.AddCell(GetPhrase("Skrevet af: Akvariet.dk", _normalFont));
                    else
                        topTable.AddCell(GetPhrase($"Skrevet af: {item.Contributor.DisplayName}", _normalFont));

                    string tradenames = string.Empty;
                    foreach (Tradename tName in item.Tradenames)
                        tradenames = tradenames + tName.ToString() + ", ";
                    if (tradenames.Length > 0)
                    {
                        tradenames = tradenames.Substring(0, tradenames.Length - 2);
                        topTable.AddCell(GetPhrase(tradenames, _normalFont));
                    }

                    pdfDocument.Add(topTable);
                    pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    if (item.Images.Count > 0)
                    {
                        pdfDocument.Add(ImageCell(item.Images));
                        pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                        pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    }
                    PdfPTable table = new PdfPTable(4)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                    };
                    table.DefaultCell.Border = _tableBorder;

                    table.AddCell(GetPhrase("Gruppe: ", _boldFont));
                    PdfPCell cell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = _tableBorder,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        UseAscender = true
                    };
                    cell.AddElement(GetPhrase(item.PlantGroup.ToString(), _normalFont));
                    table.AddCell(cell);

                    table.AddCell(GetPhrase("Størrelse :", _boldFont));
                    cell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = _tableBorder,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        UseAscender = true
                    };
                    cell.AddElement(GetPhrase("H: " + item.Height.ToString() + ", B: " + item.Width.ToString(), _normalFont));
                    table.AddCell(cell);

                    table.AddCell(GetPhrase("Levested:", _boldFont));
                    cell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = _tableBorder,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        UseAscender = true
                    };
                    cell.AddElement(GetPhrase(item.Origin.ToString(), _normalFont));
                    table.AddCell(cell);

                    table.AddCell(GetPhrase("Placering:", _boldFont));
                    cell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = _tableBorder,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        UseAscender = true
                    };
                    cell.AddElement(GetPhrase(EnumWords.PlantTankPositionsToString(item.TankPosition), _normalFont));
                    table.AddCell(cell);

                    string characteristics = string.Empty;
                    foreach (PlantCharacteristic tName in item.Characteristics)
                        characteristics = characteristics + tName.ToString() + ", ";
                    if (characteristics.Length > 0)
                    {
                        characteristics = characteristics.Substring(0, characteristics.Length - 2);
                        topTable.AddCell(GetPhrase(characteristics, _normalFont));
                    }
                    if (!string.IsNullOrEmpty(characteristics))
                    {
                        table.AddCell(GetPhrase("Egenskaber:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true
                        };
                        cell.AddElement(GetPhrase(characteristics, _normalFont));
                        table.AddCell(cell);
                    }

                    string specialString = string.Empty;
                    foreach (PlantCare plantCare in item.Care)
                        specialString = specialString + plantCare.ToString() + ", ";
                    if (specialString.Length > 2)
                        specialString = specialString.Substring(0, specialString.Length - 2);
                    specialString = specialString.Trim();
                    if (!string.IsNullOrEmpty(specialString))
                    {
                        table.AddCell(GetPhrase("Specielt:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true
                        };
                        cell.AddElement(GetPhrase(characteristics, _normalFont));
                        table.AddCell(cell);
                    }

                    table.AddCell(GetPhrase("Lys:", _boldFont));
                    table.AddCell(GetPhrase(item.LightRequirements.ToString(), _normalFont));
                    table.AddCell(GetPhrase("CO2:", _boldFont));
                    table.AddCell(GetPhrase(EnumWords.PlantCO2RequirementsToString(item.CO2Requirements), _normalFont));

                    table.AddCell(GetPhrase("Temperatur:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterTemperature.ToString(), _normalFont));
                    table.AddCell(GetPhrase("pH:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterPh.ToString(), _normalFont));

                    table.AddCell(GetPhrase("Hårdhed:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterHardness.ToString(), _normalFont));
                    table.AddCell(GetPhrase("Vækst:", _boldFont));
                    table.AddCell(GetPhrase(EnumWords.PlantGrowthSpeedToString(item.GrowthSpeed), _normalFont));

                    table.AddCell(spacer4);

                    if (!string.IsNullOrEmpty(item.Description))
                    {

                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_TOP,
                            Border = _tableBorder,
                            Colspan = 4,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        cell.AddElement(GetPhrase(item.Description, _normalFont));
                        table.AddCell(cell);
                    }

                    pdfDocument.Add(table);

                    pdfDocument.Close();
                    byte[] pdfBytes = memoryStream.ToArray();
                    memoryStream.Close();
#if (DEBUG)
                    //try
                    //{
                    //    string fileName = Path.Combine(@"D:\tempFiles", $"{ item.ScientificNameText}.pdf");
                    //    if (File.Exists(fileName))
                    //        File.Delete(fileName);
                    //    File.WriteAllBytes(fileName, pdfBytes);
                    //}
                    //catch { }
#endif
                    return pdfBytes;
                }
            }
            catch (Exception ex)
            {
                if (item != null)
                    logger.Warn(ex, $"Some error creating pdf for {item.ScientificNameText}");
                else
                    logger.Warn(ex, $"Some error creating pdf, and item is null");
                return null;
            }
        }

        public byte[] CreateAnimalPDF(int animalId)
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    return CreateAnimalPDF(repo.GetById(animalId));
                }
            }
        }

        public byte[] CreateAnimalPDF(Animal item)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(
                $"http://test.akvariet.dk/#/animal/{item.Id}",
                QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            if (item == null)
            {
                logger.Warn($"Error creating pdf because item is null");
                return null;
            }
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDocument = new Document(PageSize.A4, 10, 10, 10, 10);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, memoryStream);
                    HeaderFooter headers = new HeaderFooter(item.Id, qrCode);
                    pdfWriter.PageEvent = headers;
                    DefineFont passFont = new DefineFont();
                    // always set this to the largest font used
                    headers.FontPointSize = 8;

                    // set up the highlight or bold font
                    passFont.FontFamily = "Helvetica";
                    passFont.FontSize = 8;
                    passFont.IsBold = true;
                    passFont.IsItalic = false;
                    passFont.IsUnderlined = false;
                    passFont.ForeColor = BaseColor.BLACK;

                    headers.BoldFont = passFont;

                    // now setup the normal text font
                    passFont.FontSize = 7;
                    passFont.IsBold = false;

                    headers.NormalFont = passFont;

                    headers.LeftMargin = 10;
                    headers.BottomMargin = 25;
                    headers.RightMargin = 10;
                    headers.TopMargin = 25;

                    headers.PageNumberSettings = HeaderFooter.PageNumbers.None;

                    headers.FooterLine = _copyright;
                    headers.HasHeader = false;
                    headers.PrintDate = true;
                    headers.HeaderLines = new string[] { item.ScientificNameText };


                    pdfDocument.SetMargins(headers.LeftMargin, headers.RightMargin, headers.TopMargin + headers.Headerheight, headers.BottomMargin + headers.FooterHeight);
                    pdfDocument.Open();

                    pdfDocument.NewPage();

                    pdfDocument.AddAuthor(_copyright);
                    pdfDocument.AddCreator("Akvariet.dk");
                    pdfDocument.AddTitle(item.ScientificNameText);
                    pdfDocument.AddCreationDate();

                    float bottomY = pdfDocument.Bottom;
                    float width = pdfDocument.PageSize.Width;
                    float textX = width / 2;


                    PdfPTable topTable = new PdfPTable(1)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                    };
                    topTable.DefaultCell.Border = _tableBorder;

                    PdfPCell spacer = new PdfPCell()
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER
                    };
                    spacer.AddElement(new Phrase(Chunk.NEWLINE));

                    PdfPCell spacer4 = new PdfPCell()
                    {
                        Colspan = 4,
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER

                    };
                    spacer4.AddElement(new Phrase(Chunk.NEWLINE));

                    PdfPCell cellSciName = new PdfPCell(GetPhrase(item.ScientificNameText, FontFactory.GetFont(FontFactory.HELVETICA, 24, Font.BOLD)))
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = Rectangle.NO_BORDER
                    };
                    //cellSciName.AddElement();
                    topTable.AddCell(cellSciName);
                    topTable.AddCell(spacer);

                    if (item.Contributor == null)
                        topTable.AddCell(GetPhrase("Skrevet af: Akvariet.dk", _normalFont));
                    else
                        topTable.AddCell(GetPhrase($"Skrevet af: {item.Contributor.DisplayName}", _normalFont));

                    string tradenames = string.Empty;
                    foreach (Tradename tName in item.Tradenames)
                        tradenames = tradenames + tName.ToString() + ", ";
                    if (tradenames.Length > 0)
                    {
                        tradenames = tradenames.Substring(0, tradenames.Length - 2);
                        topTable.AddCell(GetPhrase(tradenames, _normalFont));
                    }

                    pdfDocument.Add(topTable);
                    pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    if (item.Images.Count > 0)
                    {
                        pdfDocument.Add(ImageCell(item.Images));
                        pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                        pdfDocument.Add(new Paragraph(Chunk.NEWLINE));
                    }
                    PdfPTable table = new PdfPTable(4)
                    {
                        HorizontalAlignment = Element.ALIGN_CENTER,
                    };
                    table.DefaultCell.Border = _tableBorder;

                    table.AddCell(GetPhrase("Orden: ", _boldFont));
                    table.AddCell(GetPhrase(item.Order.ToString(), _normalFont));
                    table.AddCell(GetPhrase("Familie: ", _boldFont));
                    table.AddCell(GetPhrase(item.Family.ToString(), _normalFont));

                    table.AddCell(GetPhrase("Gruppe: ", _boldFont));
                    table.AddCell(GetPhrase(item.CommonGroup.ToString(), _normalFont));
                    table.AddCell(GetPhrase("Størrelse:", _boldFont));
                    table.AddCell(GetPhrase(item.Size.ToString(), _normalFont));

                    table.AddCell(GetPhrase("Levested:", _boldFont));
                    PdfPCell cell = new PdfPCell()
                    {
                        VerticalAlignment = Element.ALIGN_MIDDLE,
                        Border = _tableBorder,
                        Colspan = 3,
                        HorizontalAlignment = Element.ALIGN_LEFT,
                        UseAscender = true
                    };
                    cell.AddElement(GetPhrase(item.Origin.ToString(), _normalFont));
                    table.AddCell(cell);

                    table.AddCell(GetPhrase("Niveau: ", _boldFont));
                    table.AddCell(GetPhrase(EnumWords.AnimalSwimLevelToString(item.SwimLevel), _normalFont));
                    table.AddCell(GetPhrase("Bio belastning: ", _boldFont));
                    table.AddCell(GetPhrase(EnumWords.AnimalBioloadToString(item.Bioload), _normalFont));

                    table.AddCell(GetPhrase("Temperatur:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterTemperature.ToString(), _normalFont));
                    table.AddCell(GetPhrase("pH:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterPh.ToString(), _normalFont));

                    table.AddCell(GetPhrase("Hårdhed:", _boldFont));
                    table.AddCell(GetPhrase(item.WaterHardness.ToString(), _normalFont));
                    table.AddCell(GetPhrase("Min. liter:", _boldFont));
                    table.AddCell(GetPhrase(item.MinTankSize.ToString("#0.#"), _normalFont));

                    table.AddCell(GetPhrase("Min. antal:", _boldFont));
                    table.AddCell(GetPhrase(item.MingroupSize.ToString(), _normalFont));
                    table.AddCell(GetPhrase("Planteakvarie:", _boldFont));
                    table.AddCell(GetPhrase(EnumWords.AnimalPlantedTankToString(item.PlantedTank), _normalFont));

                    if (item.DietaryRequirements.Count > 0)
                    {
                        table.AddCell(GetPhrase("Foder:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        string foodText = string.Empty;
                        foreach (Diet diet in item.DietaryRequirements)
                            foodText = foodText + diet.Name + ", ";
                        if (foodText.Length > 0)
                            foodText = foodText.Substring(0, foodText.Length - 2);
                        cell.AddElement(GetPhrase(foodText, _normalFont));
                        table.AddCell(cell);
                    }

                    if (item.CommonIllnesses.Count > 0)
                    {
                        table.AddCell(GetPhrase("Sygdomme:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        string foodText = string.Empty;
                        foreach (Illness diet in item.CommonIllnesses)
                            foodText = foodText + diet.Name + ", ";
                        if (foodText.Length > 0)
                            foodText = foodText.Substring(0, foodText.Length - 2);
                        cell.AddElement(GetPhrase(foodText, _normalFont));
                        table.AddCell(cell);
                    }

                    if (item.Temperament != null)
                    {
                        table.AddCell(GetPhrase("Temperament:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        cell.AddElement(GetPhrase(item.Temperament.ToString(), _normalFont));
                        table.AddCell(cell);
                    }

                    if (item.Sociableness.Count > 0)
                    {
                        table.AddCell(GetPhrase("Social:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        string foodText = string.Empty;
                        foreach (Social diet in item.Sociableness)
                            foodText = foodText + diet.ToString() + ", ";
                        if (foodText.Length > 0)
                            foodText = foodText.Substring(0, foodText.Length - 2);
                        cell.AddElement(GetPhrase(foodText, _normalFont));
                        table.AddCell(cell);
                    }

                    if (item.PreferedHabitat.Count > 0)
                    {
                        table.AddCell(GetPhrase("Biotop:", _boldFont));
                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_MIDDLE,
                            Border = _tableBorder,
                            Colspan = 3,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        string foodText = string.Empty;
                        foreach (Habitat diet in item.PreferedHabitat)
                            foodText = foodText + diet.ToString() + ", ";
                        if (foodText.Length > 0)
                            foodText = foodText.Substring(0, foodText.Length - 2);
                        cell.AddElement(GetPhrase(foodText, _normalFont));
                        table.AddCell(cell);
                    }
                    table.AddCell(spacer4);

                    if (!string.IsNullOrEmpty(item.Description))
                    {

                        cell = new PdfPCell()
                        {
                            VerticalAlignment = Element.ALIGN_TOP,
                            Border = _tableBorder,
                            Colspan = 4,
                            HorizontalAlignment = Element.ALIGN_LEFT,
                            UseAscender = true

                        };
                        cell.AddElement(GetPhrase(item.Description, _normalFont));
                        table.AddCell(cell);
                    }

                    pdfDocument.Add(table);

                    pdfDocument.Close();
                    byte[] pdfBytes = memoryStream.ToArray();
                    memoryStream.Close();
#if (DEBUG)
                    //try
                    //{
                    //    string fileName = Path.Combine(@"D:\tempFiles", $"{ item.ScientificNameText}.pdf");
                    //    if (File.Exists(fileName))
                    //        File.Delete(fileName);
                    //    File.WriteAllBytes(fileName, pdfBytes);
                    //}
                    //catch { }
#endif
                    return pdfBytes;
                }
            }
            catch (Exception ex)
            {
                if (item != null)
                    logger.Warn(ex, $"Some error creating pdf for {item.ScientificNameText}");
                else
                    logger.Warn(ex, $"Some error creating pdf, and item is null");
                return null;
            }
        }

        public byte[] CreateHabitatPDF(int habitatId)
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Habitat> repo = new Repository<Habitat>(uow))
                {
                    return CreateHabitatPDF(repo.GetById(habitatId));
                }
            }
        }

        public byte[] CreateHabitatPDF(Habitat item)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(
                $"http://test.akvariet.dk//#/biotoper",
                QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            if (item == null)
            {
                logger.Warn($"Error creating pdf because item is null");
                return null;
            }
            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    Document pdfDocument = new Document(PageSize.A4, 10, 10, 25, 10);
                    PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, memoryStream);
                    HeaderFooter headers = new HeaderFooter(item.Id, qrCode);
                    pdfWriter.PageEvent = headers;
                    DefineFont passFont = new DefineFont();
                    // always set this to the largest font used
                    headers.FontPointSize = 8;

                    // set up the highlight or bold font
                    passFont.FontFamily = "Helvetica";
                    passFont.FontSize = 8;
                    passFont.IsBold = true;
                    passFont.IsItalic = false;
                    passFont.IsUnderlined = false;
                    passFont.ForeColor = BaseColor.BLACK;

                    headers.BoldFont = passFont;

                    // now setup the normal text font
                    passFont.FontSize = 7;
                    passFont.IsBold = false;

                    headers.NormalFont = passFont;

                    headers.LeftMargin = 10;
                    headers.BottomMargin = 25;
                    headers.RightMargin = 10;
                    headers.TopMargin = 10;

                    headers.PageNumberSettings = HeaderFooter.PageNumbers.None;

                    headers.FooterLine = _copyright;
                    headers.HasHeader = false;
                    headers.PrintDate = true;
                    headers.HeaderLines = new string[] { item.Text };


                    pdfDocument.SetMargins(headers.LeftMargin, headers.RightMargin, headers.TopMargin + headers.Headerheight, headers.BottomMargin + headers.FooterHeight);
                    pdfDocument.Open();

                    pdfDocument.NewPage();

                    pdfDocument.AddAuthor(_copyright);
                    pdfDocument.AddCreator("Akvariet.dk");
                    pdfDocument.AddTitle(item.Text);
                    pdfDocument.AddCreationDate();

                    float bottomY = pdfDocument.Bottom;
                    float width = pdfDocument.PageSize.Width;
                    float textX = width / 2;

                    pdfDocument.Add(new Phrase(Chunk.NEWLINE));
                    pdfDocument.Add(new Phrase(Chunk.NEWLINE));
                    pdfDocument.Add(GetPhrase(item.Text, FontFactory.GetFont(FontFactory.HELVETICA, 24, Font.BOLD)));
                    pdfDocument.Add(new Phrase(Chunk.NEWLINE));

                    string[] descParts = item.Description.Split(new[] { "<br />" }, StringSplitOptions.None); 
                    foreach(string part in descParts)
                    {
                        Paragraph para = GetDefaultParagraph();
                        para.Add(part);
                        para.Trim();
                        pdfDocument.Add(para);
                    }

                    pdfDocument.Close();
                    byte[] pdfBytes = memoryStream.ToArray();
                    memoryStream.Close();

                    return pdfBytes;
                }
            }
            catch (Exception ex)
            {
                if (item != null)
                    logger.Warn(ex, $"Some error creating pdf for {item.Text}");
                else
                    logger.Warn(ex, $"Some error creating pdf, and item is null");
                return null;
            }
        }
        #endregion

        #region private methods
        private PdfPTable ImageCell(IList<FishbaseImage> fbImages)
        {
            int columns = 4;
            if (fbImages.Count < 4)
            {
                columns = fbImages.Count;
            }

            PdfPTable table = new PdfPTable(columns)
            {
                HorizontalAlignment = Element.ALIGN_CENTER,
            };
            table.DefaultCell.Border = _tableBorder;

            foreach (FishbaseImage fbi in fbImages)
            {
                System.Drawing.Image orgImg = TDK.Tools.ImageTools.AddTextBlock(fbi.Image, System.Drawing.Color.White, fbi.CopyRight, false);
                Image img = Image.GetInstance(TDK.Tools.ImageTools.ImageToByteArray(orgImg));
                img.ScalePercent(24f);
                PdfPCell cell = new PdfPCell(img)
                {
                    HorizontalAlignment = Element.ALIGN_CENTER,
                    VerticalAlignment = Element.ALIGN_MIDDLE,
                    Border = Rectangle.NO_BORDER
                };
                table.AddCell(cell);
            }
            return table;
        }

        private Phrase GetPhrase(string text, Font font)
        {
            return new Phrase() { new Chunk(text, font) };
        }

        private Paragraph GetDefaultParagraph()
        {
            return new Paragraph()
            {
                SpacingBefore = _defaultSpacingBefore,
                SpacingAfter = _defaultSpacingAfter,
                Alignment = Element.ALIGN_LEFT,
                Font = FontFactory.GetFont(FontFactory.HELVETICA, 12f, BaseColor.BLACK)
            };
        }

        private byte[] CreatePDFFromHTML(string title)
        {
            try
            {
                if (string.IsNullOrEmpty(title))
                    title = TDK.Tools.StringTools.RandomString(20);

                string filename = Path.Combine(_tempPath, $"{ title}.pdf");
                if (File.Exists(filename))
                    File.Delete(filename);

                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                byte[] pdfBytes = htmlToPdf.GeneratePdf(_contentHtml);

                File.WriteAllBytes(@"D:\tempFiles\test.pdf", pdfBytes);

                File.WriteAllBytes(filename, pdfBytes);
                return pdfBytes;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Could not generate PDF");
                return null;
            }
        }
        #endregion

    }
}