﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using System;

namespace WebAkvarietdk.PDF
{
    public struct DefineFont
    {
        public string FontFamily { get; set; }
        public int FontSize { get; set; }
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public bool IsUnderlined { get; set; }
        public BaseColor ForeColor { get; set; }
    }

    public class HeaderFooter : PdfPageEventHelper
    {
        #region Startup_Stuff

        private string[] _headerLines;
        private string _footerLine;

        private DefineFont _boldFont;
        private DefineFont _normalFont;

        private iTextSharp.text.Font fontTxtBold;
        private iTextSharp.text.Font fontTxtRegular;

        private int _fontPointSize = 0;

        private bool _hasFooter = false;
        private bool _hasHeader = false;

        private int _headerWidth = 0;
        private int _headerHeight = 0;

        private int _footerWidth = 0;
        private int _footerHeight = 0;

        private int _leftMargin = 0;
        private int _rightMargin = 0;
        private int _topMargin = 0;
        private int _bottomMargin = 0;

        private PageNumbers _numberSettings;

        private DateTime runTime = DateTime.Now;

        private Image _qrCodeImage;

        public enum PageNumbers
        {
            None,
            HeaderPlacement,
            FooterPlacement
        }

        // This is the contentbyte object of the writer
        PdfContentByte cb;

        PdfTemplate headerTemplate;
        PdfTemplate footerTemplate;

        public string[] HeaderLines { get { return _headerLines; } set { _headerLines = value; _hasHeader = true; } }
        public string FooterLine { get { return _footerLine; } set { _footerLine = value; _hasFooter = true; } }
        public DefineFont BoldFont { get { return _boldFont; } set { _boldFont = value; } }
        public DefineFont NormalFont { get { return _normalFont; } set { _normalFont = value; } }
        public int FontPointSize { get { return _fontPointSize; } set { _fontPointSize = value; } }
        public int LeftMargin { get { return _leftMargin; } set { _leftMargin = value; } }
        public int RightMargin { get { return _rightMargin; } set { _rightMargin = value; } }
        public int TopMargin { get { return _topMargin; } set { _topMargin = value; } }
        public int BottomMargin { get { return _bottomMargin; } set { _bottomMargin = value; } }
        public int Headerheight { get { return _headerHeight; } }
        public int FooterHeight { get { return _footerHeight; } }
        public PageNumbers PageNumberSettings { get { return _numberSettings; } set { _numberSettings = value; } }
        public bool HasHeader { get { return _hasHeader; } set { _hasHeader = value; } }
        public bool HasFooter { get { return _hasFooter; } set { _hasFooter = value; } }
        public bool PrintDate { get; set; } = false;
        public bool PrintTime { get; set; } = false;
        public bool PrintQRCode { get; set; } = true;
        #endregion

        #region Constructors
        public HeaderFooter(int itemId, QRCode qrCode)
        {
            System.Drawing.Bitmap qrCodeImage = qrCode.GetGraphic(5);
            _qrCodeImage = Image.GetInstance(TDK.Tools.ImageTools.ImageToByteArray(qrCodeImage));
            _qrCodeImage.ScalePercent(24f);
        }
        #endregion

        #region Write_Headers_Footers

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            if (_hasHeader)
            {
                float[] widths = new float[2] { 90f, 10f };

                PdfPTable hdrTable = new PdfPTable(2)
                {
                    TotalWidth = document.PageSize.Width - (_leftMargin + _rightMargin),
                    WidthPercentage = 95,
                    
                };
                hdrTable.SetWidths(widths);
                hdrTable.LockedWidth = true;

                PdfPCell leftCell = new PdfPCell(_qrCodeImage)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0
                };
                string rightLine = DateTime.Now.ToString("dd/MM yyyy");
                Paragraph rightPara = new Paragraph(5, rightLine, fontTxtBold);
                rightPara.Font.Size = _fontPointSize;
                PdfPCell rightCell = new PdfPCell(rightPara)
                {
                    HorizontalAlignment = Element.ALIGN_LEFT,
                    Border = 0
                };
                hdrTable.AddCell(leftCell);
                hdrTable.AddCell(rightCell);

                hdrTable.WriteSelectedRows(0, -1, _leftMargin, document.PageSize.Height - _topMargin, writer.DirectContent);

                cb.Stroke();
            }

            if (_hasFooter)
            {
                // footer line is the width of the page so it is centered horizontally 
                PdfPTable ftrTable = new PdfPTable(1);
                float[] widths = new float[1] { 100 };

                ftrTable.TotalWidth = document.PageSize.Width - 10;
                ftrTable.WidthPercentage = 95;
                ftrTable.SetWidths(widths);

                string OneLine;

                if (_numberSettings == PageNumbers.FooterPlacement)
                {
                    OneLine = string.Concat(_footerLine, writer.PageNumber.ToString());
                }
                else
                {
                    OneLine = _footerLine;
                }

                Paragraph onePara = new Paragraph(0, OneLine, fontTxtRegular);
                onePara.Font.Size = _fontPointSize;

                PdfPCell oneCell = new PdfPCell(onePara);
                oneCell.HorizontalAlignment = Element.ALIGN_CENTER;
                oneCell.Border = 0;
                ftrTable.AddCell(oneCell);

                ftrTable.WriteSelectedRows(0, -1, _leftMargin, (_footerHeight), writer.DirectContent);

                //Move the pointer and draw line to separate footer section from rest of page
                cb.MoveTo(_leftMargin, document.PageSize.GetBottom(_footerHeight + 2));
                cb.LineTo(document.PageSize.Width - _leftMargin, document.PageSize.GetBottom(_footerHeight + 2));
                cb.Stroke();
            }
        }

        #endregion


        #region Setup_Headers_Footers_Happens_here

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            // create the fonts that are to be used
            // first the hightlight or Bold font
            fontTxtBold = FontFactory.GetFont(_boldFont.FontFamily, _boldFont.FontSize, _boldFont.ForeColor);
            if (_boldFont.IsBold)
            {
                fontTxtBold.SetStyle(Font.BOLD);
            }
            if (_boldFont.IsItalic)
            {
                fontTxtBold.SetStyle(Font.ITALIC);
            }
            if (_boldFont.IsUnderlined)
            {
                fontTxtBold.SetStyle(Font.UNDERLINE);
            }

            // next the normal font
            fontTxtRegular = FontFactory.GetFont(_normalFont.FontFamily, _normalFont.FontSize, _normalFont.ForeColor);
            if (_normalFont.IsBold)
            {
                fontTxtRegular.SetStyle(Font.BOLD);
            }
            if (_normalFont.IsItalic)
            {
                fontTxtRegular.SetStyle(Font.ITALIC);
            }
            if (_normalFont.IsUnderlined)
            {
                fontTxtRegular.SetStyle(Font.UNDERLINE);
            }

            // now build the header and footer templates
            try
            {
                float pageHeight = document.PageSize.Height;
                float pageWidth = document.PageSize.Width;

                _headerWidth = (int)pageWidth - ((int)_rightMargin + (int)_leftMargin);
                _footerWidth = _headerWidth;

                if (_hasHeader)
                {
                    // i basically dummy build the headers so i can trial fit them and see how much space they take.
                    float[] widths = new float[1] { 90f };

                    PdfPTable hdrTable = new PdfPTable(1);
                    hdrTable.TotalWidth = document.PageSize.Width - (_leftMargin + _rightMargin);
                    hdrTable.WidthPercentage = 95;
                    hdrTable.SetWidths(widths);
                    hdrTable.LockedWidth = true;

                    _headerHeight = 0;

                    for (int hdrIdx = 0; hdrIdx < (_headerLines.Length < 2 ? 2 : _headerLines.Length); hdrIdx++)
                    {
                        Paragraph hdrPara = new Paragraph(5, hdrIdx > _headerLines.Length - 1 ? string.Empty : _headerLines[hdrIdx], (hdrIdx > 0 ? fontTxtRegular : fontTxtBold));
                        PdfPCell hdrCell = new PdfPCell(hdrPara);
                        hdrCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        hdrCell.Border = 0;
                        hdrTable.AddCell(hdrCell);
                        _headerHeight = _headerHeight + (int)hdrTable.GetRowHeight(hdrIdx);
                    }

                    // iTextSharp underestimates the size of each line so fudge it a little 
                    // this gives me 3 extra lines to play with on the spacing
                    _headerHeight = _headerHeight + (_fontPointSize * 3);

                }

                if (_hasFooter)
                {
                    _footerHeight = (_fontPointSize * 2);
                }

                document.SetMargins(_leftMargin, _rightMargin, (_topMargin + _headerHeight), _footerHeight);

                cb = writer.DirectContent;

                if (_hasHeader)
                {
                    headerTemplate = cb.CreateTemplate(_headerWidth, _headerHeight);
                }

                if (_hasFooter)
                {
                    footerTemplate = cb.CreateTemplate(_footerWidth, _footerHeight);
                }
            }
            catch (DocumentException de)
            {

            }
            catch (System.IO.IOException ioe)
            {

            }
        }

        #endregion

        #region Cleanup_Doc_Processing

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            if (_hasHeader)
            {
                headerTemplate.BeginText();
                headerTemplate.SetTextMatrix(0, 0);

                if (_numberSettings == PageNumbers.HeaderPlacement)
                {
                }

                headerTemplate.EndText();
            }

            if (_hasFooter)
            {
                footerTemplate.BeginText();
                footerTemplate.SetTextMatrix(0, 0);

                if (_numberSettings == PageNumbers.FooterPlacement)
                {
                }

                footerTemplate.EndText();
            }
        }

        #endregion

    }
}