﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Tools
{
    public class EMailResult
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }

    public static class EMailSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static SmtpClient GetSmtpClient()
        {
            if (Akvariet.Common.AkvarietConfiguration.SMTPPassword.Contains("?"))
            {
                logger.Fatal("Remember to set the SMTP Password in Web.config!!");
                throw new ArgumentException("Remember to set the SMTP Password in Web.config!!");
            }

            return new SmtpClient(Akvariet.Common.AkvarietConfiguration.SMTPServer, Akvariet.Common.AkvarietConfiguration.SMTPPort)
            {
                Credentials = new NetworkCredential(Akvariet.Common.AkvarietConfiguration.SMTPUser, Akvariet.Common.AkvarietConfiguration.SMTPPassword),
                EnableSsl = true
            };
        }

        private static EMailResult SendAMessage(MailMessage message)
        {
            try
            {
                using (SmtpClient smtpClient = GetSmtpClient())
                {
                    smtpClient.Send(message);
                }
                return new EMailResult() { Result = true, Message = "E-Mail sent" };

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Could not send the e-mail.");
                return new EMailResult() { Result = false, Message = "Could not send the E-Mail." };
            }
        }

        public static EMailResult SendNewsletter(string subject, string htmlBody, List<string> recievers)
        {

            MailMessage mailMessage = new MailMessage()
            {
                From = new MailAddress(Akvariet.Common.AkvarietConfiguration.ContactEMail, Akvariet.Common.AkvarietConfiguration.EMailSenderName),
                Subject = subject,
                IsBodyHtml = true,
                Body = htmlBody
            };
            foreach (string email in recievers)
                mailMessage.Bcc.Add(email);

            return SendAMessage(mailMessage);

        }

        public static EMailResult SendEmail(string subject, string body)
        {

            MailMessage mailMessage = new MailMessage()
            {
                From = new MailAddress(Akvariet.Common.AkvarietConfiguration.ContactEMail, Akvariet.Common.AkvarietConfiguration.EMailSenderName),
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };
            mailMessage.To.Add(new MailAddress(Akvariet.Common.AkvarietConfiguration.ContactEMail, Akvariet.Common.AkvarietConfiguration.EMailSenderName));
            return SendAMessage(mailMessage);
        }

        public static EMailResult SendEmail(string subject, string body, IList<DTODBFile> files)
        {

            MailMessage mailMessage = new MailMessage()
            {
                From = new MailAddress(Akvariet.Common.AkvarietConfiguration.ContactEMail, Akvariet.Common.AkvarietConfiguration.EMailSenderName),
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };
            mailMessage.To.Add(new MailAddress(Akvariet.Common.AkvarietConfiguration.ContactEMail, Akvariet.Common.AkvarietConfiguration.EMailSenderName));
            foreach (DTODBFile file in files)
            {
                Stream stream = new MemoryStream(file.FileContent);
                Attachment att = new Attachment(stream, file.Filename, file.ContentType);
                mailMessage.Attachments.Add(att);
            }
            return SendAMessage(mailMessage);
        }
    }
}