﻿using static Akvariet.Fishbase.Model.Animal;
using static Akvariet.Fishbase.Model.Plant;

namespace WebAkvarietdk.Tools
{
    public static class EnumWords
    {
        public static string AnimalBioloadToString(AnimalBioload value)
        {
            switch (value)
            {
                case AnimalBioload.Heavy:
                    return "Kraftigt";
                case AnimalBioload.Low:
                    return "Lavt";
                case AnimalBioload.Medium:
                    return "Middel";
                default:
                    return "Ukendt";
            }
        }

        public static string AnimalSwimLevelToString(AnimalSwimLevel value)
        {
            switch (value)
            {
                case AnimalSwimLevel.All:
                    return "Alle vandlag";
                case AnimalSwimLevel.Bottom:
                    return "Bundlevende";
                case AnimalSwimLevel.Lower:
                    return "Nedre vandlag";
                case AnimalSwimLevel.LowerMiddle:
                    return "Nederste halvdel";
                case AnimalSwimLevel.Middle:
                    return "Midterste vandlag";
                case AnimalSwimLevel.MiddleUpper:
                    return "Lige over miderste vandlag";
                case AnimalSwimLevel.Top:
                    return "Overfladen";
                case AnimalSwimLevel.Upper:
                    return "Øverste vandlag";
                default:
                    return "Ukendt";
            }
        }

        public static string AnimalPlantedTankToString(AnimalPlantedTank value)
        {
            switch (value)
            {
                case AnimalPlantedTank.No:
                    return "Nej";
                case AnimalPlantedTank.Partial:
                    return "Kræver frit svømmeareal";
                case AnimalPlantedTank.Yes:
                    return "Ja";
                default:
                    return "Ukendt";
            }
        }

        public static string AnimalFiltrationToString(AnimalFiltration value)
        {
            switch (value)
            {
                case AnimalFiltration.Heavy:
                    return "Kraftig";
                case AnimalFiltration.Low:
                    return "Lavt";
                case AnimalFiltration.Medium:
                    return "Middel";
                case AnimalFiltration.None:
                    return "Ingen";
                default:
                    return "Ukendt";
            }
        }
        
        public static string PlantTankPositionsToString(PlantTankPositions value)
        {
            switch(value)
            {
                case PlantTankPositions.Back:
                    return "Baggrund";
                case PlantTankPositions.Floating:
                    return "Flydeplante";
                case PlantTankPositions.Front:
                    return "Forgrund";
                case PlantTankPositions.Middle:
                    return "Midt";
                case PlantTankPositions.Solitary:
                    return "Solo";
                default:
                    return "Ukendt";
            }
        }

        public static string PlantCO2RequirementsToString(PlantCO2Requirements value)
        {
            switch(value)
            {
                default:
                    return "Ukendt";
                case PlantCO2Requirements.High:
                    return "Højt";
                case PlantCO2Requirements.Low:
                    return "Lavt";
                case PlantCO2Requirements.Medium:
                    return "Middel";
                case PlantCO2Requirements.None:
                    return "Ingen";
            }
        }

        public static string PlantGrowthSpeedToString(PlantGrowthSpeed value)
        {
            switch(value)
            {
                default:
                    return "Ukendt";
                case PlantGrowthSpeed.Fast:
                    return "Hurtig";
                case PlantGrowthSpeed.Moderate:
                    return "Middel";
                case PlantGrowthSpeed.Slow:
                    return "Langsomt";
                case PlantGrowthSpeed.VeryFast:
                    return "Meget hurtig";
                case PlantGrowthSpeed.VerySlow:
                    return "Meget langsom";
            }
        }
    }
}