DROP PROCEDURE IF EXISTS simpleSearchAnimal;
DELIMITER $$
CREATE PROCEDURE simpleSearchAnimal(IN scientificName TEXT)
BEGIN
	SELECT 
		a.id, 'animal' as EntryType
	FROM 
		animal a,
		scientificname sn
	WHERE
		(a.scientificname_id=sn.id AND concat_ws('', sn.genus, sn.species) like scientificName)
UNION DISTINCT
	SELECT
		a2.id, 'animal' as EntryType
	FROM 
		animal a2,
		scientificname sn2,
		scientificnametoanimal sna
	WHERE
		a2.id=sna.animal_id AND sn2.Id=sna.scientificname_id AND concat_ws('', sn2.genus, sn2.species) like scientificName
UNION DISTINCT
	SELECT 
		a.id, 'animal' as EntryType
	FROM
		animal a
	WHERE
		a.description LIKE scientificName;
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchPlant;
DELIMITER $$
CREATE PROCEDURE simpleSearchPlant(IN scientificName TEXT)
BEGIN
	SELECT 
		p.id, 'plant' as EntryType
	FROM 
		plant p,
		scientificname sn
	WHERE
		(p.scientificname_id=sn.id AND concat_ws('', sn.genus, sn.species) like scientificName)
UNION DISTINCT
	SELECT
	p.id, 'plant' as EntryType
FROM 
	plant p,
    scientificname sn,
    scientificnametoplant snp
WHERE
	p.id=snp.Plant_id AND
    sn.Id=snp.scientificname_id AND 
    concat_ws(sn.genus, ' ', sn.species) like scientificName
UNION DISTINCT
	SELECT 
		id, 'plant'
	FROM
		plant
	WHERE
		description LIKE scientificName;
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchBlog;
DELIMITER $$
CREATE PROCEDURE simpleSearchBlog(IN scientificName TEXT)
BEGIN
	SELECT
		b.id, 'blog' as EntryType
	FROM 
		blogentity b
	WHERE
		b.Title LIKE scientificName OR b.Body LIKE scientificName
        ;
END;
$$
DELIMITER ;

-- call simpleSearchAnimal('%apistogramma%');
-- call simpleSearchPlant('%apistogramma%');
-- call simpleSearchBlog('%apistogramma%');
