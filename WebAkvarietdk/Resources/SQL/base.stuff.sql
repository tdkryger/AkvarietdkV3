-- drop database akvariet_dk_db2;
-- CREATE DATABASE akvariet_dk_db2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- Husk at importere billeder først!!

USE akvariet_dk_db2;

-- ALTER TABLE `akvariet_dk_db2`.`scientificname` ADD INDEX `ScientificName` (`Genus` ASC, `Species` ASC);

-- ALTER TABLE blogentity DROP INDEX `blogentityFullText`;
ALTER TABLE blogentity ADD FULLTEXT INDEX `blogentityFullText` (`Title` ASC,  `Body` ASC);
ALTER TABLE plant ADD FULLTEXT INDEX `plantFullText` (`Description` ASC);
ALTER TABLE animal ADD FULLTEXT INDEX `animalFullText` (`Description` ASC);

DROP PROCEDURE IF EXISTS originSearch;
DELIMITER $$
CREATE PROCEDURE originSearch(IN origin TEXT)
BEGIN
	SELECT Id, `Text`, `suborigin` FROM origin WHERE `Text` LIKE origin OR `suborigin` LIKE origin;
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchAnimal;
DELIMITER $$
CREATE PROCEDURE simpleSearchAnimal(IN scientificName TEXT)
BEGIN
	SELECT
		a2.id, 'animal' as EntryType
	FROM 
		animal a2,
		scientificname sn2,
		scientificnametoanimal sna
	WHERE
		a2.id=sna.animal_id AND sn2.Id=sna.scientificname_id AND concat_ws('', sn2.genus, sn2.species) like scientificName
UNION DISTINCT
	SELECT 
		a.id, 'animal' as EntryType
	FROM
		animal a
	WHERE
		 MATCH (a.description) AGAINST (scientificName) OR
         MATCH (a.scientificNameText) AGAINST (scientificName);
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchPlant;
DELIMITER $$
CREATE PROCEDURE simpleSearchPlant(IN scientificName TEXT)
BEGIN
	SELECT
	p.id, 'plant' as EntryType
FROM 
	plant p,
    scientificname sn,
    scientificnametoplant snp
WHERE
	p.id=snp.Plant_id AND
    sn.Id=snp.scientificname_id AND 
    concat_ws(sn.genus, ' ', sn.species) like scientificName
UNION DISTINCT
	SELECT 
		id, 'plant'
	FROM
		plant
	WHERE
     MATCH (description) AGAINST (scientificName) OR
         MATCH (scientificNameText) AGAINST (scientificName);
END;
$$
DELIMITER ;

DROP PROCEDURE IF EXISTS simpleSearchBlog;
DELIMITER $$
CREATE PROCEDURE simpleSearchBlog(IN scientificName TEXT)
BEGIN
	SELECT b.id, 'blog' as EntryType FROM blogentity b WHERE MATCH (title, body) AGAINST (scientificName);
END;
$$
DELIMITER ;

DROP TABLE IF EXISTS fishbaseRaw;
CREATE TABLE fishbaseRaw
(
	fishbaseId int(32) NOT NULL,
	genus VARCHAR(255) NOT NULL,
	species VARCHAR(255) NOT NULL,
	xmlSummary LONGTEXT,
	xmlPointData LONGTEXT,
	xmlCommonNames LONGTEXT,
	xmlPhotos LONGTEXT,
	PRIMARY KEY (fishbaseId)
)  CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- call simpleSearchAnimal('%apistogramma%');
-- call simpleSearchPlant('%apistogramma%');
-- call simpleSearchBlog('%apistogramma%');
-- call simpleSearchBlog('%led%');

INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Plante');
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Fisk'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Akvarie');
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Elektronik');
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Akvarieforening'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Udstilling'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Udstyr'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('EBay'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('Gør-Det-Selv'); 


INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('130L'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('96L A'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('96L Pygmæ'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('54L A'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('54L B'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('30L'); 
INSERT INTO `akvariet_dk_db2`.`tagentity` (`TagName`) VALUES ('15L'); 

INSERT INTO `akvariet_dk_db2`.`user` (`DisplayName`,`Username`,`EMail`,`Role`) VALUES ('Thomas D. Kryger','tdkryger','tdkryger@akvariet.dk',99);
INSERT INTO `akvariet_dk_db2`.`user` (`DisplayName`,`Username`,`EMail`,`Role`) VALUES ('Admin','admin','tdkryger@akvariet.dk',99);

INSERT INTO `akvariet_dk_db2`.`blogentity` (`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`, contributor_id) VALUES (
'Akvarierne 13/11 2014', 
'Lige et par billeder af mine 3 aktive akvarier',
'<p><h3>Lige et par billeder af mine 3 aktive akvarier:</h3></p><div><b>130l med grønne sværddragere og pansermaller</b><br /><img src="/Handlers/ImageHandler.ashx?imageId=652" style="max-width:600px"/></div><div><b>63l med diamant tetra og 1 par apistogramma sp. viejeta II</b><br /><img src="/Handlers/ImageHandler.ashx?imageId=683" style="max-width:600px" /></div><div><b>og tilsidst mit 63l med kejsertetra:</b><br /><img src="/Handlers/ImageHandler.ashx?imageId=684" style="max-width:600px"/></div>',
Date('2014-11-13 13:23:44.657'),
0,
652, (SELECT Id FROM user WHERE DisplayName='Thomas D. Kryger'));

INSERT INTO `akvariet_dk_db2`.`blogentity` (`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`, contributor_id) VALUES (
'Akvariecomputer', 
'Tanker om en Gør-det-selv akvariecomputer',
'<p>I forbindelse med mit projekt med at skifte fra lysstofrør til LEDs fik jeg den ide at du kunne være fedt at have solopgang og solnedgang, samt en måde at justere lysstyrken på LEDs.</p><p>I første omgang var planen at bruge et modul fra <a href="https://www.ghielectronics.com/" target="_blank">GHI Electronics</a> med .NET micro framework så jeg kunne kode i C#.&nbsp;<br><span style="line-height: 1.42857143;">Der var dog nogle få ting der generede mig med modulerne fra GHI:</span></p><p></p><ul><li><span style="line-height: 1.42857143;">Det er ikke realtime system</span></li><li><span style="line-height: 1.42857143;">Det første modul jeg ville bruge var ikke helt kraftigt nok, og det blev sat ud af produktion</span></li><li><span style="line-height: 1.42857143;">Det andet modul jeg ville bruge er meget dyrt</span></li></ul><p></p><p>Så jeg skiftede til <a href="http://arduino.cc/" target="_blank">Arduino</a> platformen, mest fordi der er et stort community omkring platformen, men også realtime.</p><p><br></p><p>Min plan er at den færdige akvariecomputer skal have følgende sensorer og funktioner:</p><p></p><ul><li><span style="line-height: 1.42857143;">Vandtemperatur. Mulighed for op til 4 temperatursensor.</span></li><li><span style="line-height: 1.42857143;"><a href="https://www.atlas-scientific.com/product_pages/kits/ph-kit.html?" target="_blank">pH</a> til at regulere CO2 via relæ.</span></li><li><span style="line-height: 1.42857143;"><a href="https://www.atlas-scientific.com/product_pages/kits/do_kit.html?" target="_blank">Ilt</a></span></li><li><span style="line-height: 1.42857143;"><a href="https://www.atlas-scientific.com/product_pages/kits/ec_k0_1_kit.html?" target="_blank">Ledningsevne</a>.</span></li><li><span style="line-height: 1.42857143;">Vandniveau.</span></li><li><span style="line-height: 1.42857143;">Gennemstrømning fra udvendigt filter.</span></li><li><span style="line-height: 1.42857143;">Styre et antal dæmpere/drivers til LEDs</span></li><li><span style="line-height: 1.42857143;">Tænde/slukke CO2</span></li><li><span style="line-height: 1.42857143;">Tænde/slukke varmelegeme og tænde/slukke blæser til at afkøle vandoverfladen</span></li><li><span style="line-height: 1.42857143;">Tænde/slukke luftpumpe</span></li><li><span style="line-height: 1.42857143;">Dosering af flydende gødning</span></li><li><span style="line-height: 1.42857143;">Webserver til visning og opsætning og dermed netkort</span></li><li><span style="line-height: 1.42857143;">Ur med automatisk indstilling og styring af sommer/vintertid</span></li><li>Skærm og en eller flere knapper til visning og opsætning</li></ul>',
Date('2014-10-30 09:57'),
0,
711, (SELECT Id FROM user WHERE DisplayName='Thomas D. Kryger'));

INSERT INTO `akvariet_dk_db2`.`blogentity` (`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`, contributor_id) VALUES (
'LED lys', 
'Lidt snak om at bruge LED\'er på akvarier, og de ting man skal være opmærksom på når man gør det selv',
'<ul><li>Planteakvarier har behov for mere lys end et akvarie kun med fisk.<br> <span>Afhængigt af planternes behov kan det være 0,25w/l til over 1 w/l (Lavt behov: 0,25 w/l til 0,5 w/l , Middel behov: 0,5 w/l til 1 w/l, Højt behov: 1+ w/l)</span></li><li>Kunstigt lys bør idealt være stærkere i det røde spektrum</li><li>Stærkt rødt lys kan få planterne til at blive høje og tynde, og har en tendens til at fremme blågrøn alger</li><li>Stærkt blåt lys kan få planterne til at blive mere kompakte og buskede, og har en tendens til at fremme general algevækst</li><li>Balancen mellem rødt og blåt lys bør være 2:1</li><li>Grønt lys giver et bedre udtryk for os mennesker</li><li>Et eksempel:<br> Dagslyskilde (5000K – rødt lys)<br> Plus en ”arktisk” hvid (12000K til 20000K) eller ”arktisk” dagslys (10000K) kilde.</li><li>Giver et 2:1 forhold mellem rødt og blåt lys, med en smule grønt</li><li>Nogle planter har evnen til at ændre det pigment de bruger til fotosyntese alt efter det tilgængelige lys:</li><li>Rød-bladede planter bliver grønne når lyset er for lavt, eller når der ikke er nok blåt og/eller grønt lys.</li><li>Grøn-bladede planter producerer røde blade når de er tæt på lyskilden, eller ved overdrevet kraftig belysning.</li></ul><div><p style="text-align: center;"></p><h4 style="background-color: rgb(255, 0, 0);">Advarsel! Se aldrig direkte på en lysdiode. Lyset er så stærkt, at det kan give øjenskader.</h4><ul><li>Målt i PAR/Watt udsender ingen lyskilde mere PAR-stråling end LEDS. PAR (Photosynthetically active radiation) måles i Micro mol og er et udtryk for fotonaktiviteten i området fra 400-700 nanometer.</li><li>Hvis de behandles korrekt holder lysdioder op til 500.000 timer, før de skal skiftes. Det er 10-13 år!</li><li>Dioder giver ”shimming-effekt” dvs. de kaster lys/skygge. Man skal lige vænne til det, men derefter vil man nødig undvære denne virkning. Det betyder f. eks at fiskene kaster skygge på bunden af akvariet. Hvis der kommer bevægelse i overfladen, vil man kunne se lyset spille på planter, dekoration og bund. Det er dog ikke alle der syntes om dette.</li><li>LED ‘er er en fleksibel løsning. Man kan designe lamper til ethvert akvarium uanset størrelse. Man kan også bruge dem til udbygning/lystilskud i en eksisterende lampe.</li><li>LED lamper er billigere i drift end T5 systemer.</li></ul></div><div><p>Når der handles dioder skal du undgå de såkaldte emittere. Dette er rene dioder, der er på størrelse med en halv ært. De er meget vanskelige at lodde på.</p><p>Køb i stedet den type LED‘er, der er monteret på såkaldte ”stjerner”. Disse er på størrelse med en tikrone, og meget nemmere at lodde og montere.</p><p>Man skal være forsigtig når ledningerne loddes på dioderne. De tåler 260°C i 5 sekunder, og det er ikke meget.</p><p>Hvis dioderne overophedes kan de skifte farve, og levetiden kan nedsættes drastisk.</p><h2>Hvor mange LED ‘er?</h2><p>Når man skal beregne hvor mange dioder der skal bruges, kan man anvende følgende formel til et overslag:</p><p align=center style=text-align:center>(2/3 ∗ Watt) / 2.4 = antal 3 watt dioder</p><p>Formlen skal forstås således at Watt, er det nuværende forbrug til lyset. Tag 2/3 af dette og del med 2,4.</p><p>Eksempel: Vi vil erstatte 2x24W med dioder. 2/3 af 48W delt med 2,4=13.3. Der skal altså bruges ca. 13 dioder på 3 watt. . at vi går fra 48W til 39W.</p><p>Dette er selvfølgelig vejledende. Er akvariet meget højt, skal der selvfølgelig bruges flere LED ‘er, da man er nødt til at bruge linser med snæver spredning. Hvis ikke man monterer dioderne lidt tæt på hinanden, vil man opleve spotlight-effekt på bunden. Hvis man ikke har planter med høje krav til lys, kan man naturligvis reducere lysmængden.</p><h2>Hvor meget lys er nok lys?</h2><p>Det kommer helt an på hvilke planter du har valgt til dit akvarium.</p><p>Har du udelukkende planter med lave krav, er 0,25 til 0,5 Watt per L tilstrækkeligt.</p><p>Til ”mellem” planter skal du bruge 0,5 til op mod 1 Watt per L, mens planter med meget høje lyskrav kræver mere en 1 Watt per L.</p><h2>Drivere</h2><p>Drivere kan sammenlignes med ballasten + glimtænder i et armatur til lysstofrør.</p><p>Der findes en masse forskellige drivere. Nogle er beregnet til at levere et fast antal ampere, og andre er variable. Antal ampere (A) bestemmer hvor mange watt de enkelte LED ‘er er på. Der er også forske på mængden af volt (V) de enkelte kan håndtere. Antal volt bestemmer hvor mange LED ‘er den enkelte driver kan håndtere. De fleste kan dæmpes med noget der hedder PWM eller med en spænding på 0-10V.</p><p>Normalt vil drivere, der ikke kan reguleres, give enten 350 eller 700mA. Jo højere strømstyrke, des mere lys og varme udvikler dioderne.</p><p>De fleste 3W dioder tåler strømstyrker op til 1000mA og enkelte 1500mA. Dette dog kun kortvarigt.</p><p>Til akvariebrug, hvor lyset er tændt mange timer hver dag, bør man holde sig til max. 750mA.</p><h2>Køling</h2><p>LED ‘er skal som minimum have en køleplade. Dette hjælper med en lang levetid, og forhindre at LED ‘en overopheder. De fleste LED ‘er kan ikke tåle temperaturer over 50°C i længere tid. Hvis temperaturen på kølepladen overstiger 50°C, skal der en større køleplade til, eller også skal man montere en blæser.</p></div><h2>Sammenligning af kold, dagslys og varm hvid på et 63L akvarie:</h2><div><h3>3x 3W Kold hvid</h3><img src="/Handlers/ImageHandler.ashx?imageId=706" title="3x 3W Kold hvid" style="width: 100%"><br></div><div><h3>3x 3W Dagslys hvid</h3><img src="/Handlers/ImageHandler.ashx?imageId=686" title="3x 3W Dagslys hvid" style="width: 100%"><br></div><div><h3>3x 3W Varm hvid</h3><img src="/Handlers/ImageHandler.ashx?imageId=708" title="3x 3W Varm hvid" style="width: 100%"><br></div>',
Date('2016-03-18 09:57'),
0,
711, (SELECT Id FROM user WHERE DisplayName='Thomas D. Kryger'));

INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvarierne 13/11 2014'), (SELECT id FROM tagentity WHERE TagName='Akvarie'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvarierne 13/11 2014'), (SELECT id FROM tagentity WHERE TagName='130L'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvarierne 13/11 2014'), (SELECT id FROM tagentity WHERE TagName='96L A'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvarierne 13/11 2014'), (SELECT id FROM tagentity WHERE TagName='96L Pygmæ'));

INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvariecomputer'), (SELECT id FROM tagentity WHERE TagName='Akvarie'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvariecomputer'), (SELECT id FROM tagentity WHERE TagName='Elektronik'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='Akvariecomputer'), (SELECT id FROM tagentity WHERE TagName='Gør-Det-Selv'));

INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='LED lys'), (SELECT id FROM tagentity WHERE TagName='Elektronik'));
INSERT INTO `akvariet_dk_db2`.`tagentitytoblogentity` (`BlogEntity_id`,`TagEntity_id`) VALUES ((SELECT id FROM blogentity WHERE title='LED lys'), (SELECT id FROM tagentity WHERE TagName='Gør-Det-Selv'));



INSERT INTO `animalgroup` VALUES (1,'Arowana','XOA','Afrika',1),(2,'Arowana','XOS','Mellem- og sydamerikanske',1),(3,'Arowanaer og Barramundi','XOX','Asien og Australien',1),(4,'Barbe','BA','Afrika',1),(5,'Barbe','BX','Asien og Australien',1),(6,'Barbe (Danio osv)','BXB','Asien',1),(7,'Barbe (Rasbora)','BXR','Asien',1),(8,'Barbe','BE','Europa',1),(9,'Barbe','BN','Nord Amerika',1),(10,'Bitterling','TB','Verden',1),(11,'Brakvandsfisk','SBA','Afrika',1),(12,'Brakvandsfisk','SBX','Asien og Australien',1),(13,'Brakvandsfisk','SBS','Mellem- og sydamerikanske',1),(14,'Brakvandsfisk','SBE','Europa',1),(15,'Brakvandsfisk','SBN','Nord Amerika',1),(16,'Malle','WA','Afrika',1),(17,'Malle (Catles, Synodontis, Chiloglanis osv)','WAS','Afrika',1),(18,'Cichlide','CAM','Afrika, Malawi',1),(19,'Cichlide','CAT','Afrika, Tanganika',1),(20,'Cichlide','CAV','Afrika, Viktoria',1),(21,'Cichlide','CA','Afrika, Floder og Vest Afrika',1),(22,'Cichlide','CX','Asien og Australien',1),(23,'Dværg Cichlide','CSD','Mellem- og sydamerikanske',1),(24,'Cichlide','CSG','Mellem- og sydamerikanske',1),(25,'Scalare','CSS','Mellem- og sydamerikanske',1),(26,'Discus, Opdræt','CTZ','Mellem- og sydamerikanske',1),(27,'Discus','CTW','Mellem- og sydamerikanske',1),(28,'Ål','EA','Afrika',1),(29,'Ål, Polypterus','EAP','Afrika',1),(30,'Ål, ’Spiny’','EAS','Afrika',1),(31,'Ål','EX','Asien og Australien',1),(32,'Ål, ’Spiny’','EAX','Asien og Australien',1),(33,'Ål','ES','Mellem- og sydamerikanske',1),(34,'Ål','EEN','Europa og Nord Amerika',1),(35,'Elefantfisk, ’Nilepikes’ (Mormyriden osv.)','MO','Afrika',1),(36,'Fladfisk','FA','Afrika',1),(37,'Fladfisk','FE','Europa',1),(38,'Fladfisk','FN','Nord Amerika',1),(39,'Fladfisk','FS','Mellem- og sydamerikanske',1),(40,'Fladfisk','FX','Asien og Australien',1),(41,'Frøer, Tudser, Salamander','YFA','Afrika',1),(42,'Frøer, Tudser, Salamander','YFX','Asien og Australien',1),(43,'Frøer, Tudser, Salamander','YFE','Europa',1),(44,'Frøer, Tudser, Salamander','YEN','Nord Amerika',1),(45,'Frøer, Tudser, Salamander','YFS','Mellem- og sydamerikanske',1),(46,'Galaxias og Kneria','BXX','Verden',1),(47,'Goby, Gudgeon, Blenni osv.','OA','Afrika',1),(48,'Goby, Gudgeon, Blenni osv.','OX','Asien og Australien',1),(49,'Goby, Gudgeon, Blenni osv.','OS','Mellem- og sydamerikanske',1),(50,'Goby, Gudgeon, Blenni osv.','OE','Europa',1),(51,'Goby, Gudgeon, Blenni osv.','ON','Nord Amerika',1),(52,'Halvnæb','LH','Verden',1),(53,'\'Hardyheaders’, ’Silversides’ osv.','RXQ','Verden',1),(54,'Insekter','YU','Verden',1),(55,'Killifisk','KE','Europa',1),(56,'Killifisk','KS','Mellem- og sydamerikanske',1),(57,'Killifisk','KA','Afrika',1),(58,'Killifisk','KX','Asien og Australien',1),(59,'Killifisk','KN','Nord Amerika',1),(60,'Labyrintfisk','GGA','Afrika',1),(61,'Labyrintfisk','GGX','Asien',1),(62,'Bladfisk','GB','Afrika og Sydamerika',1),(63,'Levendefødende','LS','Verden',1),(64,'Smerling','UA','Afrika',1),(65,'Smerling','UX','Asien og Australien',1),(66,'Smerling','US','Mellem- og sydamerikanske',1),(67,'Smerling','UE','Europa',1),(68,'Smerling','UN','Nord Amerika',1),(69,'Lungefisk','EYL','Verden',1),(70,'Dyndspringer','OYA','Afrika',1),(71,'Dyndspringer','OYX','Asien',1),(72,'\'Perca’ (Perches)','PA','Afrika',1),(73,'\'Perca’ (Perches)','PX','Asien og Australien',1),(74,'\'Perca’ (Perches)','PND','Nord Amerika',1),(75,'\'Perca’ (Perches)','PEN','Europa og Nord Amerika',1),(76,'\'Pipefish’, ’Needlefish’','XNA','Afrika',1),(77,'\'Pipefish’, ’Needlefish’','XNX','Asien og Australien',1),(78,'\'Pipefish’, ’Needlefish’','XNS','Mellem- og sydamerikanske',1),(79,'Havedamsfisk','TX','Asien, Australien og Kina',1),(80,'Havedamsfisk','TE','Europa',1),(81,'Havedamsfisk','TXK','Koi, Nishikigoi',1),(82,'Havedamsfisk','TNN','Ni-øjer',1),(83,'Havedamsfisk','TN','Nord Amerika',1),(84,'Havedamsfisk, stør, gar osv.','TNS','Verden',1),(85,'Regnbuefisk','RAN','Afrika (Madagaska) og Nord Amerika',1),(86,'Regnbuefisk','RXM','Asien og Australien',1),(87,'Regnbuefisk (Popondetta, Blue Eyes osv.)','RXP','Asien og Australien',1),(88,'Reptiler, slanger osv.','YRA','Afrika',1),(89,'Reptiler, slanger osv.','YRX','Asien og Australien',1),(90,'Reptiler, slanger osv.','YRS','Mellem- og sydamerikanske',1),(91,'Reptiler, slanger osv.','YRE','Europa',1),(92,'Reptiler, slanger osv.','YRN','Nord Amerika',1),(93,'Sardiner','ATA','Afrika',1),(94,'Sardiner','ATE','Europa og Nord Amerika',1),(95,'Sardiner','ATS','Mellem- og sydamerikanske',1),(96,'Sardiner','ATX','Asien og Australien',1),(97,'Havedamsfisk, Guldfisk','TXG','Verden',1),(98,'Rejer og Krabber','XYA','Afrika',1),(99,'Rejer og Krabber','XYX','Asien og Australien',1),(100,'Rejer og Krabber','XYS','Mellem- og sydamerikanske',1),(101,'Rejer og Krabber','XYE','Europa',1),(102,'Rejer og Krabber','XYN','Nord Amerika',1),(103,'Snegle og Muslinger','YSA','Afrika',1),(104,'Snegle og Muslinger','YSX','Asien og Australien',1),(105,'Snegle og Muslinger','YSS','Mellem- og sydamerikanske',1),(106,'Snegle og Muslinger','YSE','Europa',1),(107,'Snegle og Muslinger','YSN','Nord Amerika',1),(108,'Slangehoved fisk','GC','Afrika og Asien',1),(109,'Pilrokke','XRA','Afrika',1),(110,'Pilrokke','XRX','Asien og Australien',1),(111,'Pilrokke','XRS','Mellem- og sydamerikanske',1),(112,'Pilrokke','XRE','Europa',1),(113,'Pilrokke','XRN','Nord Amerika',1),(114,'Tetra','AA','Afrika',1),(115,'Tetra 1','ASD','Mellem- og sydamerikanske',1),(116,'Tetra 2','ASO','Mellem- og sydamerikanske',1),(117,'Tetra, Mellem til Stor','ASG','Mellem- og sydamerikanske',1),(118,'Tetra (Metynnis)','ASS','Mellem- og sydamerikanske',1),(119,'Tetra (Piratfisk og Pacu)','ASP','Mellem- og sydamerikanske',1),(120,'Tetra, rovfisk','ASR','Mellem- og sydamerikanske',1),(121,'Tetra, Små','ASK','Mellem- og sydamerikanske',1),(122,'Skildpade','YTA','Afrika',1),(123,'Skildpade','YTX','Asien og Australien',1),(124,'Skildpade','YTS','Mellem- og sydamerikanske',1),(125,'Skildpade','YTE','Europa',1),(126,'Skildpade','YTN','Nord Amerika',1),(127,'Ukendt','?','Verden',1),(128,'Tetra, blyantfisk','ASL','Mellem- og sydamerikanske',1),(129,'','CS','',1);
INSERT INTO `order` VALUES (1,'Ukendt','Ukendt'),(2,'Branchiopoda','Krebsdyr'),(3,'Copepoda','Krebsdyr'),(4,'Ostracoda','Krebsdyr'),(5,'Cirrepedia','Krebsdyr'),(6,'Stomatopoda','Krebsdyr'),(7,'Mysidacea','Krebsdyr'),(8,'Decapoda','Krebsdyr'),(9,'Amphipoda','Krebsdyr'),(10,'Isopoda','Krebsdyr'),(11,'Thysanura','Insekter'),(12,'Diplura','Insekter'),(13,'Protura','Insekter'),(14,'Collembola','Insekter'),(15,'Ephemeroptera','Insekter'),(16,'Odonata','Insekter'),(17,'Plecoptera','Insekter'),(18,'Grylloblattodea','Insekter'),(19,'Orthoptera','Insekter'),(20,'Phasmida','Insekter'),(21,'Dermaptera','Insekter'),(22,'Embioptera','Insekter'),(23,'Dictyoptera','Insekter'),(24,'Isoptera','Insekter'),(25,'Zoraptera','Insekter'),(26,'Psocoptera','Insekter'),(27,'Mallophaga','Insekter'),(28,'Siphunculata (Anoplura)','Insekter'),(29,'Hemiptera','Insekter'),(30,'Thysanoptera','Insekter'),(31,'Neuroptera','Insekter'),(32,'Coleoptera','Insekter'),(33,'Strepsiptera','Insekter'),(34,'Mecoptera','Insekter'),(35,'Siphonaptera','Insekter'),(36,'Diptera','Insekter'),(37,'Lepidoptera','Insekter'),(38,'Trichoptera','Insekter'),(39,'Hymenoptera','Insekter'),(40,'Anguilliformes','Benfisk'),(41,'Salmoniformes','Benfisk'),(42,'Cypriniformes','Benfisk'),(43,'Siluriformes','Benfisk'),(44,'Perciformes','Benfisk'),(45,'Caudata','Padder'),(46,'Salientia','Padder'),(47,'Testudines','Krybdyr'),(48,'Squamata','Krybdyr'),(49,'Crocodilia','Krybdyr'),(50,'Characiformes','Fisk'),(51,'Clupeiformes','Fisk'),(52,'Elopiformes','Fisk'),(53,'Osteoglossiformes','Fisk'),(54,'Osmeriformes','Fisk'),(55,'Gonorynchiformes','Fisk'),(56,'Polypteriformes','Fisk'),(57,'Synbranchiformes','Fisk'),(58,'Gymnotiformes','Fisk'),(59,'Lepidosireniformes','Fisk'),(60,'Ceratodontiformes','Fisk'),(61,'Pleuronectiformes','Fisk'),(62,'Cyprinodontiformes','Fisk'),(63,'Beloniformes','Fisk'),(64,'Scorpaeniformes','Fisk'),(65,'Mugiliformes','Fisk'),(66,'Percopsiformes','Fisk'),(67,'Atheriniformes','Fisk'),(68,'Gasterosteiformes','Fisk'),(69,'Esociformes','Fisk'),(70,'Gadiformes','Fisk'),(71,'Petromyzontiformes','Fisk'),(72,'Acipenseriformes','Fisk'),(73,'Lepisosteiformes','Fisk'),(74,'Carcharhiniformes','Fisk'),(75,'Amiiformes','Fisk'),(76,'Syngnathiformes','Fisk'),(77,'Rajiformes','Fisk'),(78,'Pristiformes','Fisk');
INSERT INTO `social` VALUES (8,'Algeæder'),(6,'Æg har særlige krav'),(12,'Æg kræver specielle forhold'),(2,'Æglægger'),(11,'Beskyttet af CITES'),(14,'Beskyttet under CITES'),(5,'Bobblerede'),(7,'Kun som par eller trio'),(9,'Let at holde'),(3,'Levendefødende'),(10,'Meget svær at holde'),(4,'Mundruger'),(1,'Stimefisk'),(13,'Svær at holde');
INSERT INTO `language` VALUES (1,'Dansk','DK'),(2,'Engelsk','EN'),(3,'Tysk','DE'),(4,'Holland','NL');


INSERT INTO `akvariet_dk_db2`.`algea` (`Name`,`Cause`,`Symptoms`,`Treatment`) VALUES (
	'Brune alger',
	'Årsagen til denne type alge er for lidt lys.',
	'Små brune busklignende vækster, på sten, rødder og planter (primært på bladkanten). De sidder godt fast, når de forsøges skrabt væk med neglen.',
	'Behandlingen er derfor en af to muligheder. Hvis lysstofrørene er mere end 1 år gamle, trænger de til en udskiftning, da de på det tidspunkt har mistet 90 procent af deres lyseffekt (også selv om den lysstyrke der kan ses med øjet ikke har ændret sig). Er der nye rør i amaturet, tyder det på, at de ikke er tændt længe nok. Prøve at øge lysmængden med en time mere hver dag.');

INSERT INTO `akvariet_dk_db2`.`algea` (`Name`,`Cause`,`Symptoms`,`Treatment`) VALUES (
	'Grønne alger',
	'I modsætning til den brune alge, kommer denne alge typisk af for meget lys.',
	'Denne alge er kendetegnet ved at være en klar grøn belægning der sætter sig på glasset, planterne, rødderne og stenene.',
	'Prøv at mindske lysmængden med en time dagligt, og se om det hjælper.');
    
INSERT INTO `akvariet_dk_db2`.`algea` (`Name`,`Cause`,`Symptoms`,`Treatment`) VALUES (
	'Blå alger',
	'Den kommer af får dårlig vandpleje og for megen fodring.',
	'Denne algetype minder meget om den brune, men er mere fedtet i konsistensen.',
	'Reducer fodermængden, skift vandet lidt oftere og rens eventuelt også filteret lidt oftere.');
    
INSERT INTO `akvariet_dk_db2`.`algea` (`Name`,`Cause`,`Symptoms`,`Treatment`) VALUES (
	'Trådalger',
	'Den kommer som regel af en kombination af overfodring, for dårlig vandpleje og for høj lysintensitet.',
	'Denne algetype er let at kende, idet den er karakteriseret ved lange trådagtige alger, der typisk hæfter på plantebladene.',
	'Prøv at reducere fodringsmængden og lysmængden, og øge vandskiftene.');    
    
INSERT INTO `illness` VALUES (1,'Fiskedræber (hvide pletter)','Encellede snyltere','Fiskedræberen, Ichthyopthirius multifiliis, er en encellet kugleformet og behåret organisme. Ichthysporen svømmer frit og skal inden 48 timer finde sig en vært. Den borer sig ind under fisken overhud, hvor den æder af fiskens væsker. Efter 1-3 uger borer den sig ud og sætter sig fast på en plante eller sten, hvor den efter 8-10 timer har delt sig i op til 1000 nysporer, som svømmer ud for at finde sig en vært.','1. bølge: Enkelte små hvide prikker - ½-1 mm - på enkelte fisk. Det overses ofte.\n2. bølge: Alle fisk er angrebet og med mange prikker over krop og finner. Fiskene forsøger at gnide snylteren af på planter sten m.m. Fiskene patter efter luft i overfladen, da også gællerne er angrebet. Efter 1 uge forsvinder de fleste pletter.\n3. og 4. bølge: Pletterne kommer massivt igen. Ofte kommer svampeangreb til (Saprolegnia. Massedødsfald. Enkelte fisk kan i visse tilfælde overleve, men sættes nye fisk ned til dem, vil de hurtigt blive angrebet.\n\nUnder mikroskop kan man konstatere en lille cirkelrund, roterende organisme med en hesteskoformet cellekærne.\nAngriber alle fisk!\nDen findes også i en saltvandsudgave.\nFiskedræberen kan indslæbes med foderdyr, men om regel indføres snylteren med inficerede fisk.','Der findes i handelen effektive midler mod fiskedræber. Det er vigtigt, at følge brugsanvisningen. Fiskene behandles i et særskilt helt tomt glasakvarium. Har opholdsakvariet stået tomt helt uden fisk i over 48 timer er alle snyltere og deres larver døde, men blot en enkelt lille efterladt fiskeunge i akvariet vil gøre, at sygdommen kommer igen.'),(2,'Oodinium (Forkølelsespletter)','Encellede snyltere','Oodinium er en såkaldt dino-flagellat. Den indeholder klorofyl, men kan svømme, altså en sammenblanding af dyr og plante. Angrebene kaldes forkølelsespletter, da de ofte kommer efter store vandskift, temperaturfald eller andre store forandringer i miljøet. Er ofte følgesvend til sygdomme som fisketuberkulose og fiskedræber eller situationer, hvor fiskenes kondition er svækket. Formeringen er stort set som fiskedræberens.','Fisken ser ud, som om den er dækket af et lag pudder, især på ryg og finner. Undertiden har pudderlaget et gulligt skær.\nUnder mikroskop ses snylteren som en pæreformet organisme, der med en stilk har sat sig fast på fisken. Der ses ingen bevægelse i organismen.\nAngriber hovedsagelig barber og rasboraarter, labyrintfisk og tandkarper. Den ses ikke på karpelaks og maller.','Der findes i handelen effektive midler mod oodinium.'),(3,'Indvendige snyltende flagellater, bl.a. hulsyge','Encellede snyltere','Flagellater af slægterne Hexamita, Octominus eller Spironucleus. Det er små pæreformede, encellede organismer forsynet med svingtråde – heraf navnet flagellater.\nDe formerer sig i fiskenes tarmsystem, hvorfra de via blodsystemet spredes rundt i kroppen og kan angribe nyrer, lever, milt, galdeblære, svømmeblære m.m. Visse flagellater forårsager bylder, som efterlader sig huller, som ikke heles, den såkaldte hulsyge, som fortrinsvis kendes fra Diskosfisk, Skalarer og andre cichlider.','Fiskene holder helt eller delvist op med at æde. De bliver sløve i bevægelserne og undertiden mørke i farvetegningen. Afføringen er trådtynd og hvidgrå og flagrer efter fisken.','I akvariehandelen fås præparatet Hexa-ex. Flagellaangreb kan også behandles med den receptpligtige medicin metronidazol, der bl.a. forhandles under navne som Flagyl og Trichomol.\nBlandt Diskosfolk anvendes først og fremmest den såkaldte varmekur, hvor temperaturen hæves fra de normale 28-29°C til 35°, hvor den holdes i en uge. Derefter sænkes temperaturen til de normale 28-29° i en uge og hæves til slut i endnu en uge til 35°.'),(4,'Neonsyge','Encellede snyltere','Et snyltende sporedyr (Plistophora hyphessobryconis). Der sidder bl.a. sporer i halefinnen på angrebne fisk. Når andre fisk så nipper i halen, vil sporen i maven på den nye fisk udvikle sig og sende nye sporer ud i hele fisken muskulatur.','Urolig svømning, manglende ædelyst og ligevægtsforstyrrelser. På et senere stadium kan halepartiet angribes af skimmelsvamp (Saprolegnia).\nSygdommen angriber især karpelaks. På yngel af karpelaks kan angreb ses ved, at ungerne skruer sig op gennem vandet for derefter at falde mod bunden.','Uhelbredelig. Angrebne fisk fjernes og aflives straks for at undgå smitte.'),(5,'Ichthyoponus','Svampesygdomme','Ichthyosporidium hoferi, der er en 5-20 μ stor algesvamp, som lever indvendig i den angrebne fisk. Den formerer sig i fisken og sender svampesporer ud til alle organer. Smitter ved, at andre fisk æder den døde inficerede fisk eller ved, at svampen sender direkte sporer ud i vandet fra bylder på fiskens krop.','Mange! Indfalden bug, sår, bylder og knuder, Gule over brun- til sort-farvede ansamlinger i muskulaturen, fremtrædende øjne, misdannelser, skælrejsning og ligevægtsforstyrrelser. Kan være meget svær at skelne fra fisketuberkulose.','Uhelbredelig. Syge fisk fjernes og aflives så hurtigt som muligt for at undgå yderligere infektioner af akvariets fisk.'),(6,'Udvendige svampeangreb','Svampesygdomme','Angreb af skimmelsvampe (Saprolegnia eller Achlya) som følge af, at overhuden og fiskens slimlag er beskadiget. Skader fra fangstnet rammer ofte øjnene (fungus), mens mundfungus ofte opstår ved, at fiskene i panik er svømmet ind i akvarieruderne. Ubehandlet vil svampenes hyfer ikke alene trænge ind i overhuden, men også i fiskens muskulatur. Det vil efterhånden føre til fiskens død.','Hvide vattot-lignende belægninger. Findes det på mund eller øjne, kaldes det henholdsvis mundfungus og øjefungus.','Der findes effektive svampemidler i handelen.'),(7,'Bugvattersot','Virus sygdomme','Sandsynligvis en virusinfektion efterfulgt af en infektion af bakterieslægterne Aeromonas eller  Pseudomonas.','Udspilet bug. Skælrejsning. Evt. udstående øjne.\nLabyrintfisk skulle være særligt udsatte.','Uhelbredelig. Angrebne fisk fjernes og aflives straks for at undgå smitte.'),(8,'Hindbærsyge','Virus sygdomme','Virusinfektion.','Samling af hvide, knappenålsstore knuder på finner, gællelåg eller kropssider.\nAngriber først og fremmest saltvandsfisk, men kan også forekomme på ferskvandsfisk.','Syge fisk bør isoleres. Knuderne fjernes operativt, på finnerne ved at klippe de angrebne områder af, på kroppen ved at skære dem af med en skarp skalpel eller et barberblad. De behandlede områder pensles med et desinficerende middel, f.eks. euflavin.'),(9,'Fisketuberkulose (Høstpest)','Bakterielle sygdomme','Stavformede bakterier, 5-20 tusindedele mm. Findes tit i raske fisk i mindre mængder. Forekommer tit i øget mængde i fisk, der er angrebne af snyltere.\nVitaminmangel ser ud til at øge risikoen for angreb af fisketuberkulose.\nFisketuberkulose skyldes bakterien Mycobacterium piscium, som imidlertid trives bedst ved temperaturer på 25-30° C og dør ved 37°. Den kan således ikke leve i mennesker.\nMen bakterien Mycobacterium marinum kan også forårsage en fiskesygdom, som regnes under kategorien fisketuberkulose, og den kan være meget alvorlig for mennesker, hvis man inficeres og ikke kommer til behandling i tide.\nFår man en infektion,  f.eks. i et sår på hånden, efter at have været i kontakt med akvarievand, så skal man straks søge læge og gøre opmærksom på, at det kan dreje sig om en infektion forårsaget af bakterien Mycobacterium marinum.','Fiskene svømmer langsommere og ofte i ryk efterfulgt af en baglæns bevægelse. Undertiden ligevægtsforstyrrelser. Huden kan miste farven og blive mælket at se på. Fiskene søger som oftest op til overfladen. På gællerne kan man se afgrænsede røde områder. ','Fisken bør aflives med en skarp kniv og fisken gennemkoges, så alle bakterier er dræbt, før den lægges i dagrenovationen. De anvendte redskaber skoldes efter brugen.'),(10,'Finneråd','Bakterielle sygdomme','Bakterieinfektion.','Finnerne ”smelter” bort. Rammer især Sort Spenops, men undertiden også andre ungefødende tandkarper. Skimmelsvamp (Saprolegnia) støder ofte til. ','Rodalon (Benzalkoniumklorid). Stamopløsning: 1 ml Rodalon i 1 liter destilleret vand. Til endeligt bad: 5 ml stamopløsning pr. liter vand. Pas på ikke at overdosere!'),(11,'Columnarissyge','Bakterielle sygdomme','Angreb af bakterierne af slægterne Cytophaga og Flexibacter.','Svampelignende belægning omkring munden og rundt om på kroppen. Fisken bliver sløv. Kan forveksles med et svampeangreb, men sammenholder man de to sygdomme vil man se, at svampenes tråde er meget mere grove.','Nifurpirinol. Forhandles hos akvariehandleren under navnet Furamor-P. Importerede fisk kan være resistente over for midlet.'),(12,'Rødsyge','Bakterielle sygdomme','Angreb af en eller flere bakterier af gruppen Pseudomonas.','Fiskens bug får et kraftigt rødt skær fra overfyldte blodkar. Sygdommen kun kendt fra  barber.','Uhelbredelig. Angrebne fisk aflives.'),(13,'Gyrodactylus','Orme/Ikter','En 0,3-0,8 mm aflang snylter, Gyrodactylus, der med den ene ende sætter sig fast på fisken og den anden ende æder af hudens slimlag. Den føder levende unger, som straks efter fødslen sætter sig fast på fiskens hud ved siden af moderdyret.','Fisken hud og finner ser ud, som om de er overtrukket med et mælkeagtigt lag. Fisken er urolig og gnider sig mod planter og sten m.m.','Et 20 minutters bad i en kogsaltsopløsning – 10-15 g pr liter vand – får snylteren til at slippe fisken.'),(14,'Dactylogyrus','Orme/Ikter','En snylter meget lig Gyrodactylus, men som kun sætter sig på fiskens gæller. Den er æglæggende. Æggene klækker på 3-4 timer, og den fimrehårsbeklædte larve skal finde sig en vært inden for et par timer.','Fisken er urolig og gnider hovedet/gællerne mod planter og sten m.m.','Et 20 minutters bad i en kogsaltsopløsning – 10-15 g pr liter vand – får snylteren til at slippe fisken.'),(15,'Endetarmsorm','Orme/Ikter','Endetarmsormen, Camallanus lacustris, overføres med cyclops fra søer og vandhuller, der huser fisk, idet cyclops fungerer som mellemvært i endetarmsormens formering.','De grårøde ormeender hænger ud af gattet.','Ormen har forankret sig oppe i fiskens tarm ved hjælp af nogle modhager. Det hjælper derfor ikke at prøve at trække ormen ud med en pincet.\nI stedet gives en ormekur.Et par tabletter med piperazin eller mebendazol(Vermox) knuses og opløses i 10 ml vand.Foderet lægges i vandet, så det kan opsuge opløsningen, før det gives til fiskene.Der fodres med det medicinerede foder til ormene er væk, normalt en uges tid.'),(16,'Orm i øjet (Diplostomum volvens)','Orme/Ikter','Fiskene er en af to mellemværter for en ikte (Prolaria spathaseum), som lever i tarmen hos flere af vore svømmefugle. Iktens æg falder med fuglenes afføring ned i vandet, hvor de klækkes. Larven opsøger herefter en snegl, hvori den lever og til sidst udvikler en sporocyst, som efter en tid sender en antal gaffelhalecercarier ud i vandet. Vi kan få dem ind i akvarierne enten gennem en inficeret snegl eller sammen med levende foder som cyclops. Når gaffelhalecercarierne kommer ind i mundhulen eller i gællerne, borer de sig ind i fisken og finder vej til øjet, hvorved fisken blindes. I naturen vil det gøre fisken til et nemmere bytte for fuglene og kredsløbet er sluttet.','Fisken bliver blind på det ene øje eller på begge øjnene.','Uhelbredelig.'),(17,'Karpelus (Argulus)','Snyltende krebsdyr','Karpelusene kommer ind i akvariet sammen med det levende foder. De er sorte til mørkegrønne og er lette at kende fra cyclops, dels på grund af deres runde kropsform, dels på grund af deres jævne svømmemåde, som er helt anderledes end cyclops’  hoppende svømmefacon. Hvis foderet hældes i en hvid skål før fodringen, kan man derfor let få øje på dem og sortere dem fra.','Det op til 13 mm store krebsdyr kan ses på fisken, som svømmer uroligt rundt og forsøger at gnide dem af sig.','Fjernes fra fisken med en pincet, hvis de ikke slipper af sig selv.'),(18,'Ankerorm','Snyltende krebsdyr','Ankerormene fås ind i akvariet enten på nyindkøbte fisk eller sammen med det levende foder. Halvdelen af den op til 2 cm lange krop kan være boret ind i fisken.','Ses som 10-12 mm lange pinde, der stikker ud fra fisken. I den yderste ende er den forsynet med to ægsække, som vi kender det fra de nært beslægtede Cyclops.','På større fisk fjernes ankerormen forsigtigt med en pincet, og det efterladte sår pensles med euflavin. Snylteren kan forinden duppes med metrifonat, da det er lidt lettere at trække parasitten ud, når den er død.\n\nPå mindre fisk kan man prøve at klippe snylterens krop over med en saks og lade det være op til fisken selv at udstøde den døde kropsdel og hele det fremkomne sår.En behandling med euflavin kan måske forhindre infektioner.'),(19,'Artystone trysiba','Snyltende krebsdyr','Snylteren Artystone trysibia, en snyltende isopod med et interessant livsforløb. Se den efterfølgende artikel: Historie fra det virkelige liv.','Et hul på 1-2 mm i huden ved gællehulen. Ud fra hullet blafrer nogle \'hudlapper\'.','Ingen. Større fisk, som f.eks. diskosfisk, der af og til er påvist som vært for denne snylter, kan sandsynligvis overleve i flere år med snylteren inden i sig. Mindre fisk dør.');    

-- update `akvariet_dk_db2`.`plant` p SET p.scientificNameText = (SELECT concat_ws(' ', sn.genus, sn.species) FROM scientificname sn WHERE sn.id=p.scientificname_id);

-- update `akvariet_dk_db2`.`animal` a SET a.scientificNameText = (SELECT concat_ws(' ', sn.genus, sn.species) FROM scientificname sn WHERE sn.id=a.scientificname_id);

ALTER TABLE plant ADD FULLTEXT INDEX `plantFullTextscientificNameText` (`scientificNameText` ASC);
ALTER TABLE animal ADD FULLTEXT INDEX `animalFullTextscientificNameText` (`scientificNameText` ASC);

INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Flod med svag strøm', 1);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Flod med moderat til stærk strøm', 2);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Flod med rivende strøm', 3);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Stor flod eller sø med klippekyst', 4);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Stor flod eller sø med sandkyst', 5);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Skovvandløb ed sort vand', 6);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Skovvandløb ed klart vand', 7);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Bjergbæk med rivende strøm', 8);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Lavvandede og meget varme vandområder', 9);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Lavvandede og meget varme vandområder - Sumpe', 13);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Lavvandede og meget varme vandområder - Rismarker', 14);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Lavvandede og meget varme vandområder - Savannevandløb', 15);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Underjordiske huler', 10);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Brakvandsområder', 11);
INSERT INTO `akvariet_dk_db2`.`habitat` (`Text`, BiotopAkvarietId) VALUES ('Tidevandsområder', 12);

update akvariet_dk_db2.habitat SET Description=
'Dette biotop findes i bugter, vige og sidevande. I selve flodens hovedløb er der altid en mærkbar strøm, og det er ofte fisk, der bliver alt for store til private akvarier, der lever her.<br />
For at finde de steder, hvor der er meget svag eller næsten ingen strøm, må vi fra hovedløbet søge ind mod bredderne. Skjult af en tæt bevoksning, kan der findes nogle små sidekanaler, der ofte kun fører nogle få meter ind til nogle sumpagtige, ret lavvandede sidevande. Disse er som regel kun forbundet med selve floden ved kanalerne og står ofte i forbindelse med nærliggende sumpe. Der er således kun en meget svag gennemstrømning af disse sidevande, men gennemstrømningen kan dog blive forstærket noget af tilløbende skovvandløb. <br />
Hvis man kommer sejlende ude på floden ses disse sidevande kun som bevoksede områder med en stedvis meget kraftig sivagtig bevoksning, men kommer man ind i dem, opdager man hurtigt, at de må være et  paradis for mindre fisk. Vandet er klart med en lige netop mærkbar strøm og træerne, der nogle steder vokser helt ude i vandet, har kraftige rodnet, der sammen med nedfaldne grene danner fortrinlige skjulesteder for de mange mindre fisk. <br />
Bundlaget er ret blødt og nedfaldne blade danner et tykt tæppe på bunden. Kun de steder hvor et tilløb forøger strømmen kan bundens finc sand skimtes. Større sten ses kun sjældent i bladlaget, men f.eks. hvor en kilde fra skoven løber ud og danner et område med en noget kraftig strøm, ser man hvordan det fine sand er blandet op med noget grovere grus, og en del mindre sten, der er afrundede af strømmen, ses også. <br />
Vandet er ret varmt i disse forholdsvis lavvandede sidevande og en gennemsnitstemperatur på mellem 26°C og 29°C er ikke ualmindeligt. <br />
Der er en ret kraftig plantevækst i dette habitat. <br />
Mellem rødderne og rodende i den bløde bund ses maller, medens stimer af ungfisk hurtigt bevæger sig fra plantetykning til plantetykning på jagt efter foder og beskyttelse. Oppe ved overfladen står de insektædende fisk stille og venter på at et insekt skal falde ned i vandet, og de mange små ringe på vandoverfladen viser, at det er noget, der sker uafbrudt.<br />
Dette tætbeplantede biotop med den kun ret ringe strøm kan også findes i en stor del af flodens bugter og vige. Her inde i en sådan bugt mærkes hovedflodens kraftige strøm næsten ikke, og selvom breddens bevoksning af store træer giver skygge for en del af bugten, så er vandets temperatur ofte mellem 28°C og 30°C. Plantevæksten er også her meget kraftig, og selvom der her ofte er tale om sivagtige planter og flydeplanter, der kan dække store dele af bugten, så finder vi også mange akvarieegnede planter. <br />
Bunden består af sand og/ eller mudder, hvor der i de fleste tilfælde er en del nedfaldne grene og blade.<br />
I dette biotop ses ofte store stimer af vore bedst kendte akvariefisk, altså forholdsvis små fisk, der her kan finde foder i form af bløddyr, vandinsekter og andre insekter, der falder ned i vandet fra de på bredden voksende træer og buske. <br />
Alle, der har rejst på troperries floder, kender de sand- og mudderbanker, der af strømmen dannes et stykke fra bredden. Mellem disse banker og bredden opstår et strømroligt område, hvor plantevæksten kan blive meget kraftig. Det er ofte bregnearter og ret finløvede planter, der først får fodfæste her, og de danner i løbet af kort tid et godt miljø for forskellige maller og andre fisk, der hovedsageligt lever af de bløddyr, der hurtigt vil findes på disse banker. <br />'
WHERE BiotopAkvarietId=1;

update akvariet_dk_db2.habitat SET Description=
'
Biotoper med den moderate til stærke strøm, altså en tydelig mærkbar strøm, der dog ikke er så kraftig, at den river alt med sig, finder vi mange steder i så godt som alle floder i den tropiske verden. Det er ofte i selve flodens hovedløb, vi skal finde dette biotop.<br />
Vi finder selvfølgelig floder, der strømmer gennem frugtbar jord, hvor træbevoksningen er fældet for mange år siden, men mange af vore akvariefisk kommer dog fra tropiske egne, hvor floderne stadig løber gennem jungle og urskov, som kun har forandret sig meget lidt i de sidste 70-80 år. Her står træbevoksningen helt ned til flodbredderne, og i regntiden stiger vandet og oversvømmer det omliggende land, hvorved træerne kommer til at stå ude i selve floden. Rødder og grene danner et vildnis, som kan gøre det til en meget vanskelig sag at fange de fisk, der søger ly her.<br />
Plantevæksten er mere sparsom her end i det foregående biotop, men uden planter er biotoper så absolut ikke, og der hvor et væltet træ, et par store sten eller en sand banke giver noget læ for den værste strøm, kan der være en forholdsvis tæt bevoksning. <br />
Hvis vi forestiller os, at vi i en båd langsomt driver ned ad en af de  store floder, vil vi, ved at tage fangstprøver forskellige steder i floden, hurtigt opdage, at her i selve flodens hovedløb er der betydeligt færre af de fisk, vi normalt betragter som akvariefisk. Selvfølgelig går der mange mindre arter inde mellem trærødderne og den spredte beplantning, men størstedelen af de fisk, der fanges bare nogle få meter ude fra bredden er større fisk, som f.eks. større cichlider, maller, piratfisk og andre af en ret betragtelig størrelse.<br />
Vi skal altså helt ind til bredden for at finde de rette forhold for de mange akvarieegnede fisk, der findes i dette biotop, og vi vil snart opdage, at uanset om vi befinder os i Afrika, Asien eller Sydamerika så er der træer og buske, der enten står helt ude i vandet eller i det mindste stikker rødderne ud i det.<br />
Undervandsplanter af forskellige arter skyder op fra bunden mellem rødderne og de mindre fiskearter ses pilende ind og ud mellem dem i deres evige jagt efter vandinsekter og andre foderemner.<br />
Nogle steder er der ret tomt for mindre fisk, og det ses tydeligt, at disse undgår at komme alt for tæt på disse steder. En nærmere undersøgelse afslører ofte, at et par cichlider eller andre revirdannende fisk har taget dette område i besiddelse og jager alle indtrængende bort. Man ser således ofte i diskusområderne i Sydamerika discusfisk stå inde langs bredden i det halvmørke, der findes under væltede træer og udhængende rødder. Andre steder i Sydamerika er denne plads i biotoper overtaget af Pterophyllum arter (scalare), Cichlasoma severum, C.festivum (flag­cichliden), forskellige Crenicichla arter (geddeligende cichlider) samt nogle pirayaarter.<br />
I Afrika er det også cichlider samt forskellige geddeligende rovfisk som f.eks. Lepisosteus productos, der har denne plads, medens det i Asien udover geddelignende rovfisk ofte er større labyrintfisk.<br />
Forholdene strækker sig selvfølgelig fra den rene klippegrund over grusede og sandede flodbunde til de bløde og mudrede bunde.<br />
'
WHERE BiotopAkvarietId=2;

update akvariet_dk_db2.habitat SET Description=
'
Dette biotop finder vi først og fremmest i bjerglandskaber, hvor store højdeforskelle giver vandmasserne fart, og slugter samt snævre floddale yderligere tvinger vandets fart op.<br />
I floder med meget stærk strøm har nogle fisk tilpasset sig de specielle forhold ved at udvikle en eminent svømmefærdighed, medens andre har tilpasset sig dette biotop ved udvikling af panserbeklædning eller sugeanordninger, hvormed de kan holde sig fast på sten og klipper i selv de værste strømhvirvler.<br />
Blandt fiskene, der klarer sig i de stærkest strømmende floder ved deres svømmefærdighed, er der arter indenfor bl.a. Barbus, Rasbora, Brachydanio og nogle karpelaks, medens de pansrede maller af mange forskellige slægter bliver beskyttet af deres panser mod skader, de kunne pådrage sig i de stenfyldte hvirvler. Panserets udformning hjælper dem også til bedre at kunne holde sig nede på bunden, hvor mange arter gnaver alger af sten og klippeflader. Af fisk med specielle sugeanordninger er der flere maller, men også karpefiskene er repræsenteret med den af alle akvarister kendte algeæder, der ofte fejlagtigt kaldes sugemalle.<br />
I Afrika har en del cichlider tilpasset sig livet i de stærkt strømmende floder. De er alle langstrakte og mangler helt eller delvis svømmeblære, hvilket tydeligt ses af deres svømmemåde. De ligger stille på bunden, for så pludselig i små eller store sæt at fare afsted, indtil de atter lader sig synke ned til bunden, hvor de ligger, som om de samler kræfter til endnu en fremrykning. Slægter som Steatocranus, Teleogramma og Leptotilapia findes ofte i stenfyldte vandløb med en meget kraftig strøm.<br />
Planter ses kun meget sjældent i disse vandløb, og på de steder, hvor der er noget plantevækst, er den meget sporadisk og består ofte af Vallisneria arter. En meget populær plante som Anubias findes i Afrika ofte i dette biotop, hvor den hæfter sig fast mellem stenene.<br />
'
WHERE BiotopAkvarietId=3;

update akvariet_dk_db2.habitat SET Description=
'
Hvis man midt i tresserne var interesseret i at holde cichlider, ja så var man faktisk "tvunget" til at indrette sit akvarium med sten, og man fik at vide, at planter var det umuligt at holde sammen med disse store fisk. De gravede planterne op eller bed dem simpelthen i stykker. Man følte sig tvunget til at bygge hele klippelandskaber op med renskurede sten, og den mindste algevækst blev betragtet som et tydeligt tegn på dovenskab hos akvaristen.<br />
Der er heldigvis sket store ting på det område siden da, man ved nu bl.a. fra naturfotos fra Malawi- og Tanganyikasøen, at algevæksten er meget kraftig disse steder og på det nærmeste er livsnødvendig for en stor del af de fisk, vi får fra disse søer. En kraftig algevækst i klippekystakvariet er altså et gode, og ud over at være foder og opholdssted for foderorganismer for mange af vore akvariefisk erstatter algerne også i nogen grad planterne i disses omsætning af affaldsstofferne i akvariet.<br />
Når vi nu skal til at se på biotoper med klipper vil det være naturligt at se på de store søer i Afrika, især Malawi- og Tanganyikasøen. Klippekyster findes selvfølgelig også i Asien og Sydamerika i såvel søer som i de fleste af de mange floder i disse verdensdele, men de sidste årtiers fantastiske interesse for cichlider fra de to søer ligefrem kræver en central placering af disse søer i behandlingen af biotoperne klippekyst og sandkyst.<br />
Begge disse søer ligger i bjerglandskaber, og sammen med Lake Rudolf, Lake Albert, Lake Edward og Lake Kiwu udgør de et søsystem i den store østafrikanske gravsænkning Rift Valley. Der er såvel i Malawi-, som Tanganyikasøen tale om fire ret tydeligt adskilt biotoper, nemlig klippekysten. sandkysten. overgangszonen og sumpzonen ved flodmundinger.<br />
Planterne er et meget sjældent syn ved klippekysten, og de få steder de findes, drejer det sig næsten udelukkende om slægterne Vallisneria og Ceratophyllum.<br />
'
WHERE BiotopAkvarietId=4;

update akvariet_dk_db2.habitat SET Description=
'
En tydelig skelnen mellem overgangszonen og sandbunden er ofte svær, da overgangszonen jo består af sand med større og mindre sten. Derfor vil også en del af fiskene fra sandkysten være at finde i overgangszonen og omvendt.<br />
Sandkysten er vel en af de almindeligste kystformer i de fleste søer og floder og består almindeligvis af mere eller mindre bløde bundformer, fra det meget bløde, næsten mudrede, til den helt rene sandbund, hvor sandkornene kan være kittet sammen til ret hårde flader.<br />
Biotoper findes især i søer og meget store floder. Her strækker sandbunden sig ofte over store strækninger kun afbrudt af spredte, mindre stenområder og grupper eller småtykninger af undervandsplanter.<br />
Der er mange forskellige plantearter repræsenteret i floder og søer med ren sandbund.<br />
'
WHERE BiotopAkvarietId=5;

update akvariet_dk_db2.habitat SET Description=
'
Vandløbene med sortvand, hvilket vil sige brunt til meget mørkebrunt vand, er hovedsagelig omtalt i litteraturen om de sydamerikanske floder og skovvandløb, men de findes også i mere eller mindre udpræget grad i Afrika og Asien. Disse vandløb fører oftest vand fra sumpe med surt humusholdigt vand til de større floder. Vandet er som regel meget brunt, men også meget klart uden grums, og det er altid surt. Alt eftersom det er tør- eller regntid, og selvfølgelig efter bundbeskaffenhed, viser målinger pH-værdier på mellem 4 og 5, medens hårdheden er under I DH.<br />
Dette vand er det selvfølgelig muligt at lave i vore akvarier, men da der er mange andre faktorer end pH og DH, der spiller ind, må vi anbefale, at akvaristen sætter sig meget nøje ind i hele vandproblematikken i speciallitteraturen, før han kaster sig ud i den rene »vandalkymisme«. En ting der taler imod at man laver alt for ekstreme vandværdier i sine akvarier er, at langt de fleste fisk fra blødvandsområderne klarer sig meget fint i vand, der er betydeligt mindre surt og betydeligt hårdere, end det vand vi ved de går i i naturen.<br />
I regnskoven danner træernes kroner et næsten ugennemtrængeligt dække for solen, og nede på skovbunden hersker der et evigt halvmørke.<br />
Dette danner et helt specielt biotop, med ingen eller kun meget få specielle planter og relativt få fiskearter. Sammen med træernes rødder danner nedfaldne grene ofte et for større fisk ugennemtrængeligt vildnis, og fiskene, der lever i disse vandløb, er da også små, men de findes til gengæld i forbavsende mængder.<br />
Bundlaget i disse vandløb er sædvanligvis sand eller mudder og er ofte dækket af et tæppe af nedfaldne blade. Disse og andre rådnende plantedele er, sammen med de humusforbindelser, der vaskes ud af bredderne, med til at give vandet dets farve. Vandplanter findes så godt som ikke i de sydamerikanske vandløb af denne art, medens en del af de asiatiske i nogen grad kan være bevokset af forskellige kun lidt lyskrævende Cryptocotyne arter.<br />
Som nævnt danner træernes kroner et næsten ugennemtrængeligt dække for solen, og det betyder selvfølgelig, at vandets temperatur er noget lavere end i vandløb, hvor solen har frit spil. Det viser sig da også, at de fleste af disse skovbække har en gennemsnitstemperatur på mellem 22 og 23°C.<br />
I tørtiden er strømmen i disse vandløb forholdsvis moderat, medens den i regntiden bliver noget kraftigere, men uanset hvornår, er der altid en tydelig mærkbar strøm, hvor man kan iagttage stimer af fisk stå lige under vandoverfladen med hovedet mod strømmen for at snappe insekter og andre foderemner, der føres forbi af strømmen.<br />
'
WHERE BiotopAkvarietId=6;

update akvariet_dk_db2.habitat SET Description=
'
Overalt i tropernes skovvandløb, der fører helt klart vand. Disse vandløb løber også gennem skove af mange forskellige typer, men såvel ved vandløbets udspring som i selve vandløbet gør bundforholdene, at indholdet af humusstoffer er betydeligt mindre end i de førnævnte sortvands-skov­vandløb, og vandet er her klart til svagt gulligt.<br />
Selvom skoven er tæt, og solen kun få steder når ned til skovbunden, er der alligevel i denne vandtype noget mere plantevækst, og der, hvor skovbækken løber gennem en lysning, vil man ofte se en relativ kraftig plantevækst.<br />
Strømforholdene er selvfølgelig meget varieret fra den rivende strøm, hvor bækken baner sig vej ned gennem en smal klippefyldt slugt til den dovent flydende strøm, der snegler sig gennem et fladt og sumpet skovområde og her får lov til at brede sig ud til den ti-dobbelte bredde.<br />
I disse skovbække er der repræsenteret mange forskellige bundforhold, der dækker alt fra den bløde mudrede bund, hvor maller af mange slægter holder til, over den sandede bund til klippebunden, hvor sandfyldte huller giver grobund for små eller store plantegrupper.<br />
Vandet i de klare skovbække er som regel surt men dog ikke i så udpræget grad som i sortvandsbækkene. De mange målinger, der er foretaget i de forskellige verdensdele, er selvfølgelig vidt forskellige, men som et gennemsnit kan man sige, at pH i disse klarvandsbække ligger mellem 5 og 6. Temperaturen ligger mellem 22 og 24°C og hårdheden mellem I og 2 DH men er ofte under I. Det er altså stadig blødt vand men lidt hårdere og knap så surt som i sortvandsvandløbene.<br />
Klarvandsbækken i regnskoven er ikke, hvad breddernes bevoksning angår, så forskellig fra sortvandsbækken. Også her danner trærødder og nedfaldne grene gode skjulesteder for mindre fisk, og også her er der på mere strømrolige steder et tykt lag blade i bunden. Medens der i sortvandsbækken kun lever meget få vandinsekter. er der i klarvandsbækken et insekdiv, der giver fiskene betydelig bedre livsbetingelser. Man vil da også se, at antallet af arter og slægter i klarvandsbækken langt overgår antallet i sortvandsbækken, og dette er hovedsageligt på grund af de mange insekter, der udgør størstedelen af de små fisks ernæring.<br />
I det klare skovvandløb er vegetationen sjældent meget kraftig, når man lige undtager de steder, hvor det løber gennem en lysning. Og alligevel er der en hel del plantearter repræsenteret.<br />
'
WHERE BiotopAkvarietId=7;

update akvariet_dk_db2.habitat SET Description=
'
Efter regnskovens trykkende varme og halvmørke skal vi nu til et helt andet naturområde. Vi skal op i bjergene, hvor vinden frisker lidt op, og hvor lyset er stærkt. Oppe mellem bjergets forholdsvis lave bevoksning finder vi det næste, meget karakteristiske biotop, nemlig bjergbækken. På afstand kan man høre, at den pludrende og klukkende baner sig vej gennem sit leje, men først når man er helt henne ved bredden, opdager man det tre til fire meter brede vandløb.<br />
Et seks til otte meter bredt bånd af store og små klippeblokke snor sig ned ad bjergsiden, og i dette løber vor bjergbæk. Her, hvor vi står og ser ned langs bækken, løber den først et stykke med en bredde af tre til fire meter, men lidt længere nede deles den op i fem til seks mindre løb af nogle store sten, der rager op over vandoverfladen. I det brede løb er strømmen ret hurtig, men der hvor stenene indsnævrer løbet, er der en rivende strøm, hvor man i første omgang ikke tror, der kan være fisk.<br />
Ved et nærmere eftersyn ser man dog, at der er nogle lynhurtige maller, der suger sig fast på sten og klippeflader, for så pludselig med kraftige og lynhurtige svømmebevægelser at bevæge sig mod strømmen i hop på 20-30 cm ad gangen. Så ligger de igen stille eller bevæger sig langsomt hen over stenfladen, medens de med deres specielle munddele ligefrem rasper al gerne af stenene. Disse maller, fra flere forskellige slægter, findes overalt i troperne, og i de sidste år er et stadig større antal arter dukket op i akvariehandelen.<br />
Andre steder i bækken, hvor store klippeblokke og sten danner områder, hvor strømmen er knap så rivende, ses andre fisketyper, der er knap så specialiserede. Nu er det langstrakte, slanke og torpedoformede fisk, hvis svømmefærdigheder tillader dem at klare sig i den stærke strøm. Danioarterne er med deres slanke kropsbygning og lynhurtige bevægelser typiske repræsentanter for disse fisk, men også indenfor andre karpefisk og karpelaks er der arter, der har tilpasset sig dette specielle biotop.<br />
Hvis vi nu skal se lidt på selve bækken, opdager vi straks, at det fine grus eller sand, som vi ellers har set i mange forskellige biotoper, fuldstændig mangler her. Bunden består af større eller mindre sten, og kun hvor der er et udtalt strømlæ, ser man grus, og her har det en kornstørrelse som hasselnødder. Det er komplet uegnet som plantebundlag, og alle næringsstoffer vaskes også væk af den rivende strøm, så det kan ikke undre nogen, at der i dette biotop kun er meget få planter.<br />
De står i små klynger på de mest strømstille steder, der hvor bækken breder sig ud på et forholdsvis jævnt stykke. Vi ser da, at den allesteds nærværende Vallisneria (sammen med javabregnen i en del af de asiatiske bjergbække) er en af de eneste planter, der kan klare sigher. Selv de mest strømstille steder i disse vandløb har en betydelig strøm, og Vallisneria\'ernes lange blade ligger udstrakt i hele deres længde i strømretningen.<br />
Udover mangelen på fint grus og planter er dette biotop særegent ved sin ringe vanddybde. Bundlagets hårde stenflader lader sig ikke udhule som skovvandløbenes bløde bund, og en vandhøjde på 20 til 60 cm er meget almindelig. Temperaturen ligger gennemsnitlig på omkring 22-23°C, og som flertallet af tropernes vandløb er også bjergbækkens vand ret blødt, selvom en hårdhed på 2-6 DH jo ligger langtfra, hvad vi ellers er stødt på i de tidligere behandlede biotoper.<br />
Stene i bækkens leje er alle afrundede af vandets løb igennem årtusinder, og man ser ingen steder sten med skarpe kanter. <br />
'
WHERE BiotopAkvarietId=8;

update akvariet_dk_db2.habitat SET Description=
'
Lavvandede og varme vandområder dækker et bredt udsnit af forskelligartede vandområder. Der er her tale om vandområder som sumpe, savannevandløb og rismarker samt damme, og de har følgende fællestræk.<br />
<ul>
<li>De er forholdsvis lavvandede.</li>
<li>Solen bager ned over dem, og der er sjældent bredbevoksning af større skyggegivende karakter.</li>
<li>Der er kun meget ringe strøm.</li>
</ul>
Et eller flere af disse træk i kombination giver nogle helt specielle forhold i vandet, og mange af de fisk, der lever her, har ganske specielle måder at klare disse forhold på.<br />
Solens bagen ned på det lavvandede område betyder, at vandet bliver meget varmt, og det betyder yderligere, at vandets indhold af ilt bliver meget ringe. Dette fænomen er velkendt for de fleste mennesker, der i en rigtig varm sommer kommer til et gadekær. Her ses ofte fisk, der gællende står i vandoverfladen og "patter" luft. Solen har, i forbindelse med den ringe vandgennemstrømning, drevet ilten ud af det unormalt varme vand, og fiskene, der ikke er indrettet til at klare sig med den ekstremt lille mængde ilt, kommer i nød og dør, hvis der ikke indtræffer vejrforandring.<br />
I tropernes sumpe, damme og andre lavvandede områder sker det samme selvfølgelig, og her sker det næsten uafbrudt i den varme tid. En del fiskefamilier har derfor gennem tiderne udviklet forskellige måder at overleve på. De har dannet alternative åndedrætsorganer, som giver fiskene mulighed for at supplere vandets iltindhold med luft hentet over vandoverfladen.<br />
De bedst kendte eksempler på disse fisk er labyrimfiskene, der tæller velkendte akvarieslægter som Colisa, Trichogaster, Trichopsis (alle guramier), Betta (kampfisk) og Ctcnoporna (afrikansk labyrimfisk), men også maller som Corydoras, Clarias og Callichthys kan nævnes.<br />
Da de her omtalte vandløb er ret lavvandede, er der ret stor sandsynlighed for, at de i den tørre tid tørrer noget ind eller helt forsvinder, og det er da også således, at mange fisk dør af denne grund. Dog hjælper mange fisks instinkter dem ofte til at søge ud i vandområder. hvor faren for total indtørring er mindst.<br />
'
WHERE BiotopAkvarietId=9;

update akvariet_dk_db2.habitat SET Description=
'
Vi skal ned i jorden, nærmere forklaret til de underjordiske vandløb og vandfyldte huler. Sådanne huler findes mange steder i verden, men Nord- og Mellemamerika skiller sig ud fra den øvrige verden ved at kunne fremvise en lang række fisk, der er mere eller mindre specialiserede til de ekstreme forhold, der findes her nede dybt under jorden, medens de andre verdensdele kun har kunnet fremvise ret få arter.<br />
Alle de fundne arter har meget nærstående slægtninge oppe i solen, og der er repræsenteret fisk fra mange forskellige familier. Mallerne er selvfølgelig i overtal, hvilket vel må siges at være ret forståeligt, da mange af disse i forvejen er »rnørkedyr«, der er mest aktive efter solnedgang. Mange slægter fra denne familie er yderligere udstyret med antenner og føletråde, der hjælper dem med at orientere sig i mørket.<br />
Ud over maller er der imidlertid også karpefisk (Cyprinidae), cichlider (Cichlidae) og karpe laks (Characidae) samt en del egentlige hulefisk af flere forskellige slægter. <br />
Hos mange af disse hulelevende fisk er de morfologiske ændringer ikke store, de har svagere syn, men til gengæld er deres andre sanser, f.eks. sidelinjesystemet og eventuelle antenner og føletråde kraftigere udviklet. De har som regel mindre farvepigmentering i huden, men dette er meget forskelligt. Hos de "ægte" hulefisk er der derimod tale om større og vidtrækkende morfologiske forandringer.<br />
Der findes flere af disse "ægte" hulefisk«, hvoraf vi kan nævne Typhlichthys subterranus, Stygicola dentatus, Ambliyopsis spelaeus, Typhlosynbranchius kronei samt selvfølgelig den bedst kendte, Anoptichthys jordani (blind hulefisk), men bortset fra sidstnævnte ses de aldrig i private akvarier.<br />
Anoptichthys jordani har helt tilpasset sig forholdene i de underjordiske huler. Her nede i mørket er synet ingen nytte til, og hos den blinde hulefisk er synsevnen da også helt forsvundet. Af selve øjenanlægget er der kun rudimenter tilbage. Hos fisk, der lever i lys, har farverne en stor betydning i artens sociale liv, men her nede i mørket er disse helt uden betydning. Den blinde hulefisk har ingen farvepigmentering, den er nærmest rødliggul (kødfarvet) med et ganske svagt sølvskær. Den bliver ca.8 cm stor, og hannen er noget slankere end hunnen.<br />
Til gengæld for det manglende syn har fisken et ualmindeligt veludviklet sidelinjesystem, og igennem dette nervesystem kan den blinde hulefisk opfatte sine omgivelser. Fisken danner ved sin svømning gennem vandet en "bølge" foran sig, og denne "bølge" kastes tilbage mod fisken af alle genstande, hvad enten det nu drejer som om en anden fisk, en stenflade eller noget helt andet. Den blinde hulefisk er så godt som aldrig i ro, den svømmer uroligt og ret hvileløst rundt i snørklede figurer, og dette giver fisken det fulde udbytte af det højt udviklede sidelinjesystem, thi kun i bevægelse kan den skabe den bølge, hvis ekko kan give den besked om forhindringer, foder og fjender. Ud over dette sidelinjesystem har den blinde hulefisk også en meget veludviklet lugtesans, der især hjælper under fødesøgning og vel også i dens sociale liv med andre artsfæller.<br />
'
WHERE BiotopAkvarietId=10;

update akvariet_dk_db2.habitat SET Description=
'
Der, hvor en flod løber ud i havet, opstår der et brakvandsområde, og tidevandet øger yderligere dette ved at føre saltholdigt vand langt op i floden ved højvande. Det salte vand trænges så ved lavvande igen tilbage mod havet, og flodens ferskvand får nu overtaget. Man skulle tro, at dette skiftende saltindhold i vandet måtte være umuligt at leve i for fisk, men alligevel ser vi, at udover nogle forholdsvis få arter, der helt har specialiseret sig til at leve under disse ekstreme forhold, så findes der også mange af de fisk, vi igennem vor akvariehobby ellers mest kender som ferskvandsfisk, her i brakvandet med et større eller mindre saltindhold.<br />
Hvis vi tænker os, at vi kommer sejlende ude fra havet op ad en af de store tropiske floder, vil vi straks bemærke, at selv ikke helt inde ved bredden er der spor af de vandplanter, vi havde forventet at finde. Vi er kommet ind ved lavvande, og noget af den eneste beplantning vi ser, ligemeget hvor vi ser hen, er nogle 6-7 meter høje buskagtige bevoksninger, hvor de nederste blade sidder et par meter over vandspejlet. Af en, der kender området, får vi at vide, at denne busk, der danner mangrovekrattet, er af slægten Rhizophora. Den er noget af det eneste, der kan vokse her under de skrappe forhold med skiftende salt- og ferskvand. Han fortæller også, at bladene sidder lige over det sted, hvor vandstanden er ved højvande. Ud over et sammenfiltret system af rødder, der alle er lange, ret lige og tynde, ser vi også, at krattet skyder et utal af luftrødder nede fra den mudrede bund lige op i luften. Ved højvande er alle disse rødder selvfølgelig helt under vand, og man behøver ikke megen fantasi for at kunne forestille sig det liv, der så vil være her mellem rødderne i mangroven s spændende, men ikke overvældende arts rige fauna.<br />
I nogle af de store tropiske floder kan højvandet mærkes mange, ja, i en del tilfælde mere end 20 km op i floden, og udover at vandstanden stiger, betyder det jo også, at brakvand føres op i floden. Dette betyder også, at plantevæksten kan være meget fattig langt op i floden, og kun gradvis tager til, efterhånden som vandets saltindhold bliver mindre og mindre. En af de første vandplanter vi møder oppe ad floden er Bacoba monnieri, og da den vokser i hele den tropiske zone, er det altså underordnet, om vi bruger den i vort brakvandsakvarium fra Afrika, Asien eller Sydamerika. Cryptocoryne ciliata og Microsorium pteropus (javabregnen) vokser udelukkende i Asien, og bør derfor kun bruges i akvarier fra denne verdensdel. Af andre planter, der skulle kunne klare saltkoncentrationer på 3-5%, nævnes i den meget sparsomme litteratur om emnet kun Vallisneria gigantea.<br />
Helt ude ved kysten er mangrovesumpen stærkest påvirket af tidevandet, og der kan være flere meters forskel på høj- og lavvande.<br />
Bunden består af meget finkornet slam, og når vandet efter højvande trækker sig tilbage, er rødder og sten dækket af et tyndt gråligt lag. I dette slam lever myriader af ganske små insekter, som udgør en stor del af føden for de fisk, der holder til her.<br />
Af fiskeslægter der hovedsageligt forekommer i brakvand kan særligt nævnes Brachygobius, Scatophagus, Monodactylus, Eleotris, Gobius, Toxotes, Stigmatogobius og Etroplus, medens bl.a. de følgende slægter har en eller flere arter, der ofte findes i brakvand. Poecilia, Ciehlasoma, Geophagus, Aplocheilichthys, Epyplatys, Hemiehromis, Pelmatochrornis, Tilapia, Pseudocorynopoma, Anableps og Dermogenys.<br />
'
WHERE BiotopAkvarietId=11;

update akvariet_dk_db2.habitat SET Description=
'
Den, der har opholdt sig på en strand, hvor tidevandet er mærkbart, har vist næppe kunnet undgå at blive fascineret af det liv, der ved lavvande kommer til syne mellem strandens sten. Der er krabber, som er ved at grave sig ned, der er krebsdyr og fisk, der er blevet fanget i en lille pyt uden mulighed for at komme væk, før højvandet igen indtræder, og der er dyr, som må dø, fordi de blev fanget på steder, hvor vandet helt forsvinder. Alt dette kan ses på en ganske almindelig nordeuropæisk strand, og det samme sker selvfølgelig i troperne, her dog med den forskel, at artsrigdommen er endnu større, og farverne på dyrene er mere spraglede.<br />
I brakvandsbiotopen behandlede vi brakvandsområdet, hvor tidevandets stigen og falden bragte mere eller mindre saltholdigt vand til biotoper. Det samme sker her, ja, man kan faktisk sige, at det er det samme biotop, vi taler om, dog med den forskel, at vi nu går så langt ind til bredden, at vi også behandler det tørlagte område.<br />
Den flodbred, vi her vil beskrive, har en jævnt opadskrånende sand og grus strand med en del sten og noget mudder. Tidevandets bevægelse er her på dette sted omkring en halv meter. Vandet når ved højvande op til det forkrøblede buskads, der vokser på bredden, medens der ved lavvande er frilagt en strandbred på et par meter.<br />
Ved lavvande er der et leben på dette stykke fugtige strand, og mangrovekrabber (Scylla serrata) findes i stort antal. Af fisk er det især forskellige kutlingearter, der er almindelige, og specielt dyndspringeren (Periopthalmus barbarus) er der mange af. De ligger overalt nede ved vandkanten, og hvis der er noget, der forskrækker dem, ser man dem lynhurtigt skøjte hen over vandet til et mere sikkert sted. Disse fisk er, med deres særlige adfærd, noget af det mest spændende, vi finder i tidevandsområdet, men også vinkekrabben (Uca gelasimus tangeri) med den kolossale klo er et særpræget syn, ligesom mange af de andre farvestrålende krabber og eremitkrebs.<br />
'
WHERE BiotopAkvarietId=12;

update akvariet_dk_db2.habitat SET Description=
'
I forbindelse med mange tropiske floder findes der ofte store, udstrakte sumpområder. Der kan både være tale om lavvandede sidevande til floderne, men også om store sumpe, der ligger langt fra floderne og afvandes af mindre vandløb.<br />
I disse vandområder, der kan være umådeligt store, er vandbevægelsen ofte meget ringe, og da bevoksningen oftest kun består af lave buskarter, er der ikke megen skygge at finde. Solen skinner ned på vandet med fuld kraft og giver mulighed for et overdådigt planteliv. Det er i dette biotop, vi har den kraftigste og mest overdådige plantevækst. Mange af de planter vi med stor succes bruger i vore akvarier er sump­planter, og i naturen vokser mange af dem en stor del af året over vandet.<br />
Plantevæksten danner visse steder uigennemtrængelige partier, medens der andre steder kan være et tæppe af overfladeplanter, som dækker så meget for solens stråler, at ingen undervandsvegetation kan klare sig nedenunder.<br />
Vandet er meget varmt, helt op til 3 5-4o°C og i løbet af tørtiden tørrer store områder ud. Nogle fisk klarer sig gennem tørtiden ved at grave sig ned i bundens mudder (Protopterus), medens andre (Anabas, Callichthys og Doras) om natten kan mave sig gennem det fugtige græs til et andet vandhul, der måske ikke indtørrer så hurtigt. Andre dør, fordi de ikke slipper ud til vandområder, der ikke tørrer ud.<br />
Der er dog en meget stor fiskegruppe, nemlig de såkaldte killies, der klarer sig på en helt speciel måde. Disse fisk, de æglæggende tandkarper, er såkaldte sæsonfisk, og de har en meget hurtig livscyklus. Når vandet efter en tørtid igen stiger op i sumpen, sætter det gang i nogle æg, der i hele tørtiden har ligget i en slags dvale eller hviletilstand i bundlaget. De små fiskelarver udvikler sig nu til fisk, der vokser forbløffende hurtigt.<br />
Det er endog så snildt indrettet, at det første vand, der måske kun er en enlig og for tidlig regnskylle, kun sætter gang i nogle af æggenes udvikling, medens andre først begynder deres udvikling på et senere tidspunkt. Herved undgår en stor del af de nyudklækkede unger døden, hvis vandet igen skulle forsvinde, inden den rigtige regntid sætter ind. I løbet af ret kort tid vokser ungerne sig store og bliver kønsmodne. De når dette, samt at finde partnere og forplante sig i de måneder, der går, før sumpen igen tørrer ud. Deres æg ligger så og venter på den næste regntid. Af slægter indenfor de æglæggende tandkarper kan især nævnes Aphyosemion, Epipiatys, Cynolebias, Rivulus, Fundulus og Aplocheilus.<br />
Udover tandkarperne er der i sumpen også andre interessante og spændende fisk. Vi har nævnt labyrintfiskene, hvis specielle åndedrætsorgan giver dem mulighed for at klare sig i sumpenes til tider meget iltfattige vand ved at supplere med atmosfærisk luft. Dette er en morsom ting at iagttage i akvariet, men udover dette har disse fisk også en meget spændende yngleadfærd, idet en stor del af dem bygger en rede i vandoverfladen af luftfyldte spytbobler, og i denne har de såvel æg som unger, som de beskytter mod andre fisk.<br />
Bundlaget i sumpen består af mudder, der er dækket af et tykt lag forrådnede plantedele, og vandet er ofte meget uklart. Det er blødt og surt med en pH på mellem 4 og 6.<br />
Se også beskrivelsen af <b>Lavvandede og meget varme vandområder</b>
'
WHERE BiotopAkvarietId=13;

update akvariet_dk_db2.habitat SET Description=
'
Når man på billeder fra østen ser rismarkerne ligge i terrasser op ad bjergsiderne, tænker man vist ikke på fisk og da slet ikke på akvariefisk. Og dog er det alligevel et faktum, at mange af de fisk, vi har i vore akvarier, findes under vandet på disse oversvømmede marker. Når man ser lidt nærmere på sagen, kan man bedre forstå, at der selvfølgelig må være fisk i disse vandområder, for andet er det jo ikke. I flere måneder om året står disse marker under vand og udgør nogle meget fine og fodermæssigt meget rige biotoper for småfisk.<br />
Vandet ledes ind på markerne ad kanalsystemer , og ad disse kanaler kommer også fiskene fra floder, søer og andre vandområder for at fordele sig ud over markerne. Når vandet så senere på året igen ledes ud i kanalsystemet, og ad dette til de føromtalte vandområder, slipper den største del af fiskene med ud, men nogle fanges af den synkende vandstand og må lade livet, og de er så med til at gøde markerne.<br />
Rismarkerne er ligesom de naturlige vandområder lavvandede og meget varme, og fisk som labyrintfisk, barber (karpefisk) og halvnæb er i overtal. Udover risen vokser der i de dårligt passede marker adskillige vandplanter som ukrudt, og ris markerne adskiller sig ikke nævneværdigt fra de naturlige, flade vandområder.<br />
Bundlaget er blød mudret jord med et større eller mindre indhold af sten. Vandet er blødt og let surt. <br />
Se også beskrivelsen af <b>Lavvandede og meget varme vandområder</b>
'
WHERE BiotopAkvarietId=14;

update akvariet_dk_db2.habitat SET Description=
'
Dette vandløb, der nærmest er en stor å eller en lille flod, snor sig gennem græs- eller busksavannen, hvor der sjældent er nogen bevoksning, der kan give skygge af betydning. Der er noget mere strøm i savannevandløbet end i de andre vandområder, der omtales i dette kapitel, men det minder på mange måder alligevel så meget om disse, at vi kan behandle savannen i dette afsnit. Som man ser det i de større floder, er der også i savannevandløbet bugter og mindre forgreninger, hvor vandet kun bevæger sig yderst trægt afsted. I disse områder er der som regel en kraftig plantevækst, som i nogen grad minder om sumpens, og mange mindre fiskearter holder til i læ af plantetykningen.<br />
I regntiden stiger vandet meget kraftigt, ja, en stigning på mellem en og to meter er meget almindelig, og store dele af savannen bliver oversvømmet. Denne oversvømmelse har en kolossal betydning for såvel plante- som fiskelivet på savannen. Vandet vasker store mængder organisk materiale ud, og denne »gødning« sætter nærmest en væksteksplosion igang. Planterne skyder frem alle steder, og på fiskene virker den kraftige vandfornyelse i forbindelse med den øgede mængde af foder som igangsætter til forplantningen. Som overalt i naturen gælder det jo om at udnytte resourcerne, medens de er til rådighed.<br />
Som det ses på illustrationen er der tale om oversvømmelser, der i mange tilfælde breder sig ud over store dele af savannen og forbinder vandløb, der i den tørre tid har store tørre landområder mellem sig. Man kan let forstå, at fiskene her i savanneområdet, som i andre oversvømmelsesområder, har store muligheder for at sprede sig fra et vandløb til et andet. Det er da også sådan, at fisk fra tropiske zoner er betydelig mere spredte på forskellige biotoper end fisk fra de tempererede zoner.<br />
Der er dog selvfølgelig visse specielle krav hos mange fisk, der gør, at kun i de vandområder, hvor disse krav kan opfyldes, kan de overleve og føre arten videre. Fisk som Barbus, Danio, Rasbora, Moenkhausia samt såvel æglæggende- som ungefødende tandkarper er som regel at finde i mange vidt forskellige biotop typer.<br />
I den tørre tid er fiskene henvist til vandløbenes hovedløb, som måske i særlig tørre perioder deles op i flere helt uafhængige små langstrakte søer- eller fladvandede sumpagtige områder. Disse vandområder er som regel meget iltfattige, og fisk uden specielle åndedrætsorganer dør ofte, hvis de ikke når at trække sig ud i de dybere vandområder, inden det sted, de lever på, afskæres.<br />
Bundlaget varierer noget, men som regel er det halvfint sand med gravere grusområder og nogle sten. Vandet er blødt og let surt. <br />
Se også beskrivelsen af <b>Lavvandede og meget varme vandområder</b>
'
WHERE BiotopAkvarietId=15;

update akvariet_dk_db2.origin  Set Text='Mellem- og sydamerika' where text='Mellem- og sydamerikanske';

DROP PROCEDURE IF EXISTS updateAnimalHabitat;
DELIMITER $$
CREATE PROCEDURE updateAnimalHabitat(IN scientificName_in varchar(190), IN BiotopId_in int)
BEGIN
DECLARE animalId INT DEFAULT NULL;
    DECLARE habitatId INT DEFAULT NULL;
    DECLARE returnValue INT DEFAULT 0;
    DECLARE done INTEGER DEFAULT 0;
    
    DECLARE animalCursor CURSOR FOR SELECT id FROM `akvariet_dk_db2`.animal WHERE `scientificNameText` LIKE scientificName_in;
    DECLARE synonymCursor CURSOR FOR SELECT a2.id FROM animal a2, scientificname sn2, scientificnametoanimal sna WHERE a2.id=sna.animal_id AND sn2.Id=sna.scientificname_id AND concat_ws('', sn2.genus, sn2.species) like scientificName_in;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    open animalCursor;
    igmLoop: loop
        fetch animalCursor into animalId;
        if done = 1 then leave igmLoop; end if;
    
		IF(animalId IS NOT NULL) THEN
			SET habitatId := (SELECT id from `akvariet_dk_db2`.habitat WHERE BiotopAkvarietId=BiotopId_in);
			IF (habitatId IS NOT NULL) THEN
				INSERT ignore INTO `akvariet_dk_db2`.`habitattoanimal` (`Animal_id`,`Habitat_id`) VALUES (animalId, habitatId);
                UPDATE `akvariet_dk_db2`.`animal` SET ViewCount=ViewCount+1, `ModifiedDate`= NOW() WHERE Id=animalId;
				SET returnValue := returnValue + 1;
			END IF;
		END IF;
	END loop igmLoop;
	close animalCursor;
    SET done := 0;
	open synonymCursor;
    synonymLoop: loop
        fetch synonymCursor into animalId;
        if done = 1 then leave synonymLoop; end if;
    
		IF(animalId IS NOT NULL) THEN
			SET habitatId := (SELECT id from `akvariet_dk_db2`.habitat WHERE BiotopAkvarietId=BiotopId_in);
			IF (habitatId IS NOT NULL) THEN
				INSERT ignore INTO `akvariet_dk_db2`.`habitattoanimal` (`Animal_id`,`Habitat_id`) VALUES (animalId, habitatId);
                UPDATE `akvariet_dk_db2`.`animal` SET ViewCount=ViewCount+1, `ModifiedDate`= NOW() WHERE Id=animalId;
				SET returnValue := returnValue + 1;
			END IF;
		END IF;
	END loop synonymLoop;
	close synonymCursor;
    
	SELECT returnValue;
END;
$$
DELIMITER ;

ALTER TABLE `akvariet_dk_db2`.`habitattoanimal` ADD UNIQUE INDEX `Uniquehabitattoanimal` (`Animal_id` ASC, `Habitat_id` ASC);

UPDATE animal SET ViewCount=0;
UPDATE plant SET ViewCount=0;
UPDATE `blogentity` SET PostRead=0;

-- From: http://datamakessense.com/mysql-rownum-row-number-function/
DROP FUNCTION IF EXISTS rownum;
DELIMITER $$
CREATE FUNCTION rownum()
  RETURNS int(11)
BEGIN
  set @prvrownum=if(@ranklastrun=CURTIME(6),@prvrownum+1,1);
  set @ranklastrun=CURTIME(6);
  RETURN @prvrownum;
END $$
$$
DELIMITER ;