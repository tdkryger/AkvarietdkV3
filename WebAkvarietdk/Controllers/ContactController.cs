﻿using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAkvarietdk.Models;
using WebAkvarietdk.Tools;

namespace WebAkvarietdk.Controllers
{
    public class ContactController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [HttpPost]
        [ActionName("Contact")]
        public HttpResponseMessage Post()
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                HttpRequest httpRequest = HttpContext.Current.Request;
                DTOContactWithFiles contact = new DTOContactWithFiles()
                {
                    Content = httpRequest.Form.Get("content"),
                    FromEMail = httpRequest.Form.Get("fromEMail"),
                    FromName = httpRequest.Form.Get("fromName"),
                    Subject = httpRequest.Form.Get("subject"),
                    Files = new List<DTODBFile>(),
                    Ip = httpRequest.Form.Get("ip"),
                    CountryCode = httpRequest.Form.Get("country_code"),
                    CountryName = httpRequest.Form.Get("country_name"),
                    RegionCode = httpRequest.Form.Get("region_code"),
                    RegionName = httpRequest.Form.Get("region_name"),
                    City = httpRequest.Form.Get("city"),
                    ZipCode = httpRequest.Form.Get("zip_code"),
                    TimeZone = httpRequest.Form.Get("time_zone"),
                    Latitude = httpRequest.Form.Get("latitude"),
                    Longitude = httpRequest.Form.Get("longitude"),
                    MetroCode = httpRequest.Form.Get("metro_code"),
                };
                HttpFileCollection hfc = HttpContext.Current.Request.Files;
                for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
                {
                    HttpPostedFile hpf = hfc[iCnt];

                    if (hpf.ContentLength > 0)
                    {
                        DTODBFile dtoFile = new DTODBFile()
                        {
                            ContentType = hpf.ContentType,
                            Filename = hpf.FileName
                        };
                        using (var binaryReader = new BinaryReader(hpf.InputStream))
                        {
                            dtoFile.FileContent = binaryReader.ReadBytes(hpf.ContentLength);
                        }
                        contact.Files.Add(dtoFile);
                    }
                }
                ContactInfoWithFiles dbValue = contact.ToContactInfoWithFiles();
                if (DTOHelper.SaveContactInfoWithFiles(dbValue))
                {
                    string mailSubject = Akvariet.Common.AkvarietConfiguration.NewFilesUploadedSubject;
                    string mailBody = "";
                    string FilePath = HttpContext.Current.Server.MapPath(Akvariet.Common.AkvarietConfiguration.NewMessageBody);

                    using (StreamReader sr = new StreamReader(FilePath))
                    {
                        mailBody = sr.ReadToEnd();
                    }
                    mailBody = mailBody
                        .Replace("{SUBJECT}", dbValue.Subject)
                        .Replace("{FROMNAME}", dbValue.FromName)
                        .Replace("{CONTENT}", dbValue.Content)
                        .Replace("{IP}", dbValue.Ip)
                        .Replace("{COUNTRY}", dbValue.CountryName);
                    if (dbValue.Files.Count == 0)
                    {
                        mailSubject = dbValue.Subject;
                    }
                    EMailResult emailResult = Tools.EMailSender.SendEmail(mailSubject, mailBody, contact.Files);
                    if (!emailResult.Result)
                    {
                        logger.Warn("Kunne ikke sende email: {0}", emailResult.Message);
                    }
                    sw.Stop();
                    logger.Info("Runtime: {0}", sw.Elapsed);

                    var response = Request.CreateResponse(HttpStatusCode.Redirect);
                    //TODO: Redirect to a thank-you page?
                    response.Headers.Location = new Uri(Request.RequestUri.GetLeftPart(UriPartial.Authority));
                    return response;
                }
                else
                {
                    sw.Stop();
                    logger.Warn("Runtime: {0}", sw.Elapsed);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
