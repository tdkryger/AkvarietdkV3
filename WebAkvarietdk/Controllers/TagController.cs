﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class TagController : ApiController
    {
        //TODO: Function that returns ALL tags
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// GET api/tag
        /// </summary>
        /// <returns>List of used tags</returns>
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                logger.Debug("TagController.Get()");
                List<TagEntity> tags = ExtendedCache.TagList;
                if (tags.Count == 0)
                {
                    sw.Stop();
                    logger.Warn("No tags found. Runtime: {0}", sw.Elapsed);
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                sw.Stop();
                logger.Info("Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, tags);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
#if (DEBUG)
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
#else
                return Request.CreateResponse(HttpStatusCode.InternalServerError);                
#endif
            }
        }


        /// <summary>
        /// GET api/tag with search parameters
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <param name="reverse"></param>
        /// <param name="animals"></param>
        /// <param name="plants"></param>
        /// <param name="blogs"></param>
        /// <returns>return a list of WebEntry/BlogEntry that are tag'ed with the tag</returns>
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get(string searchString, int page = 1, int itemsPerPage = 30, bool reverse = false, bool animals = true, bool plants = true, bool blogs = true)
        {
            Stopwatch sw = Stopwatch.StartNew();
            using (FishbaseUnitOfWork uow =new FishbaseUnitOfWork())
            {
                try
                {
                    
                    if (int.TryParse(searchString, out int tagId))
                    {
                        uow.BeginTransaction();
                        string sql = "SELECT `blogentity`.`Id`, `blogentity`.`Title`, `blogentity`.`Teaser`, `blogentity`.`Body`, `blogentity`.`Posted`, `blogentity`.`PostRead`, `blogentity`.`Image_id`, `blogentity`.`Contributor_id` "
                            + "FROM `blogentity`"
                            + "INNER JOIN `tagentitytoblogentity` ON `blogentity`.`Id`=`tagentitytoblogentity`.`BlogEntity_id`"
                            + "INNER JOIN `tagentity` ON `tagentity`.`Id`=`tagentitytoblogentity`.`TagEntity_id`"
                            + "WHERE `tagentity`.`Id`= :tagId; ";

                        List<BlogEntity> blogList = (List<BlogEntity>)uow.Session.CreateSQLQuery(sql)
                                                    .AddEntity(typeof(BlogEntity))
                                                    .SetParameter("tagId", tagId)
                                                    .List<BlogEntity>();

                        List<DTOWebEntry> dtoWebEntrys = DTOHelper.GetDTOWebEntryList(null, null, DTOHelper.GetDTOBlogEntrys(blogList));

                        if (reverse)
                            dtoWebEntrys = dtoWebEntrys.OrderBy(x => x.TimeStamp).ThenBy(x => x.Title).ToList();
                        else
                            dtoWebEntrys = dtoWebEntrys.OrderByDescending(x => x.TimeStamp).ThenBy(x => x.Title).ToList();

                        // paging
                        var dtoEntriesPaged = dtoWebEntrys.Skip((page - 1) * itemsPerPage).Take(itemsPerPage);

                        //TODO: WebEntry.ImageList and WebEntry.User
                        // json result
                        var json = new
                        {
                            page = page,
                            itemsPerPage = itemsPerPage,
                            searchString = searchString,
                            count = dtoWebEntrys.Count(),
                            elapsed = sw.Elapsed,
                            animals = 0,
                            plants = 0,
                            blogs = dtoWebEntrys.Count(),
                            data = dtoEntriesPaged.Select(x => new
                            {
                                Id = x.Id,
                                EntryType = x.EntryType,
                                FrontImage = x.FrontImage,
                                ThumbUrl = x.ThumbUrl,
                                TimeStamp = x.TimeStamp,
                                ViewCount = x.ViewCount,
                                Title = x.Title,
                                Teaser = x.Teaser,
                                Body = x.Body,
                                Url = x.Url,
                                ShortTeaser = x.ShortTeaser,
                                ShortTitle = x.ShortTitle,
                            })
                        };
                        sw.Stop();
                        logger.Info(" Runtime: {0}", sw.Elapsed);
                        uow.Commit();
                        return Request.CreateResponse(HttpStatusCode.OK, json);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, $"{searchString} is not a valid integer");
                    }

                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    sw.Stop();
                    logger.Info(" Runtime: {0}", sw.Elapsed);
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);

                }
                finally
                {
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
        }
    }
}
