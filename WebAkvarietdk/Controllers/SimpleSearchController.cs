﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class SimpleSearchController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Private methods

        #endregion


        // Source: https://www.pointblankdevelopment.com.au/blog/78/angularjs-aspnet-web-api-2-server-side-sorting-searching-and-paging
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = false)]
        public IHttpActionResult Get(string searchString, int page = 1, int itemsPerPage = 30, bool reverse = false, bool animals = true, bool plants = true, bool blogs = true)
        {
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                List<DTOPlant> dtoPlantList = new List<DTOPlant>();
                List<DTOAnimal> dtoAnimalList = new List<DTOAnimal>();
                List<DTOBlogEntry> dtoBlogEntryList = new List<DTOBlogEntry>();
                List<DTOWebEntry> dtoEntries = new List<DTOWebEntry>();
                if (!string.IsNullOrWhiteSpace(searchString))
                {
                    SimpleSearchKey simpleSearchKey = new SimpleSearchKey(searchString, animals, plants, blogs);
                    dtoEntries = SimpeSearchCaching.Get(simpleSearchKey);
                    if (dtoEntries == null)
                    {
                        using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                        {
                            uow.BeginTransaction();
                            try
                            {
                                string likeString = searchString;
                                if (!likeString.StartsWith("%"))
                                    likeString = "%" + likeString;
                                if (!likeString.EndsWith("%"))
                                    likeString = likeString + "%";
                                List<SimpleSearch> result = new List<SimpleSearch>();
                                if (animals)
                                {
                                    var animalsResults = uow.Session.CreateSQLQuery("call simpleSearchAnimal(:scientificName)")
                                        .AddEntity(typeof(SimpleSearch))
                                        .SetParameter("scientificName", likeString)
                                        .List<SimpleSearch>();
                                    result.AddRange(animalsResults);
                                }
                                if (plants)
                                {
                                    var plantResults = uow.Session.CreateSQLQuery("call simpleSearchPlant(:scientificName)")
                                        .AddEntity(typeof(SimpleSearch))
                                        .SetParameter("scientificName", likeString)
                                        .List<SimpleSearch>();
                                    result.AddRange(plantResults);
                                }
                                if (blogs)
                                {
                                    var blogResults = uow.Session.CreateSQLQuery("call simpleSearchBlog(:scientificName)")
                                        .AddEntity(typeof(SimpleSearch))
                                        .SetParameter("scientificName", likeString)
                                        .List<SimpleSearch>();
                                    result.AddRange(blogResults);
                                }

                                foreach (SimpleSearch ss in result)
                                {
                                    bool foundIt = true;
                                    switch (ss.EntryType.ToLower())
                                    {
                                        case "animal":
                                            DTOAnimal dtoAnimal = DTOHelper.GetAnimal(ss.Id);
                                            if (dtoAnimal != null)
                                                dtoAnimalList.Add(dtoAnimal);
                                            else
                                                foundIt = false;
                                            break;
                                        case "plant":
                                            DTOPlant dtoPlant = DTOHelper.GetPlant(ss.Id);
                                            if (dtoPlant != null)
                                                dtoPlantList.Add(dtoPlant);
                                            else
                                                foundIt = false;
                                            break;
                                        case "blog":
                                            DTOBlogEntry dtoBlog = DTOHelper.GetBlog(ss.Id);
                                            if (dtoBlog != null)
                                                dtoBlogEntryList.Add(dtoBlog);
                                            else
                                                foundIt = false;
                                            break;
                                    }
                                    if (!foundIt)
                                        logger.Warn("Could not find {0} with id: {1} while searching for {2}", ss.EntryType, ss.Id, searchString);
                                }
                                uow.Commit();
                            }
                            catch (Exception ex)
                            {
                                uow.Rollback();
                                string msg = "Stored procedure";
                                logger.Error(ex, msg);
                                throw new Exception(msg, ex);

                            }
                            finally
                            {
                                uow.Session.Disconnect();
                                uow.Dispose();
                            }
                        }
                        dtoEntries = DTOHelper.GetDTOWebEntryList(dtoAnimalList, dtoPlantList, dtoBlogEntryList);
                        SimpeSearchCaching.Add(simpleSearchKey, dtoEntries);
                    }  // if (dtoEntries == null)
                } //  if (!string.IsNullOrWhiteSpace(searchString))



                if (reverse)
                    dtoEntries = dtoEntries.OrderBy(x => x.Title).ToList();
                else
                    dtoEntries = dtoEntries.OrderByDescending(x => x.Title).ToList();

                // paging
                var dtoEntriesPaged = dtoEntries.Skip((page - 1) * itemsPerPage).Take(itemsPerPage);

                //TODO: WebEntry.ImageList and WebEntry.User
                // json result
                var json = new
                {
                    page = page,
                    itemsPerPage = itemsPerPage,
                    searchString = searchString,
                    count = dtoEntries.Count(),
                    elapsed = sw.Elapsed,
                    animals = dtoAnimalList.Count(),
                    plants = dtoPlantList.Count(),
                    blogs = dtoBlogEntryList.Count(),
                    data = dtoEntriesPaged.Select(x => new
                    {
                        Id = x.Id,
                        EntryType = x.EntryType,
                        FrontImage = x.FrontImage,
                        ThumbUrl = x.ThumbUrl,
                        TimeStamp = x.TimeStamp,
                        ViewCount = x.ViewCount,
                        Title = x.Title,
                        Teaser = x.Teaser,
                        Body = x.Body,
                        Url = x.Url,
                        ShortTeaser = x.ShortTeaser,
                        ShortTitle = x.ShortTitle,
                    })
                };

                sw.Stop();
                logger.Debug("Found {0} entries in {1} ", dtoEntries.Count(), sw.Elapsed);
                return Ok(json);

            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                string errorString = $"Something went wrong. Runtime: {sw.Elapsed}";
#if DEBUG
                errorString = $"Something went wrong: {ex.Message}. Runtime: {sw.Elapsed}";
#endif
                return new System.Web.Http.Results.ResponseMessageResult(
                        Request.CreateErrorResponse((HttpStatusCode)500,
                            new HttpError(errorString)));
            }
        }
    }
}
