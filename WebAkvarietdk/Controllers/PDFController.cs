﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class PDFController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get(string htmlContent, string title)
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                PDF.PDFGenerator pdf = new PDF.PDFGenerator(htmlContent);
                byte[] pdfData = pdf.CreatePDF(title);
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, pdfData);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
#if (DEBUG)
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
#else
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
#endif
            }
        }
    }
}
