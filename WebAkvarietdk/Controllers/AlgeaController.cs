﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
#if (!DEBUG)
using WebApi.OutputCache.V2;
#endif

namespace WebAkvarietdk.Controllers
{
    public class AlgeaController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<Algae> list;
            try
            {
                using (FishbaseUnitOfWork uow = DBHelper.GetUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<Algae> repo = new Repository<Algae>(uow))
                    {
                        list = repo.GetAll().OrderBy(x => x.Name).ToList();
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get(int id)
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<Algae> list = new List<Algae>();
            try
            {
                using (FishbaseUnitOfWork uow = DBHelper.GetUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<Algae> repo = new Repository<Algae>(uow))
                    {
                        list.Add(repo.GetById(id));
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
