﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class PlantController : ApiController
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET api/values
        /// <summary>
        /// Get top 5 last modified plants
        /// </summary>
        /// <returns></returns>
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("PlantController.Get()");
            List<DTOPlant> dtoAnimals;
            try
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<Plant> repo = new Repository<Plant>(uow))
                    {
                        List<Plant> list = repo.GetAll().OrderByDescending(x => x.ModifiedDate).Take(10).ToList();
                        dtoAnimals = DTOHelper.GetDTOPlantList(list);
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                sw.Stop();
                logger.Info("PlantController.Get(). Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, dtoAnimals);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "PlantController.Get(). Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // GET api/values/5
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get(int id)
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("PlantController.Get({0})", id);
            Plant plant;
            DTOPlant dtoPlant;
            try
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    using (Repository<Plant> repo = new Repository<Plant>(uow))
                    {
                        plant = repo.GetById(id);
                        uow.BeginTransaction();
                        try
                        {
                            plant.ViewCount++;
                            repo.Update(plant);
                            uow.Commit();
                        }
                        catch
                        {
                            uow.Rollback();
                        }
                        dtoPlant = new DTOPlant(plant);
                    }
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                sw.Stop();
                if (dtoPlant == null)
                {
                    logger.Warn("PlantController.Get({0}). Plant not found. Runtime: {1}", id, sw.Elapsed);
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                
                return Request.CreateResponse(HttpStatusCode.OK, dtoPlant);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "PlantController.Get({0}). Runtime: {1}", id, sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
