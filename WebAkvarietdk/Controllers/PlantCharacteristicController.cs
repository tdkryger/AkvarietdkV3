﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class PlantCharacteristicController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, ExtendedCache.PlantCharacteristic);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
