﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class HabitatController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<Habitat> list = null;
            try
            {
                using (FishbaseUnitOfWork uow = DBHelper.GetUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<Habitat> repo = new Repository<Habitat>(uow))
                    {
                        list = repo.GetAll().OrderBy(x => x.Text).ToList();
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                List<DTOHabitat> dtoList = DTOHelper.ConvertHabitatsToDTOHabitats(list);
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, dtoList);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }
    }
}
