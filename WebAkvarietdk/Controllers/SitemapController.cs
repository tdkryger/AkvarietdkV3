﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace WebAkvarietdk.Controllers
{
    public class SitemapController : ApiController
    {
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        [Route("sitemap.xml")]
        public HttpResponseMessage SitemapXml()
        {
            Uri baseUri = new Uri(Request.RequestUri, RequestContext.VirtualPathRoot);
            var sitemapNodes = Sitemap.Sitemap.GetSitemapNodes();
            string xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + Sitemap.Sitemap.GetSitemapDocument(sitemapNodes);
            return new HttpResponseMessage()
            {
                Content = new StringContent(xml, Encoding.UTF8, "application/xml")
            };
        }
    }
}
