﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class OriginController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            IList<IntStringClass> list;
            try
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    list = uow.Session.CreateSQLQuery("SELECT rownum() AS Id, Text, false AS Selected FROM origin WHERE Text != '' GROUP BY Text ORDER BY Text;")
                                        .AddEntity(typeof(IntStringClass))
                                        .List<IntStringClass>();
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }


                sw.Stop();
                logger.Info("Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
#if (DEBUG)
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
#else
                return Request.CreateResponse(HttpStatusCode.InternalServerError);                
#endif
            }
        }

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get(string text)
        {
            Stopwatch sw = Stopwatch.StartNew();
            IList<DTOOrigin> dtoList = new List<DTOOrigin>();
            IList<Origin> list;
            try
            {
                string sql = "SELECT * FROM akvariet_dk_db2.origin WHERE Text IN ({0}) AND Text != '' ORDER BY Text, SubOrigin;";
                string searchValue = string.Empty;
                string[] words = text.Split(';');
                if (words.Count() > 1)
                {
                    foreach(string word in words)
                        searchValue = searchValue + $"'{word}',";
                    searchValue = searchValue.Substring(0, searchValue.Length - 1);
                }
                else
                {
                    searchValue = $"'{text}'";
                }
                sql = string.Format(sql, searchValue);
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    list = uow.Session.CreateSQLQuery(sql)
                                        .AddEntity(typeof(Origin))
                                        .List<Origin>();
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                Parallel.ForEach(list, (item) => { dtoList.Add(new DTOOrigin(item)); });
                sw.Stop();
                logger.Info("Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, dtoList);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
#if (DEBUG)
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
#else
                return Request.CreateResponse(HttpStatusCode.InternalServerError);                
#endif
            }
        }
    }
}
