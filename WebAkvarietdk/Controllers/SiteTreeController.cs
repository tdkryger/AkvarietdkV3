﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.SiteTree;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class SiteTreeController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                IReadOnlyCollection<SitetreeNode> nodes = Sitetree.GetSiteTree();
                nodes.OrderBy(x => x.NodeType).ThenBy(x => x.Title);
                sw.Stop();
                logger.Info(" Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, nodes);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}