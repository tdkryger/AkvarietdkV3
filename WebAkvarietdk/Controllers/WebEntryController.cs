﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class WebEntryController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = false)]
        public IHttpActionResult GetAnimals(string sortBy, int page = 1, int itemsPerPage = 30, bool reverse = false)
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                List<DTOWebEntry> dtoEntries = new List<DTOWebEntry>();
                dtoEntries = DTOHelper.GetAnimalsAsDTOWebEntry(page, itemsPerPage);
                if (reverse)
                    dtoEntries = dtoEntries.OrderBy(x => x.Title).ToList();
                else
                    dtoEntries = dtoEntries.OrderByDescending(x => x.Title).ToList();

                //TODO: WebEntry.ImageList and WebEntry.User
                // json result
                var json = new
                {
                    page = page,
                    itemsPerPage = itemsPerPage,
                    count = DTOHelper.CountAnimals(),
                    elapsed = sw.Elapsed,
                    data = dtoEntries.Select(x => new
                    {
                        Id = x.Id,
                        EntryType = x.EntryType,
                        FrontImage = x.FrontImage,
                        ThumbUrl = x.ThumbUrl,
                        TimeStamp = x.TimeStamp,
                        ViewCount = x.ViewCount,
                        Title = x.Title,
                        Teaser = x.Teaser,
                        Body = x.Body,
                        Url = x.Url,
                        ShortTeaser = x.ShortTeaser,
                        ShortTitle = x.ShortTitle,
                    })
                };

                sw.Stop();
                logger.Debug("Found {0} entries in {1} ", dtoEntries.Count(), sw.Elapsed);
                return Ok(json);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                string errorString = $"Something went wrong. Runtime: {sw.Elapsed}";
#if DEBUG
                errorString = $"Something went wrong: {ex.Message}. Runtime: {sw.Elapsed}";
#endif
                return new System.Web.Http.Results.ResponseMessageResult(
                        Request.CreateErrorResponse((HttpStatusCode)500,
                            new HttpError(errorString)));
            }
        }

        // GET api/WebEntry
        /// <summary>
        /// Returns  the 6 newest web entrys. 2 Blog, 2 animal and 2 plant
        /// </summary>
        /// <returns></returns
#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get()
        {
            //TODO: Some chaching plz. Perhabs a global list that gets loaded at startup, and changed when blogs or plants or animals is changed
            var sw = Stopwatch.StartNew();
            var dtoBlogEntrys = DTOHelper.GetNewestBlogsAsync(2).Result; //DTOHelper.GetNewestBlogs(2);
            var dtoAnimals = DTOHelper.GetNewestAninals(2);
            var dtoPlants = DTOHelper.GetNewestPlants(2);

            var webEntrys = new List<DTOWebEntry>();
            foreach (DTOBlogEntry dto in dtoBlogEntrys)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }
            foreach (DTOAnimal dto in dtoAnimals)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }
            foreach (DTOPlant dto in dtoPlants)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }

            webEntrys = webEntrys.OrderByDescending(x => x.TimeStamp).ToList();

            sw.Stop();
            logger.Info("Get 2 of each. Runtime: {0}", sw.Elapsed);
            return Request.CreateResponse(HttpStatusCode.OK, webEntrys);
        }


        /// <summary>
        /// Getting the [count] most read 
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
#if (!DEBUG)
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
#endif
        public HttpResponseMessage Get(int count)
        {
            //TODO: Some chaching plz. Perhabs a global list that gets loaded at startup, and changed when blogs or plants or animals is changed
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
#if (!DEBUG)
                if (SimpeSearchCaching.FrontpageCount != count)
                {
#endif
                    SimpeSearchCaching.FrontpageCount = count;
                    SimpeSearchCaching.LoadCacheData();
#if (!DEBUG)
                }
#endif
                List<DTOWebEntry> webEntrys = SimpeSearchCaching.GetWebEntryCount(); //DTOHelper.GetWebEntrys(count);
                sw.Stop();
                logger.Info("Get count={0} total. Runtime: {1}", count, sw.Elapsed);
                return Request.CreateResponse(HttpStatusCode.OK, webEntrys);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "An error happend. Runtime: {0}", sw.Elapsed);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
            }
        }


        /// <summary>
        /// 
        /// api/WebEntry?type=*&id=*
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get(EntryTypes type, int id)
        {
            //TODO Are we using this one?
            //TODO: Some chaching plz. Perhabs a global list that gets loaded at startup, and changed when blogs or plants or animals is changed
            //TODO: ? WebEntryController.Get(EntryTypes type, int id)
            DTOWebEntry webEntry = null;

            switch (type)
            {
                // api/WebEntry?type=1&id=*
                case EntryTypes.Animal:
                    webEntry = new DTOWebEntry(DTOHelper.GetAnimal(id));
                    break;
                // api/WebEntry?type=0&id=*
                case EntryTypes.Blog:
                    break;
                // api/WebEntry?type=2&id=*
                case EntryTypes.Plant:
                    break;
            }
            if (webEntry == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);

            return Request.CreateResponse(HttpStatusCode.OK, webEntry);
        }
    }
}
