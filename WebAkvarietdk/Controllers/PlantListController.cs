﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Caching;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class PlantListController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = false)]
        public IHttpActionResult Get(string searchString, int page = 1, int itemsPerPage = 30, bool reverse = false, bool animals = true, bool plants = true, bool blogs = true)
        {
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                DTOHelper.ListTypes listType;
                if (animals)
                    listType = DTOHelper.ListTypes.Animal;
                else
                {
                    if (blogs)
                        listType = DTOHelper.ListTypes.Blog;
                    else
                        listType = DTOHelper.ListTypes.Plant;
                }
                List<DTOWebEntry> dtoEntries = DTOHelper.GetList(listType, page, itemsPerPage, reverse);

                if (reverse)
                    dtoEntries = dtoEntries.OrderBy(x => x.Title).ToList();
                else
                    dtoEntries = dtoEntries.OrderByDescending(x => x.Title).ToList();

                // paging
                var dtoEntriesPaged = dtoEntries.Skip((page - 1) * itemsPerPage).Take(itemsPerPage);


                var json = new
                {
                    page = page,
                    itemsPerPage = itemsPerPage,
                    searchString = searchString,
                    count = dtoEntries.Count(),
                    elapsed = sw.Elapsed,
                    animals = 0,
                    plants = dtoEntries.Count(),
                    blogs = 0,
                    data = dtoEntriesPaged.Select(x => new
                    {
                        Id = x.Id,
                        EntryType = x.EntryType,
                        FrontImage = x.FrontImage,
                        ThumbUrl = x.ThumbUrl,
                        TimeStamp = x.TimeStamp,
                        ViewCount = x.ViewCount,
                        Title = x.Title,
                        Teaser = x.Teaser,
                        Body = x.Body,
                        Url = x.Url,
                        ShortTeaser = x.ShortTeaser,
                        ShortTitle = x.ShortTitle,
                    })
                };

                sw.Stop();
                logger.Debug("Found {0} entries in {1} ", dtoEntries.Count(), sw.Elapsed);
                return Ok(json);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Error(ex, "Runtime: {0}", sw.Elapsed);
                string errorString = $"Something went wrong. Runtime: {sw.Elapsed}";
#if DEBUG
                errorString = $"Something went wrong: {ex.Message}. Runtime: {sw.Elapsed}";
#endif
                return new System.Web.Http.Results.ResponseMessageResult(
                        Request.CreateErrorResponse((HttpStatusCode)500,
                            new HttpError(errorString)));
            }
        }
    }
}
