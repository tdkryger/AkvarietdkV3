﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class BlogController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET api/values
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("BlogController: Getting all the blog entrys ");
            List<BlogEntity> list = new List<BlogEntity>();
            try
            {
                using (FishbaseUnitOfWork buow = new FishbaseUnitOfWork())
                {
                    buow.BeginTransaction();
                    using (Repository<BlogEntity> repo = new Repository<BlogEntity>(buow))
                    {
                        list = repo.GetAll().ToList();
                    }
                    buow.Commit();
                    buow.Session.Disconnect();
                    buow.Dispose();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "BlogController.Get()");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);

            }
            sw.Stop();
            logger.Info("BlogController.Get(). Runtime: {0}", sw.Elapsed);
            if (list.Count == 0)
            {
                logger.Warn("No blog entrys found");
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, list);

        }

        // GET api/values/5
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get(int id)
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("BlogController.Get({0})", id);
            BlogEntity item = null;
            try
            {
                using (FishbaseUnitOfWork buow = new FishbaseUnitOfWork())
                {
                    buow.BeginTransaction();
                    using (Repository<BlogEntity> repo = new Repository<BlogEntity>(buow))
                    {
                        item = repo.GetAll().FirstOrDefault(x => x.Id == id);
                        buow.BeginTransaction();
                        try
                        {
                            item.PostRead++;
                            repo.Update(item);
                            buow.Commit();
                        }
                        catch
                        {
                            buow.Rollback();
                        }
                    }
                    buow.Commit();
                    buow.Session.Disconnect();
                    buow.Dispose();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "BlogController.Get({0})", id);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            sw.Stop();
            logger.Info("BlogController.Get({0}). Runtime: {1}", id, sw.Elapsed);
            if (item == null)
            {
                logger.Warn("BlogController.Get({0}). Blog Entry not found", id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            return Request.CreateResponse(HttpStatusCode.OK, item);
        }
    }
}
