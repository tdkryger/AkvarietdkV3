﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAkvarietdk.Models;
using WebApi.OutputCache.V2;

namespace WebAkvarietdk.Controllers
{
    public class AnimalController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET api/values
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get()
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("AnimalController.Get()");
            List<DTOAnimal> dtoAnimals;
            try
            {
                using (FishbaseUnitOfWork uow = DBHelper.GetUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<Animal> repo = new Repository<Animal>(uow))
                    {
                        
                        List<Animal> list = repo.GetAll().OrderByDescending(x => x.ModifiedDate).Take(10).ToList();
                        dtoAnimals = DTOHelper.GetDTOAnimalList(list);
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                if (dtoAnimals == null)
                {
                    logger.Warn("No animals found");
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }

            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Info("AnimalController.Get(). Runtime: {0}", sw.Elapsed);
                logger.Error(ex, "AnimalController.Get");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            sw.Stop();
            logger.Info("AnimalController.Get(). Runtime: {0}", sw.Elapsed);
            return Request.CreateResponse(HttpStatusCode.OK, dtoAnimals);
        }

        // GET api/values/5
        [CacheOutput(ClientTimeSpan = 100, ServerTimeSpan = 100, AnonymousOnly = true)]
        public HttpResponseMessage Get(int id)
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("AnimalController: Getting an animal {0}", id);
            try
            {
                DTOAnimal dtoAnimal;
                using (FishbaseUnitOfWork uow = DBHelper.GetUnitOfWork())
                {
                    using (Repository<Animal> repo = new Repository<Animal>(uow))
                    {
                        Animal animal = repo.GetById(id);
                        uow.BeginTransaction();
                        try
                        {
                            animal.ViewCount++;
                            repo.Update(animal);
                            uow.Commit();
                        }
                        catch
                        {
                            uow.Rollback();
                        }
                        dtoAnimal = new DTOAnimal(animal);
                    }
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
                sw.Stop();
                logger.Info("AnimalController.Get({0}). Runtime: {1}", id, sw.Elapsed);
                if (dtoAnimal == null)
                {
                    logger.Warn("AnimalController.Get({0}). No animal found", id);
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
                return Request.CreateResponse(HttpStatusCode.OK, dtoAnimal);
            }
            catch (Exception ex)
            {
                sw.Stop();
                logger.Info("AnimalController.Get({0}). Runtime: {1}", id, sw.Elapsed);
                logger.Error(ex, "AnimalController.Get(int id)");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
