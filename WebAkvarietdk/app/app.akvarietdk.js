﻿
// Refactor to reuse code..
var app = angular.module("akvarietdk", ['ngRoute', 'angular-loading-bar', 'ngAnimate', 'ui.bootstrap']);

app.config(['$locationProvider', function ($locationProvider) {
    //$locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');


}]);

app.filter('unsafe', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
});

app.run(function ($rootScope, $location, $window) {
    //Additionally one tip is that google analytics will not fire on localhost. So if you are testing on localhost, use the following instead of the default create
    // Source: http://stackoverflow.com/a/18197712
    //ga('create', 'UA-XXXX-Y', { 'cookieDomain': 'none' });
    $window.ga('create', 'UA-99240411-1', 'auto');

    $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {
        var fullRoute = current.$$route.originalPath,
            routeParams = current.params,
            resolvedRoute,
            niceRoute,
            shrunkRoute;

        shrunkRoute = fullRoute.replace(/:id/, '');
        resolvedRoute = fullRoute.replace(/:id/, routeParams.id);
        niceRoute = fullRoute.replace(/:id/, "id=" + routeParams.id);
        $rootScope.fullRoute = shrunkRoute;

        //console.log("calling Google Analystics with url: " + $location.path());
        $window.ga('send', 'pageview', { page: $location.path() });

        //var allMetaElements = document.getElementsByTagName('meta');
        //console.log('Meta element count: ', allMetaElements.length);
        //for (var i = 0; i < allMetaElements.length; i++) {
        //    console.log(allMetaElements[i]);
        //}

    });

    $rootScope.filterText = [];
});

app.directive("formOnChange", function ($parse) {
    return {
        require: "form",
        link: function (scope, element, attrs) {
            var cb = $parse(attrs.formOnChange);
            element.on("change", function () {
                cb(scope);
            });
        }
    }
});

app.factory('GetterService', ['$http',
    function ($http, $scope) {
        var GetterService = {};

        GetterService.getTags = function () {
            return $http.get('/api/tag');
        };

        GetterService.getPlantGroups = function () {
            return $http.get('/api/PlantGroup');
        };

        return GetterService;
    }]);

app.factory('SearchService', ['$http',
    function ($http) {
        var service = {};

        service.GetSimpleSearch = function (pagingInfo) {
            return $http.get('/api/SimpleSearch', { params: pagingInfo });
        };

        return service;
    }]);

app.factory('AdvancedSearchService', ['$http',
    function ($http) {
        var service = {};

        service.GetAdvancedSimpleSearch = function (pagingInfo) {
            return $http.get('/api/AdvancedSearch', { params: pagingInfo });
        };

        return service;
    }]);

app.factory('ListService', ['$http',
    function ($http) {
        var service = {};

        service.GetSimpleSearch = function (pagingInfo) {
            return $http.get('/api/PlantList', { params: pagingInfo });
        };

        return service;
    }]);

app.factory('AnimalService', ['$http',
    function ($http) {
        var service = {};

        service.Get = function (pagingInfo) {
            console.log('/api/webentry', { params: pagingInfo });
            return $http.get('/api/webentry', { params: pagingInfo });
        };

        return service;
    }]);

app.factory('BlogsForTagService', ['$http',
    function ($http) {
        var service = {};

        service.GetSimpleSearch = function (pagingInfo) {
            return $http.get('/api/Tag', { params: pagingInfo });
        };

        return service;
    }]);

app.factory('Page', function ($window, $rootScope) {

    $rootScope.$on('cfpLoadingBar:started', function (event, data) {
        $rootScope.loadStart = new Date();
        $rootScope.elapsedMs = 0;
        $rootScope.loadstatus = "Indlæser data";
        $rootScope.loadStyle = { "background-color": "yellow" };
        console.log("Starting to loading AJAX Data"); // 'Some data'
    });

    $rootScope.$on('cfpLoadingBar:completed', function (event, data) {
        var now = new Date();
        $rootScope.elapsedMs = now.getTime() - $rootScope.loadStart.getTime();
        $rootScope.loadstatus = "Data indlæst";
        $rootScope.loadStyle = { "background-color": "green" };
        console.log("Done loading AJAX Data in ", $rootScope.elapsedMs);
    });

    var title = 'Akvariet.dk';
    var metaDescription = '';
    var metaKeywords = 'elektronik, akvarier, artikler, beskrivelser af planter, beskrivelser af fisk, beskrivelser af akvariedyr';
    var author = 'Thomas D. Kryger';

    return {
        title: function () { return title; },
        setTitle: function (newTitle) { title = 'Akvariet.dk :: ' + newTitle; },
        author: function () { return author; },
        setAuthor: function (newAuthor) {
            author = newAuthor;
            var allMetaElements = $window.document.getElementsByTagName('meta');
            for (var i = 0; i < allMetaElements.length; i++) {
                if (allMetaElements[i].getAttribute('name') === 'author') {
                    allMetaElements[i].setAttribute('content', author);
                    break;
                }
            }
        },
        reset: function () {
            title = 'Akvariet.dk';
            metaDescription = '';
            metaKeywords = '';
            author = 'Thomas D. Kryger';
        },
        metaDescription: function () { return metaDescription; },
        metaKeywords: function () { return metaKeywords; },
        setMetaDescription: function (newMetaDescription) {
            metaDescription = newMetaDescription;
            var allMetaElements = $window.document.getElementsByTagName('meta');
            for (var i = 0; i < allMetaElements.length; i++) {
                if (allMetaElements[i].getAttribute('name') === 'description') {
                    allMetaElements[i].setAttribute('content', metaDescription);
                    break;
                }
            }
        },
        appendMetaKeywords: function (newKeywords) {
            for (var key in newKeywords) {
                if (metaKeywords === '') {
                    metaKeywords += newKeywords[key].name;
                } else {
                    metaKeywords += ', ' + newKeywords[key].name;
                }
            }
            var allMetaElements = $window.document.getElementsByTagName('meta');
            for (var i = 0; i < allMetaElements.length; i++) {
                if (allMetaElements[i].getAttribute('name') === 'keywords') {
                    allMetaElements[i].setAttribute('content', metaKeywords);
                    break;
                }
            }
        }
    };
});

app.controller('MetaCtrl', function ($scope, Page) {
    $scope.Page = Page;
});

app.controller('PlantGroupCtrl', function ($scope, $http, GetterService) {
    //$scope.loading = true;
    GetterService.getPlantGroups()
        .then(function (result) {
            $scope.plantGroups = result.data;
            //$scope.loading = false;
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    //$scope.getApi();
});

app.controller('PlantCharacteristicCtrl', function ($scope, $http) {
    //$scope.loading = true;
    // Function to get the tags
    $scope.getApi = function () {
        $http.get('/api/PlantCharacteristic')
            .then(function (result) {
                $scope.plantCharacteristic = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getApi();
});

app.controller("plantCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    $scope.plantId = $scope.$routeParams.id;
    Page.reset();
    var apiUrl = '/api/plant/' + $scope.$routeParams.id;

    $scope.getItem = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.plant = result.data;
                Page.setTitle($scope.plant.ScientificName);
                Page.setMetaDescription('Beskrivelse af ' + $scope.plant.ScientificName);
                Page.setAuthor($scope.plant.User.DisplayName);
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getItem();

});

app.controller('plantlistCtrl', ['$scope', 'ListService', 'Page', function ($scope, ListService, Page) {
    //TODO: What if the list is empty? Need an "error" page
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: '',
        totalItems: 0,
        animals: false,
        plants: true,
        blogs: false

    };
    Page.reset();

    $scope.search = function () {
        //console.log("In $scope.search");
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.sort = function (sortBy) {
        //console.log("In $scope.sort");
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.selectPage = function (page) {
        //console.log("In $scope.selectPage");
        $scope.pagingInfo.page = page;
        loadSimpleSearch();
    };

    function loadSimpleSearch() {
        //console.log("In $scope.loadSimpleSearch");
        $scope.webEntries = null;
        ListService.GetSimpleSearch($scope.pagingInfo).then(function (result) {
            //console.log("result: ", result);
            Page.setTitle('Planteliste');
            Page.setMetaDescription('Planteliste, side ' + $scope.pagingInfo.page);
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
            //console.log($scope.webEntries);
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }

    // initial table load
    loadSimpleSearch();
}]);

app.controller('animalListCtrl', ['$scope', 'ListService', 'Page', function ($scope, ListService, Page) {
    //TODO: What if the list is empty? Need an "error" page
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: '',
        totalItems: 0,
        animals: true,
        plants: false,
        blogs: false

    };

    Page.reset();

    $scope.search = function () {
        //console.log("In $scope.search");
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.sort = function (sortBy) {
        //console.log("In $scope.sort");
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.selectPage = function (page) {
        //console.log("In $scope.selectPage");
        $scope.pagingInfo.page = page;
        loadSimpleSearch();
    };

    function loadSimpleSearch() {
        //console.log("In $scope.loadSimpleSearch");
        $scope.webEntries = null;
        ListService.GetSimpleSearch($scope.pagingInfo).then(function (result) {
            //console.log("result: ", result);
            Page.setTitle('Dyreliste');
            Page.setMetaDescription('Dyreliste, side ' + $scope.pagingInfo.page);
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
            //console.log($scope.webEntries);
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }

    // initial table load
    loadSimpleSearch();
}]);

app.controller('SimpleSearchController', ['$scope', 'SearchService', 'Page', function ($scope, SearchService, Page) {
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: '',
        totalItems: 0,
        animals: true,
        plants: true,
        blogs: true

    };
    Page.reset();

    Page.setTitle('Søg');
    Page.setMetaDescription('Fritekst søgning');

    $scope.simpleSearch = function () {
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.sort = function (sortBy) {
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.selectPage = function (page) {
        $scope.pagingInfo.page = page;
        loadSimpleSearch();
    };

    function loadSimpleSearch() {
        $scope.webEntries = null;
        SearchService.GetSimpleSearch($scope.pagingInfo).then(function (result) {
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }
}]);

app.controller('AdvancedSearchController', ['$scope', 'AdvancedSearchService', 'Page', 'GetterService', function ($scope, AdvancedSearchService, Page, GetterService) {
    console.log("In AdvancedSearchController");
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: '',
        totalItems: 0,
        animals: true,
        plants: true,
        blogs: true,
        searchfields: {
            blogTitle: '',
            blogText: ''
        }
    };

    $scope.formState = {
        formIsValid: false,
        blogIsValid: true,
        plantIsValid: true,
        animalIsValid: true
    };

    $scope.zone = {
        searchBlogs: false,
        searchPlants: false,
        searchAnimals: false
    };

    GetterService.getTags()
        .then(function (result) {
            $scope.tags = result.data;
            console.log("tags: ", $scope.tags);
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured while loading tags");
        });

    GetterService.getPlantGroups()
        .then(function (result) {
            $scope.plantGroups = result.data;
            //$scope.loading = false;
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured while loading plantgroups");
        });

    $scope.change = function () {
        if ($scope.zone.searchBlogs == true) {
            $scope.formState.blogIsValid = (($scope.pagingInfo.searchfields.blogTitle != '') || ($scope.pagingInfo.searchfields.blogText != ''));
        }
        else {
            $scope.formState.blogIsValid = true;
        }
        console.log("form changed");
        console.log("$scope.formState.blogIsValid: ", $scope.formState.blogIsValid);
    };

    Page.reset();

    Page.setTitle('Advanceret søgning');
    Page.setMetaDescription('Advanceret søgning');

    $scope.advancedSearch = function () {
        console.log("advancedSearch");
        $scope.pagingInfo.page = 1;
        loadAdvancedSearch();
    };

    $scope.sort = function (sortBy) {
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadAdvancedSearch();
    };

    $scope.selectPage = function (page) {
        $scope.pagingInfo.page = page;
        loadAdvancedSearch();
    };

    function loadAdvancedSearch() {
        $scope.webEntries = null;
        AdvancedSearchService.GetAdvancedSimpleSearch($scope.pagingInfo).then(function (result) {
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }
}]);

app.controller('navCtrl', function ($rootScope, $scope, $route, $routeParams, $location) {

});

app.controller('MainController', function ($scope, $route, $routeParams, $location) {
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
});

app.controller('OriginCtrl', function ($scope, $http, $rootScope) {
    //$scope.loading = true;


    $scope.origins = [];

    $scope.rand = function (i) {
        return i + Math.random();
    }

    $scope.getFiltered = function () {
        var urlString = '', i, s, len = $rootScope.filterText.length;
        if (len === 0) {
            $scope.origins = [];
            return;
        }
        for (i = 0; i < len; ++i) {
            if (i in $rootScope.filterText) {
                s = $rootScope.filterText[i];
                urlString = urlString.concat(s).concat(';');
            }
        }
        if (urlString.length > 0)
            urlString = urlString.substring(0, urlString.length - 1);

        var url = '/api/origin?text='.concat(urlString);
        console.log("getFiltered url: ", url);
        //$scope.loading = true;
        $http.get(url)
            .then(function (result) {
                //console.log("getFiltered Result: ", result);
                console.log("getFiltered Result.data: ", result.data);
                $scope.origins = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };

    $scope.getApi = function () {
        $http.get('/api/origin')
            .then(function (result) {
                $scope.originHeadlines = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };

    $scope.onCompleteOrigin = function (origin) {
        //DoLater: Handle the '+' sign in origin.Text. It is removed when calling the API
        //console.log("onCompleteOrigin -done: " + origin.Selected + " : " + origin.Text);
        //console.log("Pre-$scope.filterText: ", $rootScope.filterText);
        //console.log("Pre-$scope.filterText: ", $rootScope.filterText.toString());

        var subFilter = origin.Text;
        if (origin.Selected) {
            $rootScope.filterText.push(subFilter);
        } else {
            var index = $rootScope.filterText.indexOf(subFilter);
            if (index > -1) {
                $rootScope.filterText.splice(index, 1);
            }
        }
        //console.log("Post-$scope.filterText: ", $rootScope.filterText);
        //console.log("Post-$scope.filterText: ", $rootScope.filterText.toString());
        $scope.getFiltered();

        //$scope.$apply();
    };


    $scope.getApi();

    //console.log("Ctrl-$scope.filterText: ", $rootScope.filterText);
    //console.log("Ctrl-$scope.filterText.toString(): ", $rootScope.filterText.toString());
});

app.controller("tagCtrl", function ($scope, $http, GetterService) {
    GetterService.getTags().then(function (result) {
        $scope.tags = result;
    }, function (result) {
        //some error
        console.log(status);
        console.log("Error occured while loading tags in tagCtrl");
    });
});

app.controller("webEntryMostReadCtrl", function ($scope, $http) {
    //$scope.loading = true;
    $scope.getWebEntrys = function () {
        $http.get('/api/WebEntry?count=6')
            .then(function (result) {
                $scope.mostReadWebEntries = result;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getWebEntrys();
});

app.controller("webEntryCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, $window, Page) {
    Page.reset();
    Page.setMetaDescription('Her vil du finde lidt om elektronik til akvarier, artikler og beskrivelser af planter, fisk og andre akvariedyr.');

    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    // Show loading spinner.
    //$scope.loading = true;

    $scope.getWebEntrys = function () {
        $http.get('/api/WebEntry')
            .then(function (result) {
                $scope.webEntries = result;
                //console.log("webEntries: ", $scope.webEntries.data);
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getWebEntrys();

});

app.controller("animalCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {

    //console.log("Element: ", $element);
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    $scope.animalId = $scope.$routeParams.id;
    Page.reset();
    var apiUrl = '/api/animal/' + $scope.$routeParams.id;

    $scope.getAnimal = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.animal = result.data;
                Page.setTitle($scope.animal.ScientificName);
                Page.setMetaDescription('Beskrivelse af ' + $scope.animal.ScientificName);
                Page.setAuthor($scope.animal.User.DisplayName);
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getAnimal();
});

app.controller("blogCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    Page.reset();
    var apiUrl = '/api/Blog/' + $scope.$routeParams.id;

    $scope.getItem = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.blog = result.data;
                Page.setTitle($scope.blog.Title);
                Page.setMetaDescription($scope.blog.Teaser);
                Page.setAuthor($scope.blog.Contributor.DisplayName);
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getItem();
});

app.controller('blogListCtrl', ['$scope', 'ListService', 'Page', function ($scope, ListService, Page) {
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: '',
        totalItems: 0,
        animals: false,
        plants: false,
        blogs: true

    };

    Page.reset();

    $scope.search = function () {
        //console.log("In $scope.search");
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.sort = function (sortBy) {
        //console.log("In $scope.sort");
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.selectPage = function (page) {
        //console.log("In $scope.selectPage");
        $scope.pagingInfo.page = page;
        loadSimpleSearch();
    };

    function loadSimpleSearch() {
        //console.log("In $scope.loadSimpleSearch");
        $scope.webEntries = null;
        ListService.GetSimpleSearch($scope.pagingInfo).then(function (result) {
            //console.log("result: ", result);
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
            Page.setTitle('Artikler');
            Page.setMetaDescription('Artikler, side ' + $scope.pagingInfo.page);
            //console.log($scope.webEntries);
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }

    // initial table load
    loadSimpleSearch();
}]);

app.controller('blogsFromTagCtrl', ['$scope', 'BlogsForTagService', '$routeParams', function ($scope, BlogsForTagService, $routeParams) {
    //TODO: What if the list is empty? Need an "error" page
    $scope.maxSize = 6;
    $scope.pagingInfo = {
        page: 1,
        itemsPerPage: 30,
        reverse: true,
        searchString: $routeParams.id,
        totalItems: 0,
        animals: false,
        plants: false,
        blogs: true

    };

    Page.reset();

    $scope.search = function () {
        //console.log("In $scope.search");
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.sort = function (sortBy) {
        //console.log("In $scope.sort");
        if (sortBy === $scope.pagingInfo.sortBy) {
            $scope.pagingInfo.reverse = !$scope.pagingInfo.reverse;
        } else {
            $scope.pagingInfo.sortBy = sortBy;
            $scope.pagingInfo.reverse = false;
        }
        $scope.pagingInfo.page = 1;
        loadSimpleSearch();
    };

    $scope.selectPage = function (page) {
        //console.log("In $scope.selectPage");
        $scope.pagingInfo.page = page;
        loadSimpleSearch();
    };

    function loadSimpleSearch() {
        //console.log("In $scope.loadSimpleSearch");
        $scope.webEntries = null;
        BlogsForTagService.GetSimpleSearch($scope.pagingInfo).then(function (result) {
            //console.log("result: ", result);
            $scope.webEntries = result.data.data;
            $scope.pagingInfo.totalItems = result.data.count;
            Page.setTitle('Artikler');
            Page.setMetaDescription('Artikler, side ' + $scope.pagingInfo.page);
            //console.log($scope.webEntries);
        }, function (result) {
            //some error
            console.log(status);
            console.log("Error occured");
        });
    }

    // initial table load
    loadSimpleSearch();
}]);

app.controller("illnessCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    Page.reset();
    Page.setTitle('Sygdomme');
    Page.setMetaDescription('En liste af nogle af de mest almindeligt forekommende sygdomme.');

    var apiUrl = '/api/Illness';

    $scope.getItem = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.illnesList = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getItem();
});

app.controller("algeaCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    Page.reset();
    Page.setTitle('Alger');
    Page.setMetaDescription('En liste af nogle af de mest almindeligt forekommende algetyper.');

    var apiUrl = '/api/Algea/';

    if (typeof $scope.$routeParams.id !== 'undefined')
        apiUrl = apiUrl + $scope.$routeParams.id;

    $scope.getItem = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.algeaList = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getItem();
});

app.controller("habitatCtrl", function ($scope, $http, $sce, $route, $routeParams, $location, Page) {
    //$scope.loading = true;
    $scope.$route = $route;
    $scope.$location = $location;
    $scope.$routeParams = $routeParams;
    Page.reset();
    Page.setTitle('Biotoper');
    Page.setMetaDescription('En liste af nogle af de mest almindeligt forekommende biotoper.');


    var apiUrl = '/api/Habitat';

    $scope.getItem = function () {
        $http.get(apiUrl)
            .then(function (result) {
                $scope.habitatList = result.data;
                //$scope.loading = false;
            }, function (result) {
                //some error
                console.log(status);
                console.log("Error occured");
            });
    };
    $scope.getItem();
});

app.controller("AboutCtrl", function ($scope, Page) {
    Page.reset();
    Page.setTitle('Om Akvariet.dk');
    Page.setMetaDescription('Lidt information om Akvariet.dk');
});

app.config(['$routeProvider',
    function ($routeProvider) {
        // if !loggedIn redirectTo: '/login'

        $routeProvider
            .when("/", {
                templateUrl: "/Resources/Pages/main.html",
                controller: "webEntryCtrl",
                data: {
                    requireLogin: false
                }
            })
            .when("/advancedsearch", {
                templateUrl: '/Resources/Pages/advancedsearch.html',
                controller: 'AdvancedSearchController',
                data: {

                    requireLogin: false
                }
            })
            .when('/animal/:id', {
                templateUrl: '/Resources/Pages/animal.html',
                controller: 'animalCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/animallist/', {
                templateUrl: '/Resources/Pages/entrylist.html',
                controller: 'animalListCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/plant/:id', {
                templateUrl: '/Resources/Pages/plant.html',
                controller: 'plantCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/plantlist', {
                templateUrl: '/Resources/Pages/entrylist.html',
                controller: 'plantlistCtrl',
                data: {
                    requireLogin: false
                }

            })
            .when('/blog/:id', {
                templateUrl: '/Resources/Pages/blog.html',
                controller: 'blogCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/bloglist', {
                templateUrl: '/Resources/Pages/entrylist.html',
                controller: 'blogListCtrl',
                data: {
                    requireLogin: false
                }

            })
            .when('/tag/:id', {
                templateUrl: '/Resources/Pages/entrylist.html',
                controller: 'blogsFromTagCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/sygdomme', {
                title: 'Sygdomme',
                templateUrl: '/Resources/Pages/Illness.html',
                controller: 'illnessCtrl',
                data: {
                    meta: {
                        'title': 'Sygdomme',
                        'description': 'En liste af nogle af de mest almindeligt forekommende sygdomme.'
                    },
                    requireLogin: false
                }
            })
            .when('/alger', {
                templateUrl: '/Resources/Pages/algea.html',
                controller: 'algeaCtrl',
                data: {
                    meta: {
                        'title': 'Alger',
                        'description': 'En liste af nogle af de mest almindeligt forekommende algetyper.'
                    },
                    requireLogin: false
                }
            })
            .when('/alger/:id', {
                templateUrl: '/Resources/Pages/algea.html',
                controller: 'algeaCtrl',
                data: {
                    requireLogin: false
                }
            })
            .when('/kontakt', {
                templateUrl: '/Resources/Pages/Contact2.html',
                data: {
                    meta: {
                        'title': 'Kontakt',
                        'description': 'Her kan du finde kontakt information til Akvariet.dk.'
                    },
                    requireLogin: false
                }
            })
            .when('/search', {
                templateUrl: '/Resources/Pages/SimpleSearch.html',
                controller: 'SimpleSearchController',
                data: {
                    meta: {
                        'title': 'Søg',
                        'description': 'Fritekst søgning'
                    },
                    requireLogin: false
                }
            })
            .when('/biotoper', {
                templateUrl: '/Resources/Pages/Habitat.html',
                controller: 'habitatCtrl',
                data: {
                    meta: {
                        'title': 'Biotober',
                        'description': 'En liste af nogle af de mest almindeligt forekommende biotoper.'
                    },
                    requireLogin: false
                }
            })
            .when('/om', {
                templateUrl: '/Resources/Pages/About.html',
                controller: 'AboutCtrl',
                data: {
                    meta: {
                        'title': 'Om Akvariet.dk',
                        'description': 'Lidt information om Akvariet.dk'
                    },
                    requireLogin: false
                }
            })

            .otherwise({
                redirectTo: '/'
            });
    }]);