﻿
function showPopUp(element) {
    //document.getElementById('functionDiv').style.display
    document.getElementById('id01').style.display = 'block';
    console.log("Element: ", element.innerHTML);
    var popupContent = document.getElementById('popupContent');
    console.log("Popup: ", popupContent.innerHTML);
    popupContent.innerHTML = element.innerHTML;
}

function getPDF(element, title) {
    post('/Handlers/PDFHandler/', { htmlContent: element.innerHTML, title: title });
}


function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}


function checkCheckboxes(id, pID) {
    $('#' + pID).find(':checkbox').each(function () {
        jQuery(this).attr('checked', $('#' + id).is(':checked'));
    });
}

function isNumberKey(evt, element) {
    if ($(element).val().length == 0)
        $(element).value = "0";
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode === 46 || charCode === 8 || charCode === 44))
        return false;
    else {
        var len = $(element).val().length;
        var index = $(element).val().indexOf('.');
        if (index > 0 && (charCode === 46 || charCode === 44)) {
            return false;
        }
        if (index > 0) {
            var CharAfterdot = (len + 1) - index;
            if (CharAfterdot > 3) {
                return false;
            }
        }

    }
    return true;
}

function gen_mail_to_link(lhs, rhs, subject) {
    document.write("<a href=\"mailto");
    document.write(":" + lhs + "@");
    document.write(rhs + "?subject=" + subject + "\">" + lhs + "@" + rhs + "<\/a>");
}

function ShowHideElement(chkBox, divName) {
    var element = document.getElementById(divName);
    element.style.display = chkBox.checked ? "block" : "none";
    if (!document.getElementById('searchBlogs').checked)
        document.getElementById("missingBlogInputValues").style.display = "none";
    if (!document.getElementById('searchPlants').checked)
        document.getElementById("missingPlantInputValues").style.display = "none";
    if (!document.getElementById('searchAnimals').checked)
        document.getElementById("missingAnimalInputValues").style.display = "none";

    if (!document.getElementById('searchBlogs').checked && !document.getElementById('searchPlants').checked && !document.getElementById('searchAnimals').checked)
        document.getElementById('lowerSubmitButton').style.display = "none";
    else
        document.getElementById('lowerSubmitButton').style.display = "block";
}

function SwitchSearchType() {
    if (document.getElementById('radioFulltext').checked) {
        document.getElementById('fulltextContainer').style.display = "block";
        document.getElementById('specificContainer').style.display = "none";

    } else if (document.getElementById('radioSpecifics').checked) {
        document.getElementById('fulltextContainer').style.display = "none";
        document.getElementById('specificContainer').style.display = "block";
    }
}

var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    xhr.send();
};


function loadAndSetTags() {
    getJSON('/api/tag',
        function (err, data) {
            if (data !== null) {
                var tagContainer = document.getElementById("tagContainer");

                var tagInnerHtml = "";
                for (var i = 0; i < data.length; i += 1) {
                    tagInnerHtml = tagInnerHtml + "<span class=\"w3-tag w3-light-grey w3-small w3-margin-bottom\">" + data[i].TagName + "</span>&nbsp;";
                }
                tagContainer.innerHTML = tagInnerHtml;
            }
        });
}

function clearFormElements(elementId) {

}

// Pass the checkbox name to the function
function getCheckedBoxes(chkboxName) {
    var checkboxes = document.getElementsByName(chkboxName);
    // loop over them all
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            return true;
        }
    }
    return false;
}

function clearAllOriginCheckboxes() {
    $('#elemTagCheckList input[type="checkbox"]:checked').each(function (i, el) {
        el.checked = false;
    });
}

function isValueEmpty(elementId) {
    return (document.getElementById(elementId).value == "");
}


function validateAdvancedSearchForm() {
    $("#missingBlogInputValues").hide();
    $("#missingPlantInputValues").hide();
    $("#missingAnimalInputValues").hide();


    var areWeOk = true;

    if (document.forms["advancedSearchForm"]["searchBlogs"].checked) {
        var isACheckboxChecked = false;
        var isBlogTitleFilled = document.getElementById("blogTitle").value != "";
        var isBlogTextFilled = document.getElementById("blogText").value != "";
        $('#elemTagCheckList input[type="checkbox"]:checked').each(function (i, el) {
            if (el.checked)
                isACheckboxChecked = true;
        });
        if (!isBlogTitleFilled && !isBlogTextFilled && !isACheckboxChecked) {
            $("#missingBlogInputValues").show();
            areWeOk = false;
        }
    }

    if (document.forms["advancedSearchForm"]["searchPlants"].checked) {
        var isCheckboxChecked = true;
        var isInputsFilled = true;
        var isSelectsFilled = true;

        var checkboxCount = 0;
        $('#plantSpecific input[type="checkbox"]').each(function (i, el) {
            if (el.checked) {
                checkboxCount++;
            }
            else {
                isCheckboxChecked = false;
            }
        });
        if (checkboxCount == 0)
            isCheckboxChecked = false;

        var isInputFilledCount = 0;
        $('#plantSpecific input[type="text"]').each(function (i, el) {
            if (el.value != "") {
                isInputFilledCount++;
                //isInputsFilled = false;
                console.log("isInputFilledCount: ", isInputFilledCount);
            }
        });
        if (isInputFilledCount == 0)
            isInputsFilled = false;



        if (!isCheckboxChecked && !isInputsFilled && document.getElementById("elemPlantGroup").value == "") {
            areWeOk = false;
            document.getElementById("missingPlantInputValues").style.display = "block";
        }
    }

    if (document.forms["advancedSearchForm"]["searchAnimals"].checked) {
        console.log("In Animal");
        var isCheckboxChecked = true;
        var isInputsFilled = true;
        var checkboxCount = 0;
        console.log("Before");
        console.log("isCheckboxChecked: ", isCheckboxChecked);
        console.log("isInputsFilled: ", isInputsFilled);

        $('#animalSpecific input[type="checkbox"]').each(function (i, el) {
            checkboxCount++;
            if (el.checked) {

            }
            else {
                isCheckboxChecked = false;
            }
        });
        if (checkboxCount == 0)
            isCheckboxChecked = false;

        var isInputFilledCount = 0;
        $('#animalSpecific input[type="text"]').each(function (i, el) {
            if (el.value != "") {
                isInputFilledCount++;
            }
        });
        if (isInputFilledCount == 0)
            isInputsFilled = false;

        console.log("After");
        console.log("isCheckboxChecked: ", isCheckboxChecked);
        console.log("isInputsFilled: ", isInputsFilled);

        if (!isCheckboxChecked && !isInputsFilled) {
            areWeOk = false;
            document.getElementById("missingAnimalInputValues").style.display = "block";
        }
    }

    if (areWeOk) {
        $('#yourControllerElementID').scope.advancedSearch();
    }
    return areWeOk;
}