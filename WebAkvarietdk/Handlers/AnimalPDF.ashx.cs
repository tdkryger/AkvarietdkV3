﻿using Akvariet.Fishbase.Model;
using System;
using System.Web;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Handlers
{
    /// <summary>
    /// Summary description for AnimalPDF
    /// </summary>
    public class AnimalPDF : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string idString = context.Request.QueryString["id"];
            if (int.TryParse(idString, out int animalId))
            {
                IntStringClass animalName = DTOHelper.GetAnimalScientificName(animalId);
                if (animalName != null)
                {
                    HttpResponse httpResponse = context.Response;
                    httpResponse.ContentType = "application/pdf";
                    httpResponse.AppendHeader("Content-Disposition", $"attachment; filename={TDK.Tools.StringTools.RemoveIlligalCharsInFilepath(animalName.Text)}.pdf");

                    httpResponse.Cache.SetCacheability(HttpCacheability.Public);
                    httpResponse.Cache.SetExpires(DateTime.Now.AddMinutes(10));
                    httpResponse.Cache.SetMaxAge(TimeSpan.FromMinutes(10));


                    PDF.PDFGenerator pdf = new PDF.PDFGenerator();
                    byte[] pdfData = pdf.CreateAnimalPDF(animalId);
                    httpResponse.AppendHeader("Content-Length", pdfData.Length.ToString());
                    httpResponse.BinaryWrite(pdfData);
                    return;
                }
                else
                {
                    throw new ArgumentNullException($"Id ({animalId}) was not found");
                }
            }
            else
            {
                throw new ArgumentException($"{idString} is not a valid number");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}