﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace WebAkvarietdk.Handlers
{
    /// <summary>
    /// Summary description for ThumbHandler
    /// </summary>
    public class ThumbHandler : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void ProcessRequest(HttpContext context)
        {
            bool sendErrorImage = false;
            HttpResponse httpResponse = context.Response;
            httpResponse.ContentType = "image/png";
            httpResponse.Cache.SetCacheability(HttpCacheability.Public);
            httpResponse.Cache.SetExpires(DateTime.Now.AddMinutes(10));
            httpResponse.Cache.SetMaxAge(TimeSpan.FromMinutes(10));


            string id = context.Request.QueryString["imageId"];
            string widthString = context.Request.QueryString["width"];
            string heightString = context.Request.QueryString["height"];
            int height = 50;
            int width = 50;
            if (!int.TryParse(widthString, out width))
                width = 50;
            int fid = 0;
            if (!int.TryParse(heightString, out height))
                height = 50;
            if (height > 100 || width > 100)
            {
                height = 100;
                width = 100;
            }
            if (int.TryParse(id, out fid))
            {
                try
                {
                    FishbaseImage fbImage;
                    using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(new FishbaseUnitOfWork()))
                    {
                        fbImage = repo.GetById(fid);
                    }

                    if (fbImage != null)
                    {
                        if (ImageFormat.Jpeg.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/jpg";
                        }
                        else if (ImageFormat.Png.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/png";
                        }
                        else if (ImageFormat.Gif.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/gif";
                        }

                        ImageConverter _imageConverter = new ImageConverter();
                        byte[] xByte = (byte[])_imageConverter.ConvertTo(fbImage.Thumbnail(width, height), typeof(byte[]));
                        httpResponse.BinaryWrite(xByte);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "ThumbHandler");
                    sendErrorImage = true;
                }
            }
            else
            {
                sendErrorImage = true;
            }
            if (sendErrorImage)
            {
                //TODO: A proper error image plz
                httpResponse.ContentType = "image/jpg";
                httpResponse.WriteFile("/Resources/Images/woods.jpg");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}