﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Handlers
{
    /// <summary>
    /// Summary description for HabitatPDF
    /// </summary>
    public class HabitatPDF : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string idString = context.Request.QueryString["id"];
            if (int.TryParse(idString, out int id))
            {
                Habitat habitat = DTOHelper.GetHabitat(id);
                if (habitat != null)
                {
                    HttpResponse httpResponse = context.Response;
                    httpResponse.ContentType = "application/pdf";
                    httpResponse.AppendHeader("Content-Disposition", $"attachment; filename={TDK.Tools.StringTools.RemoveIlligalCharsInFilepath(habitat.Text)}.pdf");

                    httpResponse.Cache.SetCacheability(HttpCacheability.Public);
                    httpResponse.Cache.SetExpires(DateTime.Now.AddMinutes(10));
                    httpResponse.Cache.SetMaxAge(TimeSpan.FromMinutes(10));


                    PDF.PDFGenerator pdf = new PDF.PDFGenerator();
                    byte[] pdfData = pdf.CreateHabitatPDF(habitat);
                    httpResponse.AppendHeader("Content-Length", pdfData.Length.ToString());
                    httpResponse.BinaryWrite(pdfData);
                    return;
                }
                else
                {
                    throw new ArgumentNullException($"Habitat for id ({idString}) was not found");
                }
            }
            else
            {
                throw new ArgumentException($"{idString} is not a valid number");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}