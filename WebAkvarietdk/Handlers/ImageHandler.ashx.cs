﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;

namespace WebAkvarietdk.Handlers
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void ProcessRequest(HttpContext context)
        {
            /*
                * TODO: Add watermark on Photos from FishbaseImage as a block below the image. ie. make the canvas bigger! 
            */
            bool sendErrorImage = false;
            HttpResponse httpResponse = context.Response;
            httpResponse.ContentType = "image/png";
            httpResponse.Cache.SetCacheability(HttpCacheability.Public);
            httpResponse.Cache.SetExpires(DateTime.Now.AddMinutes(10));
            httpResponse.Cache.SetMaxAge(TimeSpan.FromMinutes(10));


            string id = context.Request.QueryString["imageId"];
            string widthString = context.Request.QueryString["width"];
            string heightString = context.Request.QueryString["height"];
            int height = 50;
            int width = 50;



            if (int.TryParse(id, out int fid))
            {
                try
                {
                    FishbaseImage fbImage;
                    using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                    {
                        using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
                        {
                            fbImage = repo.GetById(fid);
                        }
                    }

                    if (fbImage != null)
                    {
                        if (ImageFormat.Jpeg.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/jpg";
                        }
                        else if (ImageFormat.Png.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/png";
                        }
                        else if (ImageFormat.Gif.Equals(fbImage.Image.RawFormat))
                        {
                            httpResponse.ContentType = "image/gif";
                        }
                        int imgWidth = fbImage.Image.Width;
                        int imgHeight = fbImage.Image.Height;

                        if (!int.TryParse(widthString, out width))
                            width = imgWidth;
                        if (!int.TryParse(heightString, out height))
                            height = imgHeight;

                        Image tempImage = null;

                        if (height == imgHeight && width == imgWidth)
                        {
                            try
                            {
                                // No resize
                                tempImage = TDK.Tools.ImageTools.AddTextBlock(fbImage.Image, Color.White, fbImage.CopyRight, false);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex, "No resize");
                                sendErrorImage = true;
                            }
                        }
                        else
                        {
                            try
                            {
                                Image shrink = TDK.Tools.ImageTools.FixedSize(fbImage.Image, width, height, Color.White);
                                tempImage = TDK.Tools.ImageTools.AddTextBlock(shrink, Color.White, fbImage.CopyRight, false);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex, "Resize");
                                sendErrorImage = true;
                            }
                        }

                        if (tempImage == null)
                        {
                            sendErrorImage = true;
                        }
                        else
                        {
                            try
                            {
                                ImageConverter _imageConverter = new ImageConverter();
                                byte[] xByte = (byte[])_imageConverter.ConvertTo(tempImage, typeof(byte[]));
                                httpResponse.BinaryWrite(xByte);
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex, "Convert");
                                sendErrorImage = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "ImageHandler");
                    sendErrorImage = true;
                }
            }
            else
            {
                sendErrorImage = true;
            }
            if (sendErrorImage)
            {
                httpResponse.ContentType = "image/jpg";
                httpResponse.WriteFile("/Resources/Images/woods.jpg");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}