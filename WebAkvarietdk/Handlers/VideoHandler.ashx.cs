﻿using System;
using System.Web;

namespace WebAkvarietdk.Handlers
{
    /// <summary>
    /// Summary description for VideoHandler
    /// </summary>
    public class VideoHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpResponse httpResponse = context.Response;
            httpResponse.ContentType = "video/mp4";
            httpResponse.Cache.SetCacheability(HttpCacheability.Public);
            httpResponse.Cache.SetExpires(DateTime.Now.AddMinutes(10));
            httpResponse.Cache.SetMaxAge(TimeSpan.FromMinutes(10));


            string id = context.Request.QueryString["videoId"];
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}