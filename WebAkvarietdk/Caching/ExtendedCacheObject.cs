﻿using Akvariet.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAkvarietdk.Caching
{
    public class ExtendedCacheObject
    {
        #region Properties
        public List<IEntity> Items { get; set; }
        public DateTime SearchTime { get; set; }
        public string Name { get; set; }
        #endregion
    }
}