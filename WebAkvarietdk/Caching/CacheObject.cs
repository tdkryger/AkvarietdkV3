﻿using System;
using System.Collections.Generic;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Caching
{
    public class CacheObject
    {
        #region Properties
        public List<DTOWebEntry> Result { get; set; }
        public DateTime SearchTime { get; set; }
        #endregion
    }
}