﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebAkvarietdk.Caching
{
    public static class ExtendedCache
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static object _lockObject = new object();
        private static TimeSpan _defaultCacheTime = new TimeSpan(0, 15, 0);
        private static DateTime _lastLoad = DateTime.MinValue;

        #region Lists
        //TODO: More list preloaded
        private static List<TagEntity> _tagList;
        private static List<Habitat> _habitatList;
        private static List<AnimalGroup> _animalGroupList;
        private static List<PlantGroup> _plantGroup;
        private static List<PlantCharacteristic> _plantCharacteristic;
        #endregion


        #region Properties
        public static List<TagEntity> TagList { get { return GetTagList(); } }
        public static List<AnimalGroup> AnimalGroupList { get { return GetAnimalGroup(); } }
        public static List<PlantGroup> PlantGroup { get { return GetPlantGroup(); } }
        public static List<PlantCharacteristic> PlantCharacteristic { get { return GetPlantCharacteristic(); } }
        //public static List<Habitat> HabitatList { get { return GetHabitatList(); } }

        #endregion


        static ExtendedCache()
        {
            _tagList = GetTagList();
            _animalGroupList = GetAnimalGroup();
            _plantGroup = GetPlantGroup();
            _plantCharacteristic = GetPlantCharacteristic();
            //_habitatList = GetHabitatList();


            _lastLoad = DateTime.Now.AddMilliseconds(_defaultCacheTime.TotalMilliseconds);
        }


        #region Private methods
        private static List<PlantCharacteristic> GetPlantCharacteristic()
        {
            if (DateTime.Now >= _lastLoad)
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<PlantCharacteristic> repo = new Repository<PlantCharacteristic>(uow))
                    {
                        _plantCharacteristic = repo.GetAll().ToList();
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
            _animalGroupList = _animalGroupList.OrderBy(x => x.Name).ThenBy(x => x.Zone).ToList();
            return _plantCharacteristic;
        }

        private static List<AnimalGroup> GetAnimalGroup()
        {
            if (DateTime.Now >= _lastLoad)
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<AnimalGroup> repo = new Repository<AnimalGroup>(uow))
                    {
                        _animalGroupList = repo.GetAll().ToList();
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
            _animalGroupList = _animalGroupList.OrderBy(x => x.Name).ThenBy(x=>x.Zone).ToList();
            return _animalGroupList;
        }

        private static List<PlantGroup> GetPlantGroup()
        {
            if (DateTime.Now >= _lastLoad)
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (Repository<PlantGroup> repo = new Repository<PlantGroup>(uow))
                    {
                        _plantGroup = repo.GetAll().ToList();
                    }
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
            _plantGroup = _plantGroup.OrderBy(x => x.Text).ToList();
            return _plantGroup;
        }

        //private static List<Habitat> GetHabitatList()
        //{
        //    if (DateTime.Now >= _lastLoad)
        //    {
        //        using (Repository<Habitat> repo = new Repository<Habitat>(uow))
        //        {
        //            _habitatList = repo.GetAll().ToList();
        //        }
        //    }
        //    return _habitatList;
        //}

        private static List<TagEntity> GetTagList()
        {
            if (DateTime.Now >= _lastLoad)
            {
                 string sql = "SELECT `tagentity`.`Id`, `tagentity`.`TagName`"
                            + "FROM `tagentity`"
                            + "INNER JOIN `tagentitytoblogentity` ON `tagentitytoblogentity`.`TagEntity_id`=`tagentity`.`Id`;";
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    _tagList = (List<TagEntity>)uow.Session.CreateSQLQuery(sql)
                                                    .AddEntity(typeof(TagEntity))
                                                    .List<TagEntity>();
                    uow.Commit();
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
            return _tagList;
        }

        #endregion
    }
}