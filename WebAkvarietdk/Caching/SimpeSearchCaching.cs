﻿using NLog;
using System;
using System.Collections.Generic;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Caching
{
    public static class SimpeSearchCaching
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Dictionary<SimpleSearchKey, CacheObject> _cache = new Dictionary<SimpleSearchKey, CacheObject>();
        private static object _lockObject = new object();
        private static TimeSpan _defaultCacheTime = new TimeSpan(0, 2, 0);
        private static List<DTOWebEntry> _webEntrys = null;

        public static int FrontpageCount { get; set; } = 6;

        #region Public methods
        public static void LoadCacheData()
        {
            //TODO: Cache more plz!
            _webEntrys = DTOHelper.GetWebEntrys(FrontpageCount);
        }

        public static List<DTOWebEntry> GetWebEntryCount()
        {
            if (_webEntrys == null)
                LoadCacheData();
            return _webEntrys;
        }

        public static void Add(SimpleSearchKey searchKey, List<DTOWebEntry> result)
        {

            Add(searchKey, result, _defaultCacheTime);
        }

        public static void Add(SimpleSearchKey searchKey,  List<DTOWebEntry> result, TimeSpan cacheTime)
        {
            lock (_lockObject)
            {
                try
                {
                    if (!_cache.ContainsKey(searchKey))
                    {
                        _cache.Add(searchKey, new CacheObject() { Result = result, SearchTime = DateTime.Now.AddMilliseconds(cacheTime.TotalMilliseconds) });
                    }
                    else
                    {
                        CacheObject co = _cache[searchKey];
                        _cache.Remove(searchKey);
                        co.SearchTime = DateTime.Now;
                        _cache.Add(searchKey, co);
                    }
                }
                catch(Exception ex)
                {
                    logger.Warn(ex, "Error in SimpeSearchCaching");
                }
            }
        }

        public static List<DTOWebEntry> Get(SimpleSearchKey searchKey)
        {
            try
            {
                if (_cache.ContainsKey(searchKey))
                {
                    CacheObject co = _cache[searchKey];
                    if (DateTime.Now <= co.SearchTime)
                        return co.Result;
                    else
                    {
                        _cache.Remove(searchKey);
                        return null;
                    }
                }
                else
                    return null;
            }
            catch(Exception ex)
            {
                logger.Warn(ex, "Error in SimpeSearchCaching");
                return null;
            }
        }
        #endregion
    }
}