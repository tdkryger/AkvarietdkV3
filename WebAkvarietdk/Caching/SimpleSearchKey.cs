﻿namespace WebAkvarietdk.Caching
{
    public class SimpleSearchKey
    {
        public string SearchString { get; }
        public bool Animals { get; }
        public bool Plants { get; }
        public bool Blogs { get; }

        public SimpleSearchKey(string searchString, bool animals, bool plants, bool blogs)
        {
            this.SearchString = searchString;
            this.Animals = animals;
            this.Plants = plants;
            this.Blogs = blogs;
        }

        public bool Equals(SimpleSearchKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.SearchString == SearchString && other.Animals == Animals && other.Plants == Plants && other.Blogs == Blogs;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(SimpleSearchKey)) return false;
            return Equals((SimpleSearchKey)obj);
        }

        public override int GetHashCode()
        {
            int r = SearchString.GetHashCode();
            if (Animals) r = r + 100;
            if (Plants) r = r + 200;
            if (Blogs) r = r + 400;
            return r;
        }
    }
}