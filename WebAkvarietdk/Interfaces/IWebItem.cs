﻿using System.Collections.Generic;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Interfaces
{
    public interface IWebItem
    {
        int Id { get; set; }
        List<DTOComment> Comments { get; set; }
    }
}
