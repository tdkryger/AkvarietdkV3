﻿using Akvariet.Fishbase.Model;

namespace WebAkvarietdk.Models
{
    public class DTODBFile
    {
        public int Id { get; set; }
        public byte[] FileContent { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }

        public DTODBFile() { }

        public DTODBFile(DBFile item)
        {
            this.Id = item.Id;
            this.FileContent = item.FileContent;
            this.Filename = item.Filename;
            this.ContentType = item.ContentType;
        }

        public DBFile ToDBFile()
        {
            return new DBFile()
            {
                ContentType = this.ContentType,
                FileContent = this.FileContent,
                Filename = this.Filename,
                Id = this.Id
            };
        }
    }
}