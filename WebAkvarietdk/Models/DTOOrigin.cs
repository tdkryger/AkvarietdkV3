﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAkvarietdk.Models
{
    public class DTOOrigin
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        public string SubOrigin { get; set; }
        public string Combined { get { return $"'{Text} - {SubOrigin.Replace("'", "")}'"; } }
        #endregion

        #region Constructors
        public DTOOrigin(Origin item)
        {
            this.Id = item.Id;
            this.Text = item.Text;
            this.SubOrigin = item.SubOrigin;
        }
        #endregion
    }
}