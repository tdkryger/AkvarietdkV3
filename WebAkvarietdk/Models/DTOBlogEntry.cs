﻿using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;

namespace WebAkvarietdk.Models
{
    public class DTOBlogEntry : Interfaces.IWebItem
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Properties
        public int Id { get; set; }
        public List<DTOImage> ImageList { get; set; } = new List<DTOImage>();
        public DateTime TimeStamp { get; set; }
        public int ViewCount { get; set; }
        public List<DTOComment> Comments { get; set; } = new List<DTOComment>();

        public DTOImage FrontImage { get; set; }

        public string Title { get; set; }
        public string Teaser { get; set; }
        public string Body { get; set; }
        public DTOUser User { get; set; }

        public int CommentCount { get { return Comments.Count; } }
        public List<DTOTag> Tags { get; set; } = new List<DTOTag>();
        #endregion

        #region Constructors
        public DTOBlogEntry() { }

        public DTOBlogEntry(BlogEntity item)
        {
            try
            {
                if (item.Tags != null)
                {
                    foreach(TagEntity tag in item.Tags)
                    {
                        this.Tags.Add(new DTOTag(tag));
                    }
                }
                this.Id = item.Id;
                this.TimeStamp = item.Posted;
                this.ViewCount = item.PostRead;
                this.Title = item.Title;
                this.Teaser = item.Teaser;
                this.Body = item.Body;
                if (item.Image != null)
                    this.FrontImage = new DTOImage()
                    {
                        Id = item.Image.Id,
                        ImageUrl = $"/Handlers/ImageHandler.ashx?imageId={item.Image.Id}",
                        Title = item.Image.Title
                    };

                this.ImageList = new List<DTOImage>();
                this.ImageList.Add(new DTOImage()
                {
                    ImageUrl = $"/Handlers/ImageHandler.ashx?imageId={item.Image.Id}",
                    Id = item.Image.Id,
                    Title = item.Image.Title
                });
                this.User = new DTOUser(item.Contributor);
            }
            catch (Exception ex)
            {
                if (item != null)
                    logger.Error(ex, "Exception in Constructor of DTOBlogEntry. BlogEntity: id={0} ", item.Id);
                else
                    logger.Error(ex, "Exception in Constructor of DTOBlogEntry. BlogEntity is null ");

            }
        }
        #endregion
    }
}