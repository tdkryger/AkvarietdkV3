﻿namespace WebAkvarietdk.Models
{
    public class DTOImage
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
    }
}