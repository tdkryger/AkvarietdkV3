﻿using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using WebAkvarietdk.Tools;

namespace WebAkvarietdk.Models
{
    public class DTOAnimal : Interfaces.IFishbaseDTO
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region From Interfaces
        public int Id { get; set; }
        public string ScientificName { get; set; }
        public string Tradenames { get; set; }
        public string Synonyms { get; set; }
        public string Origin { get; set; }
        public string Temperature { get; set; }
        public string Hardness { get; set; }
        public string Ph { get; set; }
        public string Describtion { get; set; }
        public List<DTOImage> ImageList { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ViewCount { get; set; }
        public List<DTOComment> Comments { get; set; } = new List<DTOComment>();
        public string PDFUrl { get; }
        #endregion

        public string Family { get; set; }
        public string Order { get; set; }
        public string SubOrder { get; set; }
        public string CommonGroup { get; set; }
        public string Size { get; set; }
        public string BioLoad { get; set; }
        public string SwimLevel { get; set; }
        public List<string> DietaryRequirements { get; set; } = new List<string>();
        public List<string> CommonIllnesses { get; set; } = new List<string>();
        public string Temperament { get; set; }
        public List<string> Sociableness { get; set; } = new List<string>();
        public int MinGroupSize { get; set; }
        public string PlantedTank { get; set; }
        public int MinTankSize { get; set; }
        public string MinFiltrationLevel { get; set; }
        public List<DTOHabitat> PreferedHabitat { get; set; } = new List<DTOHabitat>();
        public bool BreadInCaptivity { get; set; } = false;
        public bool RequiresWood { get; set; } = false;
        public DTOUser User { get; set; }
        public int CommentCount { get { return Comments.Count; } }

        #region Constructor
        public DTOAnimal() { }

        public DTOAnimal(Animal animal)
        {
            try
            {
                #region From Interfaces
                this.Id = animal.Id;
                this.ScientificName = animal.ScientificName.ToString();
                this.Tradenames = string.Empty;
                this.Synonyms = string.Empty;
                foreach (Tradename tradename in animal.Tradenames)
                    this.Tradenames = this.Tradenames + tradename.ToString() + ", ";
                if (this.Tradenames.Length > 2)
                    this.Tradenames = this.Tradenames.Substring(0, this.Tradenames.Length - 2);
                foreach (ScientificName synonym in animal.Synonyms)
                    this.Synonyms = this.Synonyms + synonym.ToString() + ", ";
                if (this.Synonyms.Length > 2)
                    this.Synonyms = this.Synonyms.Substring(0, this.Synonyms.Length - 2);
                this.Origin = animal.Origin.ToString();
                this.Temperature = $"{animal.WaterTemperature.Min:#0.#}-{animal.WaterTemperature.Max:#0.#}°C";
                if (animal.WaterHardness.Min == animal.WaterHardness.Max)
                    this.Hardness = "Ukendt";
                else
                    this.Hardness = $"{animal.WaterHardness.Min:#0.#}-{animal.WaterHardness.Max:#0.#}° GH";
                if (animal.WaterPh.Min == animal.WaterPh.Max)
                    this.Ph = "Ukendt";
                else
                    this.Ph = $"{animal.WaterPh.Min:#0.#}-{animal.WaterPh.Max:#0.#}";
                if (animal.WaterPh.AddSalt)
                    this.Ph = this.Ph + " (Brak eller saltvand)";
                this.Describtion = animal.Description;

                this.ImageList = new List<DTOImage>();
                foreach (FishbaseImage image in animal.Images)
                    this.ImageList.Add(new DTOImage()
                    {
                        ImageUrl = $"/Handlers/ImageHandler.ashx?imageId={image.Id}" + Akvariet.Common.AkvarietConfiguration.ImageSize,
                        Id = image.Id,
                        Title = image.Title
                    });
                this.CreateDate = animal.CreateDate;
                this.ModifiedDate = animal.ModifiedDate;
                this.ViewCount = animal.ViewCount;
                #endregion

                if (animal.Order.ToString() == "Ukendt - Ukendt")
                    this.Order = "Ukendt";
                else
                    this.Order = animal.Order.ToString();
                this.SubOrder = animal.SubOrder.Name;
                this.Family = animal.Family.Name;
                this.CommonGroup = animal.CommonGroup.ToString();
                if (animal.Size.Min == 0)
                {
                    this.Size = "Ukendt";
                }
                else
                {
                    if ((animal.Size.Min == animal.Size.Max) || (animal.Size.Min > animal.Size.Max))
                    {
                        this.Size = $"Op til {animal.Size.Min:#0.#} cm";
                    }
                    else
                    {
                        this.Size = $"{animal.Size.Min:#0.#}-{animal.Size.Max:#0.#} cm";

                    }
                }

                this.BioLoad = EnumWords.AnimalBioloadToString(animal.Bioload);
                this.SwimLevel = EnumWords.AnimalSwimLevelToString(animal.SwimLevel);
                foreach (Diet diet in animal.DietaryRequirements)
                {
                    this.DietaryRequirements.Add(diet.Name);
                }
                //TODO: Rethink this. Link to list of illnesses?
                foreach (Illness illness in animal.CommonIllnesses)
                    this.CommonIllnesses.Add(illness.Name);
                this.Temperament = animal.Temperament.Text;
                foreach (Social social in animal.Sociableness)
                    this.Sociableness.Add(social.Text);
                this.MinGroupSize = animal.MingroupSize;
                this.PlantedTank = EnumWords.AnimalPlantedTankToString(animal.PlantedTank);
                this.MinTankSize = (int)animal.MinTankSize;
                this.MinFiltrationLevel = EnumWords.AnimalFiltrationToString(animal.MinFiltrationLevel);
                foreach (Habitat habitat in animal.PreferedHabitat)
                {
                    this.PreferedHabitat.Add(new DTOHabitat(habitat));
                }
                this.BreadInCaptivity = animal.BreadInCaptivity;
                this.RequiresWood = animal.RequiresWood;
                this.User = new DTOUser(animal.Contributor);
                this.PDFUrl = $"/Handlers/AnimalPDF.ashx?id={animal.Id}";
            }
            catch (Exception ex)
            {
                if (animal != null)
                    logger.Error(ex, "Exception in Constructor of DTOAnimal. Animal: id={0} ", animal.Id);
                else
                    logger.Error(ex, "Exception in Constructor of DTOAnimal. Animal is null ");
            }
        }
        #endregion

        #region Public Methods
        public string PreferedHabitatText()
        {
            string value = string.Empty;
            if (this.PreferedHabitat != null)
            {
                foreach (DTOHabitat habitat in this.PreferedHabitat)
                {
                    value = value + habitat.Text + ", ";
                }
                if (value.Length > 2)
                    value = value.Substring(0, value.Length - 2).Trim();
            }
            return value;

        }
        #endregion
    }
}