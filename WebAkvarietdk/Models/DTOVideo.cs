﻿namespace WebAkvarietdk.Models
{
    public class DTOVideo : DTOImage
    {
        public string HtmlTag { get { return GetHtmlTag(); } }
        public string MediaType { get; set; } = "video/mp4";


        #region Private methods
        private string GetHtmlTag()
        {
            return $"<video controls autoplay><source src=\"movie.mp4\" type=\"{MediaType}\">Your browser does not support the video tag.</video>";
        }
        #endregion
    }
}