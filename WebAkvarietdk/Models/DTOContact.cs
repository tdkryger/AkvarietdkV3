﻿using System.ComponentModel.DataAnnotations;


namespace WebAkvarietdk.Models
{
    public class DTOContact
    {
        [Required]
        public string FromName { get; set; }
        [Required]
        public string FromEMail { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Content { get; set; }

    }
}