﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebAkvarietdk.Models
{
    public class DTOContactWithFiles : DTOContact
    {
        #region Properties
        public int Id { get; set; }
        public List<DTODBFile> Files { get; set; } = new List<DTODBFile>();
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string TimeZone { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MetroCode { get; set; }
        #endregion

        #region Constructors
        public DTOContactWithFiles() : base() { }

        public DTOContactWithFiles(ContactInfoWithFiles item)
        {
            this.Content = item.Content;
            this.FromEMail = item.FromEMail;
            this.FromName = item.FromName;
            this.Id = item.Id;
            this.Subject = item.Subject;

            Parallel.ForEach(item.Files, (file) => { this.Files.Add(new DTODBFile(file)); });
        }
        #endregion

        #region Public methods
        public ContactInfoWithFiles ToContactInfoWithFiles()
        {
            ContactInfoWithFiles value = new ContactInfoWithFiles()
            {
                Content = this.Content,
                FromEMail = this.FromEMail,
                FromName = this.FromName,
                Subject = this.Subject,
                Id = this.Id,
                Files = new List<DBFile>(),
                Ip = this.Ip,
                CountryCode = this.CountryCode,
                CountryName = this.CountryName,
                RegionCode = this.RegionCode,
                RegionName = this.RegionName,
                City = this.City,
                ZipCode = this.ZipCode,
                TimeZone = this.TimeZone,
                Latitude = this.Latitude,
                Longitude = this.Longitude,
                MetroCode = this.MetroCode
            };
            Parallel.ForEach(this.Files, (file) => { value.Files.Add(file.ToDBFile()); });
            return value;
        }
        #endregion
    }
}