﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAkvarietdk.Caching;
using TDK.Tools.Extensions;
using System.Drawing;
using System.IO;

namespace WebAkvarietdk.Models
{
    public static class DTOHelper
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static bool SaveContactInfoWithFiles(ContactInfoWithFiles item)
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    uow.Session.Save(item);
                    uow.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    logger.Warn(ex, "Failed to save contact data");
                    return false;
                }
            }
        }

        public static IntStringClass GetAnimalScientificName(int id)
        {
            IntStringClass item = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                var sql = $"SELECT Id, scientificNameText AS Text, true AS Selected FROM animal WHERE Id={id};";
                item = uow.Session.CreateSQLQuery(sql)
                                .AddEntity(typeof(IntStringClass))
                                .UniqueResult<IntStringClass>();
                uow.Commit();
            }
            return item;
        }

        public static IntStringClass GetPlantName(int id)
        {
            IntStringClass item = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                string sql = $"SELECT Id, scientificNameText AS Text, true AS Selected FROM plant WHERE Id={id};";
                item = uow.Session.CreateSQLQuery(sql)
                                .AddEntity(typeof(IntStringClass))
                                .UniqueResult<IntStringClass>();
                uow.Commit();
            }
            return item;
        }

        public enum ListTypes { Plant, Animal, Blog };

        /// <summary>
        /// Is expensive... especially with all the animals in the db..
        /// </summary>
        /// <param name="listType"></param>
        /// <param name="page"></param>
        /// <param name="itemsPerPage"></param>
        /// <param name="reverse"></param>
        /// <returns></returns>
        public static List<DTOWebEntry> GetList(ListTypes listType, int page = 1, int itemsPerPage = 30, bool reverse = false)
        {
            string keyName = string.Empty;
            bool animals = false, plants = false, blogs = false;
            switch (listType)
            {
                case ListTypes.Animal:
                    animals = true;
                    keyName = "animallist";
                    break;
                case ListTypes.Blog:
                    blogs = true;
                    keyName = "bloglist";
                    break;
                case ListTypes.Plant:
                    plants = true;
                    keyName = "plantlist";
                    break;
            }
            SimpleSearchKey simpleSearchKey = new SimpleSearchKey(keyName, animals, plants, blogs);
            List<DTOPlant> dtoPlantList = new List<DTOPlant>();
            List<DTOAnimal> dtoAnimalList = new List<DTOAnimal>();
            List<DTOBlogEntry> dtoBlogEntryList = new List<DTOBlogEntry>();
            List<DTOWebEntry> dtoEntries = new List<DTOWebEntry>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    dtoEntries = SimpeSearchCaching.Get(simpleSearchKey);
                    if (dtoEntries == null)
                    {
                        List<SimpleSearch> result = new List<SimpleSearch>();
                        if (animals)
                        {
                            var animalsResults = uow.Session.CreateSQLQuery("select id, 'animal' as EntryType from animal order by scientificnametext;")
                                .AddEntity(typeof(SimpleSearch))
                                .List<SimpleSearch>();
                            result.AddRange(animalsResults);
                        }
                        if (plants)
                        {
                            var plantResults = uow.Session.CreateSQLQuery("select id, 'plant' as EntryType from plant order by scientificnametext;")
                                .AddEntity(typeof(SimpleSearch))
                                .List<SimpleSearch>();
                            result.AddRange(plantResults);
                        }
                        if (blogs)
                        {
                            var blogResults = uow.Session.CreateSQLQuery("select id, 'blog' as EntryType from blogentity order by title;")
                                .AddEntity(typeof(SimpleSearch))
                                .List<SimpleSearch>();
                            result.AddRange(blogResults);
                        }

                        foreach (SimpleSearch ss in result)
                        {
                            bool foundIt = true;
                            switch (ss.EntryType.ToLower())
                            {
                                case "animal":
                                    DTOAnimal dtoAnimal = GetAnimal(ss.Id);
                                    if (dtoAnimal != null)
                                        dtoAnimalList.Add(dtoAnimal);
                                    else
                                        foundIt = false;
                                    break;
                                case "plant":
                                    DTOPlant dtoPlant = GetPlant(ss.Id);
                                    if (dtoPlant != null)
                                        dtoPlantList.Add(dtoPlant);
                                    else
                                        foundIt = false;
                                    break;
                                case "blog":
                                    DTOBlogEntry dtoBlog = GetBlog(ss.Id);
                                    if (dtoBlog != null)
                                        dtoBlogEntryList.Add(dtoBlog);
                                    else
                                        foundIt = false;
                                    break;
                            }
                            if (!foundIt)
                                logger.Warn("Could not find {0} with id: {1} while searching for {2}", ss.EntryType, ss.Id, simpleSearchKey.SearchString);
                        }
                        dtoEntries = GetDTOWebEntryList(dtoAnimalList, dtoPlantList, dtoBlogEntryList);
                        SimpeSearchCaching.Add(simpleSearchKey, dtoEntries, new TimeSpan(2, 0, 0));
                    }
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    string msg = string.Format("GetList {0} ", simpleSearchKey.SearchString);
                    logger.Error(ex, msg);
                    //throw new Exception(msg, ex);

                }
                finally
                {
                    uow.Session.Disconnect();
                    uow.Dispose();
                }
            }
            return dtoEntries;
        }

        public static List<LastModifiedFishbaseItem> GetAnimalIds()
        {
            IList<LastModifiedFishbaseItem> ids = new List<LastModifiedFishbaseItem>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                ids = uow.Session.CreateSQLQuery("SELECT Id, ModifiedDate, scientificNameText FROM animal;")
                                .AddEntity(typeof(LastModifiedFishbaseItem))
                                .List<LastModifiedFishbaseItem>();
                uow.Commit();
            }
            return ids.ToList();
        }

        public static List<LastModifiedFishbaseItem> GetPlantIds()
        {
            IList<LastModifiedFishbaseItem> ids = new List<LastModifiedFishbaseItem>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                ids = uow.Session.CreateSQLQuery("SELECT Id, ModifiedDate, scientificNameText FROM plant;")
                                .AddEntity(typeof(LastModifiedFishbaseItem))
                                .List<LastModifiedFishbaseItem>();
                uow.Commit();
            }
            return ids.ToList();
        }

        public static List<LastModifiedBlog> GetBlogIds()
        {
            IList<LastModifiedBlog> ids = new List<LastModifiedBlog>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                ids = uow.Session.CreateSQLQuery("SELECT Id, Posted, Title FROM blogentity;")
                                .AddEntity(typeof(LastModifiedBlog))
                                .List<LastModifiedBlog>();
                uow.Commit();
            }
            return ids.ToList();
        }

        public static List<DTOWebEntry> GetAnimalsAsDTOWebEntry(int page, int itemsPerPage)
        {
            List<Animal> animals;
            List<DTOAnimal> dtoAnimals;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    using (Repository<Animal> repo = new Repository<Animal>(uow))
                    {
                        animals = repo.GetAll().Skip((page - 1) * itemsPerPage).Take(itemsPerPage).ToList();
                        dtoAnimals = GetDTOAnimalList(animals);
                    }
                    uow.Commit();
                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return GetDTOWebEntryList(dtoAnimals, null, null);
        }

        public static List<DTOWebEntry> GetWebEntrys(int count)
        {
            int divCount = count / 3;

            List<DTOBlogEntry> dtoBlogEntrys = GetMostReadBlogs(divCount);
            List<DTOAnimal> dtoAnimals = GetMostReadAninals(divCount);
            List<DTOPlant> dtoPlants = GetMostReadPlants(divCount);

            List<DTOWebEntry> webEntrys = new List<DTOWebEntry>();
            foreach (DTOBlogEntry dto in dtoBlogEntrys)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }
            foreach (DTOAnimal dto in dtoAnimals)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }
            foreach (DTOPlant dto in dtoPlants)
            {
                webEntrys.Add(new DTOWebEntry(dto));
            }

            return webEntrys.OrderByDescending(x => x.TimeStamp).ToList();
        }

        public static int CountAnimals()
        {
            int count = 0;

            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    //count = uow.Session.QueryOver<Animal>().RowCount();
                    count = (int)uow.Session.CreateQuery("select count(*) from Animal").UniqueResult();
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    logger.Error(ex, "select count(*) from animal");

                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return count;
        }

        public static DTOHabitat GetDTOHabitat(int habitatId)
        {
            return new DTOHabitat(GetHabitat(habitatId));
        }

        public static Habitat GetHabitat(int habitatId)
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                try
                {
                    using (Repository<Habitat> repo = new Repository<Habitat>(uow))
                    {
                        return repo.GetById(habitatId);
                    }
                }
                finally
                {
                    uow.Commit();
                }
            }
        }

        public static List<DTOHabitat> ConvertHabitatsToDTOHabitats(List<Habitat> habitats)
        {
            List<DTOHabitat> dtoList = new List<DTOHabitat>();
            Parallel.ForEach(habitats, (habitat) => { dtoList.Add(new DTOHabitat(habitat)); });
            return dtoList;
        }

        public static List<DTOWebEntry> GetDTOWebEntryList(List<DTOAnimal> dtoAnimals, List<DTOPlant> dtoPlants, List<DTOBlogEntry> dtoBlogs)
        {
            List<DTOWebEntry> dtoWebEntries = new List<DTOWebEntry>();

            if (dtoAnimals != null)
                Parallel.ForEach(dtoAnimals, (dtoItem) => { dtoWebEntries.Add(new DTOWebEntry(dtoItem)); });
            if (dtoPlants != null)
                Parallel.ForEach(dtoPlants, (dtoItem) => { dtoWebEntries.Add(new DTOWebEntry(dtoItem)); });
            if (dtoBlogs != null)
                Parallel.ForEach(dtoBlogs, (dtoItem) => { dtoWebEntries.Add(new DTOWebEntry(dtoItem)); });

            return dtoWebEntries;

        }

        public static DTOBlogEntry GetBlog(int id)
        {
            DTOBlogEntry dtoItem = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    using (Repository<BlogEntity> repo = new Repository<BlogEntity>(uow))
                    {
                        BlogEntity item = repo.GetById(id);
                        dtoItem = new DTOBlogEntry(item);
                    }
                    uow.Commit();
                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return dtoItem;
        }

        public static DTOAnimal GetAnimal(int id)
        {
            DTOAnimal dtoAnimal = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    using (Repository<Animal> repo = new Repository<Animal>(uow))
                    {
                        Animal animal = repo.GetById(id);
                        dtoAnimal = new DTOAnimal(animal);
                    }
                    uow.Commit();
                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return dtoAnimal;
        }

        public static DTOPlant GetPlant(int id)
        {
            DTOPlant dtoPlant = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                try
                {
                    uow.BeginTransaction();
                    using (Repository<Plant> repo = new Repository<Plant>(uow))
                    {
                        Plant plant = repo.GetById(id);
                        dtoPlant = new DTOPlant(plant);
                    }
                    uow.Commit();
                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return dtoPlant;
        }

        public static List<DTOBlogEntry> GetDTOBlogEntrys(TagEntity tag)
        {
            List<DTOBlogEntry> dtoList = new List<DTOBlogEntry>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<BlogEntity> repo = new Repository<BlogEntity>(uow))
                {
                    List<BlogEntity> itemList = repo.GetAll().Where(x => x.Tags.Contains(tag)).OrderByDescending(x => x.Posted).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOBlogEntry(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static List<DTOAnimal> GetDTOAnimalList(List<Animal> animals)
        {
            List<DTOAnimal> dtoAnimals = new List<DTOAnimal>();
            Parallel.ForEach(animals, (animal) => { dtoAnimals.Add(new DTOAnimal(animal)); });
            return dtoAnimals;
        }

        public static List<DTOAnimal> GetDTOAnimalList()
        {
            List<Animal> animals;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                try
                {
                    using (Repository<Animal> repo = new Repository<Animal>(uow))
                    {
                        animals = repo.GetAll().ToList();
                    }
                    uow.Commit();
                }
                finally
                {
                    uow.Session.Disconnect();
                }
            }
            return GetDTOAnimalList(animals);
        }

        public static List<DTOPlant> GetDTOPlantList(List<Plant> plants)
        {
            List<DTOPlant> dtoPlants = new List<DTOPlant>();
            Parallel.ForEach(plants, (plant) => { dtoPlants.Add(new DTOPlant(plant)); });
            return dtoPlants;
        }

        public static List<DTOBlogEntry> GetDTOBlogEntrys(List<BlogEntity> blogs)
        {
            List<DTOBlogEntry> list = new List<DTOBlogEntry>();
            Parallel.ForEach(blogs, (blog) => { list.Add(new DTOBlogEntry(blog)); });
            return list;
        }

        public static List<DTOAnimal> GetNewestAninals(int count)
        {
            List<DTOAnimal> dtoList = new List<DTOAnimal>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    List<Animal> itemList = repo.GetAll().OrderByDescending(x => x.ModifiedDate).Take(count).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOAnimal(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static List<DTOPlant> GetNewestPlants(int count)
        {
            List<DTOPlant> dtoList = new List<DTOPlant>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<Plant> repo = new Repository<Plant>(uow))
                {
                    List<Plant> itemList = repo.GetAll().OrderByDescending(x => x.ModifiedDate).Take(count).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOPlant(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static async Task<List<DTOBlogEntry>> GetNewestBlogsAsync(int count)
        {
            var dtoList = new List<DTOBlogEntry>();
            var blogList = new List<BlogEntity>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {


                using (var conn = new MySqlConnection(Akvariet.Common.AkvarietConfiguration.MySQLConnectionString))
                {
                    await conn.OpenAsync();


                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        using (MySqlDataReader reader = await MySqlHelper.ExecuteReaderAsync(conn, $"SELECT `blogentity`.`Id` AS blogentity_Id,`blogentity`.`Title`,`blogentity`.`Teaser`,`blogentity`.`Body`,`blogentity`.`Posted`,`blogentity`.`PostRead`,`blogentity`.`Image_id`,`blogentity`.`Contributor_id`, `user`.`Id` AS user_Id,`user`.`DisplayName`,`user`.`Username`,`user`.`EMail`,`user`.`Role`,`user`.`JoinDateTime`, `fishbaseimage`.`Id` AS fishbaseimage_Id,`fishbaseimage`.`Image`,`fishbaseimage`.`Content`,`fishbaseimage`.`Description`,`fishbaseimage`.`Title` AS ImageTitle,`fishbaseimage`.`CopyRight`,`fishbaseimage`.`CreateDate` FROM blogentity INNER JOIN `user` ON `blogentity`.`Contributor_id`=`user`.`Id` INNER JOIN `fishbaseimage` ON `blogentity`.`Image_id`=`fishbaseimage`.`Id` ORDER BY `blogentity`.`Posted` DESC,`blogentity`.`Title` LIMIT {count};"))
                        {
                            while (reader.Read())
                            {
                                // Get comments
                                int.TryParse(reader.GetStringSafe("Role"), out int userRole);
                                var imageBlob = (byte[])reader["Image"];

                                var blogEntity = new BlogEntity
                                {
                                    Body = reader.GetStringSafe("Body"),
                                    Contributor = new User(),
                                    Title = reader.GetStringSafe("Title"),
                                    Posted = reader.GetDateTime("Posted"),
                                    PostRead = reader.GetInt32("PostRead"),
                                    Teaser = reader.GetStringSafe("Teaser"),
                                    Id = reader.GetInt32("blogentity_Id")
                                };

                                blogEntity.Contributor.DisplayName = reader.GetStringSafe("DisplayName");
                                blogEntity.Contributor.EMail = reader.GetStringSafe("EMail");
                                blogEntity.Contributor.Id = reader.GetInt32("user_Id");
                                blogEntity.Contributor.JoinDateTime = reader.GetDateTime("JoinDateTime");

                                blogEntity.Contributor.Role = (Akvariet.Common.UserLevels)userRole;
                                blogEntity.Contributor.Username = reader.GetStringSafe("Username");

                                using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
                                {
                                    blogEntity.Image = repo.GetById(reader.GetInt32("fishbaseimage_Id"));
                                }

                                /*
                                    blogEntity.Image = new FishbaseImage();
                                    blogEntity.Image.Content = reader.GetStringSafe("Content");
                                    blogEntity.Image.CopyRight = reader.GetStringSafe("CopyRight");
                                    blogEntity.Image.CreateDate = reader.GetDateTime("CreateDate");
                                    blogEntity.Image.Description = reader.GetStringSafe("Description");
                                    blogEntity.Image.Id = reader.GetInt32("fishbaseimage_Id");
                                    //TODO: Fix Exception in next line
                                    //blogEntity.Image.Image = (Bitmap)((new ImageConverter()).ConvertFrom(imageBlob));
                                    blogEntity.Image.Title = reader.GetStringSafe("ImageTitle");
                                   */

                                blogEntity.Tags = await GetTagsForBlogAsync(reader.GetInt32("blogentity_Id"));

                                blogList.Add(blogEntity);
                            }
                        }
                    }

                    Parallel.ForEach(blogList, (item) => { dtoList.Add(new DTOBlogEntry(item)); });
                    return dtoList;
                }
            }
        }

        private static async Task<List<TagEntity>> GetTagsForBlogAsync(int blogId)
        {
            var list = new List<TagEntity>();

            using (MySqlConnection con = new MySqlConnection(Akvariet.Common.AkvarietConfiguration.MySQLConnectionString))
            {
                await con.OpenAsync();
                using (MySqlDataReader reader = await MySqlHelper.ExecuteReaderAsync(con, $"SELECT `tagentity`.`Id`,`tagentity`.`TagName` FROM `tagentity` INNER JOIN `tagentitytoblogentity` ON `tagentity`.`Id`=`tagentitytoblogentity`.`TagEntity_id` WHERE `tagentitytoblogentity`.`BlogEntity_id`={blogId};"))
                {
                    while (reader.Read())
                    {
                        list.Add(new TagEntity
                        {
                            Id = reader.GetInt32("Id"),
                            TagName = reader.GetStringSafe("TagName")
                        });
                    }
                }
            }
            return list;
        }

        public static List<DTOBlogEntry> GetNewestBlogs(int count)
        {
            var dtoList = new List<DTOBlogEntry>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<BlogEntity> repo = new Repository<BlogEntity>(uow))
                {
                    var itemList = repo.GetAll().OrderByDescending(x => x.Posted).Take(count).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOBlogEntry(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static List<DTOAnimal> GetMostReadAninals(int count)
        {
            List<DTOAnimal> dtoList = new List<DTOAnimal>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    List<Animal> itemList = repo.GetAll().OrderByDescending(x => x.ViewCount).Take(count).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOAnimal(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static List<DTOPlant> GetMostReadPlants(int count)
        {
            List<DTOPlant> dtoList = new List<DTOPlant>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                uow.BeginTransaction();
                using (Repository<Plant> repo = new Repository<Plant>(uow))
                {
                    List<Plant> itemList = repo.GetAll().OrderByDescending(x => x.ViewCount).Take(count).ToList();
                    Parallel.ForEach(itemList, (item) => { dtoList.Add(new DTOPlant(item)); });
                }
                uow.Commit();
            }
            return dtoList;
        }

        public static List<DTOBlogEntry> GetMostReadBlogs(int count)
        {
            List<DTOBlogEntry> dtoList = new List<DTOBlogEntry>();
            try
            {
                using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                {
                    uow.BeginTransaction();
                    using (BlogEntityRepository repo = new BlogEntityRepository(uow))
                    {
                        List<BlogEntity> itemList = repo.GetMostReadBlogs(count);
                        foreach (BlogEntity item in itemList)
                            dtoList.Add(new DTOBlogEntry(item));
                    }
                    uow.Commit();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return dtoList;
        }

        public static int GetCommentCountForUser(User user)
        {
            //TODO: extract comment count from DB
            return 0;
        }

    }
}