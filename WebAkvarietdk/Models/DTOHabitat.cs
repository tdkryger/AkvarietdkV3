﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAkvarietdk.Models
{
    public class DTOHabitat
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public int BiotopAkvarietId { get; set; }
        public string PDFUrl { get; }
        #endregion

        #region Constructors
        public DTOHabitat(Habitat item)
        {
            this.Id = item.Id;
            this.Text = item.Text;
            this.Description = item.Description;
            this.BiotopAkvarietId = item.BiotopAkvarietId;
            this.PDFUrl = $"/Handlers/HabitatPDF.ashx?id={item.Id}";
        }
        #endregion

        public override string ToString()
        {
            return Text;
        }

        #region private Methods

        #endregion
    }
}