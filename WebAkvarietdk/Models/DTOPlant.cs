﻿using Akvariet.Fishbase.Model;
using NLog;
using System;
using System.Collections.Generic;
using WebAkvarietdk.Tools;

namespace WebAkvarietdk.Models
{
    public class DTOPlant : Interfaces.IFishbaseDTO
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Properties
        #region From Interfaces
        public int Id { get; set; }
        public string ScientificName { get; set; }
        public string Tradenames { get; set; }
        public string Synonyms { get; set; }
        public string Origin { get; set; }
        public string Temperature { get; set; }
        public string Hardness { get; set; }
        public string Ph { get; set; }
        public string Describtion { get; set; }
        public List<DTOImage> ImageList { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ViewCount { get; set; }
        public List<DTOComment> Comments { get; set; } = new List<DTOComment>();
        public string PDFUrl { get; }
        #endregion

        public string TankPosition { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string LightRequirements { get; set; }
        public string CO2Requirements { get; set; }
        public string PlantGroup { get; set; }
        public string GrowthSpeed { get; set; }
        public List<string> Care { get; set; } = new List<string>();
        public List<string> Characteristics { get; set; } = new List<string>();
        public DTOUser User { get; set; }
        public int CommentCount { get { return Comments.Count; } }
        #endregion

        #region Constructor
        public DTOPlant() { }

        public DTOPlant(Plant plant)
        {
            try
            {
                #region From Interfaces
                this.Id = plant.Id;
                this.ScientificName = plant.ScientificName.ToString();
                this.Tradenames = string.Empty;
                this.Synonyms = string.Empty;
                if (plant.Tradenames != null)
                {
                    foreach (Tradename tradename in plant.Tradenames)
                    {
                        this.Tradenames = this.Tradenames + tradename.ToString() + ", ";
                    }
                    if (this.Tradenames.Length > 2)
                        this.Tradenames = this.Tradenames.Substring(0, this.Tradenames.Length);
                }
                else
                {
                    logger.Warn("Tradenames on plant with id:{0} is null", plant.Id);
                }

                if (plant.Synonyms != null)
                {
                    foreach (ScientificName synonym in plant.Synonyms)
                    {
                        this.Synonyms = this.Synonyms + synonym.ToString() + ", ";
                    }
                    if (this.Synonyms.Length > 2)
                        this.Synonyms = this.Synonyms.Substring(0, this.Synonyms.Length);
                }
                else
                {
                    logger.Warn("Synonyms on plant with id:{0} is null", plant.Id);
                }

                this.Origin = plant.Origin.ToString().Replace("(", " (");
                this.Temperature = $"{plant.WaterTemperature.Min:#0.#}-{plant.WaterTemperature.Max:#0.#}°C";
                if (plant.WaterHardness.Min == plant.WaterHardness.Max)
                    this.Hardness = "Ukendt";
                else
                    this.Hardness = $"{plant.WaterHardness.Min:#0.#}-{plant.WaterHardness.Max:#0.#}° GH";
                if (plant.WaterPh.Min == plant.WaterPh.Max)
                    this.Ph = "Ukendt";
                else
                    this.Ph = $"{plant.WaterPh.Min:#0.#}-{plant.WaterPh.Max:#0.#}";
                if (plant.WaterPh.AddSalt)
                    this.Ph = this.Ph + " (Brak eller saltvand)";
                this.Describtion = plant.Description;

                this.ImageList = new List<DTOImage>();
                if (plant.Images != null)
                {
                    foreach (FishbaseImage image in plant.Images)
                    {
                        this.ImageList.Add(new DTOImage()
                        {
                            ImageUrl = $"/Handlers/ImageHandler.ashx?imageId={image.Id}" + Akvariet.Common.AkvarietConfiguration.ImageSize,
                            Id = image.Id,
                            Title = image.Title
                        });
                    }
                }
                else
                {
                    logger.Warn("Images on plant with id:{0} is null", plant.Id);
                }

                this.CreateDate = plant.CreateDate;
                this.ModifiedDate = plant.ModifiedDate;
                this.ViewCount = plant.ViewCount;
                #endregion

                this.TankPosition = EnumWords.PlantTankPositionsToString(plant.TankPosition);
                if (plant.Height.Min == 0)
                {
                    this.Height = "Ukendt";
                }
                else
                {
                    if ((plant.Height.Min == plant.Height.Max) || (plant.Height.Min > plant.Height.Max))
                    {
                        this.Height = $"Op til {plant.Height.Min:#0.#} cm";
                    }
                    else
                    {
                        this.Height = $"{plant.Height.Min:#0.#}-{plant.Height.Max:#0.#} cm";
                    }
                }

                if (plant.Width.Min == 0)
                {
                    this.Width = "Ukendt";
                }
                else
                {
                    if ((plant.Width.Min == plant.Width.Max) || (plant.Width.Min > plant.Width.Max))
                    {
                        this.Width = $"{plant.Width.Min:#0.#} cm";
                    }
                    else
                    {
                        this.Width = $"{plant.Width.Min:#0.#}-{plant.Width.Max:#0.#} cm";
                    }
                }

                if (plant.LightRequirements.Min == 0)
                {
                    this.LightRequirements = "Ukendt";
                }
                else
                {
                    if (plant.LightRequirements.Min == plant.LightRequirements.Max)
                    {
                        this.LightRequirements = $"{plant.LightRequirements.Min:#0.#} W/L";
                    }
                    else
                    {
                        this.LightRequirements = $"{plant.LightRequirements.Min:#0.#}-{plant.LightRequirements.Max:#0.#} W/L";
                    }
                }
                this.CO2Requirements = EnumWords.PlantCO2RequirementsToString(plant.CO2Requirements);
                this.PlantGroup = plant.PlantGroup.Text;
                this.GrowthSpeed = EnumWords.PlantGrowthSpeedToString(plant.GrowthSpeed);
                foreach (PlantCare care in plant.Care)
                    this.Care.Add(care.Text);
                foreach (PlantCharacteristic item in plant.Characteristics)
                    this.Characteristics.Add(item.Text);
                this.User = new DTOUser(plant.Contributor);
                this.PDFUrl = $"/Handlers/PlantPDF.ashx?id={plant.Id}";
            }
            catch (Exception ex)
            {
                if (plant != null)
                    logger.Error(ex, "Exception in Constructor of DTOPlant. Plant: id={0} ", plant.Id);
                else
                    logger.Error(ex, "Exception in Constructor of DTOPlant. Plant is null ");
            }
        }
        #endregion
    }
}