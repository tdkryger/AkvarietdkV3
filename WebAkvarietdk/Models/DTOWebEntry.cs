﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using WebAkvarietdk.Interfaces;

namespace WebAkvarietdk.Models
{
    //TODO: Comments
    public class DTOWebEntry
    {
        #region Fields
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string _shortTeaser;
        private string _shortTitle;
        #endregion

        #region Properties
        public int Id { get; protected set; }
        public EntryTypes EntryType { get; protected set; }
        public List<DTOImage> ImageList { get; protected set; } = new List<DTOImage>();
        public string FrontImage { get; protected set; } = "/Resources/Images/nopicture.text.jpg";
        public string ThumbUrl { get; protected set; } = "/Resources/Images/nopicture.text.jpg";
        public DateTime TimeStamp { get; protected set; }
        public int ViewCount { get; protected set; }

        public string Title { get; protected set; }
        public string Teaser { get; protected set; }
        public string Body { get; protected set; }

        public string Url { get { return GetMoreUrl(); } }

        public DTOUser User { get; set; }

        public string ShortTeaser { get { return GetShortTeaser(); } }
        public string ShortTitle { get { return GetShortTitle(); } }

        public List<DTOTag> Tags { get; protected set; } = new List<DTOTag>();
        #endregion

        #region Constructors

        public DTOWebEntry(IWebItem webItem)
        {
            if (webItem.Id == 0)
            {
                logger.Trace("webItem.Id is 0. WebItem is {0}", webItem);
            }
            this.Id = webItem.Id;
            if (webItem is IFishbaseDTO)
            {
                IFishbaseDTO dtoItem = (IFishbaseDTO)webItem;
                Title = dtoItem.ScientificName;
                _shortTitle = Title;

                if (string.IsNullOrEmpty(dtoItem.Describtion))
                {
                    Teaser = dtoItem.Tradenames;

                }
                else
                {
                    Teaser = dtoItem.Describtion;
                }
                _shortTeaser = TDK.Tools.StringTools.ShortString(Teaser, Akvariet.Common.AkvarietConfiguration.ShortTeaserLength);

                Id = dtoItem.Id;
                if (dtoItem.ImageList == null || dtoItem.ImageList.Count == 0)
                {
                    if (dtoItem is DTOAnimal)
                    {
                        ThumbUrl = "/Resources/Images/fish1.50x50.png";
                        FrontImage = ThumbUrl;
                    }
                    else
                    {
                        if (dtoItem is DTOPlant)
                        {
                            ThumbUrl = "/Resources/Images/plants-reeds.50x50.png";
                            FrontImage = ThumbUrl;
                        }
                        else
                        {
                            ThumbUrl = "/Resources/Images/nopicture.text.jpg";
                            FrontImage = "/Resources/Images/nopicture.text.jpg";
                        }
                    }
                }
                else
                {
                    ImageList.AddRange(dtoItem.ImageList);
                    FrontImage = $"Handlers/ImageHandler.ashx?imageId={dtoItem.ImageList[0].Id}" + Akvariet.Common.AkvarietConfiguration.ImageSize;
                    ThumbUrl = $"/Handlers/ThumbHandler.ashx?imageId={dtoItem.ImageList[0].Id}&width=50";
                }
                Title = $"{dtoItem.ScientificName}";
                TimeStamp = dtoItem.ModifiedDate;
                ViewCount = dtoItem.ViewCount;
                if (dtoItem is DTOAnimal)
                {
                    SetDTOAnimal((DTOAnimal)dtoItem);
                }
                else
                {
                    if (dtoItem is DTOPlant)
                    {
                        SetDTOPlant((DTOPlant)dtoItem);
                    }
                }
            }
            else
            {
                if (webItem is DTOBlogEntry dtoItem)
                {
                    SetDTOBlogEntry(dtoItem);
                }
            }

        }
        #endregion

        #region Public methods

        #endregion

        #region Private methods
        private string GetMoreUrl()
        {
            string dotNetController = string.Empty;
            switch (this.EntryType)
            {
                case EntryTypes.Animal:
                    dotNetController = "animal";
                    break;
                case EntryTypes.Blog:
                    dotNetController = "blog";
                    break;
                case EntryTypes.Plant:
                    dotNetController = "plant";
                    break;
            }
            return $"{dotNetController}/{this.Id}";
        }

        private string GetShortTitle()
        {
            if (string.IsNullOrEmpty(_shortTitle))
                return TDK.Tools.StringTools.ShortString(Title, 30);
            else
                return _shortTitle;
        }

        private string GetShortTeaser()
        {
            if (!string.IsNullOrEmpty(_shortTeaser))
                return _shortTeaser;
            else
            {
                return string.IsNullOrEmpty(Teaser) ? TDK.Tools.StringTools.ShortString(Title, Akvariet.Common.AkvarietConfiguration.ShortTeaserLength) : TDK.Tools.StringTools.ShortString(Teaser, Akvariet.Common.AkvarietConfiguration.ShortTeaserLength);
            }
        }

        private void SetDTOAnimal(DTOAnimal animal)
        {
            EntryType = EntryTypes.Animal;
            Body = GetAnimalBody(animal);
            this.User = animal.User;
            this.Tags.Clear();
        }

        private void SetDTOPlant(DTOPlant plant)
        {
            EntryType = EntryTypes.Plant;
            Body = GetPlantBody(plant);
            this.User = plant.User;
            this.Tags.Clear();
        }

        private void SetDTOBlogEntry(DTOBlogEntry blogEntry)
        {
            this.Tags.AddRange(blogEntry.Tags);
            this.Id = blogEntry.Id;
            EntryType = EntryTypes.Blog;
            this.Body = blogEntry.Body;
            this.Title = blogEntry.Title;
            this.TimeStamp = blogEntry.TimeStamp;
            this.ViewCount = blogEntry.ViewCount;
            this.Teaser = blogEntry.Teaser;
            this.ImageList.AddRange(blogEntry.ImageList);
            this.User = blogEntry.User;

            //if (blogEntry.t)

            if (blogEntry.FrontImage == null)
            {
                if (this.ImageList.Count > 0)
                {
                    this.FrontImage = this.ImageList[0].ImageUrl + Akvariet.Common.AkvarietConfiguration.ImageSize;
                }
            }
            else
            {
                this.FrontImage = blogEntry.FrontImage.ImageUrl + Akvariet.Common.AkvarietConfiguration.ImageSize;

            }
            this.ThumbUrl = $"/Handlers/ThumbHandler.ashx?imageId={blogEntry.FrontImage.Id}&width=50";
        }

        private string GetAnimalBody(DTOAnimal animal)
        {
            string diet = string.Empty;
            string illnesses = "Ingen specielle sygdomme";
            string socials = string.Empty;


            if (animal.DietaryRequirements.Count > 0)
            {
                foreach (string s in animal.DietaryRequirements)
                    diet += s + ", ";
                diet = diet.Substring(0, diet.Length - 2);
            }

            if (animal.CommonIllnesses.Count > 0)
            {
                foreach (string s in animal.CommonIllnesses)
                    illnesses += s + ", ";
                illnesses = illnesses.Substring(0, illnesses.Length - 2);
            }

            if (animal.Sociableness.Count > 0)
            {
                foreach (string s in animal.Sociableness)
                    socials += s + ", ";
                socials = socials.Substring(0, socials.Length - 2);
            }

            string rawHtml = string.Empty;
            using (StreamReader sr = new StreamReader(VirtualPathProvider.OpenFile("/Resources/Templates/CompactAnimal.html")))
            {
                rawHtml = sr.ReadToEnd();
            }

            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("{DATE}", animal.ModifiedDate.ToString("d/MM yyyy"));
            values.Add("{TITLE}", this.Title);
            values.Add("{COMMONNAMES}", animal.Tradenames);
            values.Add("{ORDER}", animal.Order);
            values.Add("{FAMILY}", animal.Family);
            values.Add("{COMMONGROUP}", animal.CommonGroup);
            values.Add("{ORIGIN}", animal.Origin);
            //values.Add("{LIFESPAN}", animal.);
            values.Add("{BIOLOAD}", animal.BioLoad);
            values.Add("{SWIMLEVEL}", animal.SwimLevel);
            values.Add("{TEMPERATURE}", animal.Temperature);
            values.Add("{PH}", animal.Ph);
            values.Add("{HARDNESS}", animal.Hardness);
            values.Add("{DIET}", diet);
            values.Add("{ILLNESSES}", illnesses);
            values.Add("{TEMPERAMENT}", animal.Temperament);
            values.Add("{SOCIALS}", socials);
            values.Add("{SIZE}", animal.Size);
            values.Add("{MINGROUPSIZE}", animal.MinGroupSize.ToString());

            values.Add("{PLANTEDTANK}", animal.PlantedTank);
            values.Add("{MINTANKSIZE}", animal.MinTankSize.ToString());
            values.Add("{HABITAT}", animal.PreferedHabitatText());
            values.Add("{DESCRIPTION}", animal.Describtion);

            return TDK.Tools.HtmlTools.HtmlTemplate(rawHtml, values);
        }

        private string GetPlantBody(DTOPlant plant)
        {
            string Characteristics = string.Empty;
            string care = string.Empty;


            if (plant.Care.Count > 0)
            {
                foreach (string s in plant.Care)
                    care += s + ", ";
                care = care.Substring(0, care.Length - 2);
            }

            if (plant.Characteristics.Count > 0)
            {
                foreach (string s in plant.Characteristics)
                    Characteristics += s + ", ";
                Characteristics = Characteristics.Substring(0, Characteristics.Length - 2);
            }
            string rawHtml = string.Empty;
            using (StreamReader sr = new StreamReader(VirtualPathProvider.OpenFile("/Resources/Templates/CompactPlant.html")))
            {
                rawHtml = sr.ReadToEnd();
            }

            Dictionary<string, string> values = new Dictionary<string, string>();
            values.Add("{DATE}", plant.ModifiedDate.ToString("d/MM yyyy"));
            values.Add("{TITLE}", this.Title);
            values.Add("{COMMONNAMES}", plant.Tradenames);
            values.Add("{COMMONGROUP}", plant.PlantGroup);
            values.Add("{ORIGIN}", plant.Origin);
            values.Add("{TEMPERATURE}", plant.Temperature);
            values.Add("{PH}", plant.Ph);
            values.Add("{HARDNESS}", plant.Hardness);
            values.Add("{SIZE}", $"Højde: {plant.Height}, Bredde: {plant.Width}");
            values.Add("{DESCRIPTION}", plant.Describtion);
            values.Add("{CHARACTERISTICS}", Characteristics);
            values.Add("{CARE}", care);
            values.Add("{LIGHT}", plant.LightRequirements);
            values.Add("{CO2}", plant.CO2Requirements);
            values.Add("{GROWTH}", plant.GrowthSpeed);

            return TDK.Tools.HtmlTools.HtmlTemplate(rawHtml, values);
        }
        #endregion

    }
}