﻿using Akvariet.Common;
using Akvariet.Fishbase.Model;

namespace WebAkvarietdk.Models
{
    public class DTOUser
    {
        #region Properties
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Username { get; set; }
        public string EMail { get; set; }
        public UserLevels Role { get; set; } = UserLevels.None;
        public int CommentCount { get; private set; }
        #endregion

        public DTOUser(User user)
        {
            if (user == null)
            {
                this.DisplayName = "Admin";
                this.EMail = Akvariet.Common.AkvarietConfiguration.ContactEMail;
                this.CommentCount = 0;
            }
            else
            {
                this.Id = user.Id;
                this.DisplayName = user.DisplayName;
                this.Username = user.Username;
                this.EMail = user.EMail;
                this.Role = user.Role;
                this.CommentCount = DTOHelper.GetCommentCountForUser(user);
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        #region Private methods
        #endregion
    }
}