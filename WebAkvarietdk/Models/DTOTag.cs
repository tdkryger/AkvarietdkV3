﻿using Akvariet.Fishbase.Model;
using System.Collections.Generic;

namespace WebAkvarietdk.Models
{
    public class DTOTag
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public List<DTOBlogEntry> Blogs { get; set; }

        public DTOTag(TagEntity tag)
        {
            this.Id = tag.Id;
            this.Text = tag.TagName;
            //TODO: Get the blogs
            //Blogs = DTOHelper.GetDTOBlogEntrys(tag);
        }
    }
}