﻿using NLog;
using System.Diagnostics;
using System.Web.Http;
using WebAkvarietdk.Models;

namespace WebAkvarietdk
{
    public static class WebApiConfig
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Register(HttpConfiguration config)
        {
            Stopwatch sw = Stopwatch.StartNew();
            logger.Debug("Starting");
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            Background.BackgroundLoader.Init();
            sw.Stop();
            logger.Debug("Runtime: {0}", sw.Elapsed);
        }
    }
}
