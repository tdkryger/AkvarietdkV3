﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebAkvarietdk.Models;

namespace WebAkvarietdk.Sitemap
{

    // Source: http://rehansaeed.com/dynamically-generating-sitemap-xml-for-asp-net-mvc/

    public static class Sitemap
    {
        public static string GetSitemapDocument(IEnumerable<SitemapNode> sitemapNodes)
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");

            foreach (SitemapNode sitemapNode in sitemapNodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc", Uri.EscapeUriString(sitemapNode.Url)),
                    sitemapNode.LastModified == null ? null : new XElement(
                        xmlns + "lastmod",
                        sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    sitemapNode.Frequency == null ? null : new XElement(
                        xmlns + "changefreq",
                        sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                    sitemapNode.Priority == null ? null : new XElement(
                        xmlns + "priority",
                        sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                root.Add(urlElement);
            }

            XDocument document = new XDocument(root);
            return document.ToString();
        }

        public static IReadOnlyCollection<SitemapNode> GetSitemapNodes()
        {
            //baseUri.
            string baseUrl = Akvariet.Common.AkvarietConfiguration.BaseUrl;
            if (!baseUrl.EndsWith("/#/"))
                baseUrl = baseUrl + "/#/";
            List<SitemapNode> nodes = new List<SitemapNode>
            {
                new SitemapNode()
                {
                    Url = baseUrl + "alger",
                    Priority = 0.8
                },
                new SitemapNode()
                {
                    Url = baseUrl + "sygdomme",
                    Priority = 0.8
                },
                new SitemapNode()
                {
                    Url = baseUrl + "biotoper",
                    Priority = 0.5
                },
                new SitemapNode()
                {
                    Url = baseUrl + "om",
                    Priority = 0.3
                },
            };
            //var animals = DTOHelper.GetAnimalIds();
            //Parallel.ForEach(animals, (animal) =>
            //{
            //    nodes.Add(
            //        new SitemapNode()
            //        {
            //            Url = $"{baseUrl}animal/{animal.Id}",
            //            LastModified = animal.ModifiedDate,
            //            Frequency = SitemapFrequency.Monthly,
            //            Priority = 1
            //        });
            //});

            foreach (LastModifiedFishbaseItem animals in DTOHelper.GetAnimalIds())
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{baseUrl}animal/{animals.Id}",
                        LastModified = animals.ModifiedDate,
                        Frequency = SitemapFrequency.Monthly,
                        Priority = 1
                    }
                    );
            }

            foreach (LastModifiedFishbaseItem plants in DTOHelper.GetPlantIds())
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{baseUrl}plant/{plants.Id}",
                        LastModified = plants.ModifiedDate,
                        Frequency = SitemapFrequency.Monthly,
                        Priority = 1
                    }
                    );
            }

            foreach (LastModifiedBlog blogs in DTOHelper.GetBlogIds())
            {
                nodes.Add(
                    new SitemapNode()
                    {
                        Url = $"{baseUrl}blog/{blogs.Id}",
                        LastModified = blogs.Posted,
                        Frequency = SitemapFrequency.Monthly,
                        Priority = 1
                    }
                    );
            }

            return nodes;
        }
    }
}