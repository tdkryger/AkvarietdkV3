﻿using Akvariet.Model;
using Akvariet.Model.Database;
using Akvariet.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AkvarietdkV3.Controllers
{
    public class BlogController : ApiController
    {
        public IEnumerable<BlogEntry> GetAllBlogs()
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
                return blogRepository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                BlogEntry blogEntry = blogRepository.Get(id);
                if (blogEntry == null)
                {
                    return NotFound();
                }
                return Ok(blogEntry);
            }
        }

        [Route("api/blog/topread/{count}")]
        public IEnumerable<BlogEntry> GetTopRead(int count)
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
                return blogRepository.GetTopRead(count);
        }

        [Route("api/blog/newest/{count}")]
        public IEnumerable<BlogEntry> GetNewest(int count)
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
                return blogRepository.GetNewest(count);
        }

        [Route("api/blog/add/{id}")]
        public IHttpActionResult AddOrUpdate(BlogEntry item)
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                if (blogRepository.AddOrUpdate(item))
                {
                    return Ok();
                }
                else
                {
                    return ResponseMessage(
                        Request.CreateResponse(
                        HttpStatusCode.BadRequest,
                        "Could not save blog entry"));
                }
            }
        }

        [Route("api/blog/delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            using (BlogRepository blogRepository = new BlogRepository(new UnitOfWork()))
            {
                if (blogRepository.Delete(id))
                {
                    return Ok();
                }
                else
                {
                    return ResponseMessage(
                        Request.CreateResponse(
                        HttpStatusCode.BadRequest,
                        "Could not delete blog entry"));
                }
            }
        }
    }
}
