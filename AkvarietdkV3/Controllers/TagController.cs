﻿using Akvariet.Model;
using Akvariet.Model.Database;
using Akvariet.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AkvarietdkV3.Controllers
{
    public class TagController : ApiController
    {
        public IEnumerable<TagEntity> GetAll()
        {
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                return tagRepository.GetAll();
            }
        }

        public IHttpActionResult Get(int id)
        {
            using (TagRepository tagRepository = new TagRepository(new UnitOfWork()))
            {
                TagEntity tagEntity = tagRepository.Get(id);
                if (tagEntity == null)
                    return NotFound();
                return Ok(tagEntity);
            }
        }

        [Route("api/tag/add/{id}")]
        public IHttpActionResult AddOrUpdate(TagEntity item)
        {
            using (TagRepository repository = new TagRepository(new UnitOfWork()))
            {
                if (repository.AddOrUpdate(item))
                {
                    return Ok();
                }
                else
                {
                    return ResponseMessage(
                        Request.CreateResponse(
                        HttpStatusCode.BadRequest,
                        "Could not save tag entry"));
                }
            }
        }

        [Route("api/tag/delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            using (TagRepository repository = new TagRepository(new UnitOfWork()))
            {
                if (repository.Delete(id))
                {
                    return Ok();
                }
                else
                {
                    return ResponseMessage(
                        Request.CreateResponse(
                        HttpStatusCode.BadRequest,
                        "Could not delete tag entry"));
                }
            }
        }
    }
}
