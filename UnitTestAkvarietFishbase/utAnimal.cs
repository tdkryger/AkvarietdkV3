﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestAkvarietFishbase
{
    [TestClass]
    public class utAnimal
    {
        #region Private helper methods

        private static readonly Random random = new Random((int)DateTime.Now.Ticks);

        private Animal CreateAnAnimal()
        {
            AnimalGroup animalGroup = DBHelper4Tests.GetAnAnimalGroup();
            Assert.IsNotNull(animalGroup, "No animal groups defined");


            Animal animal = new Animal()
            {
                Bioload = Animal.AnimalBioload.Medium,
                CommonGroup = animalGroup,
                Order = DBHelper4Tests.GetAnOrder()
            };
            animal.CommonIllnesses.Add(DBHelper4Tests.GetAnIllness());


            return animal;
        }

        private void CreateSomeAnimals(int count = 50)
        {
            for (int i = 0; i < count; i++)
            {
                IUnitOfWork unitOfWork = new FishbaseUnitOfWork();
                Animal animal = CreateAnAnimal();
                animal.ScientificName.Genus = TDK.Tools.StringTools.RandomString(75);
                animal.ScientificName.Species = TDK.Tools.StringTools.RandomString(75);
                animal.ViewCount = random.Next(0, 25);
                animal.CreateDate = DateTime.Now.AddDays(-10);
                animal.ModifiedDate = animal.CreateDate.AddDays(random.Next(0, 10));

                using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
                {
                    animalRepository.Create(animal);
                    unitOfWork.Commit();
                    Assert.AreNotEqual(0, animal.Id);
                }
            }
        }

        private Animal CreateAnAnimal_AllNew()
        {
            Animal animal = new Animal()
            {
                Bioload = Animal.AnimalBioload.Heavy,
                BreadInCaptivity = true,
                CreateDate = DateTime.Now,
                Description = TDK.Tools.StringTools.RandomString(20000),
                Family = new Family() { Name = "Some family" },
                MinFiltrationLevel = Animal.AnimalFiltration.Low,
                MingroupSize = 6,
                MinTankSize = 2000,
                ModifiedDate = DateTime.Now,
                Order = new Order() { Name = "Some Order", OrderGroup = "Test" },
                PlantedTank = Animal.AnimalPlantedTank.Partial,
                PreferedHabitat = new List<Habitat>(){
                    new Habitat() { Text = TDK.Tools.StringTools.RandomString(5000) }
                },
                Origin = new Origin() { Text = TDK.Tools.StringTools.RandomString(190) },
                CommonGroup = new AnimalGroup()
                {
                    Active = true,
                    AqualogCode = TDK.Tools.StringTools.RandomString(10),
                    Name = TDK.Tools.StringTools.RandomString(150),
                    Zone = TDK.Tools.StringTools.RandomString(150)
                },
                ScientificName = new ScientificName() { Genus = "Some Genus", Species = "Some Species" },
                Size = new Size() { Min = 3.5M, Max = 7 },
                SubOrder = new SubOrder() { Name = TDK.Tools.StringTools.RandomString(30) },
                SwimLevel = Animal.AnimalSwimLevel.Middle,
                Temperament = new Temperament() { Text = "Some temperament" },
                ViewCount = 0,
                WaterHardness = new Hardness() { Min = 3.5M, Max = 21 },
                WaterPh = new PH() { Min = 6.5M, Max = 7.5M },
                WaterTemperature = new Temperature() { Min = 22, Max = 25 },
                CommonIllnesses = new List<Illness>(),
                DietaryRequirements = new List<Diet>(),
                Images = new List<FishbaseImage>(),
                Sociableness = new List<Social>(),
                Synonyms = new List<ScientificName>(),
                Tradenames = new List<Tradename>()
            };
            animal.CommonIllnesses.Add(new Illness()
            {
                Cause = "Some Cause",
                Group = "Some group",
                Name = "Some name",
                Symptoms = "Some symptoms",
                Treatment = "Some treatment"
            });
            animal.DietaryRequirements.Add(new Diet() { Name = "Some diet" });
            animal.DietaryRequirements.Add(new Diet() { Name = "Some other diet" });
            animal.Sociableness.Add(new Social() { Text = "Some social" });
            animal.Sociableness.Add(new Social() { Text = "Some other social" });
            animal.Synonyms.Add(new ScientificName() { Genus = "Some other genus", Species = "Some other species" });
            animal.Tradenames.Add(new Tradename()
            {
                Language = new Language() { Code = "DK", Name = "Dansk" },
                Name = "Sær fisk"
            });
            animal.Tradenames.Add(new Tradename()
            {
                Language = new Language() { Code = "EN", Name = "English" },
                Name = "Strange fish"
            });
            return animal;
        }

        #endregion

        #region Animal Object
        [TestMethod]
        public void Animal_CreateObject()
        {
            Animal animal = new Animal();
            Assert.IsNotNull(animal);
        }
        #endregion

        #region Some
        [TestMethod]
        public void Test_DBHelper_CountAnimal()
        {
            Assert.Inconclusive("Not implemented");
        }
        #endregion

        #region Repository
        #region Default Repository
        [TestMethod]
        public void AnimalRepository_GetAll()
        {
            List<Animal> list = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    list = repo.GetAll().ToList();
                }
            }
            Assert.IsNotNull(list);
            Assert.AreNotEqual(0, list.Count);
        }

        [TestMethod]
        public void AnimalRepository_GetAll_AndLoop()
        {
            IQueryable<Animal> list = null;
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    list = repo.GetAll();
                }
            }
            Assert.IsNotNull(list);
            foreach (Animal an in list)
            {
                Assert.IsNotNull(an);
            }
        }

        [TestMethod]
        public void AnimalRepository_GetById()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_GetById_NotExistingId()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_Create()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalHelper.DeleteAll();
            Animal animal = CreateAnAnimal_AllNew();
            Animal animal2 = null;
            IUnitOfWork unitOfWork = new FishbaseUnitOfWork();

            using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
            {
                animalRepository.Create(animal);
                unitOfWork.Commit();
                Assert.IsNotNull(animal);
                Assert.AreNotEqual(0, animal.Id);
            }

            unitOfWork = new FishbaseUnitOfWork();

            using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
            {
                unitOfWork.BeginTransaction();

                animal2 = animalRepository.GetById(animal.Id);
                unitOfWork.Commit();
                Assert.IsNotNull(animal2);
            }
            Assert.AreEqual(animal.Bioload, animal2.Bioload);
            Assert.AreEqual(animal.ScientificName.Genus, animal2.ScientificName.Genus);
        }

        [TestMethod]
        public void AnimalRepository_Create_ValuesMissing()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_Update()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_Update_ValuesMissing()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_Delete()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalHelper.DeleteAll();
            Animal animal = CreateAnAnimal_AllNew();
            IUnitOfWork unitOfWork = new FishbaseUnitOfWork();

            using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
            {
                animalRepository.Create(animal);
                unitOfWork.Commit();
                Assert.IsNotNull(animal);
                Assert.AreNotEqual(0, animal.Id);
            }

            unitOfWork = new FishbaseUnitOfWork();

            using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
            {
                unitOfWork.BeginTransaction();
                animalRepository.Delete(animal.Id);
                unitOfWork.Commit();
            }

            Animal animal2 = null;
            using (AnimalRepository animalRepository = new AnimalRepository(new FishbaseUnitOfWork()))
            {
                animal2 = animalRepository.GetById(animal.Id);
                Assert.IsNull(animal2);
            }
        }

        [TestMethod]
        public void AnimalRepository_FindBySciName()
        {
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<Animal> repo = new Repository<Animal>(uow))
                {
                    List<Animal> list = repo.GetAll().Where(x => x.ScientificName.ToString().Contains("Apistrogramma")).ToList();
                    Assert.IsNotNull(list);
                    Assert.AreNotEqual(0, list.Count);
                }
            }
        }

        #endregion

        [TestMethod]
        public void AnimalRepository_FindBySciName_WithoutSynonym()
        {
            using (FishbaseUnitOfWork unitOfWork = new FishbaseUnitOfWork())
            {
                using (AnimalRepository animalRepository = new AnimalRepository(unitOfWork))
                {
                    ScientificName sciName = new ScientificName()
                    {
                        Genus = "Apistogramma",
                        Species = string.Empty
                    };
                    Animal list = animalRepository.GetByScientificName(sciName, false).FirstOrDefault();
                    Assert.IsNotNull(list);
                }
            }
        }

        [TestMethod]
        public void AnimalRepository_FindBySciName_WithoutSynonym_NotExisting()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_FindBySciName_WithSynonym()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_FindBySciName_WithSynonym_NotExisting()
        {
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_GetTopViewed()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalHelper.DeleteAll();
            CreateSomeAnimals();
            Assert.Inconclusive("Not implemented");
        }

        [TestMethod]
        public void AnimalRepository_GetNewest()
        {
            Assert.Inconclusive("Not implemented");
        }
        #endregion
    }
}
