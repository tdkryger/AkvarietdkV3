﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestAkvarietFishbase
{
    [TestClass]
    public class utSize
    {
        [TestMethod]
        public void CreateSize()
        {
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();

            using (Repository<Size> repository = new Repository<Size>(UnitOfWork))
            {
                Size size = new Size()
                {
                    Min = 2,
                    Max = 7
                };
                UnitOfWork.BeginTransaction();
                repository.Create(size);
                UnitOfWork.Commit();
            }
        }
    }
}
