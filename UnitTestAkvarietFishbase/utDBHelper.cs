﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestAkvarietFishbase
{
    [TestClass]
    public class utDBHelper
    {
        [TestMethod]
        public void DBHelper_GenerateDefaultIllness()
        {
            Akvariet.Fishbase.Database.Helpers.IllnessHelper.GenerateDefaultIllness();
        }

        [TestMethod]
        public void DBHelper_GenerateDefaultAnimalGroups()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.GenerateDefaultAnimalGroups();
        }

        [TestMethod]
        public void DBHelper_GenerateDefaultOrder()
        {
            Akvariet.Fishbase.Database.Helpers.OrderHelper.GenerateDefaultOrder();
        }
    }
}
