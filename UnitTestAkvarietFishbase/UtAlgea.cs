﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Akvariet.Fishbase.Model;

namespace UnitTestAkvarietFishbase
{
    [TestClass]
    public class UtAlgea
    {
        [AssemblyInitialize()]
        public static void AssemblyInit(TestContext context)
        {
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
        }

        [TestMethod]
        public void Algae_CreateObject()
        {
            Algae item = new Algae()
            {
                Cause = string.Empty,
                Name = string.Empty,
                Symptoms = string.Empty,
                Treatment = string.Empty
            };
            Assert.IsNotNull(item);
        }
    }
}
