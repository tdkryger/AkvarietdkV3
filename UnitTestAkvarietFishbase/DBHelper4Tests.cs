﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace UnitTestAkvarietFishbase
{
    static class DBHelper4Tests
    {
        private static readonly Random random = new Random((int)DateTime.Now.Ticks);

        public static AnimalGroup GetAnAnimalGroup()
        {
            AnimalGroup animalGroup = null;
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
            {
                var animalGroups = repository.GetAll();
                animalGroup = animalGroups.FirstOrDefault();
            }
            if (animalGroup == null)
            {
                Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.GenerateDefaultAnimalGroups();
                using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
                {
                    var animalGroups = repository.GetAll();
                    animalGroup = animalGroups.FirstOrDefault();
                }
            }
            return animalGroup;
        }

        public static Illness GetAnIllness()
        {
            Illness illness = null;
            using (Repository<Illness> repository = new Repository<Illness>(new FishbaseUnitOfWork()))
            {
                var list = repository.GetAll();
                illness = list.FirstOrDefault();
            }
            if (illness == null)
            {
                Akvariet.Fishbase.Database.Helpers.IllnessHelper.GenerateDefaultIllness();
                using (Repository<Illness> repository = new Repository<Illness>(new FishbaseUnitOfWork()))
                {
                    var list = repository.GetAll();
                    illness = list.FirstOrDefault();
                }
            }

            Assert.IsNotNull(illness, "No illnesses defined");
            return illness;
        }

        public static Order GetAnOrder()
        {
            Order item = null;
            using (Repository<Order> repository = new Repository<Order>(new FishbaseUnitOfWork()))
            {
                var animalGroups = repository.GetAll();
                item = animalGroups.FirstOrDefault();
            }
            if (item == null)
            {
                Akvariet.Fishbase.Database.Helpers.OrderHelper.GenerateDefaultOrder();
                using (Repository<Order> repository = new Repository<Order>(new FishbaseUnitOfWork()))
                {
                    var animalGroups = repository.GetAll();
                    item = animalGroups.FirstOrDefault();
                }
            }
            return item;

        }
    }
}
