﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestAkvarietFishbase
{
    [TestClass]
    public class utAnimalGroup
    {
        [TestMethod]
        public void AnimalGroupCreateAll()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.GenerateDefaultAnimalGroups();
        }

        [TestMethod]
        public void AnimalGroupCreate()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.DeleteAllAnimalGroups();

            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            AnimalGroup item;
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                item = new AnimalGroup()
                {
                    Name = "Catfish (Loricariids)"
                };
                UnitOfWork.BeginTransaction();
                repository.Create(item);
                UnitOfWork.Commit();
            }
            Assert.IsNotNull(item);
            Assert.AreNotEqual(0, item.Id);
        }

        [TestMethod]
        public void AnimalGroupGetAll()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.DeleteAllAnimalGroups();
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.GenerateDefaultAnimalGroups();

            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
            {
                List<AnimalGroup> list = repository.GetAll().ToList();
                Assert.IsNotNull(list);
                Assert.AreNotEqual(0, list.Count);
            }
        }

        [TestMethod]
        public void AnimalGroupGetById()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.DeleteAllAnimalGroups();
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            AnimalGroup item;
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                item = new AnimalGroup()
                {
                    Name = "Catfish (Loricariids)"
                };
                UnitOfWork.BeginTransaction();
                repository.Create(item);
                UnitOfWork.Commit();
            }

            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
            {
                AnimalGroup item2 = repository.GetById(item.Id);
                Assert.IsNotNull(item2);
                Assert.AreEqual(item.Name, item2.Name);
            }
        }

        [TestMethod]
        public void AnimalGroupUpdate()
        {
            string theZone = "The Zone";
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.DeleteAllAnimalGroups();
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            AnimalGroup item;
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                item = new AnimalGroup()
                {
                    Name = "Catfish (Loricariids)"
                };
                UnitOfWork.BeginTransaction();
                repository.Create(item);
                UnitOfWork.Commit();
            }

            UnitOfWork = new FishbaseUnitOfWork();
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                item.Zone = theZone;
                UnitOfWork.BeginTransaction();
                repository.Update(item);
                UnitOfWork.Commit();
            }

            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
            {
                AnimalGroup item2 = repository.GetById(item.Id);
                Assert.IsNotNull(item2);
                Assert.AreEqual(item.Name, item2.Name);
                Assert.AreEqual(theZone, item.Zone);
            }
        }

        [TestMethod]
        public void AnimalGroupDelete()
        {
            Akvariet.Fishbase.Database.Helpers.AnimalGroupHelper.DeleteAllAnimalGroups();
            IUnitOfWork UnitOfWork = new FishbaseUnitOfWork();
            AnimalGroup item;
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                item = new AnimalGroup()
                {
                    Name = "Catfish (Loricariids)"
                };
                UnitOfWork.BeginTransaction();
                repository.Create(item);
                UnitOfWork.Commit();
            }

            UnitOfWork = new FishbaseUnitOfWork();
            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(UnitOfWork))
            {
                UnitOfWork.BeginTransaction();
                repository.Delete(item.Id);
                UnitOfWork.Commit();
            }

            using (Repository<AnimalGroup> repository = new Repository<AnimalGroup>(new FishbaseUnitOfWork()))
            {
                AnimalGroup item2 = repository.GetById(item.Id);
                Assert.IsNull(item2);
            }
        }
    }
}
