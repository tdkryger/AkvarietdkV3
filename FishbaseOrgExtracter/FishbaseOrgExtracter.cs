﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APaF.Fishbase;
using MySql.Data.MySqlClient;
using NLog;

namespace FishbaseOrgExtracter
{
    class FishbaseOrgExtracter
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            using (MySqlConnection conn = new MySqlConnection("Server=192.168.1.36;Database=akvariet_dk_db2;Uid=newuser;Pwd=somepassword;ConvertZeroDateTime=True;"))
            {
                conn.Open();
                try
                {
                    var fbi = new FishBaseImport();
                    fbi.FishbaseStatus += Fbi_FishbaseStatus;
                    fbi.DownloadSummaryXML(@"D:\tempFiles\fishbaseOrg", conn);
                }
                finally
                {
                    conn.Close();
                }
            }
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void Fbi_FishbaseStatus(string nameUrl, int fishbaseId, int count)
        {
            //logger.Info("Importing. Url={0}, Fishbase Id={1}, Count={2}", nameUrl, fishbaseId, count);
            Console.WriteLine($"Importing. Url={nameUrl}, Fishbase Id={fishbaseId}, Count={count}");
        }
    }
}
