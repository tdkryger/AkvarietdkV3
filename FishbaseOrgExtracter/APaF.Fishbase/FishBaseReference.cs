﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APaF.Fishbase
{
    public class FishBaseReference
    {
        #region Properties
        public int ID { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        #endregion
    }
}
