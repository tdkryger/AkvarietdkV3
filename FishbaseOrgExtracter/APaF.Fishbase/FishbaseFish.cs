﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APaF.Fishbase
{
    public class FishbaseFish
    {
        #region fields
        private List<string> _commonNames = new List<string>();
        private List<string> _synonyms = new List<string>();
        private List<FishBaseReference> _references = new List<FishBaseReference>();
        #endregion

        #region Propertues
        public string FishbaseID { get; set; }
        public string XMLSummaryUrl { get; set; }
        public string XMLPointDataUrl { get; set; }
        public string XMLCommonNamesUrl { get; set; }
        public string XMLPhotoUrl { get; set; }


        public string Identifier { get; set; }
        public string Source { get; set; }
        public string Kingdom { get; set; }
        public string Phylum { get; set; }
        public string Class { get; set; }
        public string Order { get; set; }
        public string Family { get; set; }
        public string Genus { get; set; }
        public string ScientificName { get; set; }
        public List<string> CommonNames { get { return _commonNames; } }
        public List<string> Synonyms { get { return _synonyms; } }
        public string Created { get; set; }
        public string Modified { get; set; }
        public List<FishBaseReference> References { get { return _references; } }

        #endregion

        #region Public Methods

        #endregion
    }
}
