﻿using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;

namespace APaF.Fishbase
{
    public class FishBaseImport
    {
        #region consts
        private const string FISHBASE_XML_SUMMARY = "http://www.fishbase.us/maintenance/FB/showXML.php?identifier=FB-{0}";
        private const string FISHBASE_XML_POINT_DATA = "http://www.fishbase.se/webservice/Occurrence/PointData.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_COMMON_NAMES = "http://www.fishbase.se/webservice/ComNames/comnamesxml.php?Genus={0}&Species={1}";
        private const string FISHBASE_XML_PHOTOS = "http://www.fishbase.se/webservice/Photos/FishPicsList.php?Genus={0}&Species={1}&type=";

        private const string SQL_INSERT = "INSERT INTO `fishbaseRaw` (`fishbaseId`, `genus`, `species`, `xmlSummary`, `xmlPointData`, `xmlCommonNames`, `xmlPhotos`) "
            + "VALUES (@fishbaseId, @genus, @species, @xmlSummary, @xmlPointData, @xmlCommonNames, @xmlPhotos) "
            + "ON DUPLICATE KEY UPDATE "
            + "`genus`=@genus, `species`=@species, `xmlSummary`=@xmlSummary, `xmlPointData`=@xmlPointData, `xmlCommonNames`=@xmlCommonNames, `xmlPhotos`=@xmlPhotos; ";

        #endregion

        #region Fields & other private values
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly string[] FISHBASE_LATIN_NAMES = {
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesA.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesB.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesC.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesD.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesE.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesF.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesG.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesH.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesI.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesJ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesK.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesL.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesM.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesN.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesO.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesP.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesQ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesR.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesS.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesT.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesU.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesV.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesW.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesX.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesY.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesZ.htm"
                                                };

        private readonly Queue<string> downloadList = new Queue<string>();

        private int importCount;
        #endregion

        #region Events & Delegates
        public delegate void FishbaseStatusDelegate(string nameUrl, int fishbaseId, int count);
        public event FishbaseStatusDelegate FishbaseStatus;
        #endregion

        #region Public methods
        public void DownloadSummaryXML(string savePath, MySqlConnection connection)
        {
            importCount = 0;
            foreach (string url in FISHBASE_LATIN_NAMES)
            {
                DownloadXMLTask(url, savePath, connection);
                Thread.Sleep(1);
            }
        }

        public void DownloadSummaryXML(string savePath, MySqlConnection connection, int fishbaseLatinNamesIndex)
        {
            if (fishbaseLatinNamesIndex < FISHBASE_LATIN_NAMES.Length)
                DownloadXMLTask(FISHBASE_LATIN_NAMES[fishbaseLatinNamesIndex], savePath, connection);
        }

        public int FishbaseCount()
        {
            return FISHBASE_LATIN_NAMES.Length;
        }
        #endregion

        #region Private methods

        private void parseSummaryXML(string filename, MySqlConnection connection)
        {
            XmlDocument doc = new XmlDocument();

            FixBigIproblem(filename);
            string sciName = string.Empty;
            int FishBaseId = 0;
            try
            {
                doc.Load(filename);
                FishBaseId = GetElementInt(doc, tagName: "dc:identifier");
                sciName = GetElementValue(doc, "dwc:ScientificName");
            }
            catch
            {
                sciName = string.Empty;
            }
            if (!string.IsNullOrEmpty(sciName))
            {
                string[] sciParts = sciName.Split(' ');
                string genus = sciParts[0];
                string species = string.Empty;
                if (sciParts.Length == 2)
                {
                    species = sciParts[1];
                }
                else
                {
                    for (int i = 1; i < sciParts.Length; i++)
                    {
                        species = species + sciParts[i];
                    }
                }
                species = species.Trim();



                string xmlSummary = string.Empty;
                string xmlPointData = string.Empty;
                string xmlCommonNames = string.Empty;
                string xmlPhotos = string.Empty;

                using (WebClient client = new WebClient())
                {
                    try
                    {
                        FishbaseStatus?.Invoke("Summary", FishBaseId, importCount);
                        xmlSummary = System.IO.File.ReadAllText(filename);
                        if (xmlSummary == string.Empty)
                        {
                            xmlSummary = client.DownloadString(string.Format(FISHBASE_XML_SUMMARY, FishBaseId));
                            System.IO.File.WriteAllText(filename, xmlSummary);
                        }
                    }
                    catch
                    {
                        xmlSummary = string.Empty;
                    }
                    try
                    {
                        FishbaseStatus?.Invoke("Point Data", FishBaseId, importCount);
                        xmlPointData = loadXMLFromFile(filename, "xmlPointData");
                        if (xmlPointData == string.Empty)
                        {
                            xmlPointData = client.DownloadString(string.Format(FISHBASE_XML_POINT_DATA, genus, species));
                            saveXMLToFile(filename, "xmlPointData", xmlPointData);
                        }
                    }
                    catch
                    {
                        xmlPointData = string.Empty;
                    }
                    try
                    {
                        FishbaseStatus?.Invoke("Common Names", FishBaseId, importCount);
                        xmlCommonNames = loadXMLFromFile(filename, "xmlCommonNames");
                        if (xmlCommonNames == string.Empty)
                        {
                            xmlCommonNames = client.DownloadString(string.Format(FISHBASE_XML_COMMON_NAMES, genus, species));
                            saveXMLToFile(filename, "xmlCommonNames", xmlCommonNames);
                        }
                    }
                    catch
                    {
                        xmlCommonNames = string.Empty;
                    }
                    try
                    {
                        FishbaseStatus?.Invoke("Photos", FishBaseId, importCount);
                        xmlPhotos = loadXMLFromFile(filename, "xmlPhotos");
                        if (xmlPhotos == string.Empty)
                        {
                            xmlPhotos = client.DownloadString(string.Format(FISHBASE_XML_PHOTOS, genus, species));
                            saveXMLToFile(filename, "xmlPhotos", xmlPhotos);
                        }
                    }
                    catch
                    {
                        xmlPhotos = string.Empty;
                    }
                }

                FishbaseStatus?.Invoke("Saving to MySQL", FishBaseId, importCount);
                MySqlCommand cmd = new MySqlCommand(SQL_INSERT, connection);
                cmd.Parameters.AddWithValue("@fishbaseId", FishBaseId);
                cmd.Parameters.AddWithValue("@genus", genus);
                cmd.Parameters.AddWithValue("@species", species);
                cmd.Parameters.AddWithValue("@xmlSummary", xmlSummary);
                cmd.Parameters.AddWithValue("@xmlPointData", xmlPointData);
                cmd.Parameters.AddWithValue("@xmlCommonNames", xmlCommonNames);
                cmd.Parameters.AddWithValue("@xmlPhotos", xmlPhotos);
                cmd.ExecuteNonQuery();
            }
        }

        private string GetElementValue(XmlDocument _xmlDoc, string tagName, int index = 0)
        {
            XmlNodeList fbid = _xmlDoc.GetElementsByTagName(tagName);
            if (fbid.Count > 0)
            {
                return fbid[0].InnerText;
            }
            return string.Empty;
        }

        private int GetElementInt(XmlDocument _xmlDoc, string tagName, int index = 0)
        {
            int value = 0;
            XmlNodeList fbid = _xmlDoc.GetElementsByTagName(tagName);
            if (fbid.Count > 0)
            {
                string s = fbid[0].InnerText;
                s = s.Substring(3);

                int.TryParse(s, out value);
            }
            return value;
        }

        private void FixBigIproblem(string filename)
        {
            string text = File.ReadAllText(filename);
            text = text.Replace("</I>", "</i>");
            File.WriteAllText(filename, text);
        }

        private void DownloadXMLTask(string url, string savePath, MySqlConnection connection)
        {
            //WebClient web = new WebClient();
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = hw.Load(url);

            foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
            {
                importCount++;
                //link.InnerText
                HtmlAttribute att = link.Attributes["href"];
                int fishbaseID = 0;
                if (getIntegerFromString(att.Value, out fishbaseID))
                {
                    FishbaseStatus?.Invoke(url, fishbaseID, importCount);
                    string saveDir = Path.Combine(savePath, link.InnerText);
                    try
                    {
                        Directory.CreateDirectory(saveDir);
                    }
                    catch(Exception ex)
                    {

                        Console.WriteLine(link.InnerText);
                        logger.Error(ex, link.InnerText);
                        saveDir = Path.Combine(savePath, fishbaseID.ToString());
                        Directory.CreateDirectory(saveDir);
                    }
                    string xmlurl = string.Format(FISHBASE_XML_SUMMARY, fishbaseID);
                    string saveFilename = Path.Combine(saveDir, "summary.xml");
                    if (File.Exists(saveFilename))
                    {
                        FileInfo fi = new FileInfo(saveFilename);
                        if (fi.Length <= 1024)
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.WriteLine("File is less than 1K. Deleting for re-download");
                            Console.ResetColor();
                            File.Delete(saveFilename);
                        }
                    }
                    if (!File.Exists(saveFilename))
                    {
                        try
                        {
                            using (WebClient web = new WebClient())
                            {
                                web.Encoding = Encoding.UTF8;
                                web.DownloadFile(xmlurl, saveFilename);
                            }
                        }
                        catch(Exception ex)
                        {
                            logger.Error("Error: {0} {1}", xmlurl, ex.Message);
                            Debug.Print("Error: {0} {1}", xmlurl, ex.Message);
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("Error: {0} {1}", xmlurl, ex.Message);
                            Console.ResetColor();
                        }
                    }
                    if (File.Exists(saveFilename))
                    {
                        FileInfo f = new FileInfo(saveFilename);
                        if (f.Length > 0)
                            parseSummaryXML(saveFilename, connection);
                    }
                    //else
                    //    Thread.Sleep(50);
                }
                else
                {
                    logger.Error("Error: {0}", att.Value);
                    Debug.Print("Error: " + att.Value);
                    Console.WriteLine("Error: {0}", att.Value);
                }
            }
            doc = null;
            hw = null;
        }

        private bool getIntegerFromString(string input, out int value)
        {
            var stack = new Stack<char>();

            for (var i = input.Length - 1; i >= 0; i--)
            {
                if (!char.IsNumber(input[i]))
                {
                    break;
                }

                stack.Push(input[i]);
            }

            string result = new string(stack.ToArray());
            value = 0;
            return int.TryParse(result, out value);
        }

        private void saveXMLToFile(string summaryFilename, string xmlType, string xml)
        {
            string filename = summaryFilename + "." + xmlType + ".xml";
            System.IO.File.WriteAllText(filename, xml);
        }

        private string loadXMLFromFile(string summaryFilename, string xmlType)
        {
            string filename = summaryFilename + "." + xmlType + ".xml";
            if (File.Exists(filename))
            {
                return System.IO.File.ReadAllText(filename);
            }
            else
                return string.Empty;
        }
        #endregion
    }
}
