﻿using Akvariet.Fishbase.Model;
using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmAnimalGroup : Form
    {
        #region Fields
        private AnimalGroup _animalGroup;
        #endregion

        #region Properties
        public AnimalGroup SelectedAnimalGroup { get { return GetGroup(); } set { SetGroup(value); } }
        #endregion

        public frmAnimalGroup()
        {
            InitializeComponent();
        }

        #region Private methods
        private AnimalGroup GetGroup()
        {
            if (_animalGroup == null)
                _animalGroup = new AnimalGroup();
            _animalGroup.Active = checkBox1.Checked;
            _animalGroup.AqualogCode = tbAquaLog.Text;
            _animalGroup.Name = tbName.Text;
            _animalGroup.Zone = tbZone.Text;
            return _animalGroup;
        }

        private void SetGroup(AnimalGroup value)
        {
            _animalGroup = value;
            checkBox1.Checked = _animalGroup.Active;
            tbAquaLog.Text = _animalGroup.AqualogCode;
            tbName.Text = _animalGroup.Name;
            tbZone.Text = _animalGroup.Zone;
        }
        #endregion
    }
}
