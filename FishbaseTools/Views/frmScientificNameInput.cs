﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmScientificNameInput : Form
    {
        private ScientificName _selected = null;

        public ScientificName Value { get { return GetSciName(); } set { SetSciName(value); } }

        public frmScientificNameInput(List<string> genusList, List<ScientificName> sciNameList)
        {
            InitializeComponent();

            cBoxSciName.Items.Clear();
            cBox.Items.Clear();
            textBox.Text = string.Empty;
            foreach (string genus in genusList)
            {
                cBox.Items.Add(genus);
            }
            foreach (ScientificName sciName in sciNameList)
            {
                cBoxSciName.Items.Add(sciName);
            }
            rbExisting.Checked = sciNameList.Count != 0;
            rbNew.Checked = sciNameList.Count == 0;

            rbExisting_CheckedChanged(null, null);
        }

        private void SetSciName(ScientificName value)
        {
            cBoxSciName.SelectedIndex = cBoxSciName.Items.IndexOf(value);
            cBox.Text = value.Genus;
            textBox.Text = value.Species;
            _selected = value;
        }

        private ScientificName GetSciName()
        {
            if (rbExisting.Checked)
            {
                if (cBoxSciName.SelectedItem != null && cBoxSciName.SelectedItem is ScientificName)
                {
                    _selected = (ScientificName)cBoxSciName.SelectedItem;
                }
            }
            else
            {
                using (ScientificNameRepository repo = new ScientificNameRepository(new FishbaseUnitOfWork()))
                {
                    _selected = repo.GetByGenusAndSpecies(cBox.Text, textBox.Text);
                }
            }
            if (_selected == null)
            {
                _selected = new ScientificName()
                {
                    Genus = cBox.Text,
                    Species = textBox.Text
                };
            }

            return _selected;
        }

        private void cBoxLanguage_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender is ComboBox)
            {
                ComboBox comboBox = (ComboBox)sender;
                int index;
                string actual;
                string found;

                // Do nothing for certain keys, such as navigation keys.
                if ((e.KeyCode == Keys.Back) ||
                (e.KeyCode == Keys.Left) ||
                (e.KeyCode == Keys.Right) ||
                (e.KeyCode == Keys.Up) ||
                (e.KeyCode == Keys.Down) ||
                (e.KeyCode == Keys.Delete) ||
                (e.KeyCode == Keys.PageUp) ||
                (e.KeyCode == Keys.PageDown) ||
                (e.KeyCode == Keys.Home) ||
                (e.KeyCode == Keys.End))
                {
                    return;
                }

                // Store the actual text that has been typed.
                actual = comboBox.Text;

                // Find the first match for the typed value.
                index = comboBox.FindString(actual);

                // Get the text of the first match.
                if (index > -1)
                {
                    found = comboBox.Items[index].ToString();

                    // Select this item from the list.
                    comboBox.SelectedIndex = index;

                    // Select the portion of the text that was automatically
                    // added so that additional typing replaces it.
                    comboBox.SelectionStart = actual.Length;
                    comboBox.SelectionLength = found.Length;
                }
            }
        }

        private void rbExisting_CheckedChanged(object sender, EventArgs e)
        {
            cBoxSciName.Enabled = rbExisting.Checked;
            cBox.Enabled = !rbExisting.Checked;
            textBox.Enabled = !rbExisting.Checked;
        }
    }
}
