﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmTradenameInput : Form
    {
        #region Fields
        private Tradename _tradename;
        #endregion

        public string Title { get { return this.Text; } set { this.Text = value; } }
        public string LabelText { get { return label1.Text; } set { label1.Text = value; } }
        public Tradename SelectedTradename { get { return GetTradename(); } set { SetTradename(value); } }

        public frmTradenameInput()
        {
            InitializeComponent();
            List<Language> lanList = new List<Language>();
            using (Repository<Language> rep = new Repository<Language>(new FishbaseUnitOfWork()))
            {
                lanList = rep.GetAll().OrderBy(x => x.Name).ToList();
            }
            cBoxLanguage.Items.Clear();
            foreach (Language lan in lanList)
            {
                cBoxLanguage.Items.Add(lan);
            }
            cBoxLanguage.SelectedIndex = 1;
        }

        private void SetTradename(Tradename value)
        {
            _tradename = value;
            tbName.Text = value.Name;
            //TODO: Select the right object from cBoxLanguage
        }

        private Tradename GetTradename()
        {
            if (_tradename == null)
            {
                _tradename = new Tradename()
                {
                    Language = (Language)cBoxLanguage.SelectedItem,
                    Name = tbName.Text.Trim()
                };
            }
            return _tradename;
        }
    }
}
