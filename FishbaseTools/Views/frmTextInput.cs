﻿using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmTextInput : Form
    {
        public string Title { get { return this.Text; } set { this.Text = value; } }
        public string Label { get { return label1.Text; } set { label1.Text = value; } }
        public string Value { get { return tbName.Text; } set { tbName.Text = value; } }

        public frmTextInput()
        {
            InitializeComponent();
        }
    }
}
