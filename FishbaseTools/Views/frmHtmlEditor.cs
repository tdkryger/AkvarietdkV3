﻿using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmHtmlEditor : Form
    {

        public string Title { get { return this.Text; } set { this.Text = value; } }
        public string Html { get { return htmlEditorControl1.InnerHtml; } set { htmlEditorControl1.InnerHtml = value; } }
        public bool ShowExsisting { get { return pExsisting.Visible; } set { pExsisting.Visible = value; } }

        public frmHtmlEditor()
        {
            InitializeComponent();
            pExsisting.Visible = false;
        }
    }
}
