﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FishbaseTools.Views
{
    public partial class frmOrderDialog : Form
    {
        private Order _selectedOrder = null;

        public Order SelectedOrder { get { return GetOrder(); } set { SetOrder(value); } }

        public frmOrderDialog()
        {
            InitializeComponent();

            List<Order> orderList = new List<Order>();
            using (Repository<Order> rep = new Repository<Order>(new FishbaseUnitOfWork()))
            {
                orderList = rep.GetAll().OrderBy(x => x.OrderGroup).ToList();
            }
            List<string> groupList = new List<string>();
            Parallel.ForEach(orderList, (item) => { groupList.Add(item.OrderGroup); });
            List<string> distinctGenus = groupList.Distinct().ToList();
            distinctGenus.Sort();
            cBox.Items.Clear();
            foreach (string s in distinctGenus)
            {
                cBox.Items.Add(s);
            }
            cBox.SelectedIndex = 1;
        }

        private Order GetOrder()
        {
            if (_selectedOrder == null)
            {
                _selectedOrder = new Order();
            }
            _selectedOrder.Name = tbName.Text;
            _selectedOrder.OrderGroup = cBox.Text;
            return _selectedOrder;
        }

        private void SetOrder(Order value)
        {
            _selectedOrder = value;
            tbName.Text = _selectedOrder.Name;
            cBox.SelectedIndex = cBox.Items.IndexOf(value.OrderGroup);
            //TODO: CHech if this ^ works
        }

        private void cBoxLanguage_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender is ComboBox)
            {
                ComboBox comboBox = (ComboBox)sender;
                int index;
                string actual;
                string found;

                // Do nothing for certain keys, such as navigation keys.
                if ((e.KeyCode == Keys.Back) ||
                (e.KeyCode == Keys.Left) ||
                (e.KeyCode == Keys.Right) ||
                (e.KeyCode == Keys.Up) ||
                (e.KeyCode == Keys.Down) ||
                (e.KeyCode == Keys.Delete) ||
                (e.KeyCode == Keys.PageUp) ||
                (e.KeyCode == Keys.PageDown) ||
                (e.KeyCode == Keys.Home) ||
                (e.KeyCode == Keys.End))
                {
                    return;
                }

                // Store the actual text that has been typed.
                actual = comboBox.Text;

                // Find the first match for the typed value.
                index = comboBox.FindString(actual);

                // Get the text of the first match.
                if (index > -1)
                {
                    found = comboBox.Items[index].ToString();

                    // Select this item from the list.
                    comboBox.SelectedIndex = index;

                    // Select the portion of the text that was automatically
                    // added so that additional typing replaces it.
                    comboBox.SelectionStart = actual.Length;
                    comboBox.SelectionLength = found.Length;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (frmTextInput textInput = new frmTextInput())
            {
                textInput.Title = "Underorden";
                textInput.Label = "Navn";
                if (textInput.ShowDialog() == DialogResult.OK)
                {
                    lbSubOrder.Items.Add(textInput.Value);
                    SelectedOrder.SubOrders.Add(new SubOrder() { Name = textInput.Value });
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string sel = (string)lbSubOrder.SelectedItem;
            SubOrder so = SelectedOrder.SubOrders.FirstOrDefault(x => x.Name == sel);
            SelectedOrder.SubOrders.Remove(so);
            lbSubOrder.Items.Remove(sel);
        }
    }
}
