﻿namespace FishbaseTools
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslNumberAnimals = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslNumberPlants = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpAnimal = new System.Windows.Forms.TabPage();
            this.tpPlants = new System.Windows.Forms.TabPage();
            this.tpBlogs = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.lbBlogEntrys = new System.Windows.Forms.ListBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pBlogTagList = new System.Windows.Forms.Panel();
            this.clbBlogTags = new System.Windows.Forms.CheckedListBox();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.tsbBlogNewTag = new System.Windows.Forms.ToolStripButton();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.tpImages = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.lbPicture = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnRefreshPics = new System.Windows.Forms.Button();
            this.lblPicId = new System.Windows.Forms.Label();
            this.lblPicDate = new System.Windows.Forms.Label();
            this.lblPicCopy = new System.Windows.Forms.Label();
            this.lblPicTitle = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tpSupportTables = new System.Windows.Forms.TabPage();
            this.tpTools = new System.Windows.Forms.TabPage();
            this.pImportFish = new System.Windows.Forms.Panel();
            this.lbAqualogFishDone = new System.Windows.Forms.ListBox();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.lblImportAqualogFish = new System.Windows.Forms.Label();
            this.pImportImages = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lblPicImport = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPlantPDF = new System.Windows.Forms.Button();
            this.btnAnimalPDF = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnSupport = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnLoadImage = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnFishbaseOrgPictures = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnAqualogImportPlants = new System.Windows.Forms.Button();
            this.btnAqualogImportFish = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnImportHabitats = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpBlogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.pBlogTagList.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.tpImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tpTools.SuspendLayout();
            this.pImportFish.SuspendLayout();
            this.pImportImages.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1134, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslNumberAnimals,
            this.tsslNumberPlants,
            this.tsslStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 728);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1134, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslNumberAnimals
            // 
            this.tsslNumberAnimals.Name = "tsslNumberAnimals";
            this.tsslNumberAnimals.Size = new System.Drawing.Size(67, 17);
            this.tsslNumberAnimals.Text = "Antal dyr: 0";
            // 
            // tsslNumberPlants
            // 
            this.tsslNumberPlants.Name = "tsslNumberPlants";
            this.tsslNumberPlants.Size = new System.Drawing.Size(87, 17);
            this.tsslNumberPlants.Text = "Antal planter: 0";
            // 
            // tsslStatus
            // 
            this.tsslStatus.Name = "tsslStatus";
            this.tsslStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsslStatus.Size = new System.Drawing.Size(118, 17);
            this.tsslStatus.Text = "toolStripStatusLabel1";
            this.tsslStatus.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpAnimal);
            this.tabControl1.Controls.Add(this.tpPlants);
            this.tabControl1.Controls.Add(this.tpBlogs);
            this.tabControl1.Controls.Add(this.tpImages);
            this.tabControl1.Controls.Add(this.tpSupportTables);
            this.tabControl1.Controls.Add(this.tpTools);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1134, 704);
            this.tabControl1.TabIndex = 3;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpAnimal
            // 
            this.tpAnimal.Location = new System.Drawing.Point(4, 22);
            this.tpAnimal.Name = "tpAnimal";
            this.tpAnimal.Padding = new System.Windows.Forms.Padding(3);
            this.tpAnimal.Size = new System.Drawing.Size(1126, 678);
            this.tpAnimal.TabIndex = 6;
            this.tpAnimal.Text = "Dyr";
            this.tpAnimal.UseVisualStyleBackColor = true;
            // 
            // tpPlants
            // 
            this.tpPlants.Location = new System.Drawing.Point(4, 22);
            this.tpPlants.Name = "tpPlants";
            this.tpPlants.Padding = new System.Windows.Forms.Padding(3);
            this.tpPlants.Size = new System.Drawing.Size(1126, 678);
            this.tpPlants.TabIndex = 1;
            this.tpPlants.Text = "Planter";
            this.tpPlants.UseVisualStyleBackColor = true;
            // 
            // tpBlogs
            // 
            this.tpBlogs.Controls.Add(this.splitContainer3);
            this.tpBlogs.Controls.Add(this.toolStrip3);
            this.tpBlogs.Location = new System.Drawing.Point(4, 22);
            this.tpBlogs.Name = "tpBlogs";
            this.tpBlogs.Padding = new System.Windows.Forms.Padding(3);
            this.tpBlogs.Size = new System.Drawing.Size(1126, 678);
            this.tpBlogs.TabIndex = 5;
            this.tpBlogs.Text = "Blogs";
            this.tpBlogs.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 28);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.lbBlogEntrys);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.label17);
            this.splitContainer3.Panel2.Controls.Add(this.pBlogTagList);
            this.splitContainer3.Size = new System.Drawing.Size(1120, 647);
            this.splitContainer3.SplitterDistance = 373;
            this.splitContainer3.TabIndex = 1;
            // 
            // lbBlogEntrys
            // 
            this.lbBlogEntrys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBlogEntrys.FormattingEnabled = true;
            this.lbBlogEntrys.Location = new System.Drawing.Point(0, 0);
            this.lbBlogEntrys.Name = "lbBlogEntrys";
            this.lbBlogEntrys.Size = new System.Drawing.Size(373, 647);
            this.lbBlogEntrys.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(248, 146);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "html Editor";
            // 
            // pBlogTagList
            // 
            this.pBlogTagList.Controls.Add(this.clbBlogTags);
            this.pBlogTagList.Controls.Add(this.toolStrip4);
            this.pBlogTagList.Dock = System.Windows.Forms.DockStyle.Right;
            this.pBlogTagList.Location = new System.Drawing.Point(543, 0);
            this.pBlogTagList.Name = "pBlogTagList";
            this.pBlogTagList.Size = new System.Drawing.Size(200, 647);
            this.pBlogTagList.TabIndex = 0;
            // 
            // clbBlogTags
            // 
            this.clbBlogTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clbBlogTags.FormattingEnabled = true;
            this.clbBlogTags.Location = new System.Drawing.Point(0, 25);
            this.clbBlogTags.Name = "clbBlogTags";
            this.clbBlogTags.Size = new System.Drawing.Size(200, 622);
            this.clbBlogTags.TabIndex = 1;
            // 
            // toolStrip4
            // 
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbBlogNewTag});
            this.toolStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(200, 25);
            this.toolStrip4.TabIndex = 0;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // tsbBlogNewTag
            // 
            this.tsbBlogNewTag.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbBlogNewTag.Image = global::FishbaseTools.Properties.Resources.add16;
            this.tsbBlogNewTag.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBlogNewTag.Name = "tsbBlogNewTag";
            this.tsbBlogNewTag.Size = new System.Drawing.Size(23, 22);
            this.tsbBlogNewTag.Text = "Ny tag";
            // 
            // toolStrip3
            // 
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(1120, 25);
            this.toolStrip3.TabIndex = 0;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // tpImages
            // 
            this.tpImages.Controls.Add(this.splitContainer2);
            this.tpImages.Location = new System.Drawing.Point(4, 22);
            this.tpImages.Name = "tpImages";
            this.tpImages.Padding = new System.Windows.Forms.Padding(3);
            this.tpImages.Size = new System.Drawing.Size(1126, 678);
            this.tpImages.TabIndex = 4;
            this.tpImages.Text = "Billeder";
            this.tpImages.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lbPicture);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel3);
            this.splitContainer2.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer2.Size = new System.Drawing.Size(1120, 672);
            this.splitContainer2.SplitterDistance = 373;
            this.splitContainer2.TabIndex = 0;
            // 
            // lbPicture
            // 
            this.lbPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbPicture.FormattingEnabled = true;
            this.lbPicture.Location = new System.Drawing.Point(0, 0);
            this.lbPicture.Name = "lbPicture";
            this.lbPicture.Size = new System.Drawing.Size(373, 672);
            this.lbPicture.TabIndex = 0;
            this.lbPicture.SelectedIndexChanged += new System.EventHandler(this.lbPicture_SelectedIndexChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnRefreshPics);
            this.panel3.Controls.Add(this.lblPicId);
            this.panel3.Controls.Add(this.lblPicDate);
            this.panel3.Controls.Add(this.lblPicCopy);
            this.panel3.Controls.Add(this.lblPicTitle);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 572);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(743, 100);
            this.panel3.TabIndex = 1;
            // 
            // btnRefreshPics
            // 
            this.btnRefreshPics.Location = new System.Drawing.Point(643, 74);
            this.btnRefreshPics.Name = "btnRefreshPics";
            this.btnRefreshPics.Size = new System.Drawing.Size(95, 23);
            this.btnRefreshPics.TabIndex = 4;
            this.btnRefreshPics.Text = "Gendindlæs";
            this.btnRefreshPics.UseVisualStyleBackColor = true;
            this.btnRefreshPics.Click += new System.EventHandler(this.btnRefreshPics_Click);
            // 
            // lblPicId
            // 
            this.lblPicId.AutoSize = true;
            this.lblPicId.Location = new System.Drawing.Point(3, 11);
            this.lblPicId.Name = "lblPicId";
            this.lblPicId.Size = new System.Drawing.Size(44, 13);
            this.lblPicId.TabIndex = 3;
            this.lblPicId.Text = "lblPicId";
            // 
            // lblPicDate
            // 
            this.lblPicDate.AutoSize = true;
            this.lblPicDate.Location = new System.Drawing.Point(3, 85);
            this.lblPicDate.Name = "lblPicDate";
            this.lblPicDate.Size = new System.Drawing.Size(58, 13);
            this.lblPicDate.TabIndex = 2;
            this.lblPicDate.Text = "lblPicDate";
            // 
            // lblPicCopy
            // 
            this.lblPicCopy.AutoSize = true;
            this.lblPicCopy.Location = new System.Drawing.Point(3, 61);
            this.lblPicCopy.Name = "lblPicCopy";
            this.lblPicCopy.Size = new System.Drawing.Size(60, 13);
            this.lblPicCopy.TabIndex = 1;
            this.lblPicCopy.Text = "lblPicCopy";
            // 
            // lblPicTitle
            // 
            this.lblPicTitle.AutoSize = true;
            this.lblPicTitle.Location = new System.Drawing.Point(3, 39);
            this.lblPicTitle.Name = "lblPicTitle";
            this.lblPicTitle.Size = new System.Drawing.Size(55, 13);
            this.lblPicTitle.TabIndex = 0;
            this.lblPicTitle.Text = "lblPicTitle";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(743, 672);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tpSupportTables
            // 
            this.tpSupportTables.Location = new System.Drawing.Point(4, 22);
            this.tpSupportTables.Name = "tpSupportTables";
            this.tpSupportTables.Padding = new System.Windows.Forms.Padding(3);
            this.tpSupportTables.Size = new System.Drawing.Size(1126, 678);
            this.tpSupportTables.TabIndex = 3;
            this.tpSupportTables.Text = "Støtte tabeller";
            this.tpSupportTables.UseVisualStyleBackColor = true;
            // 
            // tpTools
            // 
            this.tpTools.Controls.Add(this.pImportFish);
            this.tpTools.Controls.Add(this.pImportImages);
            this.tpTools.Controls.Add(this.panel2);
            this.tpTools.Location = new System.Drawing.Point(4, 22);
            this.tpTools.Name = "tpTools";
            this.tpTools.Padding = new System.Windows.Forms.Padding(3);
            this.tpTools.Size = new System.Drawing.Size(1126, 678);
            this.tpTools.TabIndex = 2;
            this.tpTools.Text = "Værktøj";
            this.tpTools.UseVisualStyleBackColor = true;
            // 
            // pImportFish
            // 
            this.pImportFish.Controls.Add(this.lbAqualogFishDone);
            this.pImportFish.Controls.Add(this.progressBar2);
            this.pImportFish.Controls.Add(this.lblImportAqualogFish);
            this.pImportFish.Dock = System.Windows.Forms.DockStyle.Top;
            this.pImportFish.Location = new System.Drawing.Point(3, 153);
            this.pImportFish.Name = "pImportFish";
            this.pImportFish.Size = new System.Drawing.Size(1120, 182);
            this.pImportFish.TabIndex = 2;
            this.pImportFish.Visible = false;
            // 
            // lbAqualogFishDone
            // 
            this.lbAqualogFishDone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbAqualogFishDone.FormattingEnabled = true;
            this.lbAqualogFishDone.Location = new System.Drawing.Point(0, 23);
            this.lbAqualogFishDone.Name = "lbAqualogFishDone";
            this.lbAqualogFishDone.Size = new System.Drawing.Size(1120, 136);
            this.lbAqualogFishDone.TabIndex = 5;
            // 
            // progressBar2
            // 
            this.progressBar2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar2.Location = new System.Drawing.Point(0, 159);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(1120, 23);
            this.progressBar2.TabIndex = 4;
            // 
            // lblImportAqualogFish
            // 
            this.lblImportAqualogFish.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblImportAqualogFish.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImportAqualogFish.Location = new System.Drawing.Point(0, 0);
            this.lblImportAqualogFish.Name = "lblImportAqualogFish";
            this.lblImportAqualogFish.Size = new System.Drawing.Size(1120, 23);
            this.lblImportAqualogFish.TabIndex = 3;
            this.lblImportAqualogFish.Text = "Importerer Aqualog Fisk";
            this.lblImportAqualogFish.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblImportAqualogFish.Click += new System.EventHandler(this.lblImportAqualogFish_Click);
            // 
            // pImportImages
            // 
            this.pImportImages.Controls.Add(this.progressBar1);
            this.pImportImages.Controls.Add(this.lblPicImport);
            this.pImportImages.Dock = System.Windows.Forms.DockStyle.Top;
            this.pImportImages.Location = new System.Drawing.Point(3, 92);
            this.pImportImages.Name = "pImportImages";
            this.pImportImages.Size = new System.Drawing.Size(1120, 61);
            this.pImportImages.TabIndex = 1;
            this.pImportImages.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(0, 23);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1120, 38);
            this.progressBar1.Step = 1;
            this.progressBar1.TabIndex = 3;
            // 
            // lblPicImport
            // 
            this.lblPicImport.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPicImport.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPicImport.Location = new System.Drawing.Point(0, 0);
            this.lblPicImport.Name = "lblPicImport";
            this.lblPicImport.Size = new System.Drawing.Size(1120, 23);
            this.lblPicImport.TabIndex = 2;
            this.lblPicImport.Text = "Importerer billeder";
            this.lblPicImport.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.groupBox8);
            this.panel2.Controls.Add(this.groupBox7);
            this.panel2.Controls.Add(this.groupBox6);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1120, 89);
            this.panel2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPlantPDF);
            this.groupBox1.Controls.Add(this.btnAnimalPDF);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(484, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(121, 89);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PDF";
            // 
            // btnPlantPDF
            // 
            this.btnPlantPDF.Location = new System.Drawing.Point(6, 49);
            this.btnPlantPDF.Name = "btnPlantPDF";
            this.btnPlantPDF.Size = new System.Drawing.Size(105, 23);
            this.btnPlantPDF.TabIndex = 1;
            this.btnPlantPDF.Text = "Plante PDF";
            this.btnPlantPDF.UseVisualStyleBackColor = true;
            this.btnPlantPDF.Click += new System.EventHandler(this.btnPlantPDF_Click);
            // 
            // btnAnimalPDF
            // 
            this.btnAnimalPDF.Location = new System.Drawing.Point(6, 19);
            this.btnAnimalPDF.Name = "btnAnimalPDF";
            this.btnAnimalPDF.Size = new System.Drawing.Size(105, 23);
            this.btnAnimalPDF.TabIndex = 0;
            this.btnAnimalPDF.Text = "Dyre PDF";
            this.btnAnimalPDF.UseVisualStyleBackColor = true;
            this.btnAnimalPDF.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btnSupport);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox8.Location = new System.Drawing.Point(363, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(121, 89);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Støtte tabeller";
            // 
            // btnSupport
            // 
            this.btnSupport.Enabled = false;
            this.btnSupport.Location = new System.Drawing.Point(6, 19);
            this.btnSupport.Name = "btnSupport";
            this.btnSupport.Size = new System.Drawing.Size(105, 23);
            this.btnSupport.TabIndex = 0;
            this.btnSupport.Text = "Importer Støtte";
            this.btnSupport.UseVisualStyleBackColor = true;
            this.btnSupport.Click += new System.EventHandler(this.btnSupport_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnLoadImage);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox7.Location = new System.Drawing.Point(242, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(121, 89);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Billeder";
            // 
            // btnLoadImage
            // 
            this.btnLoadImage.Location = new System.Drawing.Point(6, 49);
            this.btnLoadImage.Name = "btnLoadImage";
            this.btnLoadImage.Size = new System.Drawing.Size(105, 23);
            this.btnLoadImage.TabIndex = 1;
            this.btnLoadImage.Text = "Indlæs billede";
            this.btnLoadImage.UseVisualStyleBackColor = true;
            this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Importer Billeder";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnFishbaseOrgPictures);
            this.groupBox6.Controls.Add(this.button6);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox6.Location = new System.Drawing.Point(121, 0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(121, 89);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Fishbase XML";
            // 
            // btnFishbaseOrgPictures
            // 
            this.btnFishbaseOrgPictures.Location = new System.Drawing.Point(6, 49);
            this.btnFishbaseOrgPictures.Name = "btnFishbaseOrgPictures";
            this.btnFishbaseOrgPictures.Size = new System.Drawing.Size(105, 23);
            this.btnFishbaseOrgPictures.TabIndex = 1;
            this.btnFishbaseOrgPictures.Text = "Importer Billeder";
            this.btnFishbaseOrgPictures.UseVisualStyleBackColor = true;
            this.btnFishbaseOrgPictures.Click += new System.EventHandler(this.btnFishbaseOrgPictures_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(6, 19);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(105, 23);
            this.button6.TabIndex = 0;
            this.button6.Text = "Importer Fisk";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnAqualogImportPlants);
            this.groupBox5.Controls.Add(this.btnAqualogImportFish);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(121, 89);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "AquaLog";
            // 
            // btnAqualogImportPlants
            // 
            this.btnAqualogImportPlants.Location = new System.Drawing.Point(7, 49);
            this.btnAqualogImportPlants.Name = "btnAqualogImportPlants";
            this.btnAqualogImportPlants.Size = new System.Drawing.Size(104, 23);
            this.btnAqualogImportPlants.TabIndex = 1;
            this.btnAqualogImportPlants.Text = "Importer Planter";
            this.btnAqualogImportPlants.UseVisualStyleBackColor = true;
            this.btnAqualogImportPlants.Click += new System.EventHandler(this.btnAqualogImportPlants_Click);
            // 
            // btnAqualogImportFish
            // 
            this.btnAqualogImportFish.Location = new System.Drawing.Point(6, 19);
            this.btnAqualogImportFish.Name = "btnAqualogImportFish";
            this.btnAqualogImportFish.Size = new System.Drawing.Size(105, 23);
            this.btnAqualogImportFish.TabIndex = 0;
            this.btnAqualogImportFish.Text = "Importer Fisk";
            this.btnAqualogImportFish.UseVisualStyleBackColor = true;
            this.btnAqualogImportFish.Click += new System.EventHandler(this.btnAqualogImportFish_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnImportHabitats);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(605, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 89);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Biotoper";
            // 
            // btnImportHabitats
            // 
            this.btnImportHabitats.Location = new System.Drawing.Point(6, 19);
            this.btnImportHabitats.Name = "btnImportHabitats";
            this.btnImportHabitats.Size = new System.Drawing.Size(105, 23);
            this.btnImportHabitats.TabIndex = 0;
            this.btnImportHabitats.Text = "Importer Biotoper";
            this.btnImportHabitats.UseVisualStyleBackColor = true;
            this.btnImportHabitats.Click += new System.EventHandler(this.btnImportHabitats_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1134, 750);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fishbase Tool";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tpBlogs.ResumeLayout(false);
            this.tpBlogs.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.pBlogTagList.ResumeLayout(false);
            this.pBlogTagList.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.tpImages.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tpTools.ResumeLayout(false);
            this.pImportFish.ResumeLayout(false);
            this.pImportImages.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslNumberAnimals;
        private System.Windows.Forms.ToolStripStatusLabel tsslNumberPlants;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpPlants;
        private System.Windows.Forms.TabPage tpTools;
        private System.Windows.Forms.TabPage tpSupportTables;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnAqualogImportPlants;
        private System.Windows.Forms.Button btnAqualogImportFish;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel pImportImages;
        private System.Windows.Forms.Label lblPicImport;
        private System.Windows.Forms.ToolStripStatusLabel tsslStatus;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel pImportFish;
        private System.Windows.Forms.Label lblImportAqualogFish;
        private System.Windows.Forms.ListBox lbAqualogFishDone;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnSupport;
        private System.Windows.Forms.Button btnLoadImage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabPage tpImages;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox lbPicture;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblPicDate;
        private System.Windows.Forms.Label lblPicCopy;
        private System.Windows.Forms.Label lblPicTitle;
        private System.Windows.Forms.Label lblPicId;
        private System.Windows.Forms.Button btnRefreshPics;
        private System.Windows.Forms.TabPage tpBlogs;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ListBox lbBlogEntrys;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel pBlogTagList;
        private System.Windows.Forms.CheckedListBox clbBlogTags;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton tsbBlogNewTag;
        private System.Windows.Forms.TabPage tpAnimal;
        private System.Windows.Forms.Button btnFishbaseOrgPictures;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPlantPDF;
        private System.Windows.Forms.Button btnAnimalPDF;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnImportHabitats;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

