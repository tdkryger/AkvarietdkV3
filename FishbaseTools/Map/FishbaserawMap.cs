using FishbaseTools.Domain;
using FluentNHibernate.Mapping;

namespace FishbaseTools
{


    public class FishbaserawMap : ClassMap<Fishbaseraw> {
		
		public FishbaserawMap() {
			Table("fishbaseraw");
			LazyLoad();
			Id(x => x.Id).GeneratedBy.Identity().Column("fishbaseId");
			Map(x => x.Genus).Column("genus").Not.Nullable();
			Map(x => x.Species).Column("species").Not.Nullable();
			Map(x => x.Xmlsummary).Column("xmlSummary");
			Map(x => x.Xmlpointdata).Column("xmlPointData");
			Map(x => x.Xmlcommonnames).Column("xmlCommonNames");
			Map(x => x.Xmlphotos).Column("xmlPhotos");
		}
	}
}
