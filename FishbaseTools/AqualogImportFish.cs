﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using ExcelDataReader;
using FishbaseTools.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace FishbaseTools
{
    class AqualogImportFish : IDisposable
    {
        #region Delegates and Events
        public delegate void OneDoneDelegate(string name);
        public delegate void ErrorDelegate(string text, Exception ex);
        public event OneDoneDelegate OnOneDone;
        public event ErrorDelegate OnError;
        #endregion

        #region Fields
        //private IExcelDataReader _excelReader;
        private int _counter;
        private List<AquaLogFish> _aqualogFish = new List<AquaLogFish>();
        #endregion

        #region Properties
        public int Count { get { return _counter; } }
        #endregion

        #region Constructor
        public AqualogImportFish(string filename)
        {
            FileStream stream = File.Open(filename, FileMode.Open, FileAccess.Read);

            using (IExcelDataReader _excelReader = ExcelReaderFactory.CreateBinaryReader(stream))
            {
                //_excelReader.IsFirstRowAsColumnNames = true;

                _counter = 0;
                _excelReader.Read(); // skip the first line with headers
                while (_excelReader.Read())
                {
                    AquaLogFish line = new AquaLogFish();
                    for (int i = 0; i < 26; i++)
                    {
                        line.Strings[i] = _excelReader.GetString(i);
                    }
                    _aqualogFish.Add(line);
                    _counter++;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _aqualogFish.Clear();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AqualogImportFish() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion

        #region Public methods
        public void Import()
        {
            FishbaseUnitOfWork uow = null;
            Stopwatch sw = new Stopwatch();
            foreach (AquaLogFish af in _aqualogFish)
            {
                sw.Start();
                if (uow != null)
                {
                    uow.Dispose();
                }
                uow = DBHelper.GetUnitOfWork();
                uow.Session.FlushMode = NHibernate.FlushMode.Auto;
                uow.BeginTransaction();
                string genus = af.GetString(2);// _excelReader.GetString(2);
                string species = af.GetString(3);
                ScientificName sciName = DBHelper.GetScientificName(uow, genus, species);
                string scientificName = sciName.ToString();
                string originString = "Findes allerede. Ikke tilføjet";

                //if (!DBHelper.CheckExisting(uow, sciName)) //GetAnimal(uow, sciName) == null)
                {
                    Animal animal = new Animal()
                    {
                        ScientificName = sciName
                    };

                    if (animal.Family == null)
                        animal.Family = DBHelper.GetFamily(uow, "Ukendt");
                    if (animal.Order == null)
                        animal.Order = DBHelper.GetOrder(uow, "Ukendt");
                    if (animal.SubOrder == null)
                        animal.SubOrder = new SubOrder() { Name = string.Empty };
                    if (animal.Temperament == null)
                        animal.Temperament = DBHelper.GetTemperament(uow, "Ukendt");

                    animal.CommonGroup = DBHelper.GetAnimalGroup_AqualogCode(uow, af.GetString(1));
                    //if (animal.CommonGroup == null)
                    //{
                    //    animal.CommonGroup = DBHelper.SaveAnimalGroup(uow, new AnimalGroup()
                    //    {
                    //        AqualogCode = _excelReader.GetString(1),
                    //        Name = _excelReader.GetString(1),
                    //        Zone = "Ukendt"
                    //    });
                    //}

                    if (!string.IsNullOrEmpty(af.GetString(11)))
                    {
                        string[] parts = af.GetString(11).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            Tradename tName = DBHelper.GetTradename(uow, part, DBHelper.GetLanguage(uow, "DE"));
                            if (!animal.Tradenames.Contains(tName))
                                animal.Tradenames.Add(tName);
                        }
                    }

                    if (!string.IsNullOrEmpty(af.GetString(12)))
                    {
                        string[] parts = af.GetString(12).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            Tradename tName = DBHelper.GetTradename(uow, part, DBHelper.GetLanguage(uow, "EN"));
                            if (!animal.Tradenames.Contains(tName))
                                animal.Tradenames.Add(tName);
                        }
                    }
                    if (!string.IsNullOrEmpty(af.GetString(13)))
                    {
                        // Sometimes it also uses ; as seperator
                        string[] synonymStrings = af.GetString(13).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string synonym in synonymStrings)
                        {
                            if (synonym != null)
                            {
                                string synonymCap = synonym.Replace("'", " ").Replace("*", " ").Trim();
                                if (!string.IsNullOrEmpty(synonymCap))
                                {

                                    if (synonymCap.StartsWith("?") || synonymCap.StartsWith("("))
                                        break;

                                    int idx = synonymCap.IndexOf(' ');
                                    if (idx > 0)
                                    {
                                        string subGenus = synonymCap.Substring(0, idx).Trim();
                                        subGenus = char.ToUpper(subGenus[0]) + subGenus.Substring(1);
                                        string subSpecies = synonymCap.Substring(idx).Trim();
                                        animal.Synonyms.Add(DBHelper.GetScientificName(uow, subGenus, subSpecies));
                                    }
                                }
                            }
                        }
                    }
                    animal.Origin = DBHelper.GetOrigin(uow, animal.CommonGroup.Zone, af.GetString(14));
                    decimal min = 0;
                    decimal max = 0;
                    if (!string.IsNullOrEmpty(af.GetString(15)))
                    {
                        string[] parts = af.GetString(15).Split(new Char[] { '-', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    animal.Size = DBHelper.GetSize(uow, min, max);
                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(af.GetString(16)))
                    {
                        switch (af.GetString(16))
                        {
                            case "c":
                                min = 10;
                                max = 22;
                                break;
                            case "cc":
                                min = 18;
                                max = 22;
                                break;
                            case "C":
                                min = 22;
                                max = 25;
                                break;
                            case "CC":
                                min = 24;
                                max = 29;
                                break;
                            default:
                                min = 0;
                                max = 99;
                                break;
                        }
                    }
                    animal.WaterTemperature = DBHelper.GetTemperature(uow, min, max);
                    bool addSalt = false;
                    if (!string.IsNullOrEmpty(af.GetString(17)))
                    {
                        switch (af.GetString(17))
                        {
                            case "p":
                                min = 5.8M;
                                max = 6.5M;
                                addSalt = false;
                                break;
                            case "pp":
                                min = 6.5M;
                                max = 7.2M;
                                addSalt = false;
                                break;
                            case "P":
                                min = 7.5M;
                                max = 8.5M;
                                addSalt = false;
                                break;
                            case "PP":
                                min = 0;
                                max = 0;
                                addSalt = true;
                                break;
                            default:
                                min = 0;
                                max = 0;
                                addSalt = false;
                                break;
                        }
                    }
                    animal.WaterPh = DBHelper.GetPH(uow, min, max, addSalt);

                    if (!string.IsNullOrEmpty(af.GetString(19)))
                    {
                        string[] diets = af.GetString(19).Split(',');
                        foreach (string diet in diets)
                        {
                            switch (diet)
                            {
                                case "A":
                                    animal.DietaryRequirements.Add(DBHelper.GetDiet(uow, "Altædende"));
                                    break;
                                case "B":
                                    animal.DietaryRequirements.Add(DBHelper.GetDiet(uow, "Levende og frossen fodder"));
                                    break;
                                case "R":
                                    animal.DietaryRequirements.Add(DBHelper.GetDiet(uow, "Rovdyr"));
                                    break;
                                case "G":
                                    animal.DietaryRequirements.Add(DBHelper.GetDiet(uow, "Planteæder"));
                                    break;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(af.GetString(20)))
                    {
                        bool top = false;
                        bool middle = false;
                        bool bottom = false;
                        string[] parts = af.GetString(20).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in parts)
                        {
                            switch (s)
                            {
                                case "m":
                                    middle = true;
                                    break;
                                case "o":
                                    top = true;
                                    break;
                                case "u":
                                    bottom = true;
                                    break;
                            }
                        }
                        animal.SwimLevel = Animal.AnimalSwimLevel.Unknown;
                        if (top == true && middle == false && bottom == false)
                            animal.SwimLevel = Animal.AnimalSwimLevel.Top;
                        if (top == false && middle == true && bottom == false)
                            animal.SwimLevel = Animal.AnimalSwimLevel.Middle;
                        if (top == true && middle == true && bottom == false)
                            animal.SwimLevel = Animal.AnimalSwimLevel.MiddleUpper;
                        if (top == false && middle == false && bottom == true)
                            animal.SwimLevel = Animal.AnimalSwimLevel.Bottom;
                        if (top == false && middle == true && bottom == true)
                            animal.SwimLevel = Animal.AnimalSwimLevel.LowerMiddle;
                        if (top == true && middle == true && bottom == true)
                            animal.SwimLevel = Animal.AnimalSwimLevel.All;
                    }
                    animal.PlantedTank = Animal.AnimalPlantedTank.Unknown;
                    if (!string.IsNullOrEmpty(af.GetString(21)))
                    {
                        string[] parts = af.GetString(20).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in parts)
                        {
                            if (s == "Z")
                                animal.PlantedTank = Animal.AnimalPlantedTank.Yes;
                            if (s == "O")
                                animal.PlantedTank = Animal.AnimalPlantedTank.No;
                        }
                    }

                    if (!string.IsNullOrEmpty(af.GetString(22)))
                    {
                        string[] parts = af.GetString(22).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string s in parts)
                        {
                            switch (s)
                            {
                                case "1":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Stimefisk"));
                                    break;
                                case "2":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Æglægger"));
                                    break;
                                case "3":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Levendefødende"));
                                    break;
                                case "4":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Mundruger"));
                                    break;
                                case "5":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Bobblerede"));
                                    break;
                                case "6":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Æg kræver specielle forhold"));
                                    break;
                                case "7":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Kun som par eller trio"));
                                    break;
                                case "8":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Algeæder"));
                                    break;
                                case "9":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Let at holde"));
                                    break;
                                case "10":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Svær at holde"));
                                    break;
                                case "11":
                                    animal.Sociableness.Add(DBHelper.GetSocial(uow, "Beskyttet under CITES"));
                                    break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(af.GetString(23)))
                    {
                        switch (af.GetString(23))
                        {
                            case "ss":
                                animal.MinTankSize = 30;
                                break;
                            case "s":
                                animal.MinTankSize = 96;
                                break;
                            case "m":
                                animal.MinTankSize = 128;
                                break;
                            case "L":
                                animal.MinTankSize = 250;
                                break;
                            case "XL":
                                animal.MinTankSize = 660;
                                break;
                            default:
                                animal.MinTankSize = 45;
                                break;
                        }
                    }
                    List<FishbaseImage> images = DBHelper.GetImages(uow, $"{animal.ScientificName.Genus} {animal.ScientificName.Species}");
                    Parallel.ForEach(images, (img) => { animal.Images.Add(img); });

                    try
                    {
                        uow.Session.SaveOrUpdate(animal);
                        //DBHelper.SaveAnimal(uow, animal);
                        uow.Commit();
                        //uow.Session.Flush();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        OnError?.Invoke("DBHelper.SaveAnimal call", ex);
                    }
                    originString = $"{animal.Origin.Text} ({animal.Origin.SubOrigin})";
                }
                OnOneDone?.Invoke($"{scientificName} - {originString} in {sw.Elapsed}");
                sw.Stop();
                //try
                //{
                //    Application.DoEvents();
                //}
                //catch
                //{
                //    Thread.Sleep(2);
                //}
            }

        }
        #endregion

        #region Private methods

        #endregion
    }
}

