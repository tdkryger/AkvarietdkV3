﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FishbaseTools.Domain
{
    public class PositionRaw
    {
        #region Properties
        public string Latititude { get; set; }
        public string Longitude { get; set; }
        public int Year { get; set; }
        public string NameUsed { get; set; }
        public string Depth { get; set; }
        public bool OK { get { return GetOk(); } }
        #endregion

        #region Constructor
        public PositionRaw(XmlNode node)
        {
            Latititude = TDK.Tools.XmlTools.GetElementValue(node, "lat");
            Longitude = TDK.Tools.XmlTools.GetElementValue(node, "long");
            if (int.TryParse(TDK.Tools.XmlTools.GetElementValue(node, "year"), out int year))
                Year = year;
            else
                year = 0;
            NameUsed = TDK.Tools.XmlTools.GetElementValue(node, "nameused");
            Depth = TDK.Tools.XmlTools.GetElementValue(node, "depth");
        }
        #endregion

        #region Private Methods
        private bool GetOk()
        {
            if (string.IsNullOrEmpty(Latititude) && string.IsNullOrEmpty(Longitude))
                return false;
            return true;
        }
        #endregion
    }
}
