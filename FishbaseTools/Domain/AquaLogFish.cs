﻿namespace FishbaseTools.Domain
{
    class AquaLogFish
    {
        #region Fields
        public string[] Strings { get; set; } = new string[30];
        #endregion

        public string GetString(int idx)
        {
            return Strings[idx];
        }
    }
}
