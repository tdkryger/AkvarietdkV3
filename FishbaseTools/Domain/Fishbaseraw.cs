using NHibernate.Validator.Constraints;


namespace FishbaseTools.Domain
{

    public partial class Fishbaseraw : Interfaces.IEntity
    {
        public virtual int Id { get; set; }
        [NotNullNotEmpty]
        public virtual string Genus { get; set; }
        [NotNullNotEmpty]
        public virtual string Species { get; set; }
        public virtual string Xmlsummary { get; set; }
        public virtual string Xmlpointdata { get; set; }
        public virtual string Xmlcommonnames { get; set; }
        public virtual string Xmlphotos { get; set; }
    }
}
