﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;

namespace FishbaseTools.Domain
{
    public partial class Fishbaseraw
    {
        //TODO: change DanishTradename to a list
        #region Fields
        private string _family = string.Empty;
        private string _order = string.Empty;
        private List<string> _danishTradename = new List<string>();
        private List<FishbaseImageRaw> _images = new List<FishbaseImageRaw>();
        private List<string> _synonyms = new List<string>();
        #endregion

        #region Properties
        public virtual List<string> Synonyms
        {
            get
            {
                if (_synonyms.Count == 0)
                {
                    GetSynonyms();
                }
                return _synonyms;
            }
        }

        public virtual string Family
        {
            get
            {
                if (string.IsNullOrEmpty(_family.Trim()))
                    _family = TDK.Tools.XmlTools.GetElementValue(Xmlsummary, "dwc:Family");
                return _family;
            }
        }

        public virtual string Order
        {
            get
            {
                if (string.IsNullOrEmpty(_order.Trim()))
                    _order = TDK.Tools.XmlTools.GetElementValue(Xmlsummary, "dwc:Order");
                return _order;
            }
        }

        public virtual List<string> DanishTradename
        {
            get
            {
                if (_danishTradename.Count == 0)
                    LoadDanishTradenames();
                return _danishTradename;
                // <commonName xml:lang='dan'>THE_TRADENAME</commonName>
            }
        }
        #endregion

        #region Public methods
        public virtual List<FishbaseImageRaw> GetImages()
        {
            if (_images.Count == 0)
                LoadTheImages();
            return _images;
        }

        public virtual List<PositionRaw> GetPointData()
        {
            List<PositionRaw> positions = new List<PositionRaw>();
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(this.Xmlpointdata);
                XmlNodeList xnl = xml.GetElementsByTagName("pointdata");
                foreach (XmlNode xn in xnl)
                {
                    PositionRaw pr = new PositionRaw(xn);
                    if (pr.OK)
                        positions.Add(pr);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetPointData error: {0}", ex.Message);
            }
            return positions;
        }
        #endregion

        #region Private methods
        private void GetSynonyms()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(this.Xmlsummary);
                XmlNodeList xnl = xml.GetElementsByTagName("synonym");
                foreach(XmlNode xn in xnl)
                {
                    if (TDK.Tools.XmlTools.GetElementValue(xn, "relationship").ToUpper() == "SYNONYM")
                    {
                        _synonyms.Add(xn.InnerText);
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("GetSynonyms error: {0}", ex.Message);
            }
        }

        private void LoadDanishTradenames()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(this.Xmlcommonnames);
                XmlNodeList xnl = xml.GetElementsByTagName("comnames");
                foreach (XmlNode xn in xnl)
                {
                    // if (TDK.Tools.XmlTools.GetAttributeValue(xn, "xml:lang").ToUpper() == "DAN")
                    if (TDK.Tools.XmlTools.GetElementValue(xn, "country").ToUpper() == "DENMARK")
                    {
                        string name = TDK.Tools.XmlTools.GetElementValue(xn, "comname")
                            .Replace("Ã¥", "å")
                            .Replace("Ã¸", "ø")
                            .Replace("Ã¦", "æ")
                            .Replace("Ã?", "Æ")
                            .Replace("Ã¸", "Ø");
                        if (!string.IsNullOrEmpty(name))
                            _danishTradename.Add(name);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("LoadDanishTradenames error: {0}", ex.Message);
            }

            //.Replace("Ã¥", "å").Replace("Ã¸", "ø").Replace("Ã¦", "æ").Replace("Ã?", "Æ").Replace("Ã¸", "Ø");
            //_danishTradename = TDK.Tools.XmlTools.GetElementValue(Xmlsummary, "commonName", "dan", "xml:lang");
        }

        private void LoadTheImages()
        {
            XmlDocument xml = new XmlDocument();
            try
            {
                xml.LoadXml(this.Xmlphotos);

                XmlNodeList xnl = xml.GetElementsByTagName("pictures");
                foreach (XmlNode xn in xnl)
                {
                    string url = TDK.Tools.XmlTools.GetElementValue(xn, "actual");
                    if (!string.IsNullOrEmpty(url))
                    {
                        FishbaseImageRaw fbi = new FishbaseImageRaw(url)
                        {
                            Type = TDK.Tools.XmlTools.GetElementValue(xn, "pictures", "type"),
                            Copyright = TDK.Tools.XmlTools.GetElementValue(xn, "author") + " - Fishbase.org"
                        };
                        _images.Add(fbi);
                    }

                }

            }
            catch
            {

            }
        }


        #endregion
    }
}
