﻿using System.Drawing;
using System.IO;
using System.Net;

namespace FishbaseTools.Domain
{
    public class FishbaseImageRaw
    {
        private int _maxWidth = Akvariet.Common.AkvarietConfiguration.MaxFishbasePictureWidth;
        private int _maxHeight = Akvariet.Common.AkvarietConfiguration.MaxFishbasePictureHeight;

        public string Type { get; set; }
        public Image Image { get; }
        public string Copyright { get; set; }




        public FishbaseImageRaw(string picUrl)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    using (Stream stream = client.OpenRead(picUrl))
                    {
                        Bitmap bitmap;
                        bitmap = new Bitmap(stream);

                        if (bitmap != null)
                        {
                            if (bitmap.Width > _maxWidth || bitmap.Height > _maxHeight)
                            {
                                Image = TDK.Tools.ImageTools.FixedSize(bitmap, _maxWidth, _maxHeight, Color.White);
                            }
                            else
                                Image = bitmap;
                        }

                        stream.Flush();
                        stream.Close();
                    }
                }
            }
            catch
            {
                Image = null;
            }
        }
    }
}
