﻿namespace FishbaseTools.Pages
{
    partial class UcAnimal
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.lbAnimals = new System.Windows.Forms.ListBox();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.tsbtnPrior = new System.Windows.Forms.ToolStripButton();
            this.tsbtnNext = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tdtbFilter = new System.Windows.Forms.ToolStripTextBox();
            this.pExpanders = new System.Windows.Forms.Panel();
            this.ecpSocials = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.clbSocial = new System.Windows.Forms.CheckedListBox();
            this.ecpSynonyms = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.lbSynonyms = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSynonymsAdd = new System.Windows.Forms.Button();
            this.btnSynonymsRemove = new System.Windows.Forms.Button();
            this.ecpHabitats = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.clbHabitats = new System.Windows.Forms.CheckedListBox();
            this.ecpHandelsnavne = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.lbTradenames = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTradenameAdd = new System.Windows.Forms.Button();
            this.btnTradenameRemove = new System.Windows.Forms.Button();
            this.ecpIllnesses = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.clbIllnesses = new System.Windows.Forms.CheckedListBox();
            this.ecpDiet = new MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel();
            this.clbDiets = new System.Windows.Forms.CheckedListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.chkBreed = new System.Windows.Forms.CheckBox();
            this.chkWood = new System.Windows.Forms.CheckBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboxFiltration = new System.Windows.Forms.ComboBox();
            this.cboxPlanted = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.nudTankSize = new System.Windows.Forms.NumericUpDown();
            this.nudMinGroupSize = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cboxTemperament = new System.Windows.Forms.ComboBox();
            this.cmsTemperament = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stmiTemperamentAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.label12 = new System.Windows.Forms.Label();
            this.cboxSwimLevel = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cboxBioload = new System.Windows.Forms.ComboBox();
            this.nudSizeMax = new System.Windows.Forms.NumericUpDown();
            this.nudSizeMin = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboxGroup = new System.Windows.Forms.ComboBox();
            this.cboxFamily = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpModified = new System.Windows.Forms.DateTimePicker();
            this.dtpCreated = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.gboxDescription = new System.Windows.Forms.GroupBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboxOrder = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboxOrigin = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cBoxGenus2 = new System.Windows.Forms.ComboBox();
            this.tbSpecies2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.tsbtnAnimalImages = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.stbtnNew = new System.Windows.Forms.ToolStripButton();
            this.tsbtnEdit = new System.Windows.Forms.ToolStripButton();
            this.tsbtnSave = new System.Windows.Forms.ToolStripButton();
            this.tsbtnCancel = new System.Windows.Forms.ToolStripButton();
            this.tsbrnExpandAll = new System.Windows.Forms.ToolStripButton();
            this.nudMinTemp = new System.Windows.Forms.NumericUpDown();
            this.nudMaxTemp = new System.Windows.Forms.NumericUpDown();
            this.nudHardnessMin = new System.Windows.Forms.NumericUpDown();
            this.nudHardnessMax = new System.Windows.Forms.NumericUpDown();
            this.nudPhMin = new System.Windows.Forms.NumericUpDown();
            this.nudPhMax = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.pExpanders.SuspendLayout();
            this.ecpSocials.SuspendLayout();
            this.ecpSynonyms.SuspendLayout();
            this.panel2.SuspendLayout();
            this.ecpHabitats.SuspendLayout();
            this.ecpHandelsnavne.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ecpIllnesses.SuspendLayout();
            this.ecpDiet.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTankSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinGroupSize)).BeginInit();
            this.cmsTemperament.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSizeMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSizeMin)).BeginInit();
            this.gboxDescription.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxTemp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHardnessMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHardnessMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhMax)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.lbAnimals);
            this.splitContainer4.Panel1.Controls.Add(this.toolStrip5);
            this.splitContainer4.Panel1MinSize = 100;
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.pExpanders);
            this.splitContainer4.Panel2.Controls.Add(this.panel5);
            this.splitContainer4.Panel2.Controls.Add(this.toolStrip6);
            this.splitContainer4.Size = new System.Drawing.Size(1100, 670);
            this.splitContainer4.SplitterDistance = 200;
            this.splitContainer4.TabIndex = 1;
            // 
            // lbAnimals
            // 
            this.lbAnimals.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbAnimals.FormattingEnabled = true;
            this.lbAnimals.Location = new System.Drawing.Point(0, 25);
            this.lbAnimals.Name = "lbAnimals";
            this.lbAnimals.Size = new System.Drawing.Size(200, 645);
            this.lbAnimals.TabIndex = 1;
            this.lbAnimals.SelectedIndexChanged += new System.EventHandler(this.lbAnimals_SelectedIndexChanged);
            // 
            // toolStrip5
            // 
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnPrior,
            this.tsbtnNext,
            this.toolStripSeparator1,
            this.tdtbFilter});
            this.toolStrip5.Location = new System.Drawing.Point(0, 0);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(200, 25);
            this.toolStrip5.TabIndex = 0;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // tsbtnPrior
            // 
            this.tsbtnPrior.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnPrior.Image = global::FishbaseTools.Properties.Resources.backward32;
            this.tsbtnPrior.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnPrior.Name = "tsbtnPrior";
            this.tsbtnPrior.Size = new System.Drawing.Size(23, 22);
            this.tsbtnPrior.Text = "Forrige";
            this.tsbtnPrior.Click += new System.EventHandler(this.tsbtnPrior_Click);
            // 
            // tsbtnNext
            // 
            this.tsbtnNext.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnNext.Image = global::FishbaseTools.Properties.Resources.forward32;
            this.tsbtnNext.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnNext.Name = "tsbtnNext";
            this.tsbtnNext.Size = new System.Drawing.Size(23, 22);
            this.tsbtnNext.Text = "Næste";
            this.tsbtnNext.Click += new System.EventHandler(this.tsbtnNext_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tdtbFilter
            // 
            this.tdtbFilter.Name = "tdtbFilter";
            this.tdtbFilter.Size = new System.Drawing.Size(100, 25);
            this.tdtbFilter.TextChanged += new System.EventHandler(this.tdtbFilter_TextChanged);
            // 
            // pExpanders
            // 
            this.pExpanders.AutoScroll = true;
            this.pExpanders.Controls.Add(this.ecpSocials);
            this.pExpanders.Controls.Add(this.ecpSynonyms);
            this.pExpanders.Controls.Add(this.ecpHabitats);
            this.pExpanders.Controls.Add(this.ecpHandelsnavne);
            this.pExpanders.Controls.Add(this.ecpIllnesses);
            this.pExpanders.Controls.Add(this.ecpDiet);
            this.pExpanders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pExpanders.Location = new System.Drawing.Point(596, 25);
            this.pExpanders.Name = "pExpanders";
            this.pExpanders.Size = new System.Drawing.Size(300, 645);
            this.pExpanders.TabIndex = 3;
            this.pExpanders.Resize += new System.EventHandler(this.pExpanders_Resize);
            // 
            // ecpSocials
            // 
            this.ecpSocials.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpSocials.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpSocials.Controls.Add(this.clbSocial);
            this.ecpSocials.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpSocials.ExpandedHeight = 307;
            this.ecpSocials.IsExpanded = true;
            this.ecpSocials.Location = new System.Drawing.Point(0, 1188);
            this.ecpSocials.Name = "ecpSocials";
            this.ecpSocials.Size = new System.Drawing.Size(283, 382);
            this.ecpSocials.TabIndex = 47;
            this.ecpSocials.Text = "Temperament";
            this.ecpSocials.UseAnimation = false;
            // 
            // clbSocial
            // 
            this.clbSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clbSocial.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clbSocial.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.clbSocial.FormattingEnabled = true;
            this.clbSocial.Location = new System.Drawing.Point(0, 42);
            this.clbSocial.Name = "clbSocial";
            this.clbSocial.Size = new System.Drawing.Size(283, 340);
            this.clbSocial.TabIndex = 3;
            // 
            // ecpSynonyms
            // 
            this.ecpSynonyms.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpSynonyms.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpSynonyms.Controls.Add(this.lbSynonyms);
            this.ecpSynonyms.Controls.Add(this.panel2);
            this.ecpSynonyms.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpSynonyms.ExpandedHeight = 227;
            this.ecpSynonyms.IsExpanded = true;
            this.ecpSynonyms.Location = new System.Drawing.Point(0, 961);
            this.ecpSynonyms.Name = "ecpSynonyms";
            this.ecpSynonyms.Size = new System.Drawing.Size(283, 227);
            this.ecpSynonyms.TabIndex = 46;
            this.ecpSynonyms.Text = "Synonymer";
            this.ecpSynonyms.UseAnimation = false;
            // 
            // lbSynonyms
            // 
            this.lbSynonyms.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbSynonyms.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbSynonyms.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbSynonyms.FormattingEnabled = true;
            this.lbSynonyms.Location = new System.Drawing.Point(0, 48);
            this.lbSynonyms.Name = "lbSynonyms";
            this.lbSynonyms.Size = new System.Drawing.Size(283, 147);
            this.lbSynonyms.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.panel2.Controls.Add(this.btnSynonymsAdd);
            this.panel2.Controls.Add(this.btnSynonymsRemove);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
            this.panel2.Location = new System.Drawing.Point(0, 195);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(283, 32);
            this.panel2.TabIndex = 1;
            // 
            // btnSynonymsAdd
            // 
            this.btnSynonymsAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSynonymsAdd.Location = new System.Drawing.Point(3, 6);
            this.btnSynonymsAdd.Name = "btnSynonymsAdd";
            this.btnSynonymsAdd.Size = new System.Drawing.Size(75, 23);
            this.btnSynonymsAdd.TabIndex = 1;
            this.btnSynonymsAdd.Text = "Tilføj";
            this.btnSynonymsAdd.UseVisualStyleBackColor = true;
            // 
            // btnSynonymsRemove
            // 
            this.btnSynonymsRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSynonymsRemove.Location = new System.Drawing.Point(205, 6);
            this.btnSynonymsRemove.Name = "btnSynonymsRemove";
            this.btnSynonymsRemove.Size = new System.Drawing.Size(75, 23);
            this.btnSynonymsRemove.TabIndex = 0;
            this.btnSynonymsRemove.Text = "Fjern";
            this.btnSynonymsRemove.UseVisualStyleBackColor = true;
            // 
            // ecpHabitats
            // 
            this.ecpHabitats.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpHabitats.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpHabitats.Controls.Add(this.clbHabitats);
            this.ecpHabitats.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpHabitats.ExpandedHeight = 225;
            this.ecpHabitats.IsExpanded = true;
            this.ecpHabitats.Location = new System.Drawing.Point(0, 667);
            this.ecpHabitats.Name = "ecpHabitats";
            this.ecpHabitats.Size = new System.Drawing.Size(283, 294);
            this.ecpHabitats.TabIndex = 45;
            this.ecpHabitats.Text = "Biotoper";
            this.ecpHabitats.UseAnimation = false;
            // 
            // clbHabitats
            // 
            this.clbHabitats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clbHabitats.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clbHabitats.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.clbHabitats.FormattingEnabled = true;
            this.clbHabitats.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.clbHabitats.Location = new System.Drawing.Point(0, 50);
            this.clbHabitats.Name = "clbHabitats";
            this.clbHabitats.Size = new System.Drawing.Size(283, 244);
            this.clbHabitats.TabIndex = 1;
            // 
            // ecpHandelsnavne
            // 
            this.ecpHandelsnavne.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpHandelsnavne.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpHandelsnavne.Controls.Add(this.lbTradenames);
            this.ecpHandelsnavne.Controls.Add(this.panel1);
            this.ecpHandelsnavne.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpHandelsnavne.ExpandedHeight = 162;
            this.ecpHandelsnavne.IsExpanded = true;
            this.ecpHandelsnavne.Location = new System.Drawing.Point(0, 505);
            this.ecpHandelsnavne.Name = "ecpHandelsnavne";
            this.ecpHandelsnavne.Size = new System.Drawing.Size(283, 162);
            this.ecpHandelsnavne.TabIndex = 44;
            this.ecpHandelsnavne.Text = "Handelsnavne";
            this.ecpHandelsnavne.UseAnimation = false;
            // 
            // lbTradenames
            // 
            this.lbTradenames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lbTradenames.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbTradenames.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbTradenames.FormattingEnabled = true;
            this.lbTradenames.Location = new System.Drawing.Point(0, 48);
            this.lbTradenames.Name = "lbTradenames";
            this.lbTradenames.Size = new System.Drawing.Size(283, 82);
            this.lbTradenames.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
            this.panel1.Controls.Add(this.btnTradenameAdd);
            this.panel1.Controls.Add(this.btnTradenameRemove);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(130)))), ((int)(((byte)(180)))));
            this.panel1.Location = new System.Drawing.Point(0, 130);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 32);
            this.panel1.TabIndex = 4;
            // 
            // btnTradenameAdd
            // 
            this.btnTradenameAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTradenameAdd.Location = new System.Drawing.Point(3, 6);
            this.btnTradenameAdd.Name = "btnTradenameAdd";
            this.btnTradenameAdd.Size = new System.Drawing.Size(75, 23);
            this.btnTradenameAdd.TabIndex = 1;
            this.btnTradenameAdd.Text = "Tilføj";
            this.btnTradenameAdd.UseVisualStyleBackColor = true;
            // 
            // btnTradenameRemove
            // 
            this.btnTradenameRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTradenameRemove.Location = new System.Drawing.Point(205, 6);
            this.btnTradenameRemove.Name = "btnTradenameRemove";
            this.btnTradenameRemove.Size = new System.Drawing.Size(75, 23);
            this.btnTradenameRemove.TabIndex = 0;
            this.btnTradenameRemove.Text = "Fjern";
            this.btnTradenameRemove.UseVisualStyleBackColor = true;
            // 
            // ecpIllnesses
            // 
            this.ecpIllnesses.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpIllnesses.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpIllnesses.Controls.Add(this.clbIllnesses);
            this.ecpIllnesses.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpIllnesses.ExpandedHeight = 272;
            this.ecpIllnesses.IsExpanded = true;
            this.ecpIllnesses.Location = new System.Drawing.Point(0, 145);
            this.ecpIllnesses.Name = "ecpIllnesses";
            this.ecpIllnesses.Size = new System.Drawing.Size(283, 360);
            this.ecpIllnesses.TabIndex = 43;
            this.ecpIllnesses.Text = "Sygdomme";
            this.ecpIllnesses.UseAnimation = false;
            // 
            // clbIllnesses
            // 
            this.clbIllnesses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clbIllnesses.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clbIllnesses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.clbIllnesses.FormattingEnabled = true;
            this.clbIllnesses.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.clbIllnesses.Location = new System.Drawing.Point(0, 52);
            this.clbIllnesses.Name = "clbIllnesses";
            this.clbIllnesses.Size = new System.Drawing.Size(283, 308);
            this.clbIllnesses.TabIndex = 2;
            // 
            // ecpDiet
            // 
            this.ecpDiet.ButtonSize = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonSize.Normal;
            this.ecpDiet.ButtonStyle = MakarovDev.ExpandCollapsePanel.ExpandCollapseButton.ExpandButtonStyle.MagicArrow;
            this.ecpDiet.Controls.Add(this.clbDiets);
            this.ecpDiet.Dock = System.Windows.Forms.DockStyle.Top;
            this.ecpDiet.ExpandedHeight = 263;
            this.ecpDiet.IsExpanded = true;
            this.ecpDiet.Location = new System.Drawing.Point(0, 0);
            this.ecpDiet.Name = "ecpDiet";
            this.ecpDiet.Size = new System.Drawing.Size(283, 145);
            this.ecpDiet.TabIndex = 42;
            this.ecpDiet.Text = "Føde";
            this.ecpDiet.UseAnimation = false;
            // 
            // clbDiets
            // 
            this.clbDiets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.clbDiets.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.clbDiets.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.clbDiets.FormattingEnabled = true;
            this.clbDiets.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.clbDiets.Location = new System.Drawing.Point(0, 45);
            this.clbDiets.Name = "clbDiets";
            this.clbDiets.Size = new System.Drawing.Size(283, 100);
            this.clbDiets.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.nudPhMax);
            this.panel5.Controls.Add(this.nudPhMin);
            this.panel5.Controls.Add(this.nudHardnessMax);
            this.panel5.Controls.Add(this.nudHardnessMin);
            this.panel5.Controls.Add(this.nudMaxTemp);
            this.panel5.Controls.Add(this.nudMinTemp);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.chkBreed);
            this.panel5.Controls.Add(this.chkWood);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.cboxFiltration);
            this.panel5.Controls.Add(this.cboxPlanted);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.nudTankSize);
            this.panel5.Controls.Add(this.nudMinGroupSize);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.cboxTemperament);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.cboxSwimLevel);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.cboxBioload);
            this.panel5.Controls.Add(this.nudSizeMax);
            this.panel5.Controls.Add(this.nudSizeMin);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.cboxGroup);
            this.panel5.Controls.Add(this.cboxFamily);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.dtpModified);
            this.panel5.Controls.Add(this.dtpCreated);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.gboxDescription);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.cboxOrder);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.cboxOrigin);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.cBoxGenus2);
            this.panel5.Controls.Add(this.tbSpecies2);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 25);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(596, 645);
            this.panel5.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(362, 307);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Oprættet i DK";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(10, 307);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 52;
            this.label19.Text = "Kræver rødder";
            // 
            // chkBreed
            // 
            this.chkBreed.AutoSize = true;
            this.chkBreed.Location = new System.Drawing.Point(468, 306);
            this.chkBreed.Name = "chkBreed";
            this.chkBreed.Size = new System.Drawing.Size(15, 14);
            this.chkBreed.TabIndex = 24;
            this.chkBreed.UseVisualStyleBackColor = true;
            // 
            // chkWood
            // 
            this.chkWood.AutoSize = true;
            this.chkWood.Location = new System.Drawing.Point(130, 306);
            this.chkWood.Name = "chkWood";
            this.chkWood.Size = new System.Drawing.Size(15, 14);
            this.chkWood.TabIndex = 23;
            this.chkWood.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(362, 281);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 49;
            this.label17.Text = "Min. Filtrering";
            // 
            // cboxFiltration
            // 
            this.cboxFiltration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxFiltration.FormattingEnabled = true;
            this.cboxFiltration.Location = new System.Drawing.Point(468, 278);
            this.cboxFiltration.Name = "cboxFiltration";
            this.cboxFiltration.Size = new System.Drawing.Size(121, 21);
            this.cboxFiltration.TabIndex = 22;
            // 
            // cboxPlanted
            // 
            this.cboxPlanted.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxPlanted.FormattingEnabled = true;
            this.cboxPlanted.Location = new System.Drawing.Point(130, 278);
            this.cboxPlanted.Name = "cboxPlanted";
            this.cboxPlanted.Size = new System.Drawing.Size(121, 21);
            this.cboxPlanted.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 281);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 13);
            this.label16.TabIndex = 46;
            this.label16.Text = "Planteakvarie";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(362, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Min. L";
            // 
            // nudTankSize
            // 
            this.nudTankSize.Location = new System.Drawing.Point(468, 252);
            this.nudTankSize.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudTankSize.Name = "nudTankSize";
            this.nudTankSize.Size = new System.Drawing.Size(65, 20);
            this.nudTankSize.TabIndex = 20;
            // 
            // nudMinGroupSize
            // 
            this.nudMinGroupSize.Location = new System.Drawing.Point(130, 252);
            this.nudMinGroupSize.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudMinGroupSize.Name = "nudMinGroupSize";
            this.nudMinGroupSize.Size = new System.Drawing.Size(65, 20);
            this.nudMinGroupSize.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 254);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 42;
            this.label14.Text = "Min. gruppe";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(362, 231);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Temperament";
            this.label13.Visible = false;
            // 
            // cboxTemperament
            // 
            this.cboxTemperament.ContextMenuStrip = this.cmsTemperament;
            this.cboxTemperament.FormattingEnabled = true;
            this.cboxTemperament.Location = new System.Drawing.Point(468, 228);
            this.cboxTemperament.Name = "cboxTemperament";
            this.cboxTemperament.Size = new System.Drawing.Size(121, 21);
            this.cboxTemperament.TabIndex = 18;
            this.cboxTemperament.Visible = false;
            this.cboxTemperament.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // cmsTemperament
            // 
            this.cmsTemperament.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stmiTemperamentAdd});
            this.cmsTemperament.Name = "cmsTemperament";
            this.cmsTemperament.Size = new System.Drawing.Size(102, 26);
            // 
            // stmiTemperamentAdd
            // 
            this.stmiTemperamentAdd.Name = "stmiTemperamentAdd";
            this.stmiTemperamentAdd.Size = new System.Drawing.Size(101, 22);
            this.stmiTemperamentAdd.Text = "Tilføj";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 228);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 13);
            this.label12.TabIndex = 39;
            this.label12.Text = "Vandlag";
            // 
            // cboxSwimLevel
            // 
            this.cboxSwimLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSwimLevel.FormattingEnabled = true;
            this.cboxSwimLevel.Location = new System.Drawing.Point(130, 225);
            this.cboxSwimLevel.Name = "cboxSwimLevel";
            this.cboxSwimLevel.Size = new System.Drawing.Size(150, 21);
            this.cboxSwimLevel.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(362, 203);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "\"Bioload\"";
            // 
            // cboxBioload
            // 
            this.cboxBioload.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxBioload.FormattingEnabled = true;
            this.cboxBioload.Location = new System.Drawing.Point(468, 198);
            this.cboxBioload.Name = "cboxBioload";
            this.cboxBioload.Size = new System.Drawing.Size(121, 21);
            this.cboxBioload.TabIndex = 16;
            // 
            // nudSizeMax
            // 
            this.nudSizeMax.Location = new System.Drawing.Point(215, 198);
            this.nudSizeMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudSizeMax.Name = "nudSizeMax";
            this.nudSizeMax.Size = new System.Drawing.Size(65, 20);
            this.nudSizeMax.TabIndex = 15;
            // 
            // nudSizeMin
            // 
            this.nudSizeMin.Location = new System.Drawing.Point(130, 198);
            this.nudSizeMin.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudSizeMin.Name = "nudSizeMin";
            this.nudSizeMin.Size = new System.Drawing.Size(65, 20);
            this.nudSizeMin.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Størrelse";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(42, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Gruppe";
            // 
            // cboxGroup
            // 
            this.cboxGroup.FormattingEnabled = true;
            this.cboxGroup.Location = new System.Drawing.Point(130, 115);
            this.cboxGroup.Name = "cboxGroup";
            this.cboxGroup.Size = new System.Drawing.Size(459, 21);
            this.cboxGroup.TabIndex = 5;
            this.cboxGroup.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // cboxFamily
            // 
            this.cboxFamily.FormattingEnabled = true;
            this.cboxFamily.Location = new System.Drawing.Point(130, 86);
            this.cboxFamily.Name = "cboxFamily";
            this.cboxFamily.Size = new System.Drawing.Size(459, 21);
            this.cboxFamily.TabIndex = 4;
            this.cboxFamily.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Familie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(430, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Ændret";
            // 
            // dtpModified
            // 
            this.dtpModified.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpModified.Location = new System.Drawing.Point(484, 172);
            this.dtpModified.Name = "dtpModified";
            this.dtpModified.Size = new System.Drawing.Size(105, 20);
            this.dtpModified.TabIndex = 13;
            // 
            // dtpCreated
            // 
            this.dtpCreated.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCreated.Location = new System.Drawing.Point(130, 172);
            this.dtpCreated.Name = "dtpCreated";
            this.dtpCreated.Size = new System.Drawing.Size(105, 20);
            this.dtpCreated.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Oprettet";
            // 
            // gboxDescription
            // 
            this.gboxDescription.Controls.Add(this.tbDescription);
            this.gboxDescription.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gboxDescription.Location = new System.Drawing.Point(0, 511);
            this.gboxDescription.Name = "gboxDescription";
            this.gboxDescription.Size = new System.Drawing.Size(596, 134);
            this.gboxDescription.TabIndex = 24;
            this.gboxDescription.TabStop = false;
            this.gboxDescription.Text = "Beskrivelse";
            // 
            // tbDescription
            // 
            this.tbDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbDescription.Location = new System.Drawing.Point(3, 16);
            this.tbDescription.Multiline = true;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(590, 115);
            this.tbDescription.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(430, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "pH";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Hårdhed";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Temperatur";
            // 
            // cboxOrder
            // 
            this.cboxOrder.FormattingEnabled = true;
            this.cboxOrder.Location = new System.Drawing.Point(130, 58);
            this.cboxOrder.Name = "cboxOrder";
            this.cboxOrder.Size = new System.Drawing.Size(460, 21);
            this.cboxOrder.TabIndex = 3;
            this.cboxOrder.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Orden";
            // 
            // cboxOrigin
            // 
            this.cboxOrigin.FormattingEnabled = true;
            this.cboxOrigin.Location = new System.Drawing.Point(130, 30);
            this.cboxOrigin.Name = "cboxOrigin";
            this.cboxOrigin.Size = new System.Drawing.Size(460, 21);
            this.cboxOrigin.TabIndex = 2;
            this.cboxOrigin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Levested";
            // 
            // cBoxGenus2
            // 
            this.cBoxGenus2.FormattingEnabled = true;
            this.cBoxGenus2.Location = new System.Drawing.Point(130, 3);
            this.cBoxGenus2.Name = "cBoxGenus2";
            this.cBoxGenus2.Size = new System.Drawing.Size(154, 21);
            this.cBoxGenus2.TabIndex = 0;
            this.cBoxGenus2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ComboBox_KeyUp);
            // 
            // tbSpecies2
            // 
            this.tbSpecies2.Location = new System.Drawing.Point(304, 3);
            this.tbSpecies2.Name = "tbSpecies2";
            this.tbSpecies2.Size = new System.Drawing.Size(286, 20);
            this.tbSpecies2.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(10, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "Videnskabeligt navn";
            // 
            // toolStrip6
            // 
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbtnAnimalImages,
            this.toolStripSeparator2,
            this.stbtnNew,
            this.tsbtnEdit,
            this.tsbtnSave,
            this.tsbtnCancel,
            this.tsbrnExpandAll});
            this.toolStrip6.Location = new System.Drawing.Point(0, 0);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(896, 25);
            this.toolStrip6.TabIndex = 0;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // tsbtnAnimalImages
            // 
            this.tsbtnAnimalImages.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnAnimalImages.Image = global::FishbaseTools.Properties.Resources.documentsorcopy24;
            this.tsbtnAnimalImages.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnAnimalImages.Name = "tsbtnAnimalImages";
            this.tsbtnAnimalImages.Size = new System.Drawing.Size(23, 22);
            this.tsbtnAnimalImages.Text = "Billeder";
            this.tsbtnAnimalImages.Click += new System.EventHandler(this.tsbtnAnimalImages_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // stbtnNew
            // 
            this.stbtnNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stbtnNew.Image = global::FishbaseTools.Properties.Resources.add24;
            this.stbtnNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stbtnNew.Name = "stbtnNew";
            this.stbtnNew.Size = new System.Drawing.Size(23, 22);
            this.stbtnNew.Text = "Ny";
            this.stbtnNew.Click += new System.EventHandler(this.stbtnNew_Click);
            // 
            // tsbtnEdit
            // 
            this.tsbtnEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnEdit.Image = global::FishbaseTools.Properties.Resources.edit24;
            this.tsbtnEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnEdit.Name = "tsbtnEdit";
            this.tsbtnEdit.Size = new System.Drawing.Size(23, 22);
            this.tsbtnEdit.Text = "Rediger";
            this.tsbtnEdit.Click += new System.EventHandler(this.tsbtnEdit_Click);
            // 
            // tsbtnSave
            // 
            this.tsbtnSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnSave.Enabled = false;
            this.tsbtnSave.Image = global::FishbaseTools.Properties.Resources.accept24;
            this.tsbtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnSave.Name = "tsbtnSave";
            this.tsbtnSave.Size = new System.Drawing.Size(23, 22);
            this.tsbtnSave.Text = "Godkend";
            this.tsbtnSave.Click += new System.EventHandler(this.tsbtnSave_Click);
            // 
            // tsbtnCancel
            // 
            this.tsbtnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbtnCancel.Enabled = false;
            this.tsbtnCancel.Image = global::FishbaseTools.Properties.Resources.undo24;
            this.tsbtnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnCancel.Name = "tsbtnCancel";
            this.tsbtnCancel.Size = new System.Drawing.Size(23, 22);
            this.tsbtnCancel.Text = "Fortryd";
            this.tsbtnCancel.Click += new System.EventHandler(this.tsbtnCancel_Click);
            // 
            // tsbrnExpandAll
            // 
            this.tsbrnExpandAll.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbrnExpandAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbrnExpandAll.Image = global::FishbaseTools.Properties.Resources.Upload;
            this.tsbrnExpandAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbrnExpandAll.Name = "tsbrnExpandAll";
            this.tsbrnExpandAll.Size = new System.Drawing.Size(23, 22);
            this.tsbrnExpandAll.Text = "Udvid alle";
            this.tsbrnExpandAll.Click += new System.EventHandler(this.tsbrnExpandAll_Click);
            // 
            // nudMinTemp
            // 
            this.nudMinTemp.DecimalPlaces = 1;
            this.nudMinTemp.Location = new System.Drawing.Point(130, 143);
            this.nudMinTemp.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudMinTemp.Name = "nudMinTemp";
            this.nudMinTemp.Size = new System.Drawing.Size(44, 20);
            this.nudMinTemp.TabIndex = 6;
            this.nudMinTemp.Value = new decimal(new int[] {
            259,
            0,
            0,
            65536});
            // 
            // nudMaxTemp
            // 
            this.nudMaxTemp.DecimalPlaces = 1;
            this.nudMaxTemp.Location = new System.Drawing.Point(180, 143);
            this.nudMaxTemp.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudMaxTemp.Name = "nudMaxTemp";
            this.nudMaxTemp.Size = new System.Drawing.Size(44, 20);
            this.nudMaxTemp.TabIndex = 7;
            // 
            // nudHardnessMin
            // 
            this.nudHardnessMin.DecimalPlaces = 1;
            this.nudHardnessMin.Location = new System.Drawing.Point(310, 143);
            this.nudHardnessMin.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudHardnessMin.Name = "nudHardnessMin";
            this.nudHardnessMin.Size = new System.Drawing.Size(44, 20);
            this.nudHardnessMin.TabIndex = 8;
            // 
            // nudHardnessMax
            // 
            this.nudHardnessMax.DecimalPlaces = 1;
            this.nudHardnessMax.Location = new System.Drawing.Point(360, 143);
            this.nudHardnessMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudHardnessMax.Name = "nudHardnessMax";
            this.nudHardnessMax.Size = new System.Drawing.Size(44, 20);
            this.nudHardnessMax.TabIndex = 9;
            // 
            // nudPhMin
            // 
            this.nudPhMin.DecimalPlaces = 1;
            this.nudPhMin.Location = new System.Drawing.Point(468, 143);
            this.nudPhMin.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPhMin.Name = "nudPhMin";
            this.nudPhMin.Size = new System.Drawing.Size(44, 20);
            this.nudPhMin.TabIndex = 10;
            // 
            // nudPhMax
            // 
            this.nudPhMax.DecimalPlaces = 1;
            this.nudPhMax.Location = new System.Drawing.Point(518, 143);
            this.nudPhMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPhMax.Name = "nudPhMax";
            this.nudPhMax.Size = new System.Drawing.Size(44, 20);
            this.nudPhMax.TabIndex = 11;
            // 
            // UcAnimal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer4);
            this.Name = "UcAnimal";
            this.Size = new System.Drawing.Size(1100, 670);
            this.Enter += new System.EventHandler(this.UcAnimal_Enter);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.pExpanders.ResumeLayout(false);
            this.ecpSocials.ResumeLayout(false);
            this.ecpSocials.PerformLayout();
            this.ecpSynonyms.ResumeLayout(false);
            this.ecpSynonyms.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ecpHabitats.ResumeLayout(false);
            this.ecpHabitats.PerformLayout();
            this.ecpHandelsnavne.ResumeLayout(false);
            this.ecpHandelsnavne.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ecpIllnesses.ResumeLayout(false);
            this.ecpIllnesses.PerformLayout();
            this.ecpDiet.ResumeLayout(false);
            this.ecpDiet.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTankSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinGroupSize)).EndInit();
            this.cmsTemperament.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudSizeMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSizeMin)).EndInit();
            this.gboxDescription.ResumeLayout(false);
            this.gboxDescription.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxTemp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHardnessMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHardnessMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPhMax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ListBox lbAnimals;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cBoxGenus2;
        private System.Windows.Forms.TextBox tbSpecies2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton tsbtnPrior;
        private System.Windows.Forms.ToolStripButton tsbtnNext;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ComboBox cboxOrigin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxOrder;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox gboxDescription;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.ToolStripButton tsbtnAnimalImages;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpModified;
        private System.Windows.Forms.DateTimePicker dtpCreated;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton stbtnNew;
        private System.Windows.Forms.ToolStripButton tsbtnEdit;
        private System.Windows.Forms.ToolStripButton tsbtnSave;
        private System.Windows.Forms.ToolStripButton tsbtnCancel;
        private System.Windows.Forms.ComboBox cboxFamily;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboxGroup;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudSizeMax;
        private System.Windows.Forms.NumericUpDown nudSizeMin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboxBioload;
        private System.Windows.Forms.ComboBox cboxSwimLevel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolStripButton tsbrnExpandAll;
        private System.Windows.Forms.ComboBox cboxTemperament;
        private System.Windows.Forms.ContextMenuStrip cmsTemperament;
        private System.Windows.Forms.ToolStripMenuItem stmiTemperamentAdd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel pExpanders;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpSocials;
        private System.Windows.Forms.CheckedListBox clbSocial;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpSynonyms;
        private System.Windows.Forms.ListBox lbSynonyms;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSynonymsAdd;
        private System.Windows.Forms.Button btnSynonymsRemove;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpHabitats;
        private System.Windows.Forms.CheckedListBox clbHabitats;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpHandelsnavne;
        private System.Windows.Forms.ListBox lbTradenames;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnTradenameAdd;
        private System.Windows.Forms.Button btnTradenameRemove;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpIllnesses;
        private System.Windows.Forms.CheckedListBox clbIllnesses;
        private MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecpDiet;
        private System.Windows.Forms.CheckedListBox clbDiets;
        private System.Windows.Forms.NumericUpDown nudMinGroupSize;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudTankSize;
        private System.Windows.Forms.ComboBox cboxPlanted;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboxFiltration;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox chkBreed;
        private System.Windows.Forms.CheckBox chkWood;
        private System.Windows.Forms.ToolStripTextBox tdtbFilter;
        private System.Windows.Forms.NumericUpDown nudMinTemp;
        private System.Windows.Forms.NumericUpDown nudPhMax;
        private System.Windows.Forms.NumericUpDown nudPhMin;
        private System.Windows.Forms.NumericUpDown nudHardnessMax;
        private System.Windows.Forms.NumericUpDown nudHardnessMin;
        private System.Windows.Forms.NumericUpDown nudMaxTemp;
    }
}
