﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using FishbaseTools.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FishbaseTools.Pages
{
    public partial class UcAnimal : UserControl
    {
        #region Delegates & Events
        public delegate void LoadingTableDelegate(string tableName, int current, int maximum);
        public event LoadingTableDelegate OnLoadingTable;
        #endregion

        #region Fields
        private Animal _selectedAnimal;
        private FishbaseUnitOfWork _animalUnitOfWork;
        private bool _allExpanded = false;
        #endregion

        #region Properties

        #endregion

        public UcAnimal()
        {
            InitializeComponent();
            _animalUnitOfWork = new FishbaseUnitOfWork();
            stmiTemperamentAdd.Click += StmiTemperamentAdd_Click;
            tsbrnExpandAll_Click(this, new EventArgs());
            UpdateButtons();
        }


        #region Public methods
        public void UpdateAllLists()
        {
            UpdateHabitats();
            UpdateGenusBox();
            UpdateOriginBox();
            UpdateOrderBox();
            UpdateFamilieBox();
            UpdateAnimalGroupBox();
            UpdateTemperamentBox();
            UpdateImageList();
            UpdateDietList();
            UpdateAnimalList();
            UpdateIllnessList();
            UpdateSocialList();

            cboxBioload.Items.Clear();
            cboxBioload.Items.Add("Ukendt");
            cboxBioload.Items.Add("Lavt");
            cboxBioload.Items.Add("Middel");
            cboxBioload.Items.Add("Højt");
            cboxSwimLevel.Items.Clear();
            cboxSwimLevel.Items.Add("Ukendt");
            cboxSwimLevel.Items.Add("Bund");
            cboxSwimLevel.Items.Add("Nedre vandlag");
            cboxSwimLevel.Items.Add("Midt og nedre vandlag");
            cboxSwimLevel.Items.Add("Midterste vandlag");
            cboxSwimLevel.Items.Add("Midt og øvre vandlag");
            cboxSwimLevel.Items.Add("Øvre vandlag");
            cboxSwimLevel.Items.Add("Øverste vandlag");
            cboxSwimLevel.Items.Add("Alle vandlag");

            cboxPlanted.Items.Clear();
            cboxPlanted.Items.Add("Ukendt");
            cboxPlanted.Items.Add("Ja");
            cboxPlanted.Items.Add("Delvist");
            cboxPlanted.Items.Add("Nej");

            cboxFiltration.Items.Clear();
            cboxFiltration.Items.Add("Ukendt");
            cboxFiltration.Items.Add("Kraftig");
            cboxFiltration.Items.Add("Lavt");
            cboxFiltration.Items.Add("Middel");
            cboxFiltration.Items.Add("Ingen");

        }

        public void Init()
        {
            ClearAnimalForm();
            SetAnimalEnabled(false);
        }
        #endregion


        #region Private methods
        private void CollapsControl(Control sender, int height = 20)
        {
            foreach (Control c in sender.Controls)
            {
                c.Visible = false;
            }
            sender.Height = height;
        }

        private void ExpandControl(Control sender, int height)
        {
            foreach (Control c in sender.Controls)
            {
                c.Visible = true;
            }
            sender.Height = height;
        }

        private void UpdateButtons()
        {
            lbAnimals.Enabled = tsbtnEdit.Enabled;
        }

        private void UpdateSocialList()
        {
            OnLoadingTable?.Invoke("Temperament", 0, 0);
            clbSocial.Items.Clear();
            using (Repository<Social> repo = new Repository<Social>(_animalUnitOfWork))
            {
                IQueryable<Social> list = repo.GetAll();
                int count = 0;
                foreach (Social h in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Temperament", count, list.Count());
                    clbSocial.Items.Add(h);
                }
            }
        }

        private void UpdateDietList()
        {
            OnLoadingTable?.Invoke("Fødetyper", 0, 0);
            clbDiets.Items.Clear();
            using (Repository<Diet> habitats = new Repository<Diet>(_animalUnitOfWork))
            {
                IQueryable<Diet> list = habitats.GetAll();
                int count = 0;
                foreach (Diet h in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Fødetyper", count, list.Count());
                    clbDiets.Items.Add(h);
                }
            }
        }

        private void UpdateIllnessList()
        {
            OnLoadingTable?.Invoke("Sygdomme", 0, 0);
            clbIllnesses.Items.Clear();
            using (Repository<Illness> repo = new Repository<Illness>(_animalUnitOfWork))
            {
                IQueryable<Illness> list = repo.GetAll();
                int count = 0;
                foreach (Illness h in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Sygdomme", count, list.Count());
                    clbIllnesses.Items.Add(h);
                }
            }
        }

        private void UpdateGenusBox()
        {
            OnLoadingTable?.Invoke("Videnskabelige navne", 0, 0);
            List<ScientificName> sciNames;
            using (ScientificNameRepository repository = new ScientificNameRepository(_animalUnitOfWork))
            {
                sciNames = repository.GetAll().ToList();
            }
            sciNames.OrderBy(z => z.Genus).ThenBy(z => z.Species);
            List<string> allGenus = new List<string>();
            int count = 0;
            foreach (ScientificName scientificName in sciNames)
            {
                count++;
                OnLoadingTable?.Invoke("Videnskabelige navne", count, (sciNames.Count * 2));

                allGenus.Add(scientificName.Genus);
            }
            List<string> distinctGenus = allGenus.Distinct().ToList();
            distinctGenus.Sort();
            cBoxGenus2.Items.Clear();

            foreach (string s in distinctGenus)
            {
                count++;
                OnLoadingTable?.Invoke("Videnskabelige navne", count, (sciNames.Count * 2));
                if (s != null)
                {
                    if (!string.IsNullOrEmpty(s.Trim()))
                    {
                        cBoxGenus2.Items.Add(s);
                        TreeNode tnGenus = new TreeNode()
                        {
                            Text = s
                        };
                    }
                }
            }
        }

        private void UpdateOriginBox()
        {
            OnLoadingTable?.Invoke("Levesteder", 0, 0);
            cboxOrigin.Items.Clear();

            using (Repository<Origin> repo = new Repository<Origin>(_animalUnitOfWork))
            {
                IQueryable<Origin> list = repo.GetAll();
                int count = 0;
                foreach (Origin item in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Levesteder", count, list.Count());
                    cboxOrigin.Items.Add(item);
                }
            }
        }

        private void UpdateOrderBox()
        {
            OnLoadingTable?.Invoke("Ordner", 0, 0);
            cboxOrder.Items.Clear();

            using (Repository<Order> repo = new Repository<Order>(_animalUnitOfWork))
            {
                IQueryable<Order> list = repo.GetAll();
                int count = 0;
                foreach (Order item in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Ordner", count, list.Count());
                    cboxOrder.Items.Add(item);
                }
            }
        }

        private void UpdateFamilieBox()
        {
            OnLoadingTable?.Invoke("Familier", 0, 0);
            cboxFamily.Items.Clear();

            using (Repository<Family> repo = new Repository<Family>(_animalUnitOfWork))
            {
                IQueryable<Family> list = repo.GetAll();
                int count = 0;
                foreach (Family item in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Familier", count, list.Count());
                    cboxFamily.Items.Add(item);
                }
            }

        }

        private void UpdateAnimalGroupBox()
        {
            OnLoadingTable?.Invoke("Dyregrupper", 0, 0);
            cboxGroup.Items.Clear();

            using (Repository<AnimalGroup> repo = new Repository<AnimalGroup>(_animalUnitOfWork))
            {
                IQueryable<AnimalGroup> list = repo.GetAll();
                int count = 0;
                foreach (AnimalGroup item in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Dyregrupper", count, list.Count());
                    cboxFamily.Items.Add(item);
                }
            }
        }

        private void UpdateTemperamentBox()
        {
            OnLoadingTable?.Invoke("Temperamenter", 0, 0);
            cboxTemperament.Items.Clear();

            using (Repository<Temperament> repo = new Repository<Temperament>(_animalUnitOfWork))
            {
                IQueryable<Temperament> list = repo.GetAll();
                int count = 0;
                foreach (Temperament item in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Temperamenter", count, list.Count());
                    cboxTemperament.Items.Add(item);
                }
            }
        }

        private void UpdateImageList() { }

        private void UpdateHabitats()
        {
            OnLoadingTable?.Invoke("Biotoper", 0, 0);
            clbHabitats.Items.Clear();
            using (Repository<Habitat> habitats = new Repository<Habitat>(_animalUnitOfWork))
            {
                IQueryable<Habitat> list = habitats.GetAll();
                int count = 0;
                foreach (Habitat h in list)
                {
                    count++;
                    OnLoadingTable?.Invoke("Biotoper", count, list.Count());
                    clbHabitats.Items.Add(h);
                }
            }
        }

        private void UpdateAnimalList()
        {
            OnLoadingTable?.Invoke("Dyr", 0, 0);
            lbAnimals.Items.Clear();
            IList<LastModifiedFishbaseItem> ids = new List<LastModifiedFishbaseItem>();
            ids = _animalUnitOfWork.Session.CreateSQLQuery("SELECT Id, ModifiedDate, scientificNameText FROM animal ORDER BY scientificNameText;")
                            .AddEntity(typeof(LastModifiedFishbaseItem))
                            .List<LastModifiedFishbaseItem>();
            int count = 0;
            foreach (LastModifiedFishbaseItem item in ids)
            {
                count++;
                OnLoadingTable?.Invoke("Dyr", count, ids.Count);
                lbAnimals.Items.Add(item);
            }
            lbAnimals.SelectedIndex = 0;
            lbAnimals.SelectedItem = lbAnimals.Items[lbAnimals.SelectedIndex];
            _selectedAnimal = SelectAnimal((LastModifiedFishbaseItem)lbAnimals.SelectedItem);
            UpdateAnimalInfo();
        }

        private Animal SelectAnimal(LastModifiedFishbaseItem item)
        {
            return DBHelper.GetAnimal(_animalUnitOfWork, item.Id);
        }

        private void ComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (sender is ComboBox)
            {
                ComboBox comboBox = (ComboBox)sender;
                int index;
                string actual;
                string found;

                // Do nothing for certain keys, such as navigation keys.
                if ((e.KeyCode == Keys.Back) ||
                (e.KeyCode == Keys.Left) ||
                (e.KeyCode == Keys.Right) ||
                (e.KeyCode == Keys.Up) ||
                (e.KeyCode == Keys.Down) ||
                (e.KeyCode == Keys.Delete) ||
                (e.KeyCode == Keys.PageUp) ||
                (e.KeyCode == Keys.PageDown) ||
                (e.KeyCode == Keys.Home) ||
                (e.KeyCode == Keys.End))
                {
                    return;
                }

                // Store the actual text that has been typed.
                actual = comboBox.Text;

                // Find the first match for the typed value.
                index = comboBox.FindString(actual);

                // Get the text of the first match.
                if (index > -1)
                {
                    found = comboBox.Items[index].ToString();

                    // Select this item from the list.
                    comboBox.SelectedIndex = index;

                    // Select the portion of the text that was automatically
                    // added so that additional typing replaces it.
                    comboBox.SelectionStart = actual.Length;
                    comboBox.SelectionLength = found.Length;
                }
            }
        }

        private void ClearAnimalForm()
        {
            foreach (int i in clbHabitats.CheckedIndices)
            {
                clbHabitats.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in clbDiets.CheckedIndices)
            {
                clbDiets.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in clbIllnesses.CheckedIndices)
            {
                clbIllnesses.SetItemCheckState(i, CheckState.Unchecked);
            }
            foreach (int i in clbSocial.CheckedIndices)
            {
                clbSocial.SetItemCheckState(i, CheckState.Unchecked);
            }

            lbTradenames.Items.Clear();
            lbSynonyms.Items.Clear();
            cBoxGenus2.SelectedIndex = -1;
            cboxOrigin.SelectedIndex = -1;
            tbSpecies2.Text = string.Empty;
            cboxOrder.SelectedIndex = -1;
            nudMinTemp.Value = 0;
            nudMaxTemp.Value = 0;
            nudHardnessMin.Value = 0;
            nudHardnessMax.Value = 0;
            nudPhMin.Value = 0;
            nudPhMax.Value = 0;
            tbDescription.Text = string.Empty;
            dtpCreated.Value = DateTime.Now;
            dtpModified.Value = DateTime.Now;
            cboxFamily.SelectedIndex = -1;
            cboxGroup.SelectedIndex = -1;
            nudSizeMax.Value = 0;
            nudSizeMin.Value = 0;
            cboxBioload.Text = "Ukendt";
            cboxSwimLevel.Text = "Ukendt";
            cboxTemperament.SelectedIndex = -1;
            nudMinGroupSize.Value = 1;
            nudTankSize.Value = 54;
            cboxPlanted.SelectedIndex = -1;
            cboxFiltration.SelectedIndex = -1;
            chkBreed.Checked = false;
            chkWood.Checked = false;
        }

        private void SetAnimalEnabled(bool value)
        {
            clbHabitats.Enabled = value;
            btnTradenameAdd.Enabled = value;
            btnTradenameRemove.Enabled = value;
            btnSynonymsAdd.Enabled = value;
            btnSynonymsRemove.Enabled = value;
            cboxOrigin.Enabled = value;
            cBoxGenus2.Enabled = value;
            tbSpecies2.Enabled = value;
            cboxOrder.Enabled = value;
            nudMinTemp.Enabled = value;
            nudMaxTemp.Enabled = value;
            nudHardnessMin.Enabled = value;
            nudHardnessMax.Enabled = value;
            nudPhMin.Enabled = value;
            nudPhMax.Enabled = value;
            tbDescription.Enabled = value;
            //dtpModified.Enabled = value;
            //dtpCreated.Enabled = value;
            cboxFamily.Enabled = value;
            cboxGroup.Enabled = value;
            nudSizeMax.Enabled = value;
            nudSizeMin.Enabled = value;
            cboxBioload.Enabled = value;
            cboxSwimLevel.Enabled = value;
            clbDiets.Enabled = value;
            clbIllnesses.Enabled = value;
            cboxTemperament.Enabled = value;
            clbSocial.Enabled = value;
            nudMinGroupSize.Enabled = value;
            nudTankSize.Enabled = value;
            cboxPlanted.Enabled = value;
            cboxFiltration.Enabled = value;
            chkWood.Enabled = value;
            chkBreed.Enabled = value;
        }

        private void UpdateAnimalInfo()
        {
            ClearAnimalForm();
            SetAnimalEnabled(false);
            if (_selectedAnimal != null)
            {
                tsbtnAnimalImages.Enabled = _selectedAnimal.Images.Count > 0;
                cBoxGenus2.Text = _selectedAnimal.ScientificName.Genus;
                tbSpecies2.Text = _selectedAnimal.ScientificName.Species;
                foreach (Tradename tn in _selectedAnimal.Tradenames)
                {
                    lbTradenames.Items.Add(tn);
                }

                foreach (ScientificName synonym in _selectedAnimal.Synonyms)
                {
                    lbSynonyms.Items.Add(synonym);
                }

                foreach (Habitat h in _selectedAnimal.PreferedHabitat)
                {
                    for (int i = 0; i < clbHabitats.Items.Count; i++)
                    {
                        Habitat lbHabitat = (Habitat)clbHabitats.Items[i];
                        if (lbHabitat.Id == h.Id)
                            clbHabitats.SetItemCheckState(i, CheckState.Checked);
                    }
                }

                foreach (Diet d in _selectedAnimal.DietaryRequirements)
                {
                    for (int i = 0; i < clbDiets.Items.Count; i++)
                    {
                        Diet lbHabitat = (Diet)clbDiets.Items[i];
                        if (lbHabitat.Id == d.Id)
                            clbDiets.SetItemCheckState(i, CheckState.Checked);
                    }
                }
                foreach (Illness item in _selectedAnimal.CommonIllnesses)
                {
                    for (int i = 0; i < clbIllnesses.Items.Count; i++)
                    {
                        Illness lbItem = (Illness)clbDiets.Items[i];
                        if (lbItem.Id == item.Id)
                            clbIllnesses.SetItemCheckState(i, CheckState.Checked);
                    }
                }
                foreach (Social item in _selectedAnimal.Sociableness)
                {
                    for (int i = 0; i < clbSocial.Items.Count; i++)
                    {
                        Social lbItem = (Social)clbSocial.Items[i];
                        if (lbItem.Id == item.Id)
                            clbSocial.SetItemCheckState(i, CheckState.Checked);
                    }
                }

                cboxOrigin.Text = _selectedAnimal.Origin.ToString();
                cboxOrder.Text = _selectedAnimal.Order.ToString();
                nudMinTemp.Value = _selectedAnimal.WaterTemperature.Min;
                nudMaxTemp.Value = _selectedAnimal.WaterTemperature.Max;
                nudHardnessMin.Value = _selectedAnimal.WaterHardness.Min;
                nudHardnessMax.Value = _selectedAnimal.WaterHardness.Max;
                nudPhMin.Value = _selectedAnimal.WaterPh.Min;
                nudPhMax.Value = _selectedAnimal.WaterPh.Max;
                tbDescription.Text = _selectedAnimal.Description;
                dtpCreated.Value = _selectedAnimal.CreateDate;
                dtpModified.Value = _selectedAnimal.ModifiedDate;
                cboxFamily.Text = _selectedAnimal.Family.ToString();
                cboxGroup.Text = _selectedAnimal.CommonGroup.ToString();
                nudSizeMin.Value = _selectedAnimal.Size.Min;
                nudSizeMax.Value = _selectedAnimal.Size.Max;
                switch (_selectedAnimal.Bioload)
                {
                    case Animal.AnimalBioload.Heavy:
                        cboxBioload.Text = "Højt";
                        break;
                    case Animal.AnimalBioload.Low:
                        cboxBioload.Text = "Lavt";
                        break;
                    case Animal.AnimalBioload.Medium:
                        cboxBioload.Text = "Middel";
                        break;
                    default:
                        cboxBioload.Text = "Ukendt";
                        break;

                }
                switch (_selectedAnimal.SwimLevel)
                {
                    default:
                        cboxSwimLevel.Text = "Ukendt";
                        break;
                    case Animal.AnimalSwimLevel.MiddleUpper:
                        cboxSwimLevel.Text = "Midt og øvre vandlag";
                        break;
                    case Animal.AnimalSwimLevel.Top:
                        cboxSwimLevel.Text = "Øverste vandlag";
                        break;
                    case Animal.AnimalSwimLevel.Upper:
                        cboxSwimLevel.Text = "Øvre vandlag";
                        break;
                    case Animal.AnimalSwimLevel.All:
                        cboxSwimLevel.Text = "Alle vandlag";
                        break;
                    case Animal.AnimalSwimLevel.Bottom:
                        cboxSwimLevel.Text = "Alle Bund";
                        break;
                    case Animal.AnimalSwimLevel.Lower:
                        cboxSwimLevel.Text = "Nedre vandlag";
                        break;
                    case Animal.AnimalSwimLevel.LowerMiddle:
                        cboxSwimLevel.Text = "Midt og nedre vandlag";
                        break;
                    case Animal.AnimalSwimLevel.Middle:
                        cboxSwimLevel.Text = "Midterste vandlag";
                        break;
                }
                cboxTemperament.Text = _selectedAnimal.Temperament.ToString();
                nudMinGroupSize.Value = _selectedAnimal.MingroupSize;
                nudTankSize.Value = _selectedAnimal.MinTankSize;
                switch (_selectedAnimal.PlantedTank)
                {
                    default:
                        cboxPlanted.Text = "Ukendt";
                        break;
                    case Animal.AnimalPlantedTank.No:
                        cboxPlanted.Text = "Nej";
                        break;
                    case Animal.AnimalPlantedTank.Partial:
                        cboxPlanted.Text = "Delvist";
                        break;
                    case Animal.AnimalPlantedTank.Yes:
                        cboxPlanted.Text = "Ja";
                        break;

                }
                switch (_selectedAnimal.MinFiltrationLevel)
                {
                    default:
                        cboxFiltration.Text = "Ukendt";
                        break;
                    case Animal.AnimalFiltration.Heavy:
                        cboxFiltration.Text = "Kraftig";
                        break;
                    case Animal.AnimalFiltration.Low:
                        cboxFiltration.Text = "Lavt";
                        break;
                    case Animal.AnimalFiltration.Medium:
                        cboxFiltration.Text = "Middel";
                        break;
                    case Animal.AnimalFiltration.None:
                        cboxFiltration.Text = "Ingen";
                        break;
                }
                chkBreed.Checked = _selectedAnimal.BreadInCaptivity;
                chkWood.Checked = _selectedAnimal.RequiresWood;
                //TODO: Fill Form
            }
        }

        private void SaveAnimalInfo()
        {
            //TODO: Should check if there are actual changes..
            _selectedAnimal.ModifiedDate = DateTime.Now;
            if (!string.IsNullOrEmpty(tbSpecies2.Text))
                _selectedAnimal.ScientificName.Species = tbSpecies2.Text;
            if (cboxOrigin.SelectedItem is Origin)
            {
                _selectedAnimal.Origin = (Origin)cboxOrigin.SelectedItem;
            }
            if (cboxOrder.SelectedItem is Order)
            {
                _selectedAnimal.Order = (Order)cboxOrder.SelectedItem;
            }
            if (cboxFamily.SelectedItem is Family)
            {
                _selectedAnimal.Family = (Family)cboxFamily.SelectedItem;
            }
            if (cboxGroup.SelectedItem is AnimalGroup)
            {
                _selectedAnimal.CommonGroup = (AnimalGroup)cboxGroup.SelectedItem;
            }
            _selectedAnimal.WaterTemperature = DBLookUp.GetTemperature(_animalUnitOfWork, nudMinTemp.Value, nudMaxTemp.Value);
            _selectedAnimal.WaterHardness = DBLookUp.GetHardness(_animalUnitOfWork, nudHardnessMin.Value, nudHardnessMax.Value);
            _selectedAnimal.WaterPh = DBLookUp.GetPh(_animalUnitOfWork, nudPhMin.Value, nudPhMax.Value);
            _selectedAnimal.Size = DBLookUp.GetSize(_animalUnitOfWork, nudSizeMin.Value, nudSizeMax.Value);
            switch (cboxBioload.Text.ToUpper())
            {
                default:
                    _selectedAnimal.Bioload = Animal.AnimalBioload.Unknown;
                    break;
                case "LAVT":
                    _selectedAnimal.Bioload = Animal.AnimalBioload.Low;
                    break;
                case "MIDDEL":
                    _selectedAnimal.Bioload = Animal.AnimalBioload.Medium;
                    break;
                case "HØJT":
                    _selectedAnimal.Bioload = Animal.AnimalBioload.Heavy;
                    break;
            }
            switch (cboxSwimLevel.Text.ToUpper())
            {
                default:
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Unknown;
                    break;
                case "BUND":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Bottom;
                    break;
                case "NEDRE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Lower;
                    break;
                case "MIDT OG NEDRE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.LowerMiddle;
                    break;
                case "MIDTERSTE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Middle;
                    break;
                case "MIDT OG ØVRE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.MiddleUpper;
                    break;
                case "ØVRE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Upper;
                    break;
                case "ØVERSTE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.Top;
                    break;
                case "ALLE VANDLAG":
                    _selectedAnimal.SwimLevel = Animal.AnimalSwimLevel.All;
                    break;
            }
            if (cboxTemperament.SelectedItem is Temperament)
                _selectedAnimal.Temperament = (Temperament)cboxTemperament.SelectedItem;
            _selectedAnimal.MingroupSize = (int)nudMinGroupSize.Value;
            _selectedAnimal.MinTankSize = nudMinGroupSize.Value;
            switch (cboxPlanted.Text.ToUpper())
            {
                default:
                    _selectedAnimal.PlantedTank = Animal.AnimalPlantedTank.Unknown;
                    break;
                case "UKENDT":
                    _selectedAnimal.PlantedTank = Animal.AnimalPlantedTank.Unknown;
                    break;
                case "JA":
                    _selectedAnimal.PlantedTank = Animal.AnimalPlantedTank.Yes;
                    break;
                case "DELVIST":
                    _selectedAnimal.PlantedTank = Animal.AnimalPlantedTank.Partial;
                    break;
                case "NEJ":
                    _selectedAnimal.PlantedTank = Animal.AnimalPlantedTank.No;
                    break;
            }
            switch (cboxFiltration.Text.ToUpper())
            {
                default:
                    _selectedAnimal.MinFiltrationLevel = Animal.AnimalFiltration.Unknown;
                    break;
                case "KRAFTIG":
                    _selectedAnimal.MinFiltrationLevel = Animal.AnimalFiltration.Heavy;
                    break;
                case "LAVT":
                    _selectedAnimal.MinFiltrationLevel = Animal.AnimalFiltration.Low;
                    break;
                case "MIDDEL":
                    _selectedAnimal.MinFiltrationLevel = Animal.AnimalFiltration.Medium;
                    break;
                case "INGEN":
                    _selectedAnimal.MinFiltrationLevel = Animal.AnimalFiltration.None;
                    break;
            }
            _selectedAnimal.RequiresWood = chkWood.Checked;
            _selectedAnimal.BreadInCaptivity = chkBreed.Checked;
            //TODO: All the lists

            try
            {
                _animalUnitOfWork.BeginTransaction();
                _animalUnitOfWork.Session.Update(_selectedAnimal);
                _animalUnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                _animalUnitOfWork.Rollback();
                MessageBox.Show($"Fejl ved opdatering af dyr:\n {ex.Message}", "Fejl", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MoveToPrior()
        {
            if (lbAnimals.SelectedIndex > 0)
                lbAnimals.SelectedIndex--;
            lbAnimals.SelectedItem = lbAnimals.Items[lbAnimals.SelectedIndex];
            _selectedAnimal = SelectAnimal((LastModifiedFishbaseItem)lbAnimals.SelectedItem);
            UpdateAnimalInfo();
        }

        private void MoveToNext()
        {
            if (lbAnimals.SelectedIndex < lbAnimals.Items.Count)
                lbAnimals.SelectedIndex++;
            lbAnimals.SelectedItem = lbAnimals.Items[lbAnimals.SelectedIndex];
            _selectedAnimal = SelectAnimal((LastModifiedFishbaseItem)lbAnimals.SelectedItem);
            UpdateAnimalInfo();
        }

        #endregion

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case (Keys.Control | Keys.PageUp):
                    MoveToPrior();
                    break;
                case (Keys.Control | Keys.PageDown):
                    MoveToNext();
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void lbAnimals_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbAnimals.SelectedItem is LastModifiedFishbaseItem)
            {
                _selectedAnimal = SelectAnimal((LastModifiedFishbaseItem)lbAnimals.SelectedItem);
            }
            else
            {
                _selectedAnimal = null;
            }
            UpdateAnimalInfo();
        }

        private void tsbtnPrior_Click(object sender, EventArgs e)
        {
            MoveToPrior();
        }

        private void tsbtnNext_Click(object sender, EventArgs e)
        {
            MoveToNext();
        }

        private void UcAnimal_Enter(object sender, EventArgs e)
        {
            lbAnimals.Focus();
        }

        private void tsbtnAnimalImages_Click(object sender, EventArgs e)
        {
            using (frmImages frmImg = new frmImages())
            {
                frmImg.Init(_selectedAnimal.Images);
                frmImg.ShowDialog();
            }
        }

        private void tsbrnExpandAll_Click(object sender, EventArgs e)
        {
            _allExpanded = !_allExpanded;
            if (_allExpanded)
            {
                tsbrnExpandAll.ToolTipText = "Formindsk alle";
                Image bmp = Properties.Resources.Upload;
                tsbrnExpandAll.Image = bmp;
            }
            else
            {
                tsbrnExpandAll.ToolTipText = "Udvid alle";
                Image bmp = Properties.Resources.Upload;
                bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                tsbrnExpandAll.Image = bmp;
            }
            foreach (Control c in pExpanders.Controls)
            {
                if (c is MakarovDev.ExpandCollapsePanel.ExpandCollapsePanel ecp)
                {
                    ecp.IsExpanded = _allExpanded;
                }
            }
        }

        private void StmiTemperamentAdd_Click(object sender, EventArgs e)
        {
            using (Dialogs.frmSingleLineDialog form = new Dialogs.frmSingleLineDialog())
            {
                form.Title = "Temperament";
                form.Label = form.Title;
                form.Value = string.Empty;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        Temperament temp = new Temperament()
                        {
                            Text = form.Value
                        };
                        _animalUnitOfWork.BeginTransaction();
                        _animalUnitOfWork.Session.Save(temp);
                        _animalUnitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        _animalUnitOfWork.Rollback();
                        MessageBox.Show("Der skete en fejl: " + ex.Message, "Der skete en fejl", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    UpdateTemperamentBox();
                }
            }
        }

        private void pExpanders_Resize(object sender, EventArgs e)
        {
            //TODO: handle flow better. Would be nice if, when maximized, that to or more expanders is side by side
        }

        private void stbtnNew_Click(object sender, EventArgs e)
        {
            stbtnNew.Enabled = false;
            tsbtnEdit.Enabled = false;
            tsbtnSave.Enabled = true;
            tsbtnCancel.Enabled = true;
            UpdateButtons();
            ClearAnimalForm();
            SetAnimalEnabled(true);
        }

        private void tsbtnEdit_Click(object sender, EventArgs e)
        {
            stbtnNew.Enabled = false;
            tsbtnEdit.Enabled = false;
            tsbtnSave.Enabled = true;
            tsbtnCancel.Enabled = true;
            UpdateButtons();
            SetAnimalEnabled(true);
        }

        private void tsbtnCancel_Click(object sender, EventArgs e)
        {
            stbtnNew.Enabled = true;
            tsbtnEdit.Enabled = true;
            tsbtnSave.Enabled = false;
            tsbtnCancel.Enabled = false;
            UpdateButtons();
            lbAnimals_SelectedIndexChanged(sender, e);
            SetAnimalEnabled(false);
        }

        private void tsbtnSave_Click(object sender, EventArgs e)
        {
            stbtnNew.Enabled = true;
            tsbtnEdit.Enabled = true;
            tsbtnSave.Enabled = false;
            tsbtnCancel.Enabled = false;
            UpdateButtons();
            SaveAnimalInfo();
            lbAnimals_SelectedIndexChanged(sender, e);
            SetAnimalEnabled(false);
        }

        private void tdtbFilter_TextChanged(object sender, EventArgs e)
        {
            //
        }
    }
}
