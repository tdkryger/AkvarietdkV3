﻿namespace FishbaseTools.Pages
{
    partial class UcSupportTables
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpOrigin = new System.Windows.Forms.TabPage();
            this.tpAnimalGroup = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpOrigin);
            this.tabControl1.Controls.Add(this.tpAnimalGroup);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1112, 546);
            this.tabControl1.TabIndex = 0;
            // 
            // tpOrigin
            // 
            this.tpOrigin.Location = new System.Drawing.Point(4, 22);
            this.tpOrigin.Name = "tpOrigin";
            this.tpOrigin.Padding = new System.Windows.Forms.Padding(3);
            this.tpOrigin.Size = new System.Drawing.Size(1104, 520);
            this.tpOrigin.TabIndex = 0;
            this.tpOrigin.Text = "Levested";
            this.tpOrigin.UseVisualStyleBackColor = true;
            // 
            // tpAnimalGroup
            // 
            this.tpAnimalGroup.Location = new System.Drawing.Point(4, 22);
            this.tpAnimalGroup.Name = "tpAnimalGroup";
            this.tpAnimalGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tpAnimalGroup.Size = new System.Drawing.Size(1104, 520);
            this.tpAnimalGroup.TabIndex = 1;
            this.tpAnimalGroup.Text = "Dyregruppe";
            this.tpAnimalGroup.UseVisualStyleBackColor = true;
            // 
            // UcSupportTables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "UcSupportTables";
            this.Size = new System.Drawing.Size(1112, 546);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpOrigin;
        private System.Windows.Forms.TabPage tpAnimalGroup;
    }
}
