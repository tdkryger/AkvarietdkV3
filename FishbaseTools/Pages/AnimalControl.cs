﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Akvariet.Fishbase.Model;
using WebAkvarietdk.Models;
using Akvariet.Fishbase.Database;

namespace FishbaseTools.Pages
{
    public partial class AnimalControl : UserControl
    {
        private List<LastModifiedFishbaseItem> _animals;
        private int _index = 0;
        private FishbaseUnitOfWork _uow = new FishbaseUnitOfWork();


        public AnimalControl()
        {
            InitializeComponent();
        }

        public void Init()
        {
            _animals = DTOHelper.GetAnimalIds();
        }

        private void ShowTheAnimal()
        {
            Animal theAnimal = null;
            using (Akvariet.Fishbase.Repositories.Repository<Animal> repo = new Akvariet.Fishbase.Repositories.Repository<Animal>(_uow))
                theAnimal = repo.GetById(_animals[_index].Id);
            if (theAnimal == null)
                return;
            lblSciName.Text = theAnimal.ScientificName.ToString();
        }

    }
}
