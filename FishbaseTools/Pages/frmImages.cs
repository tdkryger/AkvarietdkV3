﻿using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FishbaseTools.Pages
{
    public partial class frmImages : Form
    {
        #region Fields
        private List<FishbaseImage> _images = new List<FishbaseImage>();
        private int _index = 0;
        #endregion

        public frmImages()
        {
            InitializeComponent();
        }

        #region Public methods
        public void Init(IList<FishbaseImage> images)
        {
            _images = images.ToList();
            ShowImage();
            UpdateControls();
        }
        #endregion

        #region Private methods
        private void UpdateControls()
        {
            btnPrior.Enabled = (_index > 0);
            btnNext.Enabled = (_index < (_images.Count - 1));
        }
        
        private void ShowImage()
        {
            pictureBox1.Image = _images[_index].Image;
            lblTitle.Text = _images[_index].Title;
            lblContent.Text = _images[_index].Content;
            lblCopy.Text = _images[_index].CopyRight;
            lblDate.Text = _images[_index].CreateDate.ToString("d/M yyyy");
            lblDescription.Text = _images[_index].Description;
        }
        #endregion

        private void btnPrior_Click(object sender, EventArgs e)
        {
            if (_index > 0)
                _index--;
            ShowImage();
            UpdateControls();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (_index < (_images.Count - 1))
                _index++;
            ShowImage();
            UpdateControls();
        }
    }
}
