﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FishbaseTools
{
    class AqualogPlantImport : IDisposable
    {

        #region Delegates and Events
        public delegate void OneDoneDelegate(string name);
        public delegate void ErrorDelegate(string text, Exception ex);
        public event OneDoneDelegate OnOneDone;
        public event ErrorDelegate OnError;
        #endregion

        #region Fields
        private IExcelDataReader _excelReader;
        private int _counter;
        #endregion

        #region Properties
        public int Count { get { return _counter; } }
        #endregion

        #region Constructor
        public AqualogPlantImport(string filename)
        {
            FileStream stream = File.Open(filename, FileMode.Open, FileAccess.Read);

            FileInfo fInfo = new FileInfo(filename);
            IExcelDataReader rowCounter;
            if (fInfo.Extension == ".xlsx")
                rowCounter = ExcelReaderFactory.CreateOpenXmlReader(stream);
            else
                rowCounter = ExcelReaderFactory.CreateBinaryReader(stream);

            //rowCounter.IsFirstRowAsColumnNames = true;

            _counter = 0;
            
            while (rowCounter.Read())
                _counter++;
            rowCounter.Close();
            stream.Close();

            stream = File.Open(filename, FileMode.Open, FileAccess.Read);

            fInfo = new FileInfo(filename);
            if (fInfo.Extension == ".xlsx")
                _excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            else
                _excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            //_excelReader.IsFirstRowAsColumnNames = true;
        }
        #endregion

        #region Public methods
        public void Import()
        {
            FishbaseUnitOfWork uow = null;
            _excelReader.Read();
            int row = 0;
            // TODO: Read the data into an array, and loop in parallel?
            while (_excelReader.Read())
            {
                if (uow != null)
                {
                    uow.Dispose();
                }
                uow = DBHelper.GetUnitOfWork();
                uow.Session.FlushMode = NHibernate.FlushMode.Auto;
                uow.BeginTransaction();
                row++;
                string genus = _excelReader.GetString(2);
                string species = _excelReader.GetString(3);
                if (!string.IsNullOrEmpty(genus) && !string.IsNullOrEmpty(species))
                {
                    ScientificName sciName = DBHelper.GetScientificName(uow, genus, species);

                    Plant plant = new Plant()
                    {
                        ScientificName = sciName
                    };
                    string sOrigin = "Ukendt";
                    if (_excelReader.GetString(0) != null)
                    {
                        switch (_excelReader.GetString(1).Substring(0, 2).ToUpper())
                        {
                            case "PA":
                                sOrigin = "Afrika";
                                break;
                            case "PC":
                                sOrigin = "Kultur";
                                break;
                            case "PE":
                                sOrigin = "Europa";
                                break;
                            case "PN":
                                sOrigin = "Nordamerika";
                                break;
                            case "PS":
                                sOrigin = "Mellem- og Sydamerika";
                                break;
                            case "PT":
                                sOrigin = "Pantropisk";
                                break;
                            case "PX":
                                sOrigin = "Asien eller Australien";
                                break;
                        }
                    }
                    string subOrigin = string.Empty;
                    if (_excelReader.GetString(10) != null)
                    {
                        subOrigin = _excelReader.GetString(10);
                    }
                    plant.Origin = DBHelper.GetOrigin(uow, sOrigin, subOrigin);
                    string tempString = "Ukendt";
                    if (_excelReader.GetString(1) != null)
                    {
                        switch (_excelReader.GetString(1).ToUpper())
                        {
                            case "AWP":
                                tempString = "Vandplante";
                                break;
                            case "BPP":
                                tempString = "Havedamsplante";
                                break;
                            case "CTP":
                                tempString = "Terrarieplante";
                                break;
                            case "DLP":
                                tempString = "Landplante";
                                break;
                        }
                    }
                    plant.PlantGroup = DBHelper.GetPlantGroup(uow, tempString);
                    // Sometimes something strange happens with english tradenames 
                    if (!string.IsNullOrEmpty(_excelReader.GetString(6)))
                    {
                        string[] parts = _excelReader.GetString(6).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            if (part.Length < 190)
                            {
                                Tradename tName = DBHelper.GetTradename(uow, part, DBHelper.GetLanguage(uow, "DE"));
                                if (!plant.Tradenames.Contains(tName))
                                    plant.Tradenames.Add(tName);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(_excelReader.GetString(7)))
                    {
                        string[] parts = _excelReader.GetString(7).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            if (part.Length < 190)
                            {
                                Tradename tName = DBHelper.GetTradename(uow, part, DBHelper.GetLanguage(uow, "EN"));
                                if (!plant.Tradenames.Contains(tName))
                                    plant.Tradenames.Add(tName);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(_excelReader.GetString(8)))
                    {
                        string[] parts = _excelReader.GetString(8).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            Tradename tName = DBHelper.GetTradename(uow, part, DBHelper.GetLanguage(uow, "NL"));
                            if (!plant.Tradenames.Contains(tName))
                                plant.Tradenames.Add(tName);
                        }
                    }
                    if (!string.IsNullOrEmpty(_excelReader.GetString(9)))
                    {
                        // Sometimes it also uses ; as seperator
                        string[] synonymStrings = _excelReader.GetString(9).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string synonym in synonymStrings)
                        {
                            string synonymCap = synonym;// TDK.Tools.StringTools.Capitalize(synonym);
                            if (synonymCap.StartsWith("?") || synonymCap.StartsWith("("))
                                break;
                            int idx = synonymCap.IndexOf(' ');
                            if (idx > 0)
                            {
                                string synGenus = synonymCap.Substring(0, idx).Trim();
                                string synSpecies = synonymCap.Substring(idx).Trim();
                                if (plant.ScientificName.Genus != synGenus && plant.ScientificName.Species != synSpecies)
                                    plant.Synonyms.Add(DBHelper.GetScientificName(uow, synGenus, synSpecies));
                            }
                        }
                    }
                    decimal min = 0;
                    decimal max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(11)))
                    {
                        string[] parts = _excelReader.GetString(11).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.Height = DBHelper.GetSize(uow, min, max);
                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(12)))
                    {
                        string[] parts = _excelReader.GetString(12).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.Width = DBHelper.GetSize(uow, min, max);
                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(13)))
                    {
                        string[] parts = _excelReader.GetString(13).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.WaterHardness = DBHelper.GetHardness(uow, min, max);
                    if (_excelReader.GetString(14) != null)
                    {
                        switch (_excelReader.GetString(14))
                        {
                            default:
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.Unknown;
                                break;
                            case "~~~~~":
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.VerySlow;
                                break;
                            case "~~":
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.Slow;
                                break;
                            case "~>~>":
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.Moderate;
                                break;
                            case ">>":
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.Fast;
                                break;
                            case ">>>>>":
                                plant.GrowthSpeed = Plant.PlantGrowthSpeed.VeryFast;
                                break;
                        }
                    }

                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(15)))
                    {
                        string[] parts = _excelReader.GetString(15).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.LightRequirements = DBHelper.GetLight(uow, min, max);
                    /*
                        1 = Schatten /Shade
                        2 = Halb Schatten / Half shade
                        3 = Mittel hell / Middle bright
                        4 = Hell / Bright
                        5 = Sehr Hell / Very bright
                     */
                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(16)))
                    {
                        string[] parts = _excelReader.GetString(16).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.WaterTemperature = DBHelper.GetTemperature(uow, min, max);
                    min = 0;
                    max = 0;
                    if (!string.IsNullOrEmpty(_excelReader.GetString(17)))
                    {
                        string[] parts = _excelReader.GetString(17).Split('-');
                        if (parts.Length > 0)
                            min = TDK.Tools.StringTools.StringToDecimal(parts[0]);
                        if (parts.Length > 1)
                            max = TDK.Tools.StringTools.StringToDecimal(parts[1]);
                        else
                            max = min;
                    }
                    plant.WaterPh = DBHelper.GetPH(uow, min, max, false);

                    // Plant care
                    if (!string.IsNullOrEmpty(_excelReader.GetString(18)))
                    {
                        string[] parts = _excelReader.GetString(18).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            switch (part.Trim())
                            {
                                default:
                                    tempString = "Ukendt";
                                    break;
                                case "1":
                                    tempString = "Let";
                                    break;
                                case "2":
                                    tempString = "Lidt svær";
                                    break;
                                case "3":
                                    tempString = "Svær";
                                    break;
                                case "4":
                                    tempString = "Meget svær";
                                    break;
                                case "5":
                                    tempString = "Frøformering";
                                    break;
                                case "6":
                                    tempString = "Formering med skud";
                                    break;

                                case "7":
                                    tempString = "Formering med rhizome";
                                    break;
                                case "8":
                                    tempString = "Formering kun med licens";
                                    break;
                                case "9":
                                    tempString = "Våd-fugtig";
                                    break;
                                case "10":
                                    tempString = "Middel fugtig";
                                    break;
                                case "11":
                                    tempString = "Høj fugtighed nødvendigt";
                                    break;
                                case "12":
                                    tempString = "Høj fugtighed er godt";
                                    break;
                                case "13":
                                    tempString = "Behøver hvile periode";
                                    break;
                            }  // switch (part.Trim())
                            plant.Care.Add(DBHelper.GetPlantCare(uow, tempString));
                        } // foreach (string part in parts)
                    } // Plant care

                    // Characteristics
                    if (!string.IsNullOrEmpty(_excelReader.GetString(19)))
                    {
                        string[] parts = _excelReader.GetString(19).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            switch (part.Trim())
                            {
                                default:
                                    tempString = "Ukendt";
                                    break;
                                case "1":
                                    tempString = "Bekæmper alger";
                                    break;
                                case "2":
                                    tempString = "Flydeplante";
                                    break;
                                case "3":
                                    tempString = "Sump plante";
                                    break;
                                case "4":
                                    tempString = "Tåler brakvand";
                                    break;
                                case "5":
                                    tempString = "Uspiselig";
                                    break;
                                case "6":
                                    tempString = "Knoldplante";
                                    break;
                                case "7":
                                    tempString = "God til havedam";
                                    break;
                                case "8":
                                    tempString = "Danner tæppe";//TODO: change 'tæppe'
                                    break;
                                case "9":
                                    plant.TankPosition = Plant.PlantTankPositions.Front;
                                    break;
                                case "10":
                                    plant.TankPosition = Plant.PlantTankPositions.Middle;
                                    break;
                                case "11":
                                    plant.TankPosition = Plant.PlantTankPositions.Back;
                                    break;
                                case "12":
                                    plant.TankPosition = Plant.PlantTankPositions.Solitary;
                                    break;
                                case "13":
                                    tempString = "Plantes i grupper";
                                    break;

                                case "14":
                                    tempString = "Kan blomstre";
                                    break;
                                case "15":
                                    tempString = "Land plante";
                                    break;
                                case "16":
                                    tempString = "Også i paludarium";
                                    break;
                                case "17":
                                    tempString = "Bundplante";
                                    break;
                                case "18":
                                    tempString = "Klatreplante";
                                    break;
                                case "19":
                                    tempString = "Hængeplante";
                                    break;
                                case "20":
                                    tempString = "Epiphyt";
                                    break;
                                case "21":
                                    tempString = "Kaktus";
                                    break;
                                case "22":
                                    tempString = "Havedamsplante";
                                    break;
                            }  // switch (part.Trim())
                            plant.Characteristics.Add(DBHelper.GetPlantCharacteristics(uow, tempString));
                        } // foreach (string part in parts)
                    } // Characteristics

                    if (!string.IsNullOrEmpty(_excelReader.GetString(21)))
                    {
                        string[] parts = _excelReader.GetString(21).Split(new Char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            if (part.ToUpper() == "C")
                                plant.CO2Requirements = Plant.PlantCO2Requirements.Medium;
                        }
                    }
                    List<FishbaseImage> images = DBHelper.GetImages(uow, $"{plant.ScientificName.Genus} {plant.ScientificName.Species}");
                    Parallel.ForEach(images, (img) => { plant.Images.Add(img); });


                    try
                    {
                        uow.Session.SaveOrUpdate(plant);
                        //DBHelper.SavePlant(uow, plant);
                        uow.Commit();
                        uow.Session.Flush();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        OnError?.Invoke("DBHelper.SavePlant call", ex);
                    }
                    OnOneDone?.Invoke(plant.ScientificName.ToString());
                }  // if (!string.IsNullOrEmpty(genus) && !string.IsNullOrEmpty(species))
                else
                {
                    OnOneDone?.Invoke($"Row {row} does not contain a scientific name");
                }
            }
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AqualogPlantImport() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
