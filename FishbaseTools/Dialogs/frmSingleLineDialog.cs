﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FishbaseTools.Dialogs
{
    public partial class frmSingleLineDialog : Form
    {
        #region Properties
        public string Title { get { return this.Text; } set { this.Text = value; } }
        public string Label { get { return lblText.Text; } set { lblText.Text = value; } }
        public string Value { get { return tbText.Text; } set { tbText.Text = value; } }
        #endregion

        public frmSingleLineDialog()
        {
            InitializeComponent();
        }

        private void tbText_TextChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = tbText.Text.Length > 0;
        }
    }
}
