﻿using System.Windows.Forms;

namespace FishbaseTools
{
    public partial class frmSplash : Form
    {
        public string StatusText { get { return toolStripStatusLabel1.Text; } set { toolStripStatusLabel1.Text = value; } }
        public int Percent { get { return tspbPercent.Value; } set { SetPercent(value); } }

        public frmSplash()
        {
            InitializeComponent();

            StatusText = string.Empty;
            tspbPercent.Visible = false;
            tspbPercent.Maximum = 100;
            tspbPercent.Value = 0;
        }

        private void SetPercent(int value)
        {
            tspbPercent.Visible = (value > 0);
            tspbPercent.Value = value;
        }
    }
}
