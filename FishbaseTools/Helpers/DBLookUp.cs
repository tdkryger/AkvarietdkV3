﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishbaseTools.Helpers
{
    static class DBLookUp
    {
        public static Temperature GetTemperature(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Temperature temp = null;
            using (Akvariet.Fishbase.Repositories.Repository<Temperature> repo = new Akvariet.Fishbase.Repositories.Repository<Temperature>(uow))
            {
                temp = repo.GetAll().Where(x => x.Min == min).Where(x => x.Max == max).FirstOrDefault();
                if (temp == null)
                {
                    temp = new Temperature()
                    {
                        Min = min,
                        Max=max
                    };
                    try
                    {
                        uow.BeginTransaction();
                        repo.Update(temp);
                        uow.Commit();
                    }
                    catch(Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
            return temp;
        }

        public static PH GetPh(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            PH temp = null;
            using (Akvariet.Fishbase.Repositories.Repository<PH> repo = new Akvariet.Fishbase.Repositories.Repository<PH>(uow))
            {
                temp = repo.GetAll().Where(x => x.Min == min).Where(x => x.Max == max).FirstOrDefault();
                if (temp == null)
                {
                    temp = new PH()
                    {
                        Min = min,
                        Max = max
                    };
                    try
                    {
                        uow.BeginTransaction();
                        repo.Update(temp);
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
            return temp;
        }

        public static Size GetSize(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Size temp = null;
            using (Akvariet.Fishbase.Repositories.Repository<Size> repo = new Akvariet.Fishbase.Repositories.Repository<Size>(uow))
            {
                temp = repo.GetAll().Where(x => x.Min == min).Where(x => x.Max == max).FirstOrDefault();
                if (temp == null)
                {
                    temp = new Size()
                    {
                        Min = min,
                        Max = max
                    };
                    try
                    {
                        uow.BeginTransaction();
                        repo.Update(temp);
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
            return temp;
        }

        public static Hardness GetHardness(FishbaseUnitOfWork uow, decimal min, decimal max)
        {
            Hardness temp = null;
            using (Akvariet.Fishbase.Repositories.Repository<Hardness> repo = new Akvariet.Fishbase.Repositories.Repository<Hardness>(uow))
            {
                temp = repo.GetAll().Where(x => x.Min == min).Where(x => x.Max == max).FirstOrDefault();
                if (temp == null)
                {
                    temp = new Hardness()
                    {
                        Min = min,
                        Max = max
                    };
                    try
                    {
                        uow.BeginTransaction();
                        repo.Update(temp);
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw ex;
                    }
                }
            }
            return temp;
        }
    }
}
