﻿namespace FishbaseTools.Interfaces
{
    interface IUnitOfWork
    {
        void BeginTransaction();
        void Commit();
        void Rollback();
    }
}
