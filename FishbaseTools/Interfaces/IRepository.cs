﻿using System;
using System.Linq;

namespace FishbaseTools.Interfaces
{
    interface IRepository<T> : IDisposable where T : IEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
