﻿namespace FishbaseTools.Interfaces
{
    interface IEntity
    {
        int Id { get; set; }
    }
}
