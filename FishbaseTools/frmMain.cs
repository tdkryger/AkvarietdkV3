﻿using Akvariet.Common.Interfaces;
using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using Akvariet.Fishbase.Repositories;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TDK.Tools.Extensions;
using WebAkvarietdk.Models;

namespace FishbaseTools
{
    public partial class frmMain : Form
    {
        private int _aqualogImportCounter = 0;
        private int _aqualogImportMax = 0;
        private Pages.UcAnimal ucAnimal;
        private frmSplash splash;

        private delegate void ObjectDelegate(object obj);

        public frmMain()
        {
            InitializeComponent();

            splash = new frmSplash();
            splash.Show();
            splash.StatusText = "Forbinder til databasen";
            Application.DoEvents();

            ucAnimal = new Pages.UcAnimal();
            ucAnimal.Dock = DockStyle.Fill;
            tpAnimal.Controls.Add(ucAnimal);
            ucAnimal.OnLoadingTable += UcAnimal_OnLoadingTable;
            splash.StatusText = "Indlæser data";
            Application.DoEvents();
            UpdateStuff();
            ucAnimal.Init();
            ucAnimal.Focus();
            splash.StatusText = "Færdig";
            Application.DoEvents();


            splash.Close();
            splash.Dispose();
            splash = null;
        }

        private void UcAnimal_OnLoadingTable(string tableName, int current, int maximum)
        {
            if (splash != null)
            {
                splash.StatusText = $"Indlæser data: {tableName}";
                int percent = 0;
                if (maximum > 0 && current > 0)
                    percent = (int)(((float)current / (float)maximum) * 100);
                if (percent > 100)
                    percent = 100;
                splash.Percent = percent;
                Application.DoEvents();
            }
        }

        private void UpdateStatusBar()
        {
            tsslNumberAnimals.Text = $"Antal fisk: {DBHelper.AnimalCount()}";
            tsslNumberPlants.Text = $"Antal planter: {DBHelper.PlantCount()}";
        }

        private void UpdateStuff()
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                ucAnimal.UpdateAllLists();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void UpdateImageList()
        {
            List<FishbaseImage> list = new List<FishbaseImage>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {
                using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
                {
                    list = repo.GetAll().ToList();
                }
            }
            lbPicture.Items.Clear();
            foreach (FishbaseImage fbi in list)
            {
                lbPicture.Items.Add(fbi);
            }
            if (lbPicture.Items.Count > 0)
                lbPicture.SelectedIndex = 1;
        }

        private void UpdateProgressBar()
        {
            // do we need to switch threads?
            if (InvokeRequired)
            {
                // slightly different now, as we dont need params
                // we can just use MethodInvoker
                MethodInvoker method = new MethodInvoker(UpdateProgressBar);
                Invoke(method);
                return;
            }
            // as we didn't take a param, we just update
            progressBar1.PerformStep();
            Application.DoEvents();

        }

        private void UpdateAqualogFishImportCounter()
        {
            if (InvokeRequired)
            {
                MethodInvoker method = new MethodInvoker(UpdateAqualogFishImportCounter);
                Invoke(method);
                return;
            }
            lblImportAqualogFish.Text = $"Importerer fra Aqualog: {_aqualogImportCounter}/{_aqualogImportMax}";
        }

        private void UpdateTextBox(object obj)
        {
            // do we need to switch threads?
            if (InvokeRequired)
            {
                // we then create the delegate again
                // if you've made it global then you won't need to do this
                ObjectDelegate method = new ObjectDelegate(UpdateTextBox);
                // we then simply invoke it and return
                Invoke(method, obj);
                return;
            }
            // ok so now we're here, this means we're able to update the control
            // so we unbox the object into a string
            string text = (string)obj;
            // and update
            tsslStatus.Text = text;
        }


        private string CleanFilename(string filename, string extension)
        {
            string temp = filename.Replace(extension, "").Replace("_", " ").Trim();
            temp = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(temp.ToLower());
            return temp;
        }

        private void ImportPictureParallel(List<FileInfo> picFiles, string description)
        {
            Parallel.ForEach(picFiles, (picFile) =>
            {
                FishbaseImage fbi = new FishbaseImage()
                {
                    Title = CleanFilename(picFile.Name, picFile.Extension),
                    Description = description
                };
                fbi.Image = Image.FromFile(picFile.FullName);
                IUnitOfWork uow = new FishbaseUnitOfWork();
                using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
                {
                    uow.BeginTransaction();
                    try
                    {
                        repo.Update(fbi);
                        uow.Commit();
                    }
                    catch //(Exception ex)
                    {
                        uow.Rollback();
                        //ObjectDelegate del = new ObjectDelegate(UpdateTextBox);
                        //del.Invoke(ex.Message);
                    }
                    finally
                    {
                        UpdateProgressBar();
                        Application.DoEvents();
                    }
                }
            });
        }

        private void ImportPicture(List<FileInfo> picFiles, string description, string copyright)
        {
            foreach (FileInfo picFile in picFiles)
            {
                FishbaseImage fbi = new FishbaseImage()
                {
                    Title = CleanFilename(picFile.Name, picFile.Extension),
                    Description = description
                };
                if (fbi.Title.ToUpperInvariant().Contains("MALE"))
                    fbi.Content = "Han";
                if (fbi.Title.ToUpperInvariant().Contains("FEMALE"))
                    fbi.Content = "Hun";
                if (fbi.Title.ToUpperInvariant().Contains("JUVENILE"))
                    fbi.Content = "Unge";
                fbi.CopyRight = copyright;
                fbi.Image = Image.FromFile(picFile.FullName);

                fbi.CreateDate = TDK.Tools.ImageTools.GetImageDateTime(fbi.Image);

                IUnitOfWork uow = new FishbaseUnitOfWork();
                using (Repository<FishbaseImage> repo = new Repository<FishbaseImage>(uow))
                {
                    uow.BeginTransaction();
                    try
                    {
                        repo.Update(fbi);
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        tsslStatus.Text = ex.Message;
                    }
                    finally
                    {
                        UpdateProgressBar();
                        Application.DoEvents();
                    }
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
#if DEBUG
                folderBrowserDialog1.SelectedPath = @"D:\Dropbox\Photos\2011-03-24 jjphoto\Freshwater";
#endif
                tsslStatus.Visible = true;
                tsslStatus.Text = string.Empty;

                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    string copyright = "© Akvariet.dk";
                    if (File.Exists(Path.Combine(folderBrowserDialog1.SelectedPath, "copyright.txt")))
                    {
                        copyright = File.ReadAllText(Path.Combine(folderBrowserDialog1.SelectedPath, "copyright.txt"));
                    }
                    lblPicImport.Text = $"Importerer billeder fra {folderBrowserDialog1.SelectedPath}";
                    Application.DoEvents();
                    string description = string.Empty;
                    using (Views.frmTextInput textInput = new Views.frmTextInput())
                    {
                        textInput.Title = "Beskrivelse";
                        textInput.Label = "Beskrivelse";
                    }
                    pImportImages.Visible = true;
                    try
                    {
                        DirectoryInfo dInfo = new DirectoryInfo(folderBrowserDialog1.SelectedPath);
                        var picFiles = dInfo.GetFilesByExtensions(".jpg", ".png", ".bmp", ".gif").ToList();

                        progressBar1.Value = 0;
                        progressBar1.Maximum = picFiles.Count();
                        ImportPicture(picFiles, description, copyright);
                    }
                    catch (Exception ex)
                    {
                        tsslStatus.Text = ex.Message;
                    }
                    finally
                    {
                        pImportImages.Visible = false;
                        timer1.Enabled = true;
                    }
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tsslStatus.Visible = false;
            timer1.Enabled = false;
        }

        private void btnAqualogImportFish_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            //TODO: OpenFileDialog
            lblImportAqualogFish.Text = "Importerer Aqualog Fisk";
            pImportFish.Visible = true;
            tsslStatus.Visible = true;
            try
            {
                _aqualogImportCounter = 0;
                lbAqualogFishDone.Items.Clear();
                string fileName = @"D:\Dropbox\Akvarieting\AquaLog\DATABASE FISH 1.01.xls";
                using (AqualogImportFish importFish = new AqualogImportFish(fileName))
                {
                    importFish.OnOneDone += ImportFish_OnOneDone;
                    importFish.OnError += ImportFish_OnError;
                    progressBar2.Maximum = importFish.Count;
                    _aqualogImportMax = importFish.Count;
                    progressBar2.Value = 0;
                    progressBar2.Step = 1;
                    importFish.Import();
                }
            }
            finally
            {
                pImportFish.Visible = false;
                tsslStatus.Visible = false;
                this.Cursor = Cursors.Default;
            }
        }

        private void ImportFish_OnError(string text, Exception ex)
        {
            tsslStatus.Text = $"{text} ({ex.Message})";
        }

        private void ImportFish_OnOneDone(string name)
        {
            lbAqualogFishDone.Items.Insert(0, name);
            _aqualogImportCounter++;
            UpdateAqualogFishImportCounter();
            //UpdateStatusBar();
            progressBar2.PerformStep();
            Application.DoEvents();

        }

        private void btnSupport_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                DBHelper.GenerateDefaultSupportTableContent(true);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private List<int> GetAnimalIds()
        {
            IList<int> ids = new List<int>();
            using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
            {

                ids = uow.Session.CreateSQLQuery("SELECT Id FROM animal;")
                                .AddScalar("Id", NHibernateUtil.Int32)
                                .List<int>();

                //ids = uow.Session.CreateSQLQuery("SELECT Id FROM animal;")
                //            .List<int>();
                //using (AnimalRepository repo = new AnimalRepository(uow))
                //{
                //    IQueryable<Animal> animals = repo.GetAll();
                //    foreach(Animal animal in animals)
                //        ids.Add(animal.Id);
                //    //Parallel.ForEach(animals, (animal) => { ids.Add(animal.Id); });
                //}
            }
            return ids.ToList();
        }

        private void UpdateFromFishbaseOrgXML()
        {
            this.Cursor = Cursors.WaitCursor;
            progressBar2.Value = 0;
            lbAqualogFishDone.Items.Clear();
            lblImportAqualogFish.Text = "Opdaterer Familie, orden og handelsnavne";
            pImportFish.Visible = true;
            tsslStatus.Text = "Ny FishbaseOrgImport";
            tsslStatus.Visible = true;
            Application.DoEvents();
            int count = 0;
            int newStuff = 0;

            using (FishbaseOrgImport fbImport = new FishbaseOrgImport())
            {
                List<int> ids = GetAnimalIds();
                progressBar2.Maximum = ids.Count;
                progressBar2.Value = 0;
                progressBar2.Step = 1;
                progressBar2.Visible = true;
                foreach (int id in ids)
                {
                    count++;
                    lblImportAqualogFish.Text = $"Opdaterer Familie, orden og handelsnavne {count}/{ids.Count} ({newStuff})";
                    Application.DoEvents();
                    using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                    {
                        using (Repository<Animal> repo = new Repository<Animal>(uow))
                        {
                            Animal animal = repo.GetById(id);
                            if (animal != null)
                            {
                                tsslStatus.Text = $"{animal.ScientificName}";
                                Application.DoEvents();
                                progressBar2.PerformStep();
                                if (fbImport.UpdateAnimalWithFishbaseInfo(animal, uow))
                                {
                                    animal.ModifiedDate = DateTime.Now;
                                    uow.BeginTransaction();
                                    try
                                    {
                                        repo.Update(animal);
                                        uow.Commit();
                                        uow.Session.Flush();
                                        newStuff++;
                                        lbAqualogFishDone.Items.Insert(0, $"Fisk: {animal.ScientificName} - Ændringer gemt");
                                    }
                                    catch (Exception ex)
                                    {
                                        uow.Rollback();
                                        lbAqualogFishDone.Items.Insert(0, $"Fisk: {animal.ScientificName} - {ex.Message}");
                                    }

                                }
                                else
                                {
                                    lbAqualogFishDone.Items.Insert(0, $"Fisk: {animal.ScientificName} - Ingen ændringer");
                                }
                                animal = null;
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                            }
                        } // using (Repository<Animal> repo = new Repository<Animal>(uow))
                    } // using (FishbaseUnitOfWork uow = new FishbaseUnitOfWork())
                } // foreach (int id in ids)
            } // using (FishbaseOrgImport fbImport = new FishbaseOrgImport())
        }

        private void button6_Click(object sender, EventArgs e)
        {
            // Session crap. Rethink it
            this.Cursor = Cursors.WaitCursor;
            lbAqualogFishDone.Items.Clear();
            lblImportAqualogFish.Text = "Opdaterer Familie, orden og handelsnavne";
            pImportFish.Visible = true;
            tsslStatus.Text = "UpdateFromFishbaseOrgXML";
            tsslStatus.Visible = true;
            Application.DoEvents();
            UpdateFromFishbaseOrgXML();
            pImportFish.Visible = false;
            this.Cursor = Cursors.Default;
        }

        private void btnAqualogImportPlants_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            //TODO: OpenFileDialog
            pImportFish.Visible = true;
            tsslStatus.Visible = true;
            try
            {
                _aqualogImportCounter = 0;
                lbAqualogFishDone.Items.Clear();
                lblImportAqualogFish.Text = "Importerer Aqualog Planter";
                string fileName = @"D:\Dropbox\Akvarieting\AquaLog\DATABASE PLANT 1.0.xlsx";
                using (AqualogPlantImport importPlant = new AqualogPlantImport(fileName))
                {
                    importPlant.OnOneDone += ImportFish_OnOneDone;
                    importPlant.OnError += ImportFish_OnError;
                    progressBar2.Maximum = importPlant.Count;
                    _aqualogImportMax = importPlant.Count;
                    progressBar2.Value = 0;
                    progressBar2.Step = 1;
                    importPlant.Import();
                }
            }
            finally
            {
                pImportFish.Visible = false;
                tsslStatus.Visible = false;
                this.Cursor = Cursors.Default;
            }
        }

        private void lblImportAqualogFish_Click(object sender, EventArgs e)
        {

        }

        private void btnLoadImage_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {

                tsslStatus.Visible = true;
                tsslStatus.Text = string.Empty;


                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string copyright = "© Akvariet.dk";
                    if (File.Exists(Path.Combine(folderBrowserDialog1.SelectedPath, "copyright.txt")))
                    {
                        copyright = File.ReadAllText(Path.Combine(folderBrowserDialog1.SelectedPath, "copyright.txt"));
                    }
                    lblPicImport.Text = $"Importerer billeder fra {openFileDialog1.FileName}";
                    Application.DoEvents();
                    string description = string.Empty;
                    using (Views.frmTextInput textInput = new Views.frmTextInput())
                    {
                        textInput.Title = "Beskrivelse";
                        textInput.Label = "Beskrivelse";
                    }
                    pImportImages.Visible = true;
                    try
                    {
                        List<FileInfo> picFiles = new List<FileInfo>();
                        picFiles.Add(new FileInfo(openFileDialog1.FileName));


                        progressBar1.Value = 0;
                        progressBar1.Maximum = picFiles.Count();
                        ImportPicture(picFiles, description, copyright);
                    }
                    catch (Exception ex)
                    {
                        tsslStatus.Text = ex.Message;
                    }
                    finally
                    {
                        pImportImages.Visible = false;
                        timer1.Enabled = true;
                    }
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void lbPicture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbPicture.SelectedItem is FishbaseImage)
            {
                FishbaseImage fbi = (FishbaseImage)lbPicture.SelectedItem;
                lblPicCopy.Text = fbi.CopyRight;
                lblPicDate.Text = fbi.CreateDate.ToString();
                lblPicTitle.Text = fbi.Title;
                pictureBox1.Image = fbi.Image;
                lblPicId.Text = fbi.Id.ToString();
            }
            else
            {
                lblPicTitle.Text = "Ukendt";
            }
        }

        private void btnRefreshPics_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                UpdateImageList();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnFishbaseOrgPictures_Click(object sender, EventArgs e)
        {
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tpAnimal)
            {
                ucAnimal.Focus();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lblPicImport.Text = "Generere PDF filer";
            progressBar1.Value = 0;
            pImportImages.Visible = true;

            string path = @"d:\tempfiles\pdf\";
            var list = DTOHelper.GetAnimalIds();
            progressBar1.Maximum = list.Count;
            WebAkvarietdk.PDF.PDFGenerator pdf = new WebAkvarietdk.PDF.PDFGenerator();

            foreach (var item in list)
            {
                //try
                //{
                string filename = TDK.Tools.StringTools.RemoveIlligalCharsInFilepath($"{item.ScientificNameText}.pdf");
                string fullfilename = Path.Combine(path, filename);
                byte[] pdfData = pdf.CreateAnimalPDF(item.Id);
                if (pdfData != null)
                    File.WriteAllBytes(fullfilename, pdfData);
                progressBar1.PerformStep();
                Application.DoEvents();
                //}
                //catch { }
            }
            pImportImages.Visible = false;
        }

        private void btnPlantPDF_Click(object sender, EventArgs e)
        {
            lblPicImport.Text = "Generere PDF filer";
            progressBar1.Value = 0;
            pImportImages.Visible = true;
            string path = @"D:\tempFiles\pdf\ThePlants";
            var list = DTOHelper.GetPlantIds();
            progressBar1.Maximum = list.Count;
            WebAkvarietdk.PDF.PDFGenerator pdf = new WebAkvarietdk.PDF.PDFGenerator();
            foreach (var item in list)
            {
                //try
                //{
                string filename = TDK.Tools.StringTools.RemoveIlligalCharsInFilepath($"{item.ScientificNameText}.pdf");
                string fullfilename = Path.Combine(path, filename);
                byte[] pdfData = pdf.CreatePlantPDF(item.Id);
                if (pdfData != null)
                    File.WriteAllBytes(fullfilename, pdfData);
                progressBar1.PerformStep();
                Application.DoEvents();
                //}
                //catch { }
            }
            pImportImages.Visible = false;
        }

        private void btnImportHabitats_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            openFileDialog1.DefaultExt = "csv";
            openFileDialog1.InitialDirectory = @"D:\Dropbox\Akvariet.dk\V3\FishbaseTools\bin\Debug\app_data";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string outFileName = string.Empty;
                saveFileDialog1.DefaultExt = "sql";
                saveFileDialog1.InitialDirectory = @"D:\Dropbox\Akvariet.dk\V3\Akvariet.Fishbase\Data";
                saveFileDialog1.FileName = "habitat2.sql";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    outFileName = saveFileDialog1.FileName;
                    if (File.Exists(outFileName))
                        File.Delete(outFileName);
                    using (StreamWriter outFile = new StreamWriter(outFileName, true))
                    {
                        lblPicImport.Text = "Genererer biotop sql";
                        progressBar1.Value = 0;
                        pImportImages.Visible = true;
                        string[] lines = File.ReadAllLines(openFileDialog1.FileName);
                        progressBar1.Maximum = lines.Count();
                        Application.DoEvents();
                        foreach (string line in lines)
                        {
                            string[] part1 = line.Split(';');
                            if (part1.Length == 2)
                            {
                                string sciName = part1[0].Trim();
                                string[] biotops = part1[1].Split(',');
                                foreach (string biotop in biotops)
                                {
                                    if (int.TryParse(biotop, out int biotopId))
                                    {
                                        string sqlString = $"call updateAnimalHabitat('{sciName}', {biotopId});";
                                        outFile.WriteLine(sqlString);
                                    }
                                }
                            }
                            progressBar1.PerformStep();
                        }
                    }
                }
            }
            pImportImages.Visible = false;
            this.Cursor = Cursors.Default;
        }
    }
}
