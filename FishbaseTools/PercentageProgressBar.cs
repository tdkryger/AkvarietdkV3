﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace FishbaseTools
{
    public class PercentageProgressBar : ProgressBar
    {
        private const int WM_PAINT = 0x000F;
        private const string k_PercentTextFormat = "{0}%";

        private int m_Percent = 0;
        private Font m_TextFont = new Font("Arial", (float)8.25, FontStyle.Regular);
        private PointF m_PercentageTextPosition;

        public PercentageProgressBar()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            m_PercentageTextPosition = new PointF(Width / 2 - 10, Height / 2 - 7);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            m_PercentageTextPosition.X = Width / 2 - 10;
            m_PercentageTextPosition.Y = Height / 2 - 7;
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            switch (m.Msg)
            {
                case WM_PAINT:
                    m_Percent = (int)(((double)Value / (double)Maximum) * 100);
                    using (Graphics graphics = Graphics.FromHwnd(Handle))
                    {

                        using (Brush textBrush = new SolidBrush(ForeColor))
                            graphics.DrawString(string.Format(k_PercentTextFormat, m_Percent), m_TextFont, Brushes.Black, m_PercentageTextPosition);
                    }
                    break;
            }

        }
    }

}
