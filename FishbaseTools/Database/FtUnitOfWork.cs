﻿using FishbaseTools.Domain;
using FishbaseTools.Interfaces;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace FishbaseTools.Database
{
    class FtUnitOfWork : IUnitOfWork
    {
        private static readonly ISessionFactory _sessionFactory;
        private ITransaction _transaction;
        //private static readonly ILog _log = LogManager.GetLogger<FishbaseUnitOfWork>();

        public ISession Session { get; private set; }

        static FtUnitOfWork()
        {
            // Initialise singleton instance of ISessionFactory, static constructors are only executed once during the
            // application lifetime - the first time the UnitOfWork class is used

            _sessionFactory = CreateConnection();
        }

        public FtUnitOfWork()
        {
            Session = _sessionFactory.OpenSession();
            Session.FlushMode = FlushMode.Auto;
        }

        public void BeginTransaction()
        {
            _transaction = Session.BeginTransaction();
        }

        public void Commit()
        {
            try
            {
                // commit transaction if there is one active
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Commit();

            }
            catch //(Exception ex)
            {
                //_log.Error("UnitOfWork.Commit", ex);

                // rollback if there was an exception
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();

                throw;
            }
            finally
            {
                //Session.Dispose();
            }
        }

        public void Rollback()
        {
            try
            {
                if (_transaction != null && _transaction.IsActive)
                    _transaction.Rollback();
            }
            finally
            {
                //Session.Dispose();
            }
        }

        #region private methods
        private static ISessionFactory CreateConnection()
        {
            /*
                drop database akvariet_dk_db2;
                //CREATE DATABASE akvariet_dk_db2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
            */
            if (_sessionFactory != null)
                return _sessionFactory;
            try
            {
                string conString = Akvariet.Common.AkvarietConfiguration.MySQLConnectionStringRaw;

                FluentConfiguration _config = Fluently.Configure().Database(
                    MySQLConfiguration.Standard.ConnectionString(conString))
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Fishbaseraw>())
                        .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true));
                ISessionFactory temp = _config.BuildSessionFactory();

                return temp;
            }
            catch //(Exception ex)
            {
                //_log.Fatal("UnitOfWork.createConnection", ex);
                throw;
            }
        }
        #endregion
    }
}
