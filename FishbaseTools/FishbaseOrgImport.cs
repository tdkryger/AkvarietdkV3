﻿using Akvariet.Fishbase.Database;
using Akvariet.Fishbase.Model;
using FishbaseTools.Database;
using FishbaseTools.Domain;
using FishbaseTools.Interfaces;
using FishbaseTools.Repositories;
using System;
using System.Linq;

namespace FishbaseTools
{
    class FishbaseOrgImport : IDisposable
    {
        #region Fields
        private IUnitOfWork _uow = new FtUnitOfWork();
        private Repository<Fishbaseraw> _repo;
        #endregion

        #region Constructor
        public FishbaseOrgImport()
        {
            //_repo = new Repository<Fishbaseraw>(_uow);
        }
        #endregion

        #region Public Methods
        public bool UpdateAnimalWithFishbaseInfo(Animal animal, FishbaseUnitOfWork uow)
        {
            //_uow = uow;
            bool anyChanges = false;
            Fishbaseraw rawFish = GetFish(animal.ScientificName.Genus, animal.ScientificName.Species, new FtUnitOfWork());
            if (rawFish != null)
            {
                #region Order
                if (!string.IsNullOrEmpty(rawFish.Order))
                {
                    if (animal.Order.Name != rawFish.Order)
                    {
                        anyChanges = true;
                        animal.Order = FindOrder(rawFish.Order, uow);
                        if (animal.Order == null)
                        {
                            Order newOrder = new Order()
                            {
                                Name = rawFish.Order,
                                OrderGroup = "Fisk" //! culture!!!
                            };

                            animal.Order = newOrder;
                        }
                    }
                }
                #endregion
                #region Family
                if (!string.IsNullOrEmpty(rawFish.Family))
                {
                    if (animal.Family.Name != rawFish.Family)
                    {
                        anyChanges = true;
                        animal.Family = FindFamily(rawFish.Family, uow);
                        if (animal.Family == null)
                        {
                            Family newFamily = new Family()
                            {
                                Name = rawFish.Family
                            };
                            animal.Family = newFamily;
                        }
                    }
                }
                #endregion
                #region Danish Tradename
                if (rawFish.DanishTradename.Count > 0)
                {
                    // TODO: Fix this. Apparently the actual  name is NOT saved in the database
                    foreach (string tradename in rawFish.DanishTradename)
                    {
                        if (animal.Tradenames.Where(x => x.Name.Equals(tradename)).FirstOrDefault(x => x.Language.Code.Equals("DK")) == null)
                        {
                            anyChanges = true;
                            Tradename dkTradename = new Tradename()
                            {
                                Name = tradename
                            };
                            dkTradename.Language = FindLanguage("DK", uow);
                            animal.Tradenames.Add(dkTradename);
                        }
                    }

                }
                #endregion

                //#region Images
                //TODO: Something goes wrong and some images makes load all throw exception
                //foreach (FishbaseImageRaw fbi in rawFish.GetImages())
                //{
                //    FishbaseImage fi = null;
                //    try
                //    {
                //        fi = animal.Images
                //            .Where(x => x.Content.Equals(fbi.Type))
                //            .Where(x => x.CopyRight.Equals(fbi.Copyright))
                //            .FirstOrDefault();
                //    }
                //    catch
                //    {
                //        fi = null;
                //    }
                //    if (fi == null)
                //    {
                //        fi = new FishbaseImage()
                //        {
                //            Content = fbi.Type,
                //            CopyRight = fbi.Copyright,
                //            Title = animal.ScientificName.ToString(),
                //            CreateDate = DateTime.Now,
                //            Image = fbi.Image
                //        };

                //        animal.Images.Add(fi);
                //        anyChanges = true;
                //    }
                //}
                //#endregion

                #region Synonyms
                foreach (string synonym in rawFish.Synonyms)
                {
                    ScientificName sn = FindScientificName(synonym, uow);
                    if (sn != null)
                    {
                        if (sn.Id != animal.ScientificName.Id)
                        {
                            if (animal.Synonyms.FirstOrDefault(x => x.Id == sn.Id) == null)
                            {
                                animal.Synonyms.Add(sn);
                                anyChanges = true;
                            }
                        }
                    }
                }
                #endregion

                if (anyChanges)
                    animal.ModifiedDate = DateTime.Now;
            }
            return anyChanges;
        }

        public Fishbaseraw GetFish(string genus, string species, IUnitOfWork uow)
        {
            using (Repository<Fishbaseraw> repo = new Repository<Fishbaseraw>(uow))
                return repo.GetAll().Where(x => x.Genus.Equals(genus)).FirstOrDefault(x => x.Species.Equals(species));
            //return _repo.GetAll().Where(x => x.Genus.Equals(genus)).FirstOrDefault(x => x.Species.Equals(species));
        }


        #region Private methods
        private ScientificName FindScientificName(string name, FishbaseUnitOfWork uow)
        {
            int idx = name.IndexOf(" ");
            string genus = name.Substring(0, idx).Trim();
            string species = name.Substring(idx).Trim();
            using (Akvariet.Fishbase.Repositories.Repository<ScientificName> repo = new Akvariet.Fishbase.Repositories.Repository<ScientificName>(uow))
            {
                return repo.GetAll().Where(x => x.Genus.Equals(genus)).FirstOrDefault(x => x.Species.Equals(species));
            }
        }

        private Language FindLanguage(string code, FishbaseUnitOfWork uow)
        {
            using (Akvariet.Fishbase.Repositories.Repository<Language> repo = new Akvariet.Fishbase.Repositories.Repository<Language>(uow))
            {
                return repo.GetAll().FirstOrDefault(x => x.Code.Equals(code));
            }
        }

        private Family FindFamily(string name, FishbaseUnitOfWork uow)
        {
            using (Akvariet.Fishbase.Repositories.Repository<Family> repo = new Akvariet.Fishbase.Repositories.Repository<Family>(uow))
            {
                return repo.GetAll().FirstOrDefault(x => x.Name.Equals(name));
            }
        }

        private Order FindOrder(string name, FishbaseUnitOfWork uow)
        {
            using (Akvariet.Fishbase.Repositories.Repository<Order> repo = new Akvariet.Fishbase.Repositories.Repository<Order>(uow))
            {
                return repo.GetAll().FirstOrDefault(x => x.Name.Equals(name));
            }
        }
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    if (_repo != null)
                        _repo.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~FishbaseOrgImport() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion
    }
}
