﻿using Akvariet.FishbaseOrg.Tasks;
using System.Collections.Generic;

namespace Akvariet.FishbaseOrg.Business
{
    class TaskRunner 
    {
        public bool RunTheTask(IUrlTask task, Queue<IUrlTask> queue)
        {
            try
            {
                task.Download(queue);
                return true;
            }
            catch
            {
                //TODO: handle the error
                return false;
            }
        }
    }
}
