﻿namespace Akvariet.FishbaseOrg
{
    public static class Config
    {
        public const string FISHBASE_XML_SUMMARY = "http://www.fishbase.us/maintenance/FB/showXML.php?identifier=FB-{0}";
        public const string FISHBASE_XML_POINT_DATA = "http://www.fishbase.se/webservice/Occurrence/PointData.php?Genus={0}&Species={1}";
        public const string FISHBASE_XML_COMMON_NAMES = "http://www.fishbase.se/webservice/ComNames/comnamesxml.php?Genus={0}&Species={1}";
        public const string FISHBASE_XML_PHOTOS = "http://www.fishbase.se/webservice/Photos/FishPicsList.php?Genus={0}&Species={1}&type=";
    }
}
