﻿using System.Collections.Generic;

namespace Akvariet.FishbaseOrg.Tasks
{
    interface IUrlTask
    {
        string Url { get; set; }
        string Title { get; set; }

        void Download(Queue<IUrlTask> queue);
    }
}
