﻿using System;
using System.Collections.Generic;

namespace Akvariet.FishbaseOrg.Tasks
{
    class SummaryDownloadTask : IUrlTask
    {
        public string Url { get; set; } = string.Empty;
        public string Title { get; set; } = string.Empty;


        public SummaryDownloadTask(string url, string title)
        {
            this.Url = url;
            this.Title = title;
        }

        public void Download(Queue<IUrlTask> queue)
        {
            throw new NotImplementedException();
        }
    }
}
