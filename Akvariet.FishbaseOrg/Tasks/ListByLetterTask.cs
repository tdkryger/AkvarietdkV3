﻿using HtmlAgilityPack;
using System.Collections.Generic;

namespace Akvariet.FishbaseOrg.Tasks
{
    class ListByLetterTask : IUrlTask
    {
        public string Url { get; set; } = string.Empty;
        public string Title { get; set; }

        public ListByLetterTask(string url, string title)
        {
            this.Url = url;
            this.Title = title;
        }

        public void Download(Queue<IUrlTask> queue)
        {
            if (!string.IsNullOrEmpty(Url))
            {
                HtmlWeb hw = new HtmlWeb();
                HtmlDocument doc = hw.Load(Url);

                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    HtmlAttribute att = link.Attributes["href"];
                    if (TDK.Tools.StringTools.GetIntegerFromString(att.Value, out int fishbaseID))
                    {
                        string xmlurl = string.Format(Config.FISHBASE_XML_SUMMARY, fishbaseID);
                        queue.Enqueue(new SummaryDownloadTask(xmlurl, link.InnerText));
                    }

                }
            }
        }
    }
}
