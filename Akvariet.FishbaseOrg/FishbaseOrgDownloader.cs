﻿using Akvariet.FishbaseOrg.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Akvariet.FishbaseOrg
{
    public class FishbaseOrgDownloader
    {
        #region Fields
        private Queue<IUrlTask> _downloadQueue = new Queue<IUrlTask>();
        private int _threads = 8;
        private string[] FISHBASE_LATIN_NAMES = {
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesA.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesB.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesC.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesD.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesE.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesF.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesG.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesH.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesI.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesJ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesK.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesL.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesM.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesN.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesO.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesP.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesQ.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesR.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesS.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesT.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesU.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesV.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesW.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesX.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesY.htm",
                                                    "http://www.fishbase.se/ListByLetter/ScientificNamesZ.htm"
                                                };

        #endregion

        public bool MustStop { get; set; }

        public FishbaseOrgDownloader(int threads)
        {
            _threads = threads;
            foreach (string url in FISHBASE_LATIN_NAMES)
            {
                ListByLetterTask lbl = new ListByLetterTask(url, "Letter list");
                _downloadQueue.Enqueue(lbl);
            }
        }

        public async Task RunAsync()
        {
            MustStop = true;
            while (!MustStop)
            {
                if (_downloadQueue.Count > 0)
                    await Task.Run(() => DoWork(_downloadQueue.Dequeue()));
            }
        }

        private void DoWork(IUrlTask task)
        {
            task.Download(_downloadQueue);
            Thread.Sleep(5000);
        }
    }
}
