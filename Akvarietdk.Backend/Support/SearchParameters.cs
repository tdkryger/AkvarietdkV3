﻿using System.Data;

namespace Akvarietdk.Backend.Support
{
    public class SearchParameters
    {
        #region enums
        public enum CompareTypes {Equel, Like, GreaterThen, LesserThen, EquelOrGreaterThen, EquelOrLesserThen };
        #endregion

        #region Properties
        public DbType DbType { get; set; }
        public string FieldName { get; set; }
        public object Value { get; set; }
        public CompareTypes CompareType { get; set; }
        #endregion

        #region Constructor
        public SearchParameters(DbType dbType, string fieldName, object value)
        {
            this.DbType = dbType;
            this.FieldName = fieldName;
            this.Value = value;
            this.CompareType = CompareTypes.Equel;
        }

        public SearchParameters(DbType dbType, string fieldName, object value, CompareTypes compareType)
        {
            this.DbType = dbType;
            this.FieldName = fieldName;
            this.Value = value;
            this.CompareType = compareType;
        }
        #endregion

        #region Public mehtods
        public override string ToString()
        {
            var compareString = "=";
               switch(CompareType)
            {
                case CompareTypes.EquelOrGreaterThen:
                    compareString = ">=";
                    break;
                case CompareTypes.EquelOrLesserThen:
                    compareString = "<=";
                    break;
                case CompareTypes.GreaterThen:
                    compareString = ">";
                    break;
                case CompareTypes.LesserThen:
                    compareString = "<";
                    break;
                case CompareTypes.Like:
                    compareString = " LIKE ";
                    break;
            }
            switch (DbType)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.Date:
                case DbType.DateTime:
                case DbType.DateTime2:
                case DbType.String:
                case DbType.StringFixedLength:
                case DbType.Time:
                    return $"{FieldName}{compareString}'{Value.ToString()}'";
                default:
                    return $"{FieldName}{compareString}{Value.ToString()}";
            }
        }
        #endregion
    }
}
