﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class LightRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public LightRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Min`,`Max`,`Text` FROM `light` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Light(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Min = reader.GetDecimal(1),
                        Max = reader.GetDecimal(2),
                        Text = reader.GetString(3)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
