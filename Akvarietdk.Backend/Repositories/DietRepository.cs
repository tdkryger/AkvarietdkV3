﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class DietRepository : Abstracts.AbstractRepository
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public DietRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<Diet> ConvertList(List<IEntity> entities)
        {
            var list = new List<Diet>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is Diet castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }

        public async Task<List<Diet>> GetDietForAnimalAsync(long animalId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `diet`.`Id`,`diet`.`Name` FROM `diet` INNER JOIN `diettoanimal` ON ( `diet`.`Id`=`diettoanimal`.`Diet_id`) WHERE `diettoanimal`.`Animal_id`={animalId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name` FROM `diet` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`
                    var item = new Diet(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Name=reader.GetString(1)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
