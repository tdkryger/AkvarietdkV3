﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class HardnessRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public HardnessRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Min`,`Max` FROM `hardness` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Hardness(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Min = reader.GetDecimal(1),
                        Max=reader.GetDecimal(2)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
