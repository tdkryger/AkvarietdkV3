﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class FamilyRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public FamilyRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public methods
        public static List<Family> ConvertList(List<IEntity> entities)
        {
            var list = new List<Family>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is Family castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name` FROM `family`";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`
                    var item = new Family(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Name = reader.GetString(1)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
