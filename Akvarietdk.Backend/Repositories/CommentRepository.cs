﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class CommentRepository : Abstracts.AbstractRepository
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public CommentRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<IComment> ConvertList(List<IEntity> entities)
        {
            var list = new List<IComment>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is IComment castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }

        public async Task<List<IComment>> GetCommentsForBlogAsync(long blogId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"{GetSelectSql()} WHERE `BlogEntity_id`= {blogId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }

        public async Task<List<IComment>> GetCommentsForAnimalAsync(long animalId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"{GetSelectSql()} WHERE `Animal_id`= {animalId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }

        public async Task<List<IComment>> GetCommentsForPlantAsync(long plantId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"{GetSelectSql()} WHERE `Plant_id`= {plantId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`PostDate`,`Title`,`Body`,`Mark`,`Score`,`User_id`,`BlogEntity_id`,`Animal_id`,`Plant_id` FROM `comment` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var userRepo = new UserRepository(_appDb);
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`PostDate`,`Title`,`Body`,`Mark`,`Score`,`User_id`,`BlogEntity_id`,`Animal_id`,`Plant_id`
                    var item = new Comment(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        PostDate = reader.GetDateTime(1),
                        Title = reader.GetString(2),
                        Body = reader.GetString(3),
                        Mark = (Marks)reader.GetInt32(4),
                        Score = reader.GetInt32(5),
                        User = (User)await userRepo.FindOneAsync(reader.GetInt64(6)),
                        BlogEntity_id = reader.GetInt64(7),
                        Animal_id = reader.GetInt64(8),
                        Plant_id = reader.GetInt64(9),
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
