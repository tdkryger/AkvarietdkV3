﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class HabitatRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public HabitatRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<Habitat> ConvertList(List<IEntity> entities)
        {
            var list = new List<Habitat>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is Habitat castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }

        public async Task<List<Habitat>> GetCommentsForAnimalAsync(long animalId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `habitat`.`Id`,`habitat`.`Text`,`habitat`.`Description`,`habitat`.`BiotopAkvarietId` FROM `habitat` INNER JOIN `habitattoanimal` ON `habitattoanimal`.`Habitat_id`=`habitat`.`Id` WHERE `habitattoanimal`.`Animal_id`={animalId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `habitat`.`Id`,`habitat`.`Text`, `habitat`.`Description`, `habitat`.`BiotopAkvarietId` FROM `habitat` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Text`,`Description`,`BiotopAkvarietId`
                    var item = new Habitat(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Text=reader.GetString(1),
                        Description=reader.GetString(2),
                        BiotopAkvarietId=reader.GetInt32(3)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion
    }
}
