﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;

namespace Akvarietdk.Backend.Repositories
{
    public class IllnessRepository : Abstracts.AbstractRepository
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public IllnessRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<Illness> ConvertList(List<IEntity> entities)
        {
            var list = new List<Illness>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is Illness castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }

        public async Task<List<Illness>> GetCommentsForAnimalAsync(long animalId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `illness`.`Id`,`illness`.`Name`,`illness`.`illnessGroup`,`illness`.`Cause`,`illness`.`Symptoms`,`illness`.`Treatment` FROM `illness` INNER JOIN `illnesstoanimal` ON `illnesstoanimal`.`Illness_id`=`illness`.`Id` WHERE `illnesstoanimal`.`Animal_id`={animalId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name`,`illnessGroup`,`Cause`,`Symptoms`,`Treatment` FROM `illness` ";

        protected override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
