﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class LanguageRepository : Abstracts.AbstractRepository
    {

        #region Constructor

        public LanguageRepository(IAppDb appDb)  : base(appDb) { }

        #endregion Constructor

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name`,`Code` FROM `language`";

        protected override async Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`,`Code`
                    var item = new Language(_appDb)
                    {
                        Id = reader.GetFieldValue<int>(0),
                        Name = reader.GetString(1),
                        Code = reader.GetString(2)
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        #endregion Private methods
    }
}