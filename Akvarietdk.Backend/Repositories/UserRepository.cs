﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class UserRepository : Abstracts.AbstractRepository
    {
        #region Constructor

        public UserRepository(IAppDb appDb) : base(appDb) { }

        #endregion Constructor

        #region Private methods
        protected override string GetSelectSql() => "SELECT `Id`,`DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime` FROM `user`";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime`
                    var item = new User(_appDb)
                    {
                        DisplayName = reader.GetString(1),
                        EMail = reader.GetString(3),
                        JoinDateTime = reader.GetDateTime(5),
                        Role = (UserLevels)reader.GetInt32(4),
                        Id = reader.GetInt64(0),
                        Username = reader.GetString(2),
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion Private methods
    }
}