﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using Akvarietdk.Backend.Support;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Database.Repositories
{
    public class TradenameRepository : IRepository<Tradename>
    {
        #region Fields

        private readonly IAppDb _appDb;

        #endregion Fields



        #region Constructor

        public TradenameRepository(IAppDb appDb)
        {
            _appDb = appDb;
        }

        #endregion Constructor

        #region Public mehtods

        public async Task<Tradename> FindOneAsync(long id)
        {
            using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
            {
                cmd.CommandText = "SELECT `tradename`.`Id`,`tradename`.`Name`,`tradename`.`Language_id`,`language`.`Name`,`language`.`Code` FROM `tradename` INNER JOIN `language` ON `tradename`.`Language_id`=`language`.`Id` WHERE `tradename`.`Id`=@id;";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return result.Count > 0 ? result[0] : null;
            }
        }

        public Task<List<Tradename>> GetAsync(List<SearchParameters> searchParameters, string orderByFieldName = null, int skip = 0, int? take = null)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<Tradename>> LatestPostsAsync(int count = 10)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `tradename`.`Id`,`tradename`.`Name`,`tradename`.`Language_id`,`language`.`Name`,`language`.`Code` FROM `tradename` INNER JOIN `language` ON `tradename`.`Language_id`=`language`.`Id` ORDER BY `tradename`.`Id` DESC LIMIT {count};";
                return await ReadAllAsync(await cmd.ExecuteReaderAsync());
            }
        }

        #endregion Public mehtods

        #region Private methods

        private async Task<List<Tradename>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<Tradename>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`,`Language_id`
                    var item = new Tradename(_appDb)
                    {
                        Id = reader.GetFieldValue<int>(0),
                        Name = reader.GetFieldValue<string>(1),
                        Language = new Language(_appDb) { Id = reader.GetInt64(2) }
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        #endregion Private methods
    }
}