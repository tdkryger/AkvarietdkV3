﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class AnimalGroupRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public AnimalGroupRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region protected methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name`,`AqualogCode`,`Zone`,`Active` FROM `animalgroup`";

        protected override async Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`,`AqualogCode`,`Zone`,`Active`
                    var item = new AnimalGroup(_appDb)
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        AqualogCode = reader.GetString(2),
                        Zone = reader.GetString(3),
                        Active = reader.GetBoolean(4)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
