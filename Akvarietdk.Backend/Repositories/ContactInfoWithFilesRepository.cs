﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;

namespace Akvarietdk.Backend.Repositories
{
    public class ContactInfoWithFilesRepository : Abstracts.AbstractRepository
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public ContactInfoWithFilesRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods

        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Content`,`FromEMail`,`FromName`,`Subject`,`Ip`,`CountryCode`,`CountryName`,`RegionCode`,`RegionName`,`City`,`ZipCode`,`TimeZone`,`Latitude`,`Longitude`,`MetroCode` FROM `contactinfo` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            //TODO: File Repo
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Content`,`FromEMail`,`FromName`,`Subject`,`Ip`,`CountryCode`,`CountryName`,`RegionCode`,'
                    // `RegionName`,`City`,`ZipCode`,`TimeZone`,`Latitude`,`Longitude`,`MetroCode`
                    var item = new ContactInfoWithFiles(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Content = reader.GetString(1),
                        FromEMail = reader.GetString(2),
                        FromName=reader.GetString(3),
                        Subject=reader.GetString(4),
                        Ip = reader.GetString(5),
                        CountryCode = reader.GetString(6),
                        CountryName = reader.GetString(7),
                        RegionCode = reader.GetString(8),
                        RegionName = reader.GetString(9),
                        City = reader.GetString(10),
                        ZipCode = reader.GetString(11),
                        TimeZone = reader.GetString(12),
                        Latitude = reader.GetString(13),
                        Longitude = reader.GetString(14),
                        MetroCode = reader.GetString(15),
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
