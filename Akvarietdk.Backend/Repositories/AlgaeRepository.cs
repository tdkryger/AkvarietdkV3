﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class AlgaeRepository : Abstracts.AbstractRepository
    {
        #region Constructor

        public AlgaeRepository(IAppDb appDb) : base(appDb) { }

        #endregion Constructor

        #region Public methods
        public static List<Algae> ConvertList(List<IEntity> entities)
        {
            var list = new List<Algae>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is Algae castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }
        #endregion

        #region Protected methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Name`,`Cause`,`Symptoms`,`Treatment` FROM `algea` ";

        protected override async Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var algaeList = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Name`,`Cause`,`Symptoms`,`Treatment`
                    var algae = new Algae(_appDb)
                    {
                        Id = reader.GetFieldValue<int>(0),
                        Name = reader.GetFieldValue<string>(1),
                        Symptoms = reader.GetFieldValue<string>(3),
                        Cause = reader.GetFieldValue<string>(2),
                        Treatment = reader.GetFieldValue<string>(4)
                    };
                    algaeList.Add(algae);
                }
            }
            return algaeList;
        }

        #endregion Protected methods
    }
}