﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class DBFileRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public DBFileRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<DBFile> ConvertList(List<IEntity> entities)
        {
            var list = new List<DBFile>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is DBFile dbFile)
                {
                    list.Add(dbFile);
                }
            });

            return list;
        }

        public async Task<List<DBFile>> GetForContactgAsync(long contactId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"{GetSelectSql()} WHERE `ContactInfoWithFiles_id`= {contactId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`ContentType`,`Filename`,`FileContent`,`ContactInfoWithFiles_id` FROM `dbfile` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync().ConfigureAwait(false))
                {
                    // `Id`,`ContentType`,`Filename`,`FileContent`,`ContactInfoWithFiles_id`
                    var item = new DBFile(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        ContentType = reader.GetString(1),
                        Filename = reader.GetString(2),
                        FileContent = (byte[])reader[3],
                        ContactInfoWithFiles_Id = reader.GetInt64(4)
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
