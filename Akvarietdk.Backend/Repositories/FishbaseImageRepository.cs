﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class FishbaseImageRepository : Abstracts.AbstractRepository
    {
        #region Constructor
        public FishbaseImageRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods
        public static List<FishbaseImage> ConvertList(List<IEntity> entities)
        {
            var list = new List<FishbaseImage>();
            Parallel.ForEach(entities, (item) =>
            {
                if (item is FishbaseImage castItem)
                {
                    list.Add(castItem);
                }
            });

            return list;
        }

        public async Task<List<FishbaseImage>> GetForAnimalAsync(long animalId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `fishbaseimage`.`Id`,`fishbaseimage`.`Image`,`fishbaseimage`.`Content`,`fishbaseimage`.`Description`,`fishbaseimage`.`Title`,`fishbaseimage`.`CopyRight`,`fishbaseimage`.`CreateDate` FROM `fishbaseimage` INNER JOIN `fishbaseimagetoanimal` ON `fishbaseimagetoanimal`.`FishbaseImage_id`=`fishbaseimage`.`Id` WHERE `fishbaseimagetoanimal`.`Animal_id`={animalId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }

        public async Task<List<FishbaseImage>> GetForPlantAsync(long plantId)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT `fishbaseimage`.`Id`,`fishbaseimage`.`Image`,`fishbaseimage`.`Content`,`fishbaseimage`.`Description`,`fishbaseimage`.`Title`,`fishbaseimage`.`CopyRight`,`fishbaseimage`.`CreateDate` FROM `fishbaseimage` INNER JOIN `fishbaseimagetoplant` ON `fishbaseimagetoplant`.`FishbaseImage_id`=`fishbaseimage`.`Id` WHERE `fishbaseimagetoplant`.`Plant_id`={ plantId};";
                var list = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return ConvertList(list);
            }
        }
        #endregion

        #region Private methods

        #endregion
        protected override string GetSelectSql() => @"SELECT `Id`,`Image`,`Content`,`Description`,`Title`,`CopyRight`,`CreateDate` FROM `fishbaseimage`";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Image`,`Content`,`Description`,`Title`,`CopyRight`,`CreateDate`
                    var item = new FishbaseImage(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Content = reader.GetString(2),
                        Description = reader.GetString(3),
                        Title = reader.GetString(4),
                        CopyRight = reader.GetString(5),
                        CreateDate = reader.GetDateTime(6)
                    };
                    var img = (byte[])reader[1];
                    using (MemoryStream ms = new MemoryStream(img))
                    {
                        item.Image = Image.FromStream(ms);
                    }
                    list.Add(item);
                }
            }
            return list;
        }
    }
}
