﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using Akvarietdk.Backend.Support;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Database.Repositories
{
    public class OriginRepository : IRepository<Origin>
    {
        #region Fields

        private readonly IAppDb _appDb;

        #endregion Fields



        #region Constructor

        public OriginRepository(IAppDb appDb)
        {
            _appDb = appDb;
        }

        #endregion Constructor

        #region Public mehtods

        public async Task<Origin> FindOneAsync(long id)
        {
            using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
            {
                cmd.CommandText = "SELECT  `Id`,`Text`,`SubOrigin` FROM `origin` WHERE `Id`=@id;";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return result.Count > 0 ? result[0] : null;
            }
        }

        public Task<List<Origin>> GetAsync(List<SearchParameters> searchParameters, string orderByFieldName = null, int skip = 0, int? take = null)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<Origin>> LatestPostsAsync(int count = 10)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"SELECT  `Id`,`Text`,`SubOrigin` FROM `origin` ORDER BY `Id` DESC LIMIT {count};";
                return await ReadAllAsync(await cmd.ExecuteReaderAsync());
            }
        }

        #endregion Public mehtods

        #region Private methods

        private async Task<List<Origin>> ReadAllAsync(DbDataReader reader)
        {
            var list = new List<Origin>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `Id`,`Text`,`SubOrigin`
                    var item = new Origin(_appDb)
                    {
                        Id = reader.GetFieldValue<int>(0),
                        Text = reader.GetFieldValue<string>(1),
                        SubOrigin = reader.GetFieldValue<string>(2)
                    };
                    list.Add(item);
                }
            }
            return list;
        }

        #endregion Private methods
    }
}