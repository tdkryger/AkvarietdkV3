﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class BlogEntityRepository : Abstracts.AbstractRepository
    {
        #region Fields

        #endregion

        #region Properties

        #endregion

        #region Constructor
        public BlogEntityRepository(IAppDb appDb) : base(appDb) { }
        #endregion

        #region Public mehtods

        #endregion

        #region Private methods
        protected override string GetSelectSql() => @"SELECT `Id`,`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`,`Contributor_id`,`ReleaseDate`,`Deleted` FROM `blogentity` ";

        protected async override Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            //TODO: Image Repo
            //TODO: Tags Repo
            var commentRepo = new CommentRepository(_appDb);
            var userRepo = new UserRepository(_appDb);
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync().ConfigureAwait(false))
                {
                    // `Id`,`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`,`Contributor_id`,`ReleaseDate`,`Deleted`
                    var item = new BlogEntity(_appDb)
                    {
                        Id = reader.GetInt64(0),
                        Title = reader.GetString(1),
                        Teaser=reader.GetString(2),
                        Body=reader.GetString(3),
                        Posted = reader.GetDateTime(4),
                        PostRead = reader.GetInt64(5),
                        // Image_id
                        Contributor = (User)await userRepo.FindOneAsync(reader.GetInt64(7)),
                        ReleaseDate = reader.GetDateTime(8),
                        Deleted = reader.GetBoolean(9)    ,
                        Comments = await commentRepo.GetCommentsForBlogAsync(reader.GetInt64(0))
                        //Tags
                    };
                    list.Add(item);
                }
            }
            return list;
        }
        #endregion

    }
}
