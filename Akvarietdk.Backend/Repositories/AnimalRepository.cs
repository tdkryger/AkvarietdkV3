﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Model;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Repositories
{
    public class AnimalRepository : Abstracts.AbstractRepository
    {
        #region Constructor

        public AnimalRepository(IAppDb appDb) : base(appDb) { }
        #endregion Constructor


        #region Private methods
        protected override string GetSelectSql() => @"SELECT `animalid`,`Description`,`CreateDate`,`ModifiedDate`,`ViewCount`,`scientificNameText`,`Bioload`,`SwimLevel`,`MingroupSize`,`PlantedTank`,`MinTankSize`,`MinFiltrationLevel`,`BreadInCaptivity`,`RequiresWood`,`scientificnameId`,`Genus`,`Species`,`originId`,`originText`,`SubOrigin`,`temperatureId`,`temperatureMin`,`temperauteMax`,`hardnessId`,`hardnessMin`,`hardnessMax`,`pHId`,`phMin`,`phMax`,`AddSalt`,`orderId`,`orderName`,`OrderGroup`,`suborderId`,`suborderName`,`suborderOrderId`,`familyId`,`familyName`,`animalGroupId`,`animalGroupName`,`AqualogCode`,`Zone`,`Active`,`sizeId`,`sizeMax`,`sizeMin`,`userId`,`DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime` FROM `view_animal`";

        protected override async Task<List<IEntity>> ReadAllAsync(DbDataReader reader)
        {
            // TODO: Lists!!!
            var list = new List<IEntity>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    // `animalid`,`Description`,`CreateDate`,`ModifiedDate`,`ViewCount`,`scientificNameText`,`Bioload`,`SwimLevel`,`MingroupSize`,`PlantedTank`,
                    // `MinTankSize`,`MinFiltrationLevel`,`BreadInCaptivity`,`RequiresWood`,`scientificnameId`,
                    // `Genus`,`Species`,`originId`,`originText`,`SubOrigin`,`temperatureId`,`temperatureMin`,`temperauteMax`,`hardnessId`,`hardnessMin`,
                    // `hardnessMax`,`pHId`,`phMin`,`phMax`,`AddSalt`,`orderId`,`orderName`,`OrderGroup`,`suborderId`,`suborderName`,`suborderOrderId`,
                    // `familyId`,`familyName`,`animalGroupId`,`animalGroupName`,`AqualogCode`,`Zone`,`Active`,`sizeId`,`sizeMax`,`sizeMin`,`userId`,
                    // `DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime`
                    var item = new Animal(_appDb)
                    {
                        Id = reader.GetFieldValue<int>(0),
                        Description = reader.GetString(1),
                        CreateDate = reader.GetDateTime(2),
                        ModifiedDate = reader.GetDateTime(3),
                        ViewCount = reader.GetInt32(4),
                        ScientificNameText = reader.GetString(5),
                        Bioload = (Animal.AnimalBioload)reader.GetInt32(6),
                        SwimLevel = (Animal.AnimalSwimLevel)reader.GetInt32(7),
                        MingroupSize = reader.GetInt32(8),
                        PlantedTank = (Animal.AnimalPlantedTank)reader.GetInt32(9),
                        MinTankSize = reader.GetDecimal(10),
                        MinFiltrationLevel = (Animal.AnimalFiltration)reader.GetInt32(11),
                        BreadInCaptivity = reader.GetBoolean(12),
                        RequiresWood = reader.GetBoolean(13),
                        ScientificName = new ScientificName(_appDb) { Id = reader.GetInt32(14), Genus = reader.GetString(15), Species = reader.GetString(16) },
                        Origin = new Origin(_appDb) { Id = reader.GetInt32(17), Text = reader.GetString(18), SubOrigin = reader.GetString(19) },
                        WaterTemperature = new Temperature(_appDb) { Id = reader.GetInt32(20), Min = reader.GetDecimal(21), Max = reader.GetDecimal(22) },
                        WaterHardness = new Hardness(_appDb) { Id = reader.GetInt32(23), Min = reader.GetDecimal(24), Max = reader.GetDecimal(25) },
                        WaterPh = new PH(_appDb) { Id = reader.GetInt32(26), Min = reader.GetDecimal(27), Max = reader.GetDecimal(28), AddSalt = reader.GetBoolean(29) },
                        Order = new Order(_appDb) { Id = reader.GetInt32(30), Name = reader.GetString(31), OrderGroup = reader.GetString(32) },
                    };

                    list.Add(item);
                }
            }
            return list;
        }

        #endregion Private methods
    }
}