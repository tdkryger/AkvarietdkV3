﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Support;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Abstracts
{
    public abstract class AbstractRepository : IRepository<IEntity>
    {
        #region Fields
        protected readonly IAppDb _appDb;
        #endregion

        #region Properties

        #endregion

        #region Constructor
        protected AbstractRepository(IAppDb appDb)
        {
            _appDb = appDb;
        }
        #endregion

        #region Public mehtods
        public async Task<IEntity> FindOneAsync(long id)
        {
            using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
            {
                cmd.CommandText = $"{GetSelectSql()} WHERE `Id`=@id;";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                var result = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                return result.Count > 0 ? result[0] : null;
            }
        }

        /// <summary>
        /// Dynamic get
        /// </summary>
        /// <param name="searchParameters">List of search parameters. All are AND'ed</param>
        /// <param name="orderByFieldName">SQL Field(s) to order by. If more than one field, they must be seperated by ",".</param>
        /// <param name="skip">The number of rows to skip, if take is set to a number</param>
        /// <param name="take">How many rows to take</param>
        /// <returns></returns>
        public async Task<List<IEntity>> GetAsync(List<SearchParameters> searchParameters, string orderByFieldName = null, int skip = 0, int? take = null)
        {
            var stringBuilder = new StringBuilder();
            var first = true;
            if (searchParameters.Count > 0)
            {
                stringBuilder.Append(" WHERE ");
            }
            foreach (SearchParameters searchParams in searchParameters)
            {
                if (!first)
                {
                    stringBuilder.Append(" AND ");
                }
                first = false;
                stringBuilder.Append(searchParams.ToString());
            }
            if (!string.IsNullOrEmpty(orderByFieldName))
            {
                stringBuilder.Append($" ORDER BY {orderByFieldName} ");
            }
            if (take.HasValue)
            {
                stringBuilder.Append($" LIMIT {skip}, {take} ");
            }

            var sqlString = GetSelectSql() + stringBuilder;
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = GetSelectSql() + stringBuilder;
                return await ReadAllAsync(await cmd.ExecuteReaderAsync());
            }
        }

        public async Task<List<IEntity>> LatestPostsAsync(int count = 10)
        {
            using (var cmd = _appDb.Connection.CreateCommand())
            {
                cmd.CommandText = $"{GetSelectSql()} ORDER BY `Id` DESC LIMIT {count};";
                return await ReadAllAsync(await cmd.ExecuteReaderAsync());
            }
        }
        #endregion

        #region Protected methods
        protected abstract string GetSelectSql();

        protected abstract Task<List<IEntity>> ReadAllAsync(DbDataReader reader);
        #endregion

        #region Private methods

        #endregion
    }
}
