﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Abstracts
{
    public abstract class AbstractDto : IEntity
    {
        #region Fields

        protected readonly IAppDb _appDb;

        #endregion Fields

        #region Properties

        public long Id { get; set; }

        #endregion Properties

        #region Constructor

        protected AbstractDto(IAppDb appDb)
        {
            _appDb = appDb;
        }

        #endregion Constructor

        #region Public mehtods

        public async Task InsertAsync(MySqlTransaction mySqlTransaction = null)
        {
            using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
            {
                if (mySqlTransaction != null)
                    cmd.Transaction = mySqlTransaction;
                cmd.CommandText = GetInsertSql();
                BindParams(cmd);
                await cmd.ExecuteNonQueryAsync();
                Id = (int)cmd.LastInsertedId;
            }
        }

        public async Task UpdateAsync(MySqlTransaction mySqlTransaction = null)
        {
            using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
            {
                if (mySqlTransaction != null)
                    cmd.Transaction = mySqlTransaction;
                cmd.CommandText = GetUpdateSql();
                BindParams(cmd);
                BindId(cmd);
                await cmd.ExecuteNonQueryAsync();
            }
        }

        #endregion Public mehtods

        #region Protected methods

        protected abstract string GetInsertSql();

        protected abstract string GetUpdateSql();

        protected void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Id",
                DbType = DbType.Int64,
                Value = Id,
            });
        }

        protected abstract void BindParams(MySqlCommand cmd);

        #endregion Protected methods
    }
}