﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;

namespace Akvarietdk.Backend.Model
{
    public class SubOrder : Abstracts.AbstractDto
    {
        #region Constructor

        public SubOrder(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Properties

        public string Name { get; set; } = string.Empty;

        #endregion Properties

        #region Public methods

        public override string ToString()
        {
            return Name;
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            throw new System.NotImplementedException();
        }

        protected override string GetInsertSql()
        {
            throw new System.NotImplementedException();
        }

        protected override string GetUpdateSql()
        {
            throw new System.NotImplementedException();
        }

        #endregion Public methods
    }
}