﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Tradename : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; }
        public Language Language { get; set; }

        #endregion Properties

        #region Constructor

        public Tradename(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return $"{Name} ({Language.Code})";
        }

        #endregion Public methods

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `tradename` (`Name`,`Language_id`) VALUES (@Name,@Language_id);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `language` SET `Name`=@Name,`Code`=@Code WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Name",
                DbType = DbType.String,
                Value = Name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Language_id",
                DbType = DbType.Int64,
                Value = Language.Id,
            });
        }

        #endregion Private methods
    }
}