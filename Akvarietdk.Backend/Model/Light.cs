﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;

namespace Akvarietdk.Backend.Model
{
    public class Light : Abstracts.AbstractDto
    {
        #region Properties

        public Decimal Min { get; set; }
        public Decimal Max { get; set; }
        public string Text { get; set; }

        #endregion Properties

        #region Constructor

        public Light(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Min == 0 ? "Ukendt" : Min == Max ? $"{Min:#0.#} W/L" : $"{Min:#0.#}-{Max:#0.#} W/L";
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `light` (`Min`,`Max`,`Text`) VALUES (@Min,@Max,@Text);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `light` SET `Min`=@Min,`Max`=@Max,`Text`=@Text WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Min", Min));
            cmd.Parameters.Add(new MySqlParameter("@Max", Max));
            cmd.Parameters.Add(new MySqlParameter("@Text", Text));
        }

        #endregion Protected methods
    }
}