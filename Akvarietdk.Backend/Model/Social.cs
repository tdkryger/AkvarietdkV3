﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Social : Abstracts.AbstractDto
    {
        #region Properties

        public string Text { get; set; }

        #endregion Properties

        #region Constructor

        public Social(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Text;
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `social` (`Text`) VALUES (@Text);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `social` SET `Text`=@Text WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Text",
                DbType = DbType.String,
                Value = Text,
            });
        }

        #endregion Public methods
    }
}