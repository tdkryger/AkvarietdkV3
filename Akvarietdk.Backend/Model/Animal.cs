﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace Akvarietdk.Backend.Model
{
    public class Animal : Abstracts.AbstractDto, IFishbaseEntry, ICommentableEntity
    {
        #region Enums

        public enum AnimalBioload { Unknown, Low, Medium, Heavy };

        public enum AnimalSwimLevel { Unknown, Bottom, Lower, LowerMiddle, Middle, MiddleUpper, Upper, Top, All };

        public enum AnimalPlantedTank { Unknown, Yes, No, Partial };

        public enum AnimalFiltration { Unknown, None, Low, Medium, Heavy };

        #endregion Enums

        #region Fields

        private DateTime _modifiedDate = DateTime.Now;
        private string _scientificNameText = string.Empty;

        #endregion Fields

        #region Properties

        #region From Interface

        public ScientificName ScientificName { get; set; }
        public IList<Tradename> Tradenames { get; set; } = new List<Tradename>();
        public IList<ScientificName> Synonyms { get; set; } = new List<ScientificName>();
        public Origin Origin { get; set; }
        public Temperature WaterTemperature { get; set; }
        public Hardness WaterHardness { get; set; }
        public PH WaterPh { get; set; }
        public string Description { get; set; } = string.Empty;
        public IList<FishbaseImage> Images { get; set; } = new List<FishbaseImage>();
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime ModifiedDate { get { return GetModifiedDate(); } set { _modifiedDate = value; } }
        public int ViewCount { get; set; } = 0;
        public IList<IComment> Comments { get; set; } = new List<IComment>();
        public string CompleteScientificName { get { return ScientificName.ToString(); } }
        public string ScientificNameText { get { return GetScientificNameText(); } set { _scientificNameText = value; } }

        #endregion From Interface

        public Order Order { get; set; }
        public SubOrder SubOrder { get; set; }
        public Family Family { get; set; }
        public AnimalGroup CommonGroup { get; set; }
        public Size Size { get; set; }
        public AnimalBioload Bioload { get; set; } = AnimalBioload.Unknown;
        public AnimalSwimLevel SwimLevel { get; set; } = AnimalSwimLevel.Unknown;
        public IList<Diet> DietaryRequirements { get; set; } = new List<Diet>();
        public IList<Illness> CommonIllnesses { get; set; } = new List<Illness>();
        public Temperament Temperament { get; set; }
        public IList<Social> Sociableness { get; set; } = new List<Social>();
        public int MingroupSize { get; set; } = 1;
        public AnimalPlantedTank PlantedTank { get; set; } = AnimalPlantedTank.Unknown;
        public decimal MinTankSize { get; set; }
        public AnimalFiltration MinFiltrationLevel { get; set; } = AnimalFiltration.Unknown;
        public IList<Habitat> PreferedHabitat { get; set; }
        public bool BreadInCaptivity { get; set; } = false;
        public bool RequiresWood { get; set; } = false;
        public User Contributor { get; set; }

        #endregion Properties

        #region Constructor

        public Animal(IAppDb appDb) : base(appDb)
        {
            ScientificName = new ScientificName(_appDb);
            Origin = new Origin(_appDb);
            WaterTemperature = new Temperature(_appDb);
            WaterHardness = new Hardness(_appDb);
            WaterPh = new PH(_appDb);
            CommonGroup = new AnimalGroup(_appDb) { Name = "Ukendt" };
            Size = new Size(_appDb);
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return this.ScientificName.ToString();
        }

        #endregion Public methods

        #region Private methods

        private DateTime GetModifiedDate()
        {
            if (_modifiedDate < CreateDate && _modifiedDate == DateTime.MinValue)
            {
                _modifiedDate = CreateDate;
            }
            return _modifiedDate;
        }

        private string GetScientificNameText()
        {
            if (string.IsNullOrEmpty(_scientificNameText))
            {
                _scientificNameText = ScientificName.ToString();
            }
            return _scientificNameText;
        }

        protected override string GetInsertSql()
        {
            throw new NotImplementedException();
        }

        protected override string GetUpdateSql()
        {
            throw new NotImplementedException();
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            throw new NotImplementedException();
        }

        #endregion Private methods
    }
}