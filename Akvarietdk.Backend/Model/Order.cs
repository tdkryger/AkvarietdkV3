﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Model
{
    public class Order : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; } = "Ukendt";
        public string OrderGroup { get; set; } = "Ukendt";
        public IList<SubOrder> SubOrders { get; set; } = new List<SubOrder>();

        #endregion Properties

        #region Constructor

        public Order(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public async  Task InsertAsync()
        {
            await base.InsertAsync();
            foreach (SubOrder item in SubOrders)
            {
                if (item.Id == 0)
                    await item.InsertAsync();
                else
                    await item.UpdateAsync();
            }
        }

        public async  Task UpdateAsync()
        {
            await base.UpdateAsync();
            foreach (SubOrder item in SubOrders)
            {
                if (item.Id == 0)
                    await item.InsertAsync();
                else
                    await item.UpdateAsync();
            }
        }

        public override string ToString()
        {
            return $"{OrderGroup} - {Name}";
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `order` (`Name`,`OrderGroup`) VALUES (@Name,@OrderGroup);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `order` SET `Name`=@Name,`OrderGroup`=@OrderGroup WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Name", Name));
            cmd.Parameters.Add(new MySqlParameter("@OrderGroup", OrderGroup));
        }

        #endregion Protected methods
    }
}