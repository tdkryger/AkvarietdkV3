﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;

namespace Akvarietdk.Backend.Model
{
    public class FishbaseVideo : Abstracts.AbstractDto
    {
        // TODO: How do we store a video file?
        public string Title { get; set; }

        public string Description { get; set; }
        public string Content { get; set; }
        public string CopyRight { get; set; } = "© Akvariet.dk";
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public string MediaType { get; set; }
        public byte[] Media { get; set; }

        #region Constructor

        public FishbaseVideo(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        protected override string GetInsertSql()
        {
            throw new NotImplementedException();
        }

        protected override string GetUpdateSql()
        {
            throw new NotImplementedException();
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            throw new NotImplementedException();
        }

        #endregion Public methods
    }
}