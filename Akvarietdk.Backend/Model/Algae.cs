﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Algae : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; }
        public string Symptoms { get; set; }
        public string Cause { get; set; }
        public string Treatment { get; set; }

        #endregion Properties

        #region Constructor

        public Algae(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `algea`(`Name`,`Cause`,`Symptoms`,`Treatment`) VALUES (@name,@cause,@symptoms,@treatment);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `algea` SET `Name`=@name,`Cause`=@cause,`Symptoms`=@symptoms,`Treatment`=@treatment WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@name",
                DbType = DbType.String,
                Value = Name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@symptoms",
                DbType = DbType.String,
                Value = Symptoms,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@cause",
                DbType = DbType.String,
                Value = Cause,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@treatment",
                DbType = DbType.String,
                Value = Treatment,
            });
        }

        #endregion Private methods
    }
}