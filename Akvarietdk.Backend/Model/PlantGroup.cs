﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class PlantGroup : Abstracts.AbstractDto
    {
        public string Text { get; set; }

        #region Constructor

        public PlantGroup(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Text;
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `plantgroup` (`Text`) VALUES (@Text);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `plantgroup` SET `Text`=@Text WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Text",
                DbType = DbType.String,
                Value = Text,
            });
        }

        #endregion Public methods
    }
}