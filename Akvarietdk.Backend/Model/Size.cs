﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Size : Abstracts.AbstractDto
    {
        #region Properties

        public Decimal Min { get; set; }
        public Decimal Max { get; set; }

        #endregion Properties

        #region Constructor

        public Size(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Min == 0 ? "Ukendt" : (Min == Max) || (Min > Max) ? $"Op til {Min:#0.#} cm" : $"{Min:#0.#}-{Max:#0.#} cm";
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `size` (`Max`,`Min`) VALUES (@Max,@Min);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `size` SET `Max`=@Max,`Min`=@Min WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Max",
                DbType = DbType.String,
                Value = Max,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Min",
                DbType = DbType.String,
                Value = Min,
            });
        }

        #endregion Public methods
    }
}