﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Position : Abstracts.AbstractDto
    {
        #region Properties

        public string Latititude { get; set; }
        public string Longitude { get; set; }
        public int Year { get; set; }
        public string NameUsed { get; set; }
        public string Depth { get; set; }

        #endregion Properties

        #region Constructor

        public Position(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `position` (`Depth`,`Latititude`,`Longitude`,`NameUsed`,`Year`) VALUES (@Depth,@Latititude,@Longitude,@NameUsed,@Year);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `position` SET `Depth`=@Depth,`Latititude`=@Latititude,`Longitude`=@Longitude,`NameUsed`=@NameUsed,`Year`=@Year WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Depth",
                DbType = DbType.String,
                Value = Depth,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Latititude",
                DbType = DbType.String,
                Value = Latititude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Longitude",
                DbType = DbType.String,
                Value = Longitude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@NameUsed",
                DbType = DbType.String,
                Value = NameUsed,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Year",
                DbType = DbType.Int32,
                Value = Year,
            });
        }

        #endregion Public methods
    }
}