﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Drawing;

namespace Akvarietdk.Backend.Model
{
    public class FishbaseImage : Abstracts.AbstractDto
    {
        #region Properties

        public virtual Image Image { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string Content { get; set; }
        public virtual string CopyRight { get; set; } = "© Akvariet.dk";
        public virtual DateTime CreateDate { get; set; } = DateTime.Now;

        #endregion Properties

        #region Constructor

        public FishbaseImage(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public Methods

        public override string ToString()
        {
            return Title;
        }

        public Image Thumbnail()
        {
            return Thumbnail(120, 120);
        }

        public Image Thumbnail(int width, int height)
        {
            return TDK.Tools.ImageTools.FixedSize(Image, width, height, Color.White);
        }

        #endregion Public Methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `fishbaseimage` (`Image`,`Content`,`Description`,`Title`,`CopyRight`,`CreateDate`) VALUES (@Image,@Content,@Description,@Title,@CopyRight,@CreateDate);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `fishbaseimage` SET `Image`=@Image,`Content`=@Content,`Description`=@Description,`Title`=@Title,`CopyRight`=@CopyRight,`CreateDate`=@CreateDate WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Image", Image));
            cmd.Parameters.Add(new MySqlParameter("@Content", Content));
            cmd.Parameters.Add(new MySqlParameter("@Description", Description));
            cmd.Parameters.Add(new MySqlParameter("@Title", Title));
            cmd.Parameters.Add(new MySqlParameter("@CopyRight", CopyRight));
            cmd.Parameters.Add(new MySqlParameter("@CreateDate", CreateDate));
        }

        #endregion Protected methods
    }
}