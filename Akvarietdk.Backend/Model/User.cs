﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class User : Abstracts.AbstractDto, IUser
    {
        #region Properties

        public string DisplayName { get; set; }
        public string Username { get; set; }
        public string EMail { get; set; }
        public UserLevels Role { get; set; } = UserLevels.None;
        public DateTime JoinDateTime { get; set; } = DateTime.Now;

        #endregion Properties

        #region Constructor

        public User(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor



        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `user`(`DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime`) VALUES (@displayname,@username,@email,@role,@joindatetime);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `user` SET `DisplayName`=@displayname,`Username`=@username,`EMail`=@email,`Role`=@role,`JoinDateTime`=@joindatetime WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@displayname",
                DbType = DbType.String,
                Value = DisplayName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@username",
                DbType = DbType.String,
                Value = Username,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = DbType.String,
                Value = EMail,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@role",
                DbType = DbType.Int32,
                Value = Role,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@joindatetime",
                DbType = DbType.DateTime,
                Value = JoinDateTime,
            });
        }

        #endregion Private methods
    }
}