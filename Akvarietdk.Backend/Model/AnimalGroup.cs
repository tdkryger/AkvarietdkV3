﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class AnimalGroup : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; } = string.Empty;
        public string Zone { get; set; } = string.Empty;
        public string AqualogCode { get; set; } = string.Empty;
        public bool Active { get; set; } = true;

        #endregion Properties

        #region Constructor

        public AnimalGroup(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public Methods

        public override string ToString()
        {
            return $"{Name}";// ({Zone})";
        }

        #endregion Public Methods

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `animalgroup` (`Name`,`AqualogCode`,`Zone`,`Active`) VALUES (@Name, @AqualogCode, @Zone, @Active); ";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `animalgroup` SET `Name`=@Name,`AqualogCode`=@AqualogCode,`Zone`=@Zone,`Active`=@Active WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Name",
                DbType = DbType.String,
                Value = Name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Zone",
                DbType = DbType.String,
                Value = Zone,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@AqualogCode",
                DbType = DbType.String,
                Value = AqualogCode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Active",
                DbType = DbType.Boolean,
                Value = Active,
            });
        }

        #endregion Private methods
    }
}