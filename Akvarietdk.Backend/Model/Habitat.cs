﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;

namespace Akvarietdk.Backend.Model
{
    public class Habitat : Abstracts.AbstractDto
    {
        #region Properties

        public string Text { get; set; }
        public string Description { get; set; }
        public int BiotopAkvarietId { get; set; }

        #endregion Properties

        #region Constructor

        public Habitat(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Text;
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `habitat` (`Text`,`Description`,`BiotopAkvarietId`) VALUES (@Text,@Description,@BiotopAkvarietId);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `habitat` SET `Text`=@Text,`Description`=@Description,`BiotopAkvarietId`=@BiotopAkvarietId WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Text", Text));
            cmd.Parameters.Add(new MySqlParameter("@Description", Description));
            cmd.Parameters.Add(new MySqlParameter("@BiotopAkvarietId", BiotopAkvarietId));
        }

        #endregion Protected methods
    }
}