﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Origin : Abstracts.AbstractDto
    {
        #region Properties

        public string Text { get; set; }
        public string SubOrigin { get; set; }

        #endregion Properties

        #region Constructor

        public Origin(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            if (string.IsNullOrEmpty(SubOrigin))
                return Text;
            if (Text.Equals(SubOrigin))
                return Text;
            return $"{Text} ({SubOrigin})";
        }

        #endregion Public methods

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `origin` (`Text`,`SubOrigin`) VALUES (@Text,@SubOrigin);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `origin` SET `Text`=@Text,`SubOrigin`=@SubOrigin WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Text",
                DbType = DbType.String,
                Value = Text,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@SubOrigin",
                DbType = DbType.String,
                Value = SubOrigin,
            });
        }

        #endregion Private methods
    }
}