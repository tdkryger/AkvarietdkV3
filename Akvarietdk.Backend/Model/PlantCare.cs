﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;

namespace Akvarietdk.Backend.Model
{
    public class PlantCare : Abstracts.AbstractDto
    {
        //TODO: Add language. Missing from DB
        public string Text { get; set; }

        #region Constructor

        public PlantCare(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor



        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `plantcare` (`Text`) VALUES (@Text); ";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `plantcare` SET `Text`=@Text WHERE `Id`=@Id; ";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Text", Text));
        }

        #endregion Protected methods
    }
}