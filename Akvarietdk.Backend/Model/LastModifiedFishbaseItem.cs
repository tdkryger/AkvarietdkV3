﻿using System;

namespace Akvarietdk.Backend.Model
{
    public class LastModifiedFishbaseItem
    {
        public int Id { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ScientificNameText { get; set; }

        public override string ToString()
        {
            return ScientificNameText;
        }
    }
}