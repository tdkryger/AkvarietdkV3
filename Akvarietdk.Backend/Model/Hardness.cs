﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;

namespace Akvarietdk.Backend.Model
{
    public class Hardness : Abstracts.AbstractDto
    {
        #region Properties

        public Decimal Min { get; set; }
        public Decimal Max { get; set; }

        #endregion Properties

        #region Constructor

        public Hardness(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            if (Min == Max || Min == 0)
                return "Ukendt";
            if (Min == Max || Min > Max)
                return $"{Min:#0.#}";
            return $"{Min:#0.#}-{Max:#0.#}";
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `hardness` (`Min`,`Max`) VALUES (@Min,@Max);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `hardness` SET `Id`=@Id,`Min`=@Min,`Max`=@Max WHERE `Id`=@Id; ";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Min", Min));
            cmd.Parameters.Add(new MySqlParameter("@Max", Max));
        }

        #endregion Protected methods
    }
}