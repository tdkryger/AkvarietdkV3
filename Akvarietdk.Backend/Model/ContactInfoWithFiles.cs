﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Model
{
    public class ContactInfoWithFiles : Abstracts.AbstractDto
    {
        public string FromName { get; set; }
        public string FromEMail { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public IList<DBFile> Files { get; set; } = new List<DBFile>();
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string TimeZone { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MetroCode { get; set; }

        #region Constructor

        public ContactInfoWithFiles(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public async new Task InsertAsync()
        {
            await base.InsertAsync();
            foreach (DBFile file in Files)
            {
                if (file.Id == 0)
                    await file.InsertAsync();
                else
                    await file.UpdateAsync();
            }
        }

        public async new Task UpdateAsync()
        {
            await base.UpdateAsync();
            foreach (DBFile file in Files)
            {
                if (file.Id == 0)
                    await file.InsertAsync();
                else
                    await file.UpdateAsync();
            }
        }

        #endregion Public methods

        #region Private methods

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Content",
                DbType = DbType.String,
                Value = Content,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@FromEMail",
                DbType = DbType.String,
                Value = FromEMail,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@FromName",
                DbType = DbType.String,
                Value = FromName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Subject",
                DbType = DbType.String,
                Value = Subject,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Ip",
                DbType = DbType.String,
                Value = Ip,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CountryCode",
                DbType = DbType.String,
                Value = CountryCode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@CountryName",
                DbType = DbType.String,
                Value = CountryName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@RegionCode",
                DbType = DbType.String,
                Value = RegionCode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@RegionName",
                DbType = DbType.String,
                Value = RegionName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@City",
                DbType = DbType.String,
                Value = City,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ZipCode",
                DbType = DbType.String,
                Value = ZipCode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TimeZone",
                DbType = DbType.String,
                Value = TimeZone,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Latitude",
                DbType = DbType.String,
                Value = Latitude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Longitude",
                DbType = DbType.String,
                Value = Longitude,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@MetroCode",
                DbType = DbType.String,
                Value = MetroCode,
            });
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `contactinfo`(`Content`,`FromEMail`,`FromName`,`Subject`,`Ip`,`CountryCode`,`CountryName`,`RegionCode`,`RegionName`,`City`,`ZipCode`,`TimeZone`,`Latitude`,`Longitude`,`MetroCode`) VALUES (@Content,@FromEMail,@FromName,@Subject,@Ip,@CountryCode,@CountryName,@RegionCode,@RegionName,@City,@ZipCode,@TimeZone,@Latitude,@Longitude,@MetroCode);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `contactinfo` SET `Content`=@Content,`FromEMail`=@FromEMail,`FromName`=@FromName,`Subject`=@Subject,`Ip`=@Ip,`CountryCode`=@CountryCode,`CountryName`=@CountryName,`RegionCode`=@RegionCode,`RegionName`=@RegionName,`City`=@City,`ZipCode`=@ZipCode,`TimeZone`=@TimeZone,`Latitude`=@Latitude,`Longitude`=@Longitude,`MetroCode`=@MetroCode  WHERE `Id`=@Id;";
        }

        #endregion Private methods
    }
}