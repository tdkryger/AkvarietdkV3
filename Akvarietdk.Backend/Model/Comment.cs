﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Comment : Abstracts.AbstractDto, IComment
    {
        #region Properties

        public IUser User { get; set; }
        public DateTime PostDate { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public Marks Mark { get; set; } = Marks.Posted;
        public int Score { get; set; } = 0;
        public long BlogEntity_id { get; set; }
        public long Animal_id { get; set; }
        public long Plant_id { get; set; }

        #endregion Properties

        #region Constructor

        public Comment(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor



        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `comment` (`PostDate`,`Title`,`Body`,`Mark`,`Score`,`User_id`,`BlogEntity_id`,`Animal_id`,`Plant_id`) VALUES (@PostDate,@Title,@Body,@Mark,@Score,@User_id,@BlogEntity_id,@Animal_id,@Plant_id);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `algea` SET `Name`=@name,`Cause`=@cause,`Symptoms`=@symptoms,`Treatment`=@treatment WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@User_id",
                DbType = DbType.String,
                Value = User.Id,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PostDate",
                DbType = DbType.String,
                Value = PostDate,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Title",
                DbType = DbType.String,
                Value = Title,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Body",
                DbType = DbType.String,
                Value = Body,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Mark",
                DbType = DbType.String,
                Value = (int)Mark,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Score",
                DbType = DbType.String,
                Value = Score,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@BlogEntity_id",
                DbType = DbType.String,
                Value = BlogEntity_id,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Animal_id",
                DbType = DbType.String,
                Value = Animal_id,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Plant_id",
                DbType = DbType.String,
                Value = Plant_id,
            });
        }

        #endregion Private methods
    }
}