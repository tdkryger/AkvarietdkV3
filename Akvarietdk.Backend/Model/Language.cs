﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Language : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; }
        public string Code { get; set; }

        #endregion Properties

        #region Constructor

        public Language(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return $"{Name} ({Code})";
        }

        #endregion Public methods

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `language` (`Name`,`Code`) VALUES (@Name,@Code);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `language` SET `Name`=@Name,`Code`=@Code WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Name",
                DbType = DbType.String,
                Value = Name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Code",
                DbType = DbType.String,
                Value = Code,
            });
        }

        #endregion Private methods
    }
}