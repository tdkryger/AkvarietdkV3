﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Diet : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; }

        #endregion Properties

        #region Constructor

        public Diet(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Name;
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `diet` (`Name`) VALUES (@Name);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `diet` SET `Name`=@Name WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Name",
                DbType = DbType.String,
                Value = Name,
            });
        }

        #endregion Protected methods
    }
}