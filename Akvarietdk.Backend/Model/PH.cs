﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class PH : Abstracts.AbstractDto
    {
        #region Properties

        public Decimal Min { get; set; }
        public Decimal Max { get; set; }
        public bool AddSalt { get; set; } = false;

        #endregion Properties

        #region Constructor

        public PH(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return $"{Min:#0.#}-{Max:#0.#}";
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `ph` (`Min`,`Max`,`AddSalt`) VALUES (@Min,@Max,@AddSalt);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `ph` SET `Min`=@Min,`Max`=@Max,`AddSalt`=@AddSalt WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Min",
                DbType = DbType.String,
                Value = Min,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Max",
                DbType = DbType.String,
                Value = Max,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@AddSalt",
                DbType = DbType.Int16,
                Value = AddSalt,
            });
        }

        #endregion Public methods
    }
}