﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class PlantCharacteristic : Abstracts.AbstractDto
    {
        public string Text { get; set; }

        #region Constructor

        public PlantCharacteristic(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `plantcharacteristic` (`Text`) VALUES (@Text);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `plantcharacteristic` SET `Text`=@Text WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Text",
                DbType = DbType.String,
                Value = Text,
            });
        }

        #endregion Public methods
    }
}