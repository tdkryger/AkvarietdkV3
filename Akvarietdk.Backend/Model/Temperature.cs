﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class Temperature : Abstracts.AbstractDto
    {
        #region Properties

        public Decimal Min { get; set; }
        public Decimal Max { get; set; }

        #endregion Properties

        #region Constructor

        public Temperature(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return $"{Min:#0.#}-{Max:#0.#}°C";
        }

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `temperature` (`Min`,`Max`) VALUES (@Min,@Max);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `temperature` SET `Min`=@Min,`Max`=@Max WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Min",
                DbType = DbType.String,
                Value = Min,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Max",
                DbType = DbType.String,
                Value = Max,
            });
        }

        #endregion Public methods
    }
}