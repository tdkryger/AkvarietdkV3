﻿namespace Akvarietdk.Backend.Model
{
    public class IntStringClass
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Selected { get; set; }
    }
}