﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class TagEntity : Abstracts.AbstractDto
    {
        public string TagName { get; set; }

        #region Constructor

        public TagEntity(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `tagentity` (`TagName`) VALUES (@TagName);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `tagentity` SET `TagName`=@TagName WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@TagName",
                DbType = DbType.String,
                Value = TagName,
            });
        }

        #endregion Public methods
    }
}