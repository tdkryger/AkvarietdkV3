﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class ScientificName : Abstracts.AbstractDto
    {
        #region Properties

        public string Genus { get; set; }
        public string Species { get; set; }

        #endregion Properties

        #region Constructors

        public ScientificName(IAppDb appDb) : base(appDb)
        {
            Genus = string.Empty;
            Species = string.Empty;
        }

        public ScientificName(string text, IAppDb appDb) : base(appDb)
        {
            var idx = text.IndexOf(' ');
            if (idx > 0)
            {
                Genus = text.Substring(0, idx).Trim();
                Species = text.Substring(idx).Trim();
            }
            else
            {
                Genus = text;
                Species = string.Empty;
            }
        }

        #endregion Constructors

        #region Public methods

        public override string ToString()
        {
            return $"{Genus} {Species}";
        }

        #endregion Public methods

        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `scientificname` (`Genus`,`Species`) VALUES (@Genus,@Species);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `scientificname` SET `Genus`=@Genus,`Species`=@Species WHERE `Id` = @id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Genus",
                DbType = DbType.String,
                Value = Genus,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Species",
                DbType = DbType.String,
                Value = Species,
            });
        }

        #endregion Private methods
    }
}