﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class BlogEntity : Abstracts.AbstractDto, ICommentableEntity
    {
        #region Fields

        private const string _selectSql = "SELECT `Id`,`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`,`Image`,`Content`,`Description`,`ImageTitle`,`CopyRight`,`CreateDate`,`UserId`,`DisplayName`,`Username`,`EMail`,`Role`,`JoinDateTime` FROM `view_blogentity` ";

        #endregion Fields

        public string Title { get; set; }
        public string Teaser { get; set; }
        public string Body { get; set; }
        public DateTime Posted { get; set; }
        public long PostRead { get; set; }
        public IList<TagEntity> Tags { get; set; } = new List<TagEntity>();
        public FishbaseImage Image { get; set; }
        public User Contributor { get; set; }
        public bool Deleted { get; set; } = false;
        public DateTime ReleaseDate { get; set; }
        public IList<IComment> Comments { get; set; } = new List<IComment>();

        #region Constructor

        public BlogEntity(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor



        #region Private methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `blogentity` (`Title`,`Teaser`,`Body`,`Posted`,`PostRead`,`Image_id`,`Contributor_id`) VALUES (@Title,@Teaser,@Body,@Posted,@PostRead,@Image_id,@Contributor_id);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `blogentity` SET `Title` = @Title,`Teaser` = @Teaser,`Body` = @Body,`Posted` = @Posted,`PostRead` = @PostRead,`Image_id` = @Image_id,`Contributor_id` = @Contributor_id WHERE `Id`=@id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Title",
                DbType = DbType.String,
                Value = Title,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Teaser",
                DbType = DbType.String,
                Value = Teaser,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Body",
                DbType = DbType.String,
                Value = Body,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Posted",
                DbType = DbType.DateTime,
                Value = Posted,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@PostRead",
                DbType = DbType.Int64,
                Value = PostRead,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Image_id",
                DbType = DbType.Int64,
                Value = Image.Id,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Contributor_id",
                DbType = DbType.Int64,
                Value = Contributor.Id,
            });
        }

        #endregion Private methods
    }
}