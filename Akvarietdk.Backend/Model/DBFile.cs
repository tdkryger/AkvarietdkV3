﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System.Data;

namespace Akvarietdk.Backend.Model
{
    public class DBFile : Abstracts.AbstractDto
    {
        #region Properties

        public byte[] FileContent { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public long ContactInfoWithFiles_Id { get; set; }

        #endregion Properties

        #region Constructor

        public DBFile(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `dbfile` (`ContentType`,`Filename`,`FileContent`,`ContactInfoWithFiles_id`) VALUES (@ContentType,@Filename,@FileContent,@ContactInfoWithFiles_id);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `dbfile` SET `ContentType`=@ContentType,`Filename`=@Filename,`FileContent`=@FileContent,`ContactInfoWithFiles_id`=@ContactInfoWithFiles_id WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            // @ContentType,@Filename,@FileContent,@ContactInfoWithFiles_id
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ContentType",
                DbType = DbType.String,
                Value = ContentType,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@Filename",
                DbType = DbType.String,
                Value = Filename,
            });
            cmd.Parameters.Add(new MySqlParameter("@FileContent", FileContent));
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ContactInfoWithFiles_id",
                DbType = DbType.Int64,
                Value = ContactInfoWithFiles_Id,
            });
        }

        #endregion Protected methods
    }
}