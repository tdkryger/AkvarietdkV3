﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;

namespace Akvarietdk.Backend.Model
{
    public class Illness : Abstracts.AbstractDto
    {
        #region Properties

        public string Name { get; set; }
        public string Group { get; set; }
        public string Symptoms { get; set; }
        public string Cause { get; set; }
        public string Treatment { get; set; }

        #endregion Properties

        #region Constructor

        public Illness(IAppDb appDb) : base(appDb)
        {
        }

        #endregion Constructor

        #region Public methods

        public override string ToString()
        {
            return Name;
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `illness` (`Name`,`illnessGroup`,`Cause`,`Symptoms`,`Treatment`) VALUES (@Name,@illnessGroup,@Cause,@Symptoms,@Treatment);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `illness` SET `Id`=@Id,`Name`=@Name,`illnessGroup`=@illnessGroup,`Cause`=@Cause,`Symptoms`=@Symptoms,`Treatment`=@Treatment WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            // @Name,@illnessGroup,@Cause,@Symptoms,@Treatment
            cmd.Parameters.Add(new MySqlParameter("@Name", Name));
            cmd.Parameters.Add(new MySqlParameter("@illnessGroup", Group));
            cmd.Parameters.Add(new MySqlParameter("@Cause", Cause));
            cmd.Parameters.Add(new MySqlParameter("@Symptoms", Symptoms));
            cmd.Parameters.Add(new MySqlParameter("@Treatment", Treatment));
        }

        #endregion Protected methods
    }
}