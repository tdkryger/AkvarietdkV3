﻿using System;

namespace Akvarietdk.Backend.Model
{
    public class LastModifiedBlog
    {
        public int Id { get; set; }
        public DateTime Posted { get; set; }
        public string Title { get; set; }
    }
}