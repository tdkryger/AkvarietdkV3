﻿using Akvarietdk.Backend.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Model
{
    public class Plant : Abstracts.AbstractDto, IFishbaseEntry, ICommentableEntity
    {
        #region Fields

        private DateTime _modifiedDate = DateTime.Now;
        private string _scientificNameText = string.Empty;

        #endregion Fields

        #region Enums

        public enum PlantTankPositions { Unknown, Front, Middle, Back, Solitary, Floating };

        public enum PlantCO2Requirements { Unknown, None, Low, Medium, High };

        public enum PlantGrowthSpeed { Unknown, VerySlow, Slow, Moderate, Fast, VeryFast };

        #endregion Enums

        #region Properties

        #region From interface

        public ScientificName ScientificName { get; set; }
        public IList<Tradename> Tradenames { get; set; } = new List<Tradename>();
        public IList<ScientificName> Synonyms { get; set; } = new List<ScientificName>();
        public Origin Origin { get; set; }
        public Temperature WaterTemperature { get; set; }
        public Hardness WaterHardness { get; set; }
        public PH WaterPh { get; set; }
        public string Description { get; set; } = string.Empty;
        public IList<FishbaseImage> Images { get; set; } = new List<FishbaseImage>();
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime ModifiedDate { get { return GetModifiedDate(); } set { _modifiedDate = value; } }
        public int ViewCount { get; set; } = 0;
        public IList<IComment> Comments { get; set; } = new List<IComment>();
        public string ScientificNameText { get { return GetScientificNameText(); } set { _scientificNameText = value; } }

        #endregion From interface

        public PlantTankPositions TankPosition { get; set; } = PlantTankPositions.Unknown;
        public Size Height { get; set; }
        public Size Width { get; set; }
        public Light LightRequirements { get; set; }
        public PlantCO2Requirements CO2Requirements { get; set; } = PlantCO2Requirements.Unknown;
        public PlantGroup PlantGroup { get; set; }
        public PlantGrowthSpeed GrowthSpeed { get; set; } = PlantGrowthSpeed.Unknown;
        public IList<PlantCare> Care { get; set; } = new List<PlantCare>();
        public IList<PlantCharacteristic> Characteristics { get; set; } = new List<PlantCharacteristic>();
        public User Contributor { get; set; }

        #endregion Properties

        #region Constructor

        public Plant(IAppDb appDb) : base(appDb)
        {
            ScientificName = new ScientificName(_appDb);
            Origin = new Origin(_appDb);
            WaterTemperature = new Temperature(_appDb);
            WaterHardness = new Hardness(_appDb);
            WaterPh = new PH(_appDb);
            Width = new Size(_appDb);
            Height = new Size(_appDb);
            LightRequirements = new Light(_appDb);
        }

        #endregion Constructor

        #region Public methods

        public async Task InsertAsync()
        {
            using (var trans = await _appDb.Connection.BeginTransactionAsync().ConfigureAwait(false))
            {
                try
                {
                    using (var cmd = _appDb.Connection.CreateCommand() as MySqlCommand)
                    {
                        cmd.Transaction = trans;
                        cmd.CommandText = GetInsertSql();
                        BindParams(cmd);
                        await cmd.ExecuteNonQueryAsync();
                        Id = (int)cmd.LastInsertedId;
                    }

                    await SaveOrUpdateListAsync(trans);
                    await trans.CommitAsync().ConfigureAwait(false);
                }
                catch
                {
                    await trans.RollbackAsync().ConfigureAwait(false);
                    throw;
                }
            }
        }

        public async Task UpdateAsync()
        {
            using (var trans = await _appDb.Connection.BeginTransactionAsync().ConfigureAwait(false))
            {
                try
                {
                    await base.UpdateAsync();
                    await SaveOrUpdateListAsync(trans);
                }
                catch
                {
                    await trans.RollbackAsync().ConfigureAwait(false);
                    throw;
                }
            }
        }

        #endregion Public methods

        #region Protected methods

        protected override string GetInsertSql()
        {
            return @"INSERT INTO `plant` (`Description`,`CreateDate`,`ModifiedDate`,`ViewCount`,`scientificNameText`,`TankPosition`,`CO2Requirements`,`GrowthSpeed`,`ScientificName_id`,`Origin_id`,`WaterTemperature_id`,`WaterHardness_id`,`WaterPh_id`,`Height_id`,`Width_id`,`LightRequirements_id`,`PlantGroup_id`,`Contributor_id`) VALUES (@Description,@CreateDate,@ModifiedDate,@ViewCount,@scientificNameText,@TankPosition,@CO2Requirements,@GrowthSpeed,@ScientificName_id,@Origin_id,@WaterTemperature_id,@WaterHardness_id,@WaterPh_id,@Height_id,@Width_id,@LightRequirements_id,@PlantGroup_id,@Contributor_id);";
        }

        protected override string GetUpdateSql()
        {
            return @"UPDATE `plant` SET `Description`=@Description,`CreateDate`=@CreateDate,`ModifiedDate`=@ModifiedDate,`ViewCount`=@ViewCount,`scientificNameText`=@scientificNameText,`TankPosition`=@TankPosition,`CO2Requirements`=@CO2Requirements,`GrowthSpeed`=@GrowthSpeed,`ScientificName_id`=@ScientificName_id,`Origin_id`=@Origin_id,`WaterTemperature_id`=@WaterTemperature_id,`WaterHardness_id`=@WaterHardness_id,`WaterPh_id`=@WaterPh_id,`Height_id`=@Height_id,`Width_id`=@Width_id,`LightRequirements_id`=@LightRequirements_id,`PlantGroup_id`=@PlantGroup_id,`Contributor_id`=@Contributor_id WHERE `Id`=@Id;";
        }

        protected override void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter("@Description", Description));
            cmd.Parameters.Add(new MySqlParameter("@CreateDate", CreateDate));
            cmd.Parameters.Add(new MySqlParameter("@ModifiedDate", ModifiedDate));
            cmd.Parameters.Add(new MySqlParameter("@scientificNameText", ScientificNameText));
            cmd.Parameters.Add(new MySqlParameter("@TankPosition", TankPosition));
            cmd.Parameters.Add(new MySqlParameter("@CO2Requirements", CO2Requirements));
            cmd.Parameters.Add(new MySqlParameter("@GrowthSpeed", GrowthSpeed));
            cmd.Parameters.Add(new MySqlParameter("@ScientificName_id", ScientificName.Id));
            cmd.Parameters.Add(new MySqlParameter("@Origin_id", Origin.Id));
            cmd.Parameters.Add(new MySqlParameter("@WaterTemperature_id", WaterTemperature.Id));
            cmd.Parameters.Add(new MySqlParameter("@WaterHardness_id", WaterHardness.Id));
            cmd.Parameters.Add(new MySqlParameter("@WaterPh_id", WaterPh.Id));
            cmd.Parameters.Add(new MySqlParameter("@Height_id", Height.Id));
            cmd.Parameters.Add(new MySqlParameter("@Width_id", Width.Id));
            cmd.Parameters.Add(new MySqlParameter("@LightRequirements_id", LightRequirements.Id));
            cmd.Parameters.Add(new MySqlParameter("@PlantGroup_id", PlantGroup.Id));
            cmd.Parameters.Add(new MySqlParameter("@Contributor_id", Contributor.Id));
        }

        #endregion Protected methods

        #region Private methods

        private async Task SaveOrUpdateListAsync(MySqlTransaction mySqlTransaction)
        {
            //TODO: Remember link tables
            /*
             'scientificnametoplant'
             'fishbaseimagetoplant'
             'plantcaretoplant'
             'tradenametoplant'
             */
            foreach (Tradename item in Tradenames)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
            }
            await _appDb.ExecuteNonQueryAsync($"DELETE FROM `scientificnametoplant` WHERE `Plant_id`={this.Id};", mySqlTransaction);
            foreach (ScientificName item in Synonyms)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
                await _appDb.ExecuteNonQueryAsync($"INSERT INTO `scientificnametoplant` (`Plant_id`,`ScientificName_id`) VALUES({this.Id}, {item.Id});", mySqlTransaction);
            }
            foreach (FishbaseImage item in Images)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
            }
            foreach (Comment item in Comments)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
            }
            foreach (PlantCare item in Care)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
            }
            foreach (PlantCharacteristic item in Characteristics)
            {
                if (item.Id == 0)
                    await item.InsertAsync(mySqlTransaction);
                else
                    await item.UpdateAsync(mySqlTransaction);
            }
        }

        private DateTime GetModifiedDate()
        {
            if (_modifiedDate < CreateDate && _modifiedDate == DateTime.MinValue)
            {
                _modifiedDate = CreateDate;
            }
            return _modifiedDate;
        }

        private string GetScientificNameText()
        {
            if (string.IsNullOrEmpty(_scientificNameText))
            {
                _scientificNameText = ScientificName.ToString();
            }
            return _scientificNameText;
        }

        #endregion Private methods
    }
}