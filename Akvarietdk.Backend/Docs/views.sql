CREATE OR REPLACE VIEW view_Animal AS
SELECT 
	`animal`.`Id` AS animalid,
    `animal`.`Description`,
    `animal`.`CreateDate`,
    `animal`.`ModifiedDate`,
    `animal`.`ViewCount`,
    `animal`.`scientificNameText`,
    `animal`.`Bioload`,
    `animal`.`SwimLevel`,
    `animal`.`MingroupSize`,
    `animal`.`PlantedTank`,
    `animal`.`MinTankSize`,
    `animal`.`MinFiltrationLevel`,
    `animal`.`BreadInCaptivity`,
    `animal`.`RequiresWood`,
	`scientificname`.`Id` AS scientificnameId,
    `scientificname`.`Genus`,
    `scientificname`.`Species`,
	`origin`.`Id` AS originId,
    `origin`.`Text` AS originText,
    `origin`.`SubOrigin`,
	`temperature`.`Id` AS temperatureId,
    `temperature`.`Min` AS temperatureMin,
    `temperature`.`Max` AS temperauteMax,
	`hardness`.`Id` AS hardnessId,
    `hardness`.`Min` AS hardnessMin,
    `hardness`.`Max` AS hardnessMax,
	`ph`.`Id` AS pHId,
    `ph`.`Min` AS phMin,
    `ph`.`Max` AS phMax,
    `ph`.`AddSalt`,
	`order`.`Id` AS orderId,
    `order`.`Name` AS orderName,
    `order`.`OrderGroup`,
	`suborder`.`Id` AS suborderId,
    `suborder`.`Name` AS suborderName,
    `suborder`.`Order_id` AS suborderOrderId,
	`family`.`Id` AS familyId,
    `family`.`Name` AS familyName,
	`animalgroup`.`Id` AS animalGroupId,
    `animalgroup`.`Name` AS animalGroupName,
    `animalgroup`.`AqualogCode`,
    `animalgroup`.`Zone`,
    `animalgroup`.`Active`,
	`size`.`Id` AS sizeId,
    `size`.`Max` AS sizeMax,
    `size`.`Min` AS sizeMin,
	`user`.`Id` AS userId,
    `user`.`DisplayName`,
    `user`.`Username`,
    `user`.`EMail`,
    `user`.`Role`,
    `user`.`JoinDateTime`
FROM `animal`
INNER JOIN `scientificname` ON `animal`.`ScientificName_id`=`scientificname`.`Id`
INNER JOIN `origin` ON `animal`.`Origin_id`=`origin`.`Id`
INNER JOIN `temperature` ON `animal`.`WaterTemperature_id`=`temperature`.`Id`
INNER JOIN `hardness` ON `animal`.`WaterHardness_id`=`hardness`.`Id`
INNER JOIN `ph` ON `animal`.`WaterPh_id`=`ph`.`Id`
INNER JOIN `order` ON `animal`.`Order_id`=`order`.`Id`
INNER JOIN `suborder` ON `animal`.`SubOrder_id`=`suborder`.`Id`
INNER JOIN `family` ON `animal`.`Family_id`=`family`.`Id`
INNER JOIN `animalgroup` ON `animal`.`CommonGroup_id`=`animalgroup`.`Id`
INNER JOIN `size` ON `animal`.`Size_id`=`size`.`Id`
LEFT JOIN `user` ON `animal`.`Contributor_id`=`user`.`Id`;

-- ALTER TABLE `blogentity` ADD `ReleaseDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;
-- ALTER TABLE `blogentity` ADD `Deleted` TINYINT(1) NOT NULL DEFAULT '0';
-- UPDATE `blogentity` SET `ReleaseDate`=`Posted`;

CREATE OR REPLACE VIEW view_blogentity AS
SELECT `blogentity`.`Id`,
    `blogentity`.`Title`,
    `blogentity`.`Teaser`,
    `blogentity`.`Body`,
    `blogentity`.`Posted`,
    `blogentity`.`PostRead`,
    `blogentity`.`ReleaseDate`,
    `blogentity`.`Deleted`,
	`fishbaseimage`.`Id` AS Image_id,
    `fishbaseimage`.`Image`,
    `fishbaseimage`.`Content`,
    `fishbaseimage`.`Description`,
    `fishbaseimage`.`Title` AS ImageTitle,
    `fishbaseimage`.`CopyRight`,
    `fishbaseimage`.`CreateDate`,
	`user`.`Id` AS UserId,
    `user`.`DisplayName`,
    `user`.`Username`,
    `user`.`EMail`,
    `user`.`Role`,
    `user`.`JoinDateTime`
FROM `blogentity`
INNER JOIN `fishbaseimage` ON `blogentity`.`Image_id`=`fishbaseimage`.`Id`
INNER JOIN `user` ON `blogentity`.`Contributor_id`= `user`.`Id`
WHERE `ReleaseDate` < CURRENT_TIMESTAMP AND `Deleted`=0;



