﻿using Akvarietdk.Backend.Interfaces;
using Akvarietdk.Backend.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Akvarietdk.Backend
{
    public class BackendRepository : IDisposable
    {
        #region Fields
        private IAppDb _appDb;
        private readonly AlgaeRepository _algaeRepository;

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _appDb.Connection.Close();
                    _appDb = null;
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BackendRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
        #endregion

        #region Properties

        #endregion

        #region Constructor
        public BackendRepository(string dbConnectionString)
        {
            _appDb = new AppDb(dbConnectionString);
            _appDb.Connection.Open();
            _algaeRepository = new AlgaeRepository(_appDb);
        }
        #endregion

        #region Public mehtods

        #endregion

        #region Private methods

        #endregion
    }
}
