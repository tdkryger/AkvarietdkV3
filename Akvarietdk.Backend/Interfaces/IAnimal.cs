﻿using Akvarietdk.Backend.Model;
using System;
using System.Collections.Generic;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IAnimal
    {
        string ScientificName { get; set; }
        string Tradenames { get; set; }
        string Synonyms { get; set; }
        string Origin { get; set; }
        string Temperature { get; set; }
        string Hardness { get; set; }
        string Ph { get; set; }
        string Describtion { get; set; }
        List<FishbaseImage> ImageList { get; set; }
        DateTime CreateDate { get; set; }
        DateTime ModifiedDate { get; set; }
        int ViewCount { get; set; }
        string PDFUrl { get; }
    }
}