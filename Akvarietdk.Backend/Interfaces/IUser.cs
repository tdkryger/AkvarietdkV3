﻿using Akvarietdk.Backend.Model;
using System;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IUser
    {
        long Id { get; set; }
        string DisplayName { get; set; }
        string Username { get; set; }
        string EMail { get; set; }
        UserLevels Role { get; set; }
        DateTime JoinDateTime { get; set; }
    }
}