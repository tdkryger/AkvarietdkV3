﻿using MySql.Data.MySqlClient;
using System;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IAppDb : IDisposable
    {
        MySqlConnection Connection { get; }

        Task<int> ExecuteNonQueryAsync(string sql, MySqlTransaction mySqlTransaction=null);

        Task<int> ExecuteNonQueryAsync(string[] sql, MySqlTransaction mySqlTransaction = null);
    }
}