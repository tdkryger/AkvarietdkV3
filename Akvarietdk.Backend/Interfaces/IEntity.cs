﻿namespace Akvarietdk.Backend.Interfaces
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}