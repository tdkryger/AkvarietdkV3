﻿using System.Collections.Generic;

namespace Akvarietdk.Backend.Interfaces
{
    public interface ICommentableEntity : IEntity
    {
        IList<IComment> Comments { get; set; }
    }
}