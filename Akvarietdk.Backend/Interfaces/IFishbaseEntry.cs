﻿using Akvarietdk.Backend.Model;
using System;
using System.Collections.Generic;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IFishbaseEntry
    {
        long Id { get; set; }
        ScientificName ScientificName { get; set; }
        IList<Tradename> Tradenames { get; set; }
        IList<ScientificName> Synonyms { get; set; }
        Origin Origin { get; set; }
        Temperature WaterTemperature { get; set; }
        Hardness WaterHardness { get; set; }
        PH WaterPh { get; set; }
        String Description { get; set; }
        IList<FishbaseImage> Images { get; set; }

        DateTime CreateDate { get; set; }
        DateTime ModifiedDate { get; set; }
        int ViewCount { get; set; }
        string ScientificNameText { get; set; }
    }
}