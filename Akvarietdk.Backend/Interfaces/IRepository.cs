﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IRepository<T> where T : IEntity
    {
        Task<T> FindOneAsync(long id);

        Task<List<T>> LatestPostsAsync(int count = 10);

        Task<List<T>> GetAsync(List<Support.SearchParameters> searchParameters, string orderByFieldName = null, int skip = 0, int? take = null);

        //Task<List<T>> GetAsync<T>(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null, int? skip = null, int? take = null) where T : class, IEntity;
        //Task<int> GetCountAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class, IEntity;
        //Task<bool> GetExistsAsync<TEntity>(Expression<Func<TEntity, bool>> filter = null) where TEntity : class, IEntity;
    }
}