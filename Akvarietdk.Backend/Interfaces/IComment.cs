﻿using Akvarietdk.Backend.Model;
using System;

namespace Akvarietdk.Backend.Interfaces
{
    public interface IComment
    {
        Marks Mark { get; set; }
        DateTime PostDate { get; set; }
        string Title { get; set; }
        string Body { get; set; }
        int Score { get; set; }
        IUser User { get; set; }
    }
}