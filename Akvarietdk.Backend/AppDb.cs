﻿using Akvarietdk.Backend.Interfaces;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using MySqlConnector.Logging;
using System;
using System.Threading.Tasks;

namespace Akvarietdk.Backend
{
    public class AppDb : IAppDb
    {
        #region Fields

        private readonly ILoggerFactory _loggerFactory;
        private static NLog.Logger _logger;
        protected string _connectionString = "Server=localhost;Database=test_akvariet_dk_db2;Uid=root;Pwd=onakit8m;ConvertZeroDateTime=True;";

        #endregion Fields

        #region Properties

        public MySqlConnection Connection { get; protected set; }

        #endregion Properties

        #region Constructors, Dispose and Destructors

        public AppDb()
        {
            _connectionString = Akvariet.Common.AkvarietConfiguration.MySQLConnectionString;
            _loggerFactory = null;
            _logger = NLog.LogManager.GetCurrentClassLogger();
            Connection = new MySqlConnection(_connectionString);
        }

        public AppDb(ILoggerFactory loggerFactory)
        {
            _connectionString = Akvariet.Common.AkvarietConfiguration.MySQLConnectionString;
            _logger = null;
            MySqlConnectorLogManager.Provider = new MicrosoftExtensionsLoggingLoggerProvider(loggerFactory);
            Connection = new MySqlConnection(_connectionString);
        }

        public AppDb(string connectionString)
        {
            _connectionString = connectionString;
            _loggerFactory = null;
            _logger = NLog.LogManager.GetCurrentClassLogger();
            Connection = new MySqlConnection(_connectionString);
        }

        public AppDb(ILoggerFactory loggerFactory, string connectionString)
        {
            _connectionString = connectionString;
            _logger = null;
            MySqlConnectorLogManager.Provider = new MicrosoftExtensionsLoggingLoggerProvider(loggerFactory);
            Connection = new MySqlConnection(_connectionString);
        }

        public void Dispose()
        {
            Connection.Close();
            GC.SuppressFinalize(this);
        }

        #endregion Constructors, Dispose and Destructors

        #region Public methods

        public async Task<int> ExecuteNonQueryAsync(string sql, MySqlTransaction mySqlTransaction = null)
        {
            if (_loggerFactory == null)
            {
                _logger.Debug($"ExecuteNonQueryAsync: {sql}");
            }

            using (var cmd = Connection.CreateCommand())
            {
                if (mySqlTransaction != null)
                    cmd.Transaction = mySqlTransaction;
                cmd.CommandText = sql;
                return await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<int> ExecuteNonQueryAsync(string[] sql, MySqlTransaction mySqlTransaction = null)
        {
            var combinedCount = 0;
            if (mySqlTransaction != null)
            {
                foreach (string s in sql)
                {
                    combinedCount = combinedCount + await ExecuteNonQueryAsync(s, mySqlTransaction);
                }
            }
            else
            {
                using (var trans = await Connection.BeginTransactionAsync().ConfigureAwait(false))
                {
                    try
                    {
                        foreach (string s in sql)
                        {
                            using (var cmd = Connection.CreateCommand())
                            {
                                cmd.Transaction = trans;
                                cmd.CommandText = s;
                                combinedCount = combinedCount + await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                            }
                        }
                        await trans.CommitAsync().ConfigureAwait(false);
                    }
                    catch
                    {
                        await trans.RollbackAsync().ConfigureAwait(false);
                        throw;
                    }
                }
            }
            return combinedCount;
        }

        #endregion Public methods
    }
}